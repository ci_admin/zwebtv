<?php
/**
 * Video Category partial.
 *
 * @package ZwebTheme
 */

/* @var \WP_Term $video_category_term */

?>
<div class="video-category">
	<a
			class="video-category__link"
			href="<?php echo esc_url( get_term_link( $video_category_term ) ); ?>"
			title="<?php echo esc_attr( $video_category_term->name ); ?>">
		<picture>
			<source media="(min-width: 768px)" data-srcset="<?php the_video_category_image_src( $video_category_term->term_id ); ?>" />
			<source data-srcset="<?php the_video_category_image_src( $video_category_term->term_id, 'video_category_mobile' ); ?>" />
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif" data-src="<?php the_video_category_image_src( $video_category_term->term_id, 'video_category_mobile' ); ?>" class="video-category__image lazy" alt="<?php echo esc_attr( $video_category_term->name ); ?>" />
		</picture>

		<h2 class="video-category__title">
			<?php echo esc_html( $video_category_term->name ); ?>
		</h2>
	</a>
</div>
