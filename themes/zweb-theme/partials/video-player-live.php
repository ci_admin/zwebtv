<?php
/**
 * this is the alternative live video player design which is currently not used.
 *
 * @package ZwebTheme
 */

?>

<h1><?php esc_html_e( 'Live', 'zweb-theme' ); ?></h1>

<?php

/**
 * TODO: convert to single post template
 */
$live_video_id = 372;

try {
	$influencer      = \Zweb\Taxonomy\Influencer::get_influencer_for_video( $live_video_id );
	$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
	$video           = get_post( $live_video_id );
	$images          = get_post_meta( $live_video_id, 'bc_images', true );
} catch ( \Exception $exception ) {
	return;
}

?>

<div class="video-featured video-featured--live">
	<div class="video-featured__info">

		<div class="video-featured__influencer">
			<a href="<?php echo esc_url( get_term_link( $influencer ) ); ?>">
				<img
						class="video-featured__influencer-image lazy"
						src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
						data-src="<?php the_influencer_square_image_src( $influencer->term_id ); ?>"
						alt="<?php echo esc_attr( $influencer->name ); ?>">
				<h3 class="video-featured__influencer-name"><?php echo esc_html( $influencer->name ); ?></h3>
			</a>
		</div>
		<h2 class="video-featured__title">
			<?php echo esc_html( get_the_title( $video ) ); ?>
		</h2>

		<p>
			<?php
			$userdata = get_user_meta( $influencer_user->ID );
			echo esc_html( $userdata['description'][0] );
			?>
		</p>

	</div>

	<div class="video-featured__cover">

		<?php
		set_query_var( 'video_id', $live_video_id );
		set_query_var( 'featured_video', true );
		?>

		<?php get_template_part( 'partials/video-card', 'page' ); ?>

		<div class="video-featured__tag">
			<span class="video-tag">Live</span>
		</div>

	</div>
</div>
