<?php
/**
 * Video Card partial.
 *
 * @package ZwebTheme
 */

/* @var string $video_id The id of the video */

/* @var boolean $featured_video is featured section */

namespace ZwebTheme\Utility;

$images              = get_post_meta( $video_id, 'bc_images', true );
$player_id           = get_post_meta( $video_id, 'bc_player_id', true );
$bc_video_id         = get_post_meta( $video_id, 'bc_video_id', true );
$account_id          = get_post_meta( $video_id, 'bc_account_id', true );
$views               = get_post_meta( $video_id, 'views', true );
$influencer_username = '';
$featured_video      = $featured_video ?? false;

$video_category = \Zweb\Taxonomy\VideoCategory::get_video_category_term_for_video( $video_id );

$influencer = get_the_terms( $video_id, \Zweb\Taxonomy\Influencer::TAXONOMY_NAME );

// Do not display video with no influencer associated.
if ( ! $influencer ) {
	return;
}

try {
	$influencer_user     = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer[0]->term_id );
	$influencer_username = $influencer_user->user_login;
} catch ( \Exception $exception ) {
	// do nothing
}

$is_live       = $is_live ?? false;
$settings      = \Zweb\Admin\Settings::get_site_settings();
$is_live_state = 'live' === $settings['live']['state'];
$is_pre_live   = 'pre-live' === $settings['live']['state'];

$article_class = '';
if ( $is_live ) {
	if ( ! $is_live_state ) {
		$article_class = 'hidden';
	}
}
?>


<article
		class="video-card
		<?php echo $featured_video ? 'video-card--featured' : ''; ?>
		<?php echo $is_live ? 'live' : ''; ?>
		<?php echo esc_attr( $article_class ); ?>"

>
	<div class="video-card__wrap">
		<a href="#video"
		   data-id="<?php echo esc_attr( 'vid' . $bc_video_id . guidv4() ); ?>"
		   data-account-id="<?php echo esc_attr( $account_id ); ?>"
		   data-player-id="<?php echo esc_attr( $player_id ); ?>"
		   data-video-id="<?php echo esc_attr( $bc_video_id ); ?>"
		   data-influencer="<?php echo esc_attr( $influencer_username ); ?>"
		   data-wp-video-id="<?php echo esc_attr( $video_id ); ?>"
		   data-video-category="<?php echo isset( $video_category ) ? esc_attr( $video_category->name ) : ''; ?>"
		   class="video-card__link"
		   title="<?php esc_html_e( 'Guarda il Video', 'zweb-theme' ); ?>"
		>

			<div class="video-card__cover">
				<div class="video-card__cover-contents">
					<img class="video-card__image lazy"
						 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
						 data-src="<?php echo esc_url( $images['poster']['src'] ); ?>"
						 alt="<?php esc_attr_e( 'Watch Video', 'zweb-theme' ); ?>"
						 width="<?php echo esc_attr( $images['poster']['sources'][0]['width'] ); ?>"
						 height="<?php echo esc_attr( $images['poster']['sources'][0]['height'] ); ?>">

					<?php if ( ! $is_live ) : ?>
						<div class="video-card__duration">
							<?php the_video_duration( $video_id ); ?>
						</div>
					<?php endif; ?>


					<?php if ( has_term( 'live', \Zweb\Taxonomy\StudioCategory::TAXONOMY_NAME ) ) : ?>
						<div class="video-card__tag">
							<span class="video-tag video-tag--alt"><?php esc_html_e( 'Live Recording', 'zweb-theme' ); ?></span>
						</div>
					<?php endif ?>

					<?php if ( $is_live ) : ?>
						<div class="video-card__play">
							<svg version="1.1" id="Layer_1"
								 xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
								 y="0px"
								 viewBox="0 0 70 70"
								 style="enable-background:new 0 0 70 70;"
								 xml:space="preserve">
							<circle style="opacity:0.45;" cx="35" cy="35" r="34"/>
								<path id="Path_5" style="fill:#FFFFFF;"
									  d="M44,35l-13-8v16L44,35z"/>
						</svg>

						</div>
					<?php endif; ?>
				</div>
			</div>
		</a>
		<div class="video-card__loading"></div>
		<div class="video-card__close-full-screen">
			<svg class=""
				 xmlns="http://www.w3.org/2000/svg" width="18"
				 height="18.144">
				<use xlink:href="#search-close"></use>
			</svg>
		</div>
	</div>
	<?php if ( ! $featured_video ) : ?>
		<div class="video-card__details">
			<a class="video-card__details-link"

			   title="<?php echo esc_attr( $influencer[0]->name ); ?> Profile">
				<img alt="<?php echo esc_attr( $influencer[0]->name ); ?>"
					 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
					 data-src="<?php the_influencer_square_image_src( $influencer[0]->term_id ); ?>"
					 class="video-card__author-image lazy"/>
			</a>
			<h2 class="video-card__title">
				<a class="video-card__title-link"
				   href="<?php the_permalink(); ?>"
				   title="<?php esc_attr_e( 'Guarda il video', 'zweb-theme' ); ?>">
					<?php the_title(); ?>
				</a>
			</h2>
		</div>
		<div class="video-card__meta">
			<p class="video-card__author-name">
				<a class="video-card__author-link"
				   href="<?php echo esc_url( get_term_link( $influencer[0] ) ); ?>"
				   title="<?php sprintf( __( 'Profilo di %s', 'zweb-theme' ), $influencer[0]->name ); ?>">
					<?php echo esc_html( $influencer[0]->name ); ?>
				</a>
			</p>
			<div class="video-card__added">
				<!-- TODO replace with diff time -->
			</div>
		</div>
	<?php endif; ?>
</article>
