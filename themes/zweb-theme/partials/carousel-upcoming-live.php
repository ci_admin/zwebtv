<?php
/**
 * Carousel upcoming live video partial.
 *
 * @package ZwebTheme
 */

use Zweb\Builder\Query;

$query = Query::get_upcoming_video_query( 4, get_search_query() );

if ( ! $query->have_posts() ) {
	return;
}

?>
<section class="influencer-next-carousel container">
	<h2>
		<a href="/prossime-uscite">
			<?php esc_html_e( 'Next Live', 'zweb-theme' ); ?>
			<svg class="title-arrow" xmlns="http://www.w3.org/2000/svg" width="21.27" height="13.63">
				<use xlink:href="#arrow-right"></use>
			</svg>
		</a>
	</h2>
	<div class="influencer-next-carousel__wrap">
		<ul class="influencer-next-carousel__items">
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php
				try {
					set_query_var( 'influencer', \Zweb\Taxonomy\Influencer::get_influencer_for_video( get_the_ID() ) );
					set_query_var( 'video', get_post() );
				} catch ( \Exception $exception ) {
					continue;
				}
				?>
				<li class="influencer-next-carousel__item">
					<?php get_template_part( 'partials/influencer', 'next' ); ?>
				</li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
	</div>
</section>
