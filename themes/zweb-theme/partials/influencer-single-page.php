<?php
/**
 * Influncer single page.
 *
 * @package ZwebTheme
 */

use Zweb\Taxonomy\Influencer;

/* @var \WP_Term $influencer */

/* @var \WP_Term $influencer_video_category */

/* @var \WP_User $influencer_user */

/* @var \WP_Post $next_video */

namespace ZwebTheme\Utility;

use Zweb\Taxonomy\Influencer;

$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );

?>

<section class="influencer-page-header">
	<div class="influencer-page-header__bg">
		<picture>
			<source media="(min-width: 768px)"
					data-srcset="<?php the_influencer_landing_page_image_src( $influencer->term_id ); ?>"/>
			<source data-srcset="<?php the_influencer_landing_page_image_src( $influencer->term_id ); ?>"/>
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
				 data-src="<?php the_influencer_landing_page_image_src( $influencer->term_id ); ?>"
				 class="influencer-page-header__bg-img lazy"
				 alt="<?php echo esc_attr( $influencer->name ); ?>"/>
		</picture>
	</div>
	<div class="influencer-page-header__overlay">
		<div class="container">

			<div class="influencer-page-header__share">
				<a class="share-button" href="#social-sharing"
				   aria-controls="social-sharing">
					<svg class="share-button__icon"
						 xmlns="http://www.w3.org/2000/svg" width="19.337"
						 height="19.26">
						<path d="M15.83 12.246a3.5 3.5 0 00-2.6 1.151l-6.275-3.1a3.384 3.384 0 00.054-.759L13.5 6.131a3.507 3.507 0 10-1.18-2.623 3.553 3.553 0 00.027.427L6.042 7.247a3.507 3.507 0 10-.372 5.182l6.655 3.286v.037a3.507 3.507 0 103.506-3.507z"/>
					</svg>
				</a>
			</div>

			<div class="influencer-page-header__content">
				<?php if ( $influencer_video_category ) : ?>
					<a href="<?php echo esc_url( get_term_link( $influencer_video_category ) ); ?>"
					   class="influencer-page-header__category"><?php echo esc_html( $influencer_video_category->name ); ?></a>
				<?php endif ?>
				<h1 class="influencer-page-header__title"><?php echo esc_html( $influencer->name ); ?></h1>

				<div class="influencer-page-header__biog">
					<p>
						<?php
						$userdata = get_user_meta( $influencer_user->ID );
						echo esc_html( $userdata['description'][0] );
						?>
					</p>
				</div>

				<div class="influencer-page-header__details">
					<?php
					$views = get_user_meta( $influencer_user->ID, 'views', true );

					if ( $views ) :
						?>
						<div class="influencer-page-header__detail">
							<?php echo( esc_attr( $views ) ); ?>
							<span class="influencer-page-header__detail-label"><?php esc_html_e( 'Visualizzazioni', 'zweb-theme' ); ?></span>
						</div>
					<?php endif; ?>
					<div class="influencer-page-header__detail">
						<?php echo esc_html( Influencer::get_post_count_by_influencer( $influencer->term_id ) ); ?>
						<span class="influencer-page-header__detail-label"><?php esc_html_e( 'Z Video', 'zweb-theme' ); ?></span>
					</div>
					<?php if ( $next_video ) : ?>
						<div class="influencer-page-header__detail">
							<?php echo esc_html( date_i18n( 'j F', strtotime( $next_video->post_date ) ) ); ?>
							<span class="influencer-page-header__detail-label"><?php esc_html_e( 'Next Video', 'zweb-theme' ); ?></span>
						</div>
					<?php endif ?>
				</div>

			</div>

			<a class="influencer-page-header__down" href="#latest-video">
				<svg class="influencer-page-header__down-icon"
					 data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
					 width="22" height="11">
					<use xlink:href="#angle-down"></use>
				</svg>
			</a>
		</div>
	</div>
</section>
