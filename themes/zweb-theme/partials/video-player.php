<?php
/**
 * Video player partial.
 *
 * @package ZwebTheme
 */

$influencer_username = '';
$video_category      = \Zweb\Taxonomy\VideoCategory::get_video_category_term_for_video( get_the_ID() );
$influencer          = get_the_terms( get_the_ID(), \Zweb\Taxonomy\Influencer::TAXONOMY_NAME );

if ( $influencer ) {
	try {
		$influencer_user     = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer[0]->term_id );
		$influencer_username = $influencer_user->user_login;
	} catch ( \Exception $exception ) {
		// Do nothing for now.
	}
}
$is_live = $is_live ?? false;

$is_studio_roma_live = \Zweb\Brightcove\Live::is_studio_roma_live();

?>
<div class="video-player"
	 data-influencer="<?php echo esc_attr( $influencer_username ); ?>"
	 data-wp-video-id="<?php echo esc_attr( get_the_ID() ); ?>"
	 data-video-category="<?php echo isset( $video_category ) ? esc_attr( $video_category->name ) : ''; ?>"
>
	<?php if ( $is_live && ! $is_studio_roma_live ) : ?>
		<?php the_post_thumbnail( 'video_thumb' ); ?>
	<?php else : ?>
		<?php the_content(); ?>
	<?php endif; ?>
	<div class="video-card__close-full-screen">
		<svg class=""
			 xmlns="http://www.w3.org/2000/svg" width="18"
			 height="18.144">
			<use xlink:href="#search-close"></use>
		</svg>
	</div>

	<?php if ( $is_live ) : ?>
		<div class="video-featured__tag">
			<span class="video-tag"><?php esc_html_e( 'Live', 'zweb-theme' ); ?></span>
		</div>
	<?php endif; ?>

</div>
