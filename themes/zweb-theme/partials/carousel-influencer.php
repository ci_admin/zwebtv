<?php
/**
 * Influencer Carousel partial.
 *
 * @package ZwebTheme
 */

/* @var array $influencer_carousel_config */

if ( ! isset( $influencer_carousel_config['query'] ) || ! $influencer_carousel_config['query']->get_terms() ) {
	return;
}

$label = $influencer_carousel_config['label'] ?? __( 'Influencer', 'zweb-theme' );

?>
<section class="influencer-carousel container">
	<h2>
		<?php // only use link for the standard influencer carousel ?>
		<?php if ( isset( $influencer_carousel_config['label'] ) ) : ?>
			<?php echo esc_html( $label ); ?>
		<?php else : ?>
			<a href="/autori">
				<?php echo esc_html( $label ); ?>
				<svg class="title-arrow" data-name="Layer 1"
					 xmlns="http://www.w3.org/2000/svg" width="21.27"
					 height="13.63">
					<use xlink:href="#arrow-right"></use>
				</svg>
			</a>
		<?php endif ?>
	</h2>

	<div class="influencer-carousel__wrap swiper-container">
		<ul class="influencer-carousel__items swiper-wrapper">
			<?php foreach ( $influencer_carousel_config['query']->get_terms() as $term ) : ?>
				<li class="influencer-carousel__item swiper-slide">
					<?php set_query_var( 'influencer_term', $term ); ?>
					<?php get_template_part( 'partials/influencer', 'page' ); ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<div class="swiper-controls-prev"></div>
		<div class="swiper-controls-next"></div>
	</div>

</section>
