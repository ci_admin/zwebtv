<?php
/**
 * Video Featured partial.
 *
 * @package ZwebTheme
 */

$site_settings     = \Zweb\Admin\Settings::get_site_settings();
$featured_video_id = $site_settings['homepage']['featured_video'][0] ?? 0;

try {
	$influencer = \Zweb\Taxonomy\Influencer::get_influencer_for_video( $featured_video_id );
	$video      = get_post( $featured_video_id );
	$images     = get_post_meta( $featured_video_id, 'bc_images', true );
} catch ( \Exception $exception ) {
	return;
}

?>

<section class="container">
	<h2><?php esc_html_e( 'In evidenza', 'zweb-theme' ); ?></h2>

	<div class="video-featured">
		<div class="video-featured__info">

			<div class="video-featured__influencer">
				<a href="<?php echo esc_url( get_term_link( $influencer ) ); ?>">
					<img
					class="video-featured__influencer-image lazy"
					src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
					data-src="<?php the_influencer_square_image_src( $influencer->term_id ); ?>"
					alt="<?php echo esc_attr( $influencer->name ); ?>">
				<h3 class="video-featured__influencer-name"><?php echo esc_html( $influencer->name ); ?></h3>
				</a>
			</div>
			<h2 class="video-featured__title">
				<?php echo esc_html( get_the_title( $video ) ); ?>
			</h2>
			<!-- Linking to the influencer page, because we don't have the category page filtered by influencer -->

			<a href="<?php echo esc_url( get_the_permalink( $video ) ); ?>">
				<svg class="video-featured__info-icon"
					 xmlns="http://www.w3.org/2000/svg" width="70" height="70">
					<path d="M44 35l-13-8v16z" fill="#fff"/>
					<g fill="none" stroke="#fff" stroke-width="2">
						<circle cx="35" cy="35" r="35" stroke="none"/>
						<circle cx="35" cy="35" r="34"/>
					</g>
				</svg>
			</a>
		</div>

		<div class="video-featured__cover">

			<?php
			set_query_var( 'video_id', $featured_video_id );
			set_query_var( 'featured_video', true );
			?>

			<?php get_template_part( 'partials/video-card', 'page' ); ?>


		</div>
	</div>

</section>
