<?php
/**
 * Video page influencer partial.
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

try {
	$influencer                = \Zweb\Taxonomy\Influencer::get_influencer_for_video( get_the_ID() );
	$influencer_user           = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
	$influencer_video_category = \Zweb\Taxonomy\Influencer::get_influencer_video_category( $influencer_user->ID );
} catch ( \Exception $exception ) {
	return; // do not render anything.
}
?>
<div class="video-page__details">
	<div class="video-page__influencer">
		<section itemscope itemtype="https://schema.org/CreativeWork">
			<div class="influencer influencer--layout" itemprop="author"
				 itemscope itemtype="https://schema.org/Person">
				<div class="influencer__image-wrap">
					<div class="influencer__image">
						<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
							 data-src="<?php the_influencer_square_image_src( $influencer->term_id ); ?>"
							 class="lazy"
							 itemprop="image"
							 alt="<?php echo esc_attr( $influencer->name ); ?>">
					</div>
				</div>
				<div class="influencer__info">
					<h3 class="influencer__title">Autore</h3>
					<h2 class="influencer__name" itemprop="name">
						<?php echo esc_html( $influencer->name ); ?>
					</h2>
				</div>
				<a href="<?php echo esc_url( get_term_link( $influencer ) ); ?>"
				   class="influencer__profile-link">Vai al profilo ></a>

			</div>
		</section>
	</div>
	<div class="video-page__about">
		<ul class="video-tags">
			<?php if ( $influencer_video_category ) : ?>
				<li class="video-tags__item">
					<a href="<?php echo esc_url( get_term_link( $influencer_video_category ) ); ?>"
					   class="video-tags__link">
						<?php echo esc_html( $influencer_video_category->name ); ?>
					</a>
				</li>
			<?php endif; ?>
		</ul>
		<h2 class="video-page__title">
			<?php the_title(); ?>
		</h2>
		<p>
			<?php the_excerpt(); ?>
		</p>
		<?php $views = get_post_meta( get_the_ID(), 'views', true ); ?>
		<?php if ( $views ) : ?>
			<div class="video-page__views">
				<svg class="video-page__views-icon"
					 data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
					 width="18" height="10">
					<use xlink:href="#icon-eye"></use>
				</svg>
				<?php
				if ( $views < 100 ) {
					$views = 'Meno di 100';
				}
				echo( esc_html( $views ) );
				?>
			</div>
		<?php endif ?>
		<div class="video-page__share">
			<a class="video-page__share-link" href="#social-sharing"
			   aria-controls="social-sharing">
				<svg class="video-page__share-icon"
					 xmlns="http://www.w3.org/2000/svg" width="19.337"
					 height="19.26">
					<path d="M15.83 12.246a3.5 3.5 0 00-2.6 1.151l-6.275-3.1a3.384 3.384 0 00.054-.759L13.5 6.131a3.507 3.507 0 10-1.18-2.623 3.553 3.553 0 00.027.427L6.042 7.247a3.507 3.507 0 10-.372 5.182l6.655 3.286v.037a3.507 3.507 0 103.506-3.507z"/>
				</svg>
				<?php esc_html_e( 'Share', 'zweb-theme' ); ?>
			</a>
		</div>
	</div>

</div>
