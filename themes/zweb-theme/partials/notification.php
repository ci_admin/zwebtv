<?php
/**
 * Notification partial.
 *
 * @package ZwebTheme
 */

use Zweb\Admin\Settings;

$settings        = Settings::get_site_settings();
$state = \Zweb\Admin\Settings::get_current_application_state();
?>
<?php if ( 'announcement' === $state || 'pre-live' === $state ) : ?>
	<div class="notification container">
		<div class="notification__outer pre-live">
			<span class="notification__tag">Upcoming Live</span>
			<?php echo esc_html( \Zweb\Admin\Settings::get_annnouncement_text() ); ?>
		</div>
	</div>
<?php endif ?>
