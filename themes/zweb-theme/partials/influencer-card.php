<?php
/**
 * Influncer card partial.
 *
 * @package ZwebTheme
 */

/* @var \WP_Term $influencer */

?>
<div itemscope itemtype="https://schema.org/CreativeWork">
	<div class="influencer-card" itemprop="author" itemscope
		 itemtype="https://schema.org/Person">
		<a class="influencer-card__link"
		   rel="author"
		   href="<?php echo esc_url( get_term_link( $influencer ) ); ?>"
		   itemprop="url"
		   title="<?php esc_attr_e( 'View Profile', 'zweb-theme' ); ?>">
			<img class="influencer-card__image lazy"
				 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
				 data-src="<?php the_influencer_card_image_src( $influencer->term_id ); ?>"
				 itemprop="image"
				 alt="<?php echo esc_attr( $influencer->name ); ?>">

			<div class="influencer-card__info">
				<h2 class="influencer-card__name" itemprop="name">
					<?php echo esc_html( $influencer->name ); ?>
				</h2>
			</div>
		</a>
	</div>
</div>
