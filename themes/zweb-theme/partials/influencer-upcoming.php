<?php
/**
 * Upcoming Influncer card partial.
 *
 * @package ZwebTheme
 */

use Zweb\Taxonomy\Influencer;

/* @var \WP_Term $influencer */
try {

	$influencer_user = Influencer::get_user_from_term( $influencer->term_id );
	$upcoming_date = get_user_meta( $influencer_user->ID, 'zweb-influencer_upcoming_date', true );
} catch ( \Exception $exception ) {
	$upcoming_date = false;
}

?>
<div class="upcoming-influencer">
	<div class="upcoming-influencer__link" >
		<img class="upcoming-influencer__image lazy"
			 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
			 data-src="<?php the_influencer_upcoming_image_src( $influencer->term_id ); ?>"
			 alt="<?php esc_attr_e( 'Upcoming influencer', 'zweb-theme' ); ?>">

		<div class="upcoming-influencer__info">
			<h2 class="upcoming-influencer__title">
				<?php esc_html_e( "Who's Next", 'zweb-theme' ); ?>
			</h2>
			<?php if ( $upcoming_date ) : ?>
				<time class="upcoming-influencer__time countdown-timer"
					  date="<?php echo esc_attr( $upcoming_date ); ?>">
				<span class="upcoming-influencer__days">

				</span>
					:
					<span class="upcoming-influencer__hours">

				</span>
					:
					<span class="upcoming-influencer__mins">

				</span>
				</time>
			<?php endif ?>
			<div class="upcoming-influencer__time-label">
				<span class="upcoming-influencer__time-label-days">
					G
				</span>
				<span class="upcoming-influencer__time-label-hours">
					H
				</span>
				<span class="upcoming-influencer__time-label-mins">
					M
				</span>
			</div>
		</div>
	</div>
</div>
