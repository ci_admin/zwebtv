<?php
/**
 * Video Category Carousel partial.
 *
 * @package ZwebTheme
 */

$video_categories_query = \Zweb\Taxonomy\VideoCategory::get_video_category_carousel_query( get_search_query() );

if ( ! $video_categories_query->get_terms() ) {
	return;
}

?>

<section class="video-categories container">

	<h2>
		<a href="/generi">
			<?php esc_html_e( 'Generi', 'zweb-theme' ); ?>
			<svg class="title-arrow" data-name="Layer 1"
				 xmlns="http://www.w3.org/2000/svg" width="21.27" height="13.63">
				<use xlink:href="#arrow-right"></use>
			</svg>
		</a>
	</h2>

	<div class="video-categories__wrap">
		<ul class="video-categories__items">

			<?php foreach ( $video_categories_query->get_terms() as $term ) : ?>
				<li class="video-categories__item">
					<?php set_query_var( 'video_category_term', $term ); ?>
					<?php get_template_part( 'partials/video-category' ); ?>
				</li>
			<?php endforeach; ?>

		</ul>
	</div>
</section>
