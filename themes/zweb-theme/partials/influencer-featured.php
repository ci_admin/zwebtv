<?php
/**
 * Featured influencer partial.
 *
 * @package ZwebTheme
 */

use Zweb\Admin\Settings;
use Zweb\Builder\Query;
use Zweb\Taxonomy\Influencer;

$settings        = Settings::get_site_settings();
$is_live         = 'live' === $settings['live']['state'];
$is_announcement = 'announcement' === $settings['live']['state'];
$is_pre_live     = 'pre-live' === $settings['live']['state'];
$is_post_live    = 'post-live' === $settings['live']['state'];

try {
	if ( $is_live || $is_pre_live || $is_post_live ) {
		$featured_influencer       = get_term_by( 'slug', 'studio-roma', Influencer::TAXONOMY_NAME );
		$last_post_from_influencer = Influencer::get_latest_video_by_influencer( $featured_influencer->term_id, \Zweb\PostType\LiveVideo::POST_TYPE_NAME );
		$heading                   = $settings['live'][ $settings['live']['state'] . '-text' ];
	} else {
		$featured_influencer       = \Zweb\Taxonomy\Influencer::get_featured_influencer();
		$last_post_from_influencer = Influencer::get_latest_video_by_influencer( $featured_influencer->term_id );
		$heading                   = get_the_title( $last_post_from_influencer );
	}
} catch ( \Exception $exception ) {
	return;
}

$show_live_image = $is_live || $is_pre_live || $is_post_live ? true : false;

?>

<section
		class="featured-influencer <?php echo $is_live || $is_pre_live || $is_post_live ? 'featured-influencer--live' : ''; ?>">
	<div class="featured-influencer__bg">
		<picture>
			<source media="(min-width: 768px)"
					data-srcset="<?php the_influencer_landing_page_image_src( $featured_influencer->term_id, 'landing', $show_live_image ); ?>"/>
			<source data-srcset="<?php the_influencer_landing_page_image_src( $featured_influencer->term_id, 'featured-mobile', $show_live_image ); ?>"/>
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
				 data-src="<?php the_influencer_landing_page_image_src( $featured_influencer->term_id, 'featured-mobile', $show_live_image ); ?>"
				 class="featured-influencer__bg-img lazy"
				 alt="<?php echo esc_attr( $featured_influencer->name ); ?>"/>
		</picture>
	</div>

	<div class="featured-influencer__overlay">
		<div class="container">
			<div class="featured-influencer__content">
				<section itemscope itemtype="https://schema.org/CreativeWork">
					<div class="influencer influencer--featured <?php echo $is_live ? 'influencer--live' : ''; ?>"
						 itemprop="author" itemscope
						 itemtype="https://schema.org/Person">

						<?php if ( $is_live || $is_pre_live || $is_post_live ) : ?>
							<span class="influencer__link live <?php echo $is_live ? '' : 'hidden'; ?>">
									<div class="featured-influencer__tag">
										<span class="video-tag video-tag--large video-tag--flash"><?php esc_html_e( 'Live', 'zweb-theme' ); ?></span>
									</div>
								<div class="influencer__info influencer__info--name-only">
									<h2 class="influencer__name"
										itemprop="name">
										<?php echo esc_html( $featured_influencer->name ); ?>
									</h2>
								</div>
							</span>
						<?php else : ?>
							<a class="influencer__link" rel="author"
							   href="<?php echo esc_url( get_term_link( $featured_influencer ) ); ?>"
							   itemprop="url"
							   title="<?php esc_attr_e( 'Vai al profilo', 'zweb-theme' ); ?>">

								<div class="influencer__image">
									<img src="<?php the_influencer_square_image_src( $featured_influencer->term_id ); ?>"
										 itemprop="image"
										 alt="<?php echo esc_attr( $featured_influencer->name ); ?>">
								</div>
								<div class="influencer__info">
									<h3 class="influencer__title">Autore</h3>
									<h2 class="influencer__name"
										itemprop="name">
										<?php echo esc_html( $featured_influencer->name ); ?>
									</h2>
								</div>
							</a>
						<?php endif; ?>
					</div>
				</section>
				<h1 class="featured-influencer__heading">
					<?php echo esc_html( $heading ); ?>
				</h1>
				<?php if ( $is_live || $is_pre_live || $is_post_live ) : ?>
					<?php
					set_query_var( 'is_live', true );
					set_query_var( 'video_id', $last_post_from_influencer->ID );
					set_query_var( 'featured_video', true );
					?>

					<?php
					get_template_part( 'partials/video-card', 'page' );
					set_query_var( 'is_live', false );
					?>
					<div class="video-live-state pre-live <?php echo $is_pre_live ? '' : 'hidden'; ?>">

						<?php
						echo wp_get_attachment_image(
							$settings['live']['pre-live-image'],
							'video-thumb',
							false,
							[ 'class' => 'video-live-state__image pre-live attachment-video-thumb size-video-thumb' ]
						);
						?>

						<div class="video-live-state__overlay">
							<h2 class="video-live-state__title">
								<?php echo wp_kses_post( nl2br( $settings['live']['pre-live-image-title'] ) ); ?>
							</h2>
							<p class="video-live-state__text">
								<?php echo wp_kses_post( nl2br( $settings['live']['pre-live-image-text'] ) ); ?>
							</p>
						</div>
					</div>
					<div class="video-live-state post-live <?php echo $is_post_live ? '' : 'hidden'; ?>">

						<?php
						echo wp_get_attachment_image(
							$settings['live']['post-live-image'],
							'video-thumb',
							false,
							[ 'class' => 'video-live-state__image post-live attachment-video-thumb size-video-thumb' ]
						);
						?>

						<div class="video-live-state__overlay">
							<h2 class="video-live-state__title">
								<?php echo wp_kses_post( nl2br( $settings['live']['post-live-image-title'] ) ); ?>
							</h2>
							<p class="video-live-state__text">
								<?php echo wp_kses_post( nl2br( $settings['live']['post-live-image-text'] ) ); ?>
							</p>
						</div>

					</div>

				<?php else : ?>

					<a href="<?php echo esc_url( get_the_permalink( $last_post_from_influencer ) ); ?>"
					   class="cta-button"
					   title="<?php esc_attr_e( 'Guarda il Video', 'zweb-theme' ); ?>">
						<?php esc_attr_e( 'Guarda il Video', 'zweb-theme' ); ?>
					</a>
				<?php endif ?>
			</div>
		</div>
	</div>
</section>
