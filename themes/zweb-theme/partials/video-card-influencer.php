<?php
/**
 * Influncer video card single page.
 *
 * @package ZwebTheme
 */

use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Influencer;

/* @var \WP_Term $influencer */

/* @var \WP_Term $influencer_video_category */

/* @var \WP_User $influencer_user */

/* @var \WP_Post $next_video */

try {
	$last_video = Influencer::get_latest_video_by_influencer( $influencer->term_id );
} catch ( \Exception $exception ) {
	$last_video = false;
}

?>
<section id="latest-video">
	<div class="container">
		<div class="influencer-updates">
			<?php if ( $last_video ) : ?>
				<div class="influencer-updates__latest">
					<h2><?php echo esc_html( sprintf( __( 'Novità di %s', 'zweb-theme' ), $influencer->name ) ); ?></h2>
					<?php set_query_var( 'video_id', $last_video->ID ); ?>
					<?php get_template_part( '/partials/video-card' ); ?>
				</div>
			<?php endif ?>
			<?php if ( $next_video ) : ?>
				<div class="influencer-updates__next">
					<h2><?php esc_html_e( 'Next Video', 'zweb-theme' ); ?></h2>
					<div class="influencer-next">
						<svg class="influencer-next__icon"
							 data-name="Layer 1"
							 xmlns="http://www.w3.org/2000/svg" width="20"
							 height="20">
							<use xlink:href="#calendar"></use>
						</svg>
						<picture>
							<source media="(min-width: 768px)"
									data-srcset="<?php the_influencer_square_image_src( $influencer->term_id, 'square' ); ?>"/>
							<source data-srcset="<?php the_influencer_landing_page_image_src( $influencer->term_id, 'next-video' ); ?>"/>
							<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
								 data-src="<?php the_influencer_landing_page_image_src( $influencer->term_id, 'next-video' ); ?>"
								 class="influencer-next__img lazy"
								 alt="<?php echo esc_attr( $influencer->name ); ?>"/>
						</picture>

						<div class="influencer-next__info">
							<h3 class="influencer-next__title">
								<?php echo esc_html( $influencer->name ); ?>
							</h3>
							<p><?php echo esc_html( ucfirst( date_i18n( 'l d F - \h.H:i', strtotime( $next_video->post_date ) ) ) ); ?></p>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</section>
