<?php
/**
 * Video Carousel partial.
 *
 * @package ZwebTheme
 */

use Zweb\PostType\Video;

/* @var array $video_carousel_config */

if ( ! isset( $video_carousel_config['query'] ) || ! $video_carousel_config['query']->get_posts() ) {
	return;
}
$data_gallery_term        = '';
$data_influencer_term     = '';
$data_video_category_term = '';
$data_studio_id = '';
if ( isset( $video_carousel_config['studio_id'] ) ) {
	$data_studio_id = "data-studio-id=${video_carousel_config['studio_id']}";
}
if ( isset( $video_carousel_config['gallery_term_id'] ) ) {
	$data_gallery_term = "data-gallery-term-id=${video_carousel_config['gallery_term_id']}";
}
if ( isset( $video_carousel_config['influencer_term_id'] ) ) {
	$data_influencer_term = "data-influencer-term-id=${video_carousel_config['influencer_term_id']}";
}
if ( isset( $video_carousel_config['video_category_term_id'] ) ) {
	$data_video_category_term = "data-video-category-term-id=${video_carousel_config['video_category_term_id']}";
}
$data_relationship = '';
if ( isset( $video_carousel_config['relationship'] ) ) {
	$data_relationship = "data-relationship=${video_carousel_config['relationship']}";
}
$data_video_id = '';
if ( isset( $video_carousel_config['video_id'] ) ) {
	$data_video_id = "data-video-id=${video_carousel_config['video_id']}";
}
$data_search = '';
if ( isset( $video_carousel_config['search'] ) ) {
	$data_search = "data-search=${video_carousel_config['search']}";
}
$link = $video_carousel_config['link'] ?? false;

?>
<section
		class="video-carousel <?php echo esc_attr( isset( $video_carousel_config['pull_up'] ) ? 'video-carousel--pull-up' : '' ); ?> container"
	<?php echo esc_attr( $data_gallery_term ); ?>
	<?php echo esc_attr( $data_influencer_term ); ?>
	<?php echo esc_attr( $data_video_category_term ); ?>
	<?php echo esc_attr( $data_relationship ); ?>
	<?php echo esc_attr( $data_studio_id ); ?>
	<?php echo esc_attr( $data_video_id ); ?>
	<?php echo esc_attr( $data_search ); ?>
		data-posts="<?php echo esc_attr( $video_carousel_config['query']->found_posts ); ?>"
>

	<h2>
		<?php if ( $link ) : ?>
			<a href="<?php echo esc_attr( $link ); ?>">
		<?php endif ?>
			<?php echo esc_html( $video_carousel_config['label'] ); ?>
				<?php if ( $link ) : ?>
					<svg class="title-arrow" data-name="Layer 1"
						 xmlns="http://www.w3.org/2000/svg" width="21.27"
						 height="13.63">
						<use xlink:href="#arrow-right"></use>
					</svg>
				<?php endif ?>
		<?php if ( $link ) : ?>
			</a>
		<?php endif ?>
	</h2>
	<div class="video-carousel__wrap swiper-container">
		<?php if ( $video_carousel_config['query']->have_posts() ) : ?>
			<div class="video-carousel__items swiper-wrapper">
				<?php while ( $video_carousel_config['query']->have_posts() ) : $video_carousel_config['query']->the_post(); ?>
					<?php
					set_query_var( 'featured_video', false );
					set_query_var( 'video_id', get_the_id() );
					?>
					<div class="video-carousel__item swiper-slide">
						<?php get_template_part( 'partials/video-card', 'page' ); ?>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
			<div class="swiper-controls-prev"></div>
			<div class="swiper-controls-next"></div>
		<?php endif ?>
	</div>
</section>
