<?php
/**
 * Influencer partial.
 *
 * @package ZwebTheme
 */

/* @var \WP_Term $influencer_term */

?>
<div itemscope itemtype="https://schema.org/CreativeWork">
	<div class="influencer" itemprop="author" itemscope
		 itemtype="https://schema.org/Person">
		<a class="influencer__link" rel="author" href="<?php echo esc_url( get_term_link( $influencer_term ) ); ?>" itemprop="url"
		   title="<?php esc_attr_e( 'View Profile', 'zweb-theme' ); ?>">
			<div class="influencer__image">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
					 data-src="<?php the_influencer_square_image_src( $influencer_term->term_id ); ?>"
					 itemprop="image"
					 class="lazy"
					 alt="<?php echo esc_attr( $influencer_term->name ); ?>">
			</div>
			<div class="influencer__info">
				<h2 class="influencer__name" itemprop="name">
					<?php echo esc_html( $influencer_term->name ); ?>
				</h2>
			</div>
		</a>
	</div>
</div>
