<?php
/**
 * Social sharing partial.
 *
 * @package ZwebTheme
 */

?>

<div id="social-sharing" class="social-sharing">

	<div class="social-sharing__content">

		<a class="social-sharing__close" href="#social-sharing"
		   aria-controls="social-sharing">
			<svg class="social-sharing__close-icon"
				 xmlns="http://www.w3.org/2000/svg" width="18"
				 height="18.144">
				<use xlink:href="#search-close"></use>
			</svg>
		</a>

		<h2 class="social-sharing__title"><?php echo esc_html( 'CONDIVIDI' ); ?></h2>

		<div class="addthis_inline_share_toolbox"></div>

	</div>

</div>
