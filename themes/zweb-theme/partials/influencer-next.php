<?php
/**
 * Influncer next partial.
 *
 * @package ZwebTheme
 */

/* @var \WP_Term $influencer */

/* @var \WP_Post $video */
?>
<div class="influencer-next">
	<svg class="influencer-next__icon" data-name="Layer 1"
		 xmlns="http://www.w3.org/2000/svg" width="20" height="20">
		<use xlink:href="#calendar"></use>
	</svg>
	<picture>
		<source media="(min-width: 768px)"
				data-srcset="<?php the_influencer_square_image_src( $influencer->term_id, 'square' ); ?>"/>
		<source data-srcset="<?php the_influencer_card_image_src( $influencer->term_id ); ?>"/>
		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
			 data-src="<?php the_influencer_card_image_src( $influencer->term_id ); ?>"
			 class="influencer-next__img lazy" alt="<?php echo esc_attr( $influencer->name ); ?>"/>
	</picture>

	<div class="influencer-next__info">
		<h3 class="influencer-next__title"><?php echo esc_html( $influencer->name ); ?></h3>
		<p><?php echo esc_html( ucfirst( date_i18n( 'l d F - \h.H:i', strtotime( $video->post_date ) ) ) ); ?></p>
	</div>
</div>
