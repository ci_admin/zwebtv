<?php
/**
 * The template for displaying the header.
 *
 * @package ZwebTheme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="theme-color" content="#d23226"/>
	<link rel="icon" type="image/png"
		  href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/icon.png">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header class="site-header">
	<div class="container">
		<div class="site-header__logo" itemscope
			 itemtype="https://schema.org/Organization">
			<a itemprop="url" href="<?php echo esc_url( get_home_url() ); ?>"
			   class="logo">
				<img itemprop="logo"
					 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/logo-2021.svg"
					 alt="<?php echo esc_html( bloginfo( 'name' ) ); ?>"
					 width="40"/>
			</a>
		</div>

		<div class="site-header__tagline">
			<?php echo esc_html( 'Con ZWEB rimani in verticale' ); ?>
		</div>
		<?php if ( get_page_template_slug() !== 'templates/page-launch.php' ) : ?>

			<nav class="site-header__nav site-navigation" role="navigation"
				 itemscope="itemscope"
				 itemtype="https://schema.org/SiteNavigationElement">
				<a class="primary-nav-toggle" href="#primary-nav"
				   aria-controls="primary-nav">
					<span class="screen-reader-text"><?php esc_html_e( 'Primary Menu', 'zweb-theme' ); ?></span>
					<span class="primary-nav-toggle__icon"
						  aria-hidden="true"></span>
				</a>

				<?php
				wp_nav_menu(
					[
						'theme_location' => 'primary',
						'menu_class'     => 'primary-menu',
						'menu_id'        => 'primary-nav',
					]
				);
				?>


			</nav>
		<?php endif; ?>


		<?php
		wp_nav_menu(
			[
				'menu'       => 'social-media-icons',
				'menu_class' => 'site-header__social social-menu',
				'menu_id'    => 'social-media-icons',
				'container'  => false,
			]
		);
		?>

		<?php if ( get_page_template_slug() !== 'templates/page-launch.php' ) : ?>
			<div class="site-header__search">
				<a class="search-form__toggle" href="#searchform"
				   aria-controls="searchform">
					<span class="screen-reader-text"><?php esc_html_e( 'Search', 'zweb-theme' ); ?></span>
					<span class="search-form__icon" aria-hidden="true">
						<svg class="search-form__icon-open"
							 xmlns="http://www.w3.org/2000/svg" width="18"
							 height="18.144">
							<use xlink:href="#search"></use>
						</svg>
						<svg class="search-form__icon-close"
							 xmlns="http://www.w3.org/2000/svg" width="18"
							 height="18.144">
							<use xlink:href="#search-close"></use>
						</svg>
					</span>
				</a>
				<?php echo get_search_form(); ?>
			</div>
		<?php endif; ?>

	</div>
</header>


