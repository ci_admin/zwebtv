import Swiper from 'swiper';

export default class InfluencerCarousel {
	/**
	 * Initialize everything
	 *
	 * @param element
	 * @param {string} element.
	 */
	constructor(element) {
		this.$element = document.querySelector(element);

		// Bail out if there's no menu.
		if (!this.$element) {
			return;
		}

		this.swiper = new Swiper(element, {
			slidesPerView: 'auto',
			centeredSlides: false,
			spaceBetween: 15,
			simulateTouch: false,
			navigation: {
				nextEl: '.swiper-controls-next',
				prevEl: '.swiper-controls-prev',
			},
		});
	}
}
