const maybePad = (number) => {
	if (number < 10) {
		return `0${number}`;
	}
	return `${number}`;
};

document.querySelectorAll('time.countdown-timer').forEach((element) => {
	const countDownTime = element.getAttribute('date') * 1000;
	// Update the count down every 1 second
	const interval = setInterval(function () {
		// Get today's date and time
		const now = new Date().getTime();

		// Find the distance between now and the count down date
		const distance = countDownTime - now;

		// Time calculations for days, hours, minutes and seconds
		const days = Math.floor(distance / (1000 * 60 * 60 * 24));
		const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

		const daysEl = element.querySelector('.upcoming-influencer__days');
		daysEl.innerText = maybePad(days);
		const hoursEl = element.querySelector('.upcoming-influencer__hours');
		hoursEl.innerText = maybePad(hours);
		const minsEl = element.querySelector('.upcoming-influencer__mins');
		minsEl.innerText = maybePad(minutes);

		// If the count down is finished, write some text
		if (distance < 0) {
			clearInterval(interval);
		}
	}, 1000);
});
