import Swiper from 'swiper';

export default class VideoCarousel {
	/**
	 * Initialize everything
	 *
	 * @param element
	 * @param sliderChange
	 * @param options
	 * @param {string} element.
	 */
	constructor(element, options = {}) {
		this.settings = options;
		this.swiper = new Swiper(element, {
			slidesPerView: 'auto',
			centeredSlides: false,
			spaceBetween: 10,
			simulateTouch: false,
			navigation: {
				nextEl: '.swiper-controls-next',
				prevEl: '.swiper-controls-prev',
			},
			breakpoints: {
				768: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				1024: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
				1280: {
					slidesPerView: 4,
					spaceBetween: 20,
				},
			},
		});

		const checkedPoints = {};
		let isAjaxLoading = false;
		const container = this.swiper.$el[0].closest('.container');
		const nextSlideSelector = container.querySelector('.swiper-controls-next');
		this.swiper.on('slideChangeTransitionStart', async function () {
			const { posts } = container.dataset; // total number of objects
			const slides = this.slides.length; // current slides
			if (posts <= slides) {
				return;
			}
			if (
				this.activeIndex > 0 &&
				(this.activeIndex + 1) % 4 === 0 && // we need to fecth every 4 images as we fetch in blocks of 4
				typeof checkedPoints[this.activeIndex] === 'undefined' // check we haven't already fetched.
			) {
				checkedPoints[this.activeIndex] = true;
				isAjaxLoading = true;

				const formData = new FormData();
				formData.append('action', 'load_more_slides');
				formData.append('offset', (this.activeIndex + 1) / 4);

				Object.keys(container.dataset).forEach((dataAttribute) => {
					formData.append(dataAttribute, container.dataset[dataAttribute]);
				});

				let response = await fetch(zweb.ajax_url, {
					method: 'post',
					body: formData,
				});

				response = await response.json();
				this.appendSlide(response.data);
				isAjaxLoading = false;
				nextSlideSelector.classList.remove('swiper-button-loading');
			}
		});

		this.swiper.on('slideChange', () => {
			this.settings.onSliderChange();
		});

		this.swiper.on('reachEnd', () => {
			if (isAjaxLoading) {
				nextSlideSelector.classList.add('swiper-button-loading');
			}
		});
	}
}
