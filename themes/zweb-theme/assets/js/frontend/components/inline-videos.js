/* globals dataLayer */

import throttle from '../utilities/throttle';
import setUpdateVideoViews from '../utilities/update-video-view';

export default class InlineVideos {
	constructor(elementClass, options = {}) {
		this.selectorClass = elementClass;
		this.currentVideoPlayer = null;
		this.$videos = document.querySelectorAll(this.selectorClass);
		this.settings = options;
		this.prevTransform = null;

		this.mq = window.matchMedia(this.settings.breakpoint);

		if (!this.$videos) {
			return;
		}

		this.setUpListeners();
	}

	setupInlineVideo(video) {
		const dataTarget = video.querySelector('.video-card__link');

		const { id } = dataTarget.dataset;
		const { playerId } = dataTarget.dataset;
		const { videoId } = dataTarget.dataset;
		const { accountId } = dataTarget.dataset;

		const wrapperDiv = document.createElement('div');
		wrapperDiv.setAttribute('class', 'video-card__inline-video');

		const playerElement = document.createElement('video-js');
		playerElement.setAttribute('id', id);
		playerElement.setAttribute('data-video-id', videoId);
		playerElement.setAttribute('data-account', accountId);
		playerElement.setAttribute('data-player', playerId);
		playerElement.setAttribute('data-embed', 'default');
		playerElement.setAttribute('class', 'video-js');
		playerElement.setAttribute('controls', '');
		playerElement.setAttribute('playsinline', 'true');

		wrapperDiv.appendChild(playerElement);

		dataTarget.insertAdjacentHTML('afterEnd', wrapperDiv.outerHTML);

		const scriptElement = document.createElement('script');
		scriptElement.src = `https://players.brightcove.net/${accountId}/${playerId}_default/index.min.js`;
		// Add the script tag to the document
		document.body.appendChild(scriptElement);

		scriptElement.onload = () => {
			const player = window.bc(`${id}`);

			if (this.settings.onAddPlayer && typeof this.settings.onAddPlayer === 'function') {
				this.settings.onAddPlayer(player);
			}
		};

		// Assign click event to the button
		const button = video.querySelector('.video-card__link');
		button.onclick = (e) => this.loadVideo(e, video, id);
	}

	updateAfterLoad() {
		this.$videos = document.querySelectorAll(this.selectorClass);
		this.$videos.forEach((video) => {
			const isSetUp = video.querySelector('.video-card__inline-video');
			const videoImage = video.querySelector('.video-card__image');
			const isLazyLoaded = videoImage.classList.contains('loaded');
			if (isSetUp === null && isLazyLoaded === true) {
				this.setupInlineVideo(video);
			}
		});
	}

	removeVideo() {
		if (this.currentVideoPlayer) {
			if (
				this.settings.onRemovePlayer &&
				typeof this.settings.onRemovePlayer === 'function'
			) {
				this.settings.onRemovePlayer(this.currentVideoPlayer);
			}

			this.$currentVideo.classList.remove('is-playing');
			this.currentVideoPlayer.pause();
			this.currentVideoPlayer = null;

			this.$currentVideo = null;
		}
	}

	loadVideo(e, video, videoId) {
		if (this.settings.onPlay && typeof this.settings.onPlay === 'function') {
			this.settings.onPlay.call();
		}

		this.removeVideo();

		this.$currentVideo = video;

		e.preventDefault();
		e.stopPropagation();

		const dataTarget = video.querySelector('.video-card__link');

		const { influencer, videoCategory, wpVideoId } = dataTarget.dataset;
		setUpdateVideoViews(wpVideoId);

		this.currentVideoPlayer = window.bc(`${videoId}`);

		this.$currentVideo.classList.remove('is-loading');
		this.$currentVideo.classList.add('is-playing');
		this.currentVideoPlayer.muted(false);

		this.currentVideoPlayer.play();

		this.$currentVideo.querySelector('.video-card__close-full-screen').onclick = () => {
			this.onVideoClosed();
		};

		this.currentVideoPlayer.on('play', () => {
			this.onVideoPlay();

			dataLayer.push({
				event: 'video-played',
				influencer,
				videoCategory,
			});
		});
	}

	onVideoPlay() {
		if (this.mq.matches === false) {
			document.querySelector('body').classList.add('is-playing');
			this.$currentVideo.classList.add('is-video-playing');

			const vidCarousel = this.$currentVideo.closest('.video-carousel');
			if (vidCarousel) {
				this.prevTransform = this.$currentVideo.closest('.swiper-wrapper').style.transform;
				this.$currentVideo.closest('.swiper-wrapper').style = 'transform: none';
				this.$currentVideo.closest('.video-carousel').style.zIndex = '99';
			}

			const vidLiveHome = this.$currentVideo.closest('.featured-influencer__overlay');
			if (vidLiveHome) {
				this.$currentVideo.closest('.featured-influencer__overlay').style.zIndex = '3';
			}
		}
	}

	onVideoClosed() {
		if (this.mq.matches === false) {
			document.querySelector('body').classList.remove('is-playing');
			this.currentVideoPlayer.pause();
			this.$currentVideo.classList.remove('is-video-playing');
			const vidCarousel = this.$currentVideo.closest('.video-carousel');
			if (vidCarousel) {
				this.$currentVideo.closest('.swiper-wrapper').style.transform = this.prevTransform;
				this.$currentVideo.closest('.video-carousel').style.zIndex = '2';
			}

			const vidLiveHome = this.$currentVideo.closest('.featured-influencer__overlay');
			if (vidLiveHome) {
				this.$currentVideo.closest('.featured-influencer__overlay').style.zIndex = '2';
			}
		}
	}

	setUpListeners() {
		window.addEventListener(
			'resize',
			() => {
				throttle(this.handleResize());
			},
			false,
		);
	}

	handleResize() {
		/* TODO handle resize */
	}
}
