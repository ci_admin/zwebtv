if (document.body.classList.contains('home')) {
	let interval;
	let currentState = zweb.state;
	const checkState = async () => {
		const formData = new FormData();
		formData.append('action', 'state_call');

		const response = await fetch(zweb.ajax_url, {
			method: 'post',
			body: formData,
		});

		const jsonResponse = await response.json();

		if (jsonResponse.data.header_text) {
			document.querySelector('.featured-influencer__heading').textContent =
				jsonResponse.data.header_text;
		}
		if (currentState !== jsonResponse.data.state) {
			document.querySelectorAll(`.${currentState}`).forEach((element) => {
				element.classList.add('hidden');
			});
			document.querySelectorAll(`.${jsonResponse.data.state}`).forEach((element) => {
				element.classList.remove('hidden');
			});
			currentState = jsonResponse.data.state;
		}
		if (jsonResponse.data.state === 'post-live' || jsonResponse.data.state === 'no-live') {
			clearInterval(interval);
			window
				.videojs(document.querySelector('.video-card--featured.live video-js').id)
				.pause();
		}
	};

	if (zweb.state === 'pre-live' || zweb.state === 'live') {
		interval = setInterval(checkState, 1000 * 30);
	}
}
