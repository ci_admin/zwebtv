/**
 *
 * @description
 *
 * Create social sharing toggle
 */
export default class SocialSharing {
	constructor(element, options) {
		// Search form container selector.
		this.$overlay = document.querySelector(element);

		const defaults = {
			onOpen: null,
		};

		// Settings
		this.settings = { ...defaults, ...options };

		// Bail out if there's no social options
		if (!this.$overlay) {
			console.error( 'Social Sharing: Target not found. A valid target element must be used.' ); // eslint-disable-line
			return;
		}

		this.$overlayToggle = document.querySelector(
			`[aria-controls="${this.$overlay.getAttribute('id')}"]`,
		);

		this.$overlayClose = document.querySelector(`.social-sharing__close`);

		// Also bail early if the toggle isn't set.
		if (!this.$overlayToggle) {
			console.error( 'Social Sharing: No toggle found.' ); // eslint-disable-line
		}

		// default to close
		this.$overlay.setAttribute('aria-hidden', true);
		this.$overlayToggle.setAttribute('aria-expanded', false);

		// Setup tasks
		this.setupListeners();
	}

	setupListeners() {
		const comp = this;

		this.$overlayToggle.addEventListener('click', this.listenerSocialOpenClick.bind(comp));

		this.$overlayClose.addEventListener('click', this.listenerSocialCloseClick.bind(comp));

		document.addEventListener('keyup', this.listenerDocumentKeyup.bind(comp));
	}

	listenerSocialOpenClick(event) {
		const isExpanded = this.$overlayToggle.getAttribute('aria-expanded') === 'true';

		// Don't act like a link.
		event.preventDefault();

		// Don't bubble.
		event.stopPropagation();

		// Is the social currently open?
		if (isExpanded) {
			// Update ARIA
			this.$overlay.setAttribute('aria-hidden', true);
			this.$overlayToggle.setAttribute('aria-expanded', false);
		} else {
			// Update ARIA
			this.$overlay.setAttribute('aria-hidden', false);
			this.$overlayToggle.setAttribute('aria-expanded', true);

			this.$overlay.querySelectorAll('.at-share-btn-elements a')[0].focus();
		}
	}

	listenerSocialCloseClick(event) {
		// Don't act like a link.
		event.preventDefault();

		// Don't bubble.
		event.stopPropagation();

		this.$overlay.setAttribute('aria-hidden', true);
		this.$overlayToggle.setAttribute('aria-expanded', false);
	}

	listenerDocumentKeyup(event) {
		const isExpanded = this.$overlayToggle.getAttribute('aria-expanded') === 'true';

		// Bail early if not using the escape key or if no submenus are found.
		if (isExpanded === false || event.keyCode !== 27) {
			return;
		}

		// Close overlay
		this.close();
	}

	close() {
		// Update ARIA
		this.$overlay.setAttribute('aria-hidden', true);
		this.$overlayToggle.setAttribute('aria-expanded', false);
	}
}
