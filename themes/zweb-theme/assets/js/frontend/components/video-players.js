/* globals dataLayer */

import setUpdateVideoViews from '../utilities/update-video-view';

export default class VideoPlayers {
	constructor(breakpoint) {
		this.players = [];
		this.videoPlayer = document.querySelector('.video-player');

		if (this.videoPlayer) {
			this.videoEl = this.videoPlayer.querySelector('video');
			if (this.videoEl) {
				this.videoEl.setAttribute('playsinline', 'playsinline');
			}
		}

		this.mq = window.matchMedia(breakpoint);

		if (window.videojs !== undefined) {
			for (let x = 0; x < Object.keys(window.videojs.players).length; x += 1) {
				const setPlayer = Object.keys(window.videojs.players)[x];
				const player = window.videojs.getPlayer(setPlayer);
				const playerID = player.tagAttributes.id;
				const playerEl = document.getElementById(playerID);
				const playerParent = playerEl.closest('.video-player');

				let influencer = '';
				let videoCategory = '';
				let wpVideoId = '';

				if (playerParent) {
					const videoPlayerEl = playerEl.closest('.video-player');
					influencer = videoPlayerEl.dataset.influencer;
					videoCategory = videoPlayerEl.dataset.videoCategory;
					wpVideoId = videoPlayerEl.dataset.wpVideoId;
				}

				player.usingNativeControls(false);

				// Autoplay on mobile on single video.
				const isSingleVideo = document.body.classList.contains('single-zweb-video');
				if (this.mq.matches === false && isSingleVideo && x === 0) {
					// Wait for loadedmetadata then try to play video.
					player.on('loadedmetadata', () => {
						// Play video which returns a promise
						const promise = player.play();

						// Use promise to see if video is playing or not.
						if (promise !== undefined) {
							promise
								.then(() => {
									// Autoplay started!
									// If video playing unmute.
									player.muted(false);
								})
								.catch(() => {
									// Autoplay was prevented.
									// If autoplay prevented: mute the video and play video.
									player.muted(true);
									player.play();
								});
						}
					});
				}

				player.ready(() => {
					player.on('play', (e) => {
						this.onPlay(e);
						this.onMobileVideoPlay();

						this.videoPlayer.querySelector(
							'.video-card__close-full-screen',
						).onclick = () => {
							this.onMobileVideoClosed(player);
						};

						dataLayer.push({
							event: 'video-played',
							influencer,
							videoCategory,
						});

						setUpdateVideoViews(wpVideoId);
					});
					this.players.push(player);
				});
			}
		}
	}

	onPlay(e) {
		if (e.target) {
			const { id } = e.target;
			for (let i = 0; i < this.players.length; i += 1) {
				if (this.players[i].id() !== id) {
					window.videojs(this.players[i].id()).pause();
				}
			}
		}
	}

	onMobileVideoPlay() {
		if (this.mq.matches === false) {
			this.videoPlayer.classList.add('is-video-playing');
		}
	}

	onMobileVideoClosed(player) {
		if (this.mq.matches === false) {
			this.videoPlayer.classList.remove('is-video-playing');
			player.pause();
		}
	}

	pausePlayers() {
		for (let i = 0; i < this.players.length; i += 1) {
			if (this.players[i] !== null) {
				this.players[i].pause();
			}
		}
	}

	addPlayer(player) {
		player.on('play', (e) => {
			this.onPlay(e);
		});
		this.players.push(player);
	}

	removePlayer(player) {
		const index = this.players.indexOf(player);
		if (index > -1) {
			this.players.splice(index, 1);
		}
	}
}
