import { tns } from 'tiny-slider/src/tiny-slider';

export default class VideoCategoriesCarousel {
	/**
	 * Initialize everything
	 *
	 * @param element for carousel
	 */
	constructor(element) {
		this.$element = document.querySelector(element);

		// Bail out if there's no menu.
		if (!this.$element) {
			return;
		}

		this.slider = tns({
			container: element,
			controls: false,
			nav: false,
			fixedWidth: 150,
			loop: false,
			gutter: 10,
			slideBy: 'page',
			autoplay: false,
			controlsText: ['<span>prev</span>', '<span>next</span>'],
			responsive: {
				768: {
					disable: true,
				},
			},
		});
	}
}
