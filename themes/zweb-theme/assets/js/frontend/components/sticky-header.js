import throttle from '../utilities/throttle';

export default class StickyHeader {
	/**
	 * constructor method
	 *
	 * @param {string} element Element selector for header container.
	 */
	constructor(element) {
		this.$header = document.querySelector(element);

		this.mq = window.matchMedia('(min-width: 48em)');

		if (this.mq.matches === false) {
			this.$siteMain = document.querySelector('.site-main:not(.site-main--landing)');
		} else {
			this.$siteMain = document.querySelector(
				'.site-main:not(.site-main--landing):not(.site-main--pull-up)',
			);
		}

		if (this.$siteMain) {
			this.$siteMain.style.paddingTop = `${this.$header.offsetHeight}px`;
			setTimeout(() => {
				this.$siteMain.style.paddingTop = `${this.$header.offsetHeight}px`;
			}, 500);
		}

		window.addEventListener(
			'scroll',
			() => {
				throttle(this.handleFixedHeader());
			},
			false,
		);
	}

	/**
	 * Handles the scroll event
	 *
	 * @return {void}
	 */
	handleFixedHeader() {
		const fromTop = window.pageYOffset;
		if (fromTop > 15) {
			this.$header.classList.add('is-fixed');
		} else {
			this.$header.classList.remove('is-fixed');
		}
	}
}
