/**
 *
 * @description
 *
 * Create search form toggle
 */
export default class SearchForm {
	/**
	 * constructor method
	 *
	 * @param {string} element Element selector for navigation container.
	 * @param options
	 */
	constructor(element, options) {
		// Search form container selector.
		this.$searchForm = document.querySelector(element);

		const defaults = {
			onOpen: null,
		};

		// Settings
		this.settings = { ...defaults, ...options };

		// Bail out if there's no search form.
		if (!this.$searchForm) {
			console.error( 'Search Form: Target not found. A valid target element must be used.' ); // eslint-disable-line
			return;
		}

		this.$searchToggle = document.querySelector(
			`[aria-controls="${this.$searchForm.getAttribute('id')}"]`,
		);

		// Also bail early if the toggle isn't set.
		if (!this.$searchToggle) {
			console.error( 'Search Form: No search toggle found.' ); // eslint-disable-line
		}

		// default to close
		this.$searchForm.setAttribute('aria-hidden', true);
		this.$searchToggle.setAttribute('aria-expanded', false);

		// Setup tasks
		this.setupListeners();
	}

	/**
	 * Binds our various listeners for the plugin.
	 *
	 * @return {void}
	 */
	setupListeners() {
		const comp = this;

		this.$searchToggle.addEventListener('click', this.listenerMenuToggleClick.bind(comp));
		document.addEventListener('keyup', this.listenerDocumentKeyup.bind(comp));
	}

	/**
	 * Search toggle handler.
	 * Opens or closes the search according to current state.
	 *
	 * @param {Object} event The event object.
	 * @return {void}
	 */
	listenerMenuToggleClick(event) {
		const isExpanded = this.$searchToggle.getAttribute('aria-expanded') === 'true';

		// Don't act like a link.
		event.preventDefault();

		// Don't bubble.
		event.stopPropagation();

		// Is the search form currently open?
		if (isExpanded) {
			// Update ARIA
			this.$searchForm.setAttribute('aria-hidden', true);
			this.$searchToggle.setAttribute('aria-expanded', false);
		} else {
			/**
			 * Called before the component is opened.
			 *
			 * @callback onOpen
			 */
			if (this.settings.onOpen && typeof this.settings.onOpen === 'function') {
				this.settings.onOpen.call();
			}

			// Update ARIA
			this.$searchForm.setAttribute('aria-hidden', false);
			this.$searchToggle.setAttribute('aria-expanded', true);

			// Focus the input
			this.$searchForm.querySelectorAll('input')[0].focus();
		}
	}

	listenerDocumentKeyup(event) {
		const isExpanded = this.$searchToggle.getAttribute('aria-expanded') === 'true';

		// Bail early if not using the escape key or if no submenus are found.
		if (isExpanded === false || event.keyCode !== 27) {
			return;
		}

		// Close overlay
		this.close();
	}

	/**
	 * close search form.
	 * Closes the search.
	 *
	 * @return {void}
	 */

	close() {
		// Update ARIA
		this.$searchForm.setAttribute('aria-hidden', true);
		this.$searchToggle.setAttribute('aria-expanded', false);
	}
}
