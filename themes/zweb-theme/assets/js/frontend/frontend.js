import './utilities/closest';
import LazyLoad from 'vanilla-lazyload';
import Navigation from './components/primary-nav';
import VideoCarousel from './components/video-carousel';
import VideoCategoriesCarousel from './components/video-categories-carousel';
import InfluencerCarousel from './components/influencer-carousel';
import InfluencerNextCarousel from './components/influencer-next-carousel';
import Picturefill from './components/picturefill';
import SearchForm from './components/search-form';
import SocialSharing from './components/social-sharing';
import './components/countdown';
import './components/homepage-polling';

import InlineVideos from './components/inline-videos';
import VideoPlayers from './components/video-players';

/* eslint-disable */
import symbol from '../../images/search.svg';
import arrowRight from '../../images/arrow-right.svg';
import searchIcon from '../../images/search.svg';
import searchIconClose from '../../images/search-close.svg';
import shareIcon from '../../images/share.svg';
import angleDown from '../../images/angle-down.svg';
import calendarIcon from '../../images/calendar.svg';
import iconEye from '../../images/icon-eye.svg';

/* eslint-enable */

import StickyHeader from './components/sticky-header';

(() => new StickyHeader('.site-header'))();

const breakpoint = '(min-width: 48em)';
let primaryNav = null;
let searchForm = null;
const videoPlayers = new VideoPlayers(breakpoint);
let inlineVideos = null;
let lazyLoad = null;

(() => {
	document.querySelectorAll('.video-carousel__wrap').forEach((element) => {
		(() => {
			new VideoCarousel(element, {
				onSliderChange() {
					inlineVideos.updateAfterLoad();
					videoPlayers.pausePlayers();
					lazyLoad.update();
				},
			});
		})();
	});
})();

(() => new VideoCategoriesCarousel('.video-categories__items'))();

(() => new InfluencerCarousel('.influencer-carousel__wrap'))();

(() => new InfluencerNextCarousel('.influencer-next-carousel__items'))();
(() => new SocialSharing('.social-sharing'))();

(() => new Picturefill())();

lazyLoad = new LazyLoad({
	elements_selector: '.lazy',
	use_native: false,
	thresholds: '0px 200px 200px 0px',
	callback_loaded: () => {
		inlineVideos.updateAfterLoad();
	},
});

inlineVideos = new InlineVideos('.video-card', {
	onPlay() {
		videoPlayers.pausePlayers();
	},
	onAddPlayer(p) {
		videoPlayers.addPlayer(p);
	},
	onRemovePlayer(p) {
		videoPlayers.removePlayer(p);
	},
	breakpoint,
});

searchForm = new SearchForm('.search-form', {
	onOpen() {
		primaryNav.closeAllSubmenus(); // eslint-disable-line
	},
});

primaryNav = new Navigation('.primary-menu', {
	action: 'click',
	breakpoint: '(min-width: 64em)',
	onSubmenuOpen() {
		searchForm.close();
	},
});
