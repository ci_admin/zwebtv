<?php
/**
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;

get_header();
?>



<section class="content-page container">

	<h1><?php esc_html_e('Page not found','zweb-theme'); ?></h1>
	<p><?php esc_html_e('We could not find the page you are looking for','zweb-theme'); ?></p>
	<a href="<?php echo esc_url( get_home_url() ); ?>" class="cta-button">
		<?php esc_html_e('Go To Home','zweb-theme'); ?>
	</a>

</section>



<?php get_footer(); ?>
