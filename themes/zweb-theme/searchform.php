<?php
/**
 * The template for displaying the search form.
 *
 * @package ZwebTheme
 */

?>

<div itemscope itemtype="http://schema.org/WebSite">
	<form role="search" id="searchform" class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div class="search-form__wrap">
			<meta itemprop="target" content="<?php echo esc_url( home_url() ); ?>/?s={s}" />
			<label for="search-field">
				<?php echo esc_html_x( 'Search for:', 'label', 'zweb-theme' ); ?>
			</label>
			<input itemprop="query-input" type="search" id="search-field" value="<?php echo get_search_query(); ?>" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'zweb-theme' ); ?>" name="s" />

			<button class="search-form__submit">
				<svg class="search-form__submit-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18.1"><path d="M18 16.4l-4.7-4.7c2.4-3.3 1.6-7.9-1.6-10.3S3.8-.2 1.4 3-.3 10.9 3 13.3c2.5 1.8 5.9 1.9 8.5.1l4.7 4.7 1.8-1.7zm-15.8-9c0-2.9 2.3-5.2 5.2-5.2s5.2 2.3 5.2 5.2-2.3 5.2-5.2 5.2c-2.9 0-5.2-2.4-5.2-5.2z" fill="#fff"/></svg>
				<span class="search-form__submit-label">
					<?php echo esc_attr_x( 'Go', 'submit button', 'zweb-theme' ); ?>
				</span>
			</button>
		</div>
	</form>
</div>
