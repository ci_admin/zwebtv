<?php
/**
 * Template Name: Video Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Builder\Query;
use Zweb\Taxonomy\Influencer;

get_header();
?>
<div class="site-main">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<section class="video-page">
				<div class="container">
					<div class="video-page__player">
						<?php get_template_part( 'partials/video-player', 'page' ); ?>
					</div>

					<?php get_template_part( 'partials/influencer', 'video-page' ); ?>
				</div>
			</section>

		<?php endwhile; ?>
	<?php endif; ?>

	<?php
	try {
		$influencer = Influencer::get_influencer_for_video( get_queried_object()->ID );
		set_query_var(
			'video_carousel_config',
			[
				'query'              => Query::get_video_carousel_query( 8, null, $influencer ),
				'influencer_term_id' => $influencer->term_id,
				'label'              => sprintf( __( 'Altri video di %s', 'zweb-theme' ), $influencer->name ),
			]
		);
		get_template_part( 'partials/carousel-video' );
	} catch ( \Exception $exception ) {
		// no slider
	}
	?>

</div>
<?php get_footer(); ?>
