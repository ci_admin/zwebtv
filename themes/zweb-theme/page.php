<?php
/**
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;

get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<section class="content-page container">

		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>

	</section>

<?php endwhile; ?>

<?php get_footer(); ?>
