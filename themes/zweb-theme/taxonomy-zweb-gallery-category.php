<?php
/**
 * Template Name: Video Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;

get_header();
?>
<div class="site-main site-main--landing">
	<section class="video-landing-page">
		<div class="container">

			<h1><?php echo esc_html( get_queried_object()->name ); ?></h1>

			<ul class="video-grid">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<li class="video-grid__item">
							<?php set_query_var( 'video_id', get_the_ID() ); ?>
							<?php get_template_part( 'partials/video-card', 'page' ); ?>
						</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
	</section>
	<?php
	foreach ( Gallery::get_remaining_terms( get_queried_object()->term_id ) as $term ) {
		set_query_var(
			'video_carousel_config',
			[
				'query'           => Query::get_video_carousel_query( 8, $term ),
				'gallery_term_id' => $term->term_id,
				'label'           => $term->name,
			]
		);
		get_template_part( 'partials/carousel-video', 'page' );
	}

	get_template_part( 'partials/carousel-video-categories', 'page' );

	set_query_var( 'influencer_carousel_config', [ 'query' => Influencer::get_influencer_carousel_query() ] );
	get_template_part( 'partials/carousel-influencer' );
	?>
</div>

<?php get_footer(); ?>
