<?php
/**
 * WP Theme constants and setup functions
 *
 * @package ZwebTheme
 */

// Useful global constants.
define( 'ZWEB_THEME_VERSION', '1.6.0' );
define( 'ZWEB_THEME_TEMPLATE_URL', get_template_directory_uri() );
define( 'ZWEB_THEME_PATH', get_template_directory() . '/' );
define( 'ZWEB_THEME_INC', ZWEB_THEME_PATH . 'includes/' );

require_once ZWEB_THEME_INC . 'core.php';
require_once ZWEB_THEME_INC . 'overrides.php';
require_once ZWEB_THEME_INC . 'template-tags.php';
require_once ZWEB_THEME_INC . 'utility.php';
require_once ZWEB_THEME_INC . 'blocks.php';

// Run the setup functions.
ZwebTheme\Core\setup();
ZwebTheme\Blocks\setup();

// Require Composer autoloader if it exists.
if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require_once 'vendor/autoload.php';
}

if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for the the new wp_body_open() function that was added in 5.2
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}
