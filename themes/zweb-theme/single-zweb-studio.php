<?php
/**
 * Template Name: Influencer Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\PostType\Studio;

$studio      = get_queried_object();
$influencers = Studio::get_influencers_for_studio( $studio->ID );

get_header();
?>
	<div class="site-main site-main--landing">
		<section class="influencer-landing-page">
			<div class="container">

				<h1><?php echo esc_html( $studio->post_title ); ?></h1>

				<?php if ( $influencers ) : ?>
					<ul class="influencer-grid">
						<?php foreach ( $influencers as $influencer ) : ?>
							<li class="influencer-grid__item">
								<?php set_query_var( 'influencer', $influencer ); ?>
								<?php get_template_part( 'partials/influencer-card' ); ?>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php else : ?>
					<div><?php esc_html_e( 'No influencers for the studio', 'zweb-theme' ); ?></div>
				<?php endif ?>
			</div>
		</section>
	</div>

	<?php
	get_footer();
