<?php
/**
 * Template Name: Influencer Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Carbon\Carbon;
use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\PostType\Studio;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;
use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;

get_header();
try {
	$influencer                = get_queried_object();
	$influencer_user           = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
	$influencer_video_category = \Zweb\Taxonomy\Influencer::get_influencer_video_category( $influencer_user->ID );
	$next_video                = Influencer::get_next_video_by_influencer( $influencer->term_id );
	$is_studio_influencer      = \Zweb\PostType\Studio::is_studio_influencer( $influencer_user ) ? true : false;
	$is_studio_roma            = 'studio-roma' === $influencer->slug;

	set_query_var( 'influencer', $influencer );
	set_query_var( 'influencer_user', $influencer_user );
	set_query_var( 'influencer_video_category', $influencer_video_category );
	set_query_var( 'next_video', $next_video );
} catch ( \Exception $exception ) {
	return;  // ToDo redirect to 404.
}
?>
<div class="site-main site-main--pull-up">
	<?php get_template_part( '/partials/influencer', 'single-page' ); ?>

	<?php get_template_part( '/partials/video-card', 'influencer' ); ?>

	<?php
	set_query_var(
		'video_carousel_config',
		[
			'query'              => Query::get_video_carousel_query( 8, null, $influencer ),
			'influencer_term_id' => $influencer->term_id,
			'label'              => __( 'Tutti i video', 'zweb-theme' ),
		]
	);
	get_template_part( 'partials/carousel-video' );
	?>

	<?php
	try {
		$extra = get_term_by( 'slug', 'extra', Gallery::TAXONOMY_NAME );
		if ( $extra instanceof \WP_Term ) {
			set_query_var(
				'video_carousel_config',
				[
					'query'              => Query::get_video_carousel_query( 8, $extra, $influencer ),
					'gallery_term_id'    => $extra->term_id,
					'influencer_term_id' => $influencer->term_id,
					'label'              => $extra->name,
				]
			);
			get_template_part( 'partials/carousel-video' );
		}
	} catch ( \Exception $exception ) {
		// no gallery
	}
	?>

	<?php
	try {
		if ( ! $is_studio_influencer ) {
			set_query_var(
				'influencer_carousel_config',
				[
					'query' => Influencer::get_related_influencers_carousel_query( $influencer ),
					'label' => __( 'Influencer Correlati', 'zweb-theme' ),
				]
			);
			get_template_part( 'partials/carousel-influencer' );
		}
	} catch ( \Exception $exception ) {
		// no influencers
	}
	?>

	<?php
	if ( $is_studio_roma ) {
		try {
			$sport = get_term_by( 'slug', 'sport', Gallery::TAXONOMY_NAME );
			if ( $sport instanceof \WP_Term ) {
				set_query_var(
					'video_carousel_config',
					[
						'query'              => Query::get_video_carousel_query( 8, $sport, $influencer ),
						'gallery_term_id'    => $sport->term_id,
						'influencer_term_id' => $influencer->term_id,
						'label'              => $sport->name,
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
		} catch ( \Exception $exception ) {
			// no gallery
		}

		try {
			$news = get_term_by( 'slug', 'news', Gallery::TAXONOMY_NAME );
			if ( $news instanceof \WP_Term ) {
				set_query_var(
					'video_carousel_config',
					[
						'query'              => Query::get_video_carousel_query( 8, $news, $influencer ),
						'gallery_term_id'    => $news->term_id,
						'influencer_term_id' => $influencer->term_id,
						'label'              => $news->name,
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
		} catch ( \Exception $exception ) {
			// no gallery
		}

		try {
			$insights = get_term_by( 'slug', 'approfondimenti', Gallery::TAXONOMY_NAME );
			if ( $insights instanceof \WP_Term ) {
				set_query_var(
					'video_carousel_config',
					[
						'query'              => Query::get_video_carousel_query( 8, $insights, $influencer ),
						'gallery_term_id'    => $insights->term_id,
						'influencer_term_id' => $influencer->term_id,
						'label'              => $insights->name,
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
		} catch ( \Exception $exception ) {
			// no gallery
		}
	}

	?>

</div>

<?php get_footer(); ?>
