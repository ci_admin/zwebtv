<?php
/**
 * Template Name: Video Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Admin\Settings;
use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;
use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;



get_header();
?>
<div class="site-main">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<section class="video-page">
				<div class="container">
					<div class="video-page__player">
						<?php
						set_query_var( 'is_live', true );
						get_template_part( 'partials/video-player', 'page' );
						?>
					</div>

					<?php get_template_part( 'partials/influencer', 'video-page' ); ?>
				</div>
			</section>

		<?php endwhile; ?>
	<?php endif; ?>

	<?php
	get_template_part( 'partials/carousel-upcoming-live' );
	?>

	<?php

	set_query_var(
		'video_carousel_config',
		[
			'query' => Query::get_video_carousel_query(),
			'label' => __( 'Novità', 'zweb' ),
			'link'  => '/video',
		]
	);
	get_template_part( 'partials/carousel-video' );

	?>
</div>
<?php get_footer(); ?>
