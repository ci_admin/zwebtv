<?php
/**
 * Template Name: Launch Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;

get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<section class="content-page content-page--wide container">

		<div class="video-player"
			 data-influencer="<?php echo esc_attr( $influencer_username ); ?>"
			 data-video-category="<?php echo isset( $video_category ) ? esc_attr( $video_category->name ) : ''; ?>"
		>
			<?php the_content(); ?>

			<div class="video-card__close-full-screen">
				<svg class=""
					 xmlns="http://www.w3.org/2000/svg" width="18"
					 height="18.144">
					<use xlink:href="#search-close"></use>
				</svg>
			</div>
		</div>

	</section>

<?php endwhile; ?>

<?php get_footer(); ?>
