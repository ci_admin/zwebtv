<?php
/**
 * Template Name: Influencer Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Taxonomy\Influencer;

$influencers = Influencer::get_influencers_for_influencer_landing_page();

get_header();
?>
<div class="site-main site-main--landing">
	<section class="influencer-landing-page">
		<div class="container">

			<h1><?php esc_html_e( 'Influencers', 'zweb-theme' ); ?></h1>

			<?php if ( $influencers['upcoming'] || $influencers['present'] ) : ?>
				<ul class="influencer-grid">
					<?php foreach ( $influencers['present'] as $influencer ) : ?>
						<li class="influencer-grid__item">
							<?php set_query_var( 'influencer', $influencer ); ?>
							<?php get_template_part( 'partials/influencer-card' ); ?>
						</li>
					<?php endforeach; ?>
					<?php foreach ( $influencers['upcoming'] as $influencer ) : ?>
						<li class="influencer-grid__item">
							<?php set_query_var( 'influencer', $influencer ); ?>
							<?php get_template_part( 'partials/influencer-upcoming' ); ?>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php else : ?>
				<div><?php esc_html_e( 'No influencers', 'zweb-theme' ); ?></div>
			<?php endif ?>
		</div>
	</section>
</div>

<?php
get_footer();
