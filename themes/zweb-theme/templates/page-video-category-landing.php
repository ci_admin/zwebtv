<?php
/**
 * Template Name: Video Category Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

$video_categories_query = \Zweb\Taxonomy\VideoCategory::get_video_category_carousel_query();

get_header();
?>
<div class="site-main site-main--landing">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="video-categories__grid">
			<ul class="video-categories__grid-items">
				<?php foreach ( $video_categories_query->get_terms() as $term ) : ?>
					<li class="video-categories__item">
						<?php set_query_var( 'video_category_term', $term ); ?>
						<?php get_template_part( 'partials/video-category' ); ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>
