<?php
/**
 * Template Name: Home
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Admin\Settings;
use Zweb\Brightcove\Live;
use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;

get_header();

?>
<div class="site-main site-main--pull-up">
	<?php
	get_template_part( 'partials/notification');

	set_query_var( 'is_live', Live::is_studio_roma_live() );
	get_template_part( 'partials/influencer', 'featured' );
	?>

	<?php
	set_query_var(
		'video_carousel_config',
		[
			'query'   => Query::build_novita_query(),
			'label'   => __( 'Novità', 'zweb' ),
			'link'    => '/video',
			'relationship'    => 'novita',
			'pull_up' => true,
		]
	);
	get_template_part( 'partials/carousel-video' );

	get_template_part( 'partials/carousel-video-categories' );
	set_query_var( 'influencer_carousel_config', [ 'query' => Influencer::get_influencer_carousel_query() ] );
	get_template_part( 'partials/carousel-influencer' );


	the_studio_roma_carousel();

	$top_10         = Settings::get_curated_section( 'top_10' );

	if ( $top_10 ) {
		set_query_var(
			'video_carousel_config',
			[
				'query'   => Query::get_query_for_curated_lists( $top_10 ),
				'label'   => __( 'Top 10', 'zweb' ),
				'link'    => '/top-10',
			]
		);
		get_template_part( 'partials/carousel-video' );
	}

	?>

	<?php get_template_part( 'partials/video-featured' ); ?>

	<?php get_template_part( 'partials/carousel', 'upcoming-videos' ); ?>

</div>
<?php get_footer(); ?>
