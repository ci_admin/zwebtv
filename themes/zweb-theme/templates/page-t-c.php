<ol>
	<li><b>Binding conditions of use&nbsp;</b>
		<div style="font-weight: 400;">These conditions of use (hereinafter the</div><b><i>Conditions of Use</i></b><span style="font-weight: 400;">) govern the terms and conditions of access and use of the services (hereinafter jointly the </span><b><i>Zweb Services</i></b><span
			style="font-weight: 400;">) available on the application that the user has downloaded (hereinafter the </span><b><i>App</i></b><span style="font-weight: 400;">), managed by the company L Financial Limited with registered office in P.O. Box 296, St Peter Port, Guernsey, GY1 4NA, Company Registration Number 58652 (hereinafter the </span><b><i>Company</i></b><span
			style="font-weight: 400;">)</span><i><span style="font-weight: 400;">.</span></i>
	</li>
	<li><b>Registration</b>
		<ol>
			<li style="font-weight: 400;"><span
					style="font-weight: 400;">Registration on the App is allowed only to users over the age of 14, who will be able to access all Zweb Services.&nbsp;</span></li>
			<li style="font-weight: 400;"><span
					style="font-weight: 400;">For the registration purposes, the user, after reading the Privacy Policy pursuant to art. 13 of EU Regulation 679/2016 (</span><b><i>GDPR</i></b><span
					style="font-weight: 400;">) published on the App, must enter personal data (including e-mail address and the date of birth) indicated in the registration form together with a username and password.</span>
			</li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">The personal data entered by the user in the App registration form must be true and complete.</span></li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">The username and password entered at the time of registration are personal, cannot be transferred and must be kept strictly confidential by the user. The user is therefore obliged to keep them with the utmost diligence in order to prevent any use of the App by unauthorized third parties.&nbsp;</span>
			</li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">The user is directly and solely responsible for any use of the above mentioned data by authorized or unauthorized third parties that appears to have been carried out in violation of the normal diligence required for their safekeeping. Without prejudice to the above, in any case of unauthorized use of the username and/or password by third parties, the user undertakes to immediately inform the Company in the manner indicated in point 15 below.</span>
			</li>
			<li style="font-weight: 400;"><span style="font-weight: 400;">Users can cancel their registration at any time by clicking the "cancel registration" button.</span></li>
		</ol>
	</li>
	<li><b>Zweb Services</b></li>
	<ol>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Subject to </span><i><span style="font-weight: 400;">point &amp; click</span></i><span style="font-weight: 400;"> acceptance of the Conditions of Use, and provided they meet the minimum age requirement set forth in point 2.1, the Company will permit registered users of the App (hereinafter </span><b><i>User/s</i></b><span
				style="font-weight: 400;">) to use the Zweb Services available on the App, which allow the User to search and view videos uploaded by other Users on the App, comment on such videos, participate in the so called "</span><i><span
					style="font-weight: 400;">battle videos</span></i><span style="font-weight: 400;">" present on the App, distribute streaming audiovisual works both live (so-called </span><i><span
					style="font-weight: 400;">live streaming</span></i><span style="font-weight: 400;">) or recorded of which the User is the author or in any case has all rights required to do so (hereinafter </span><b><i>User
					Content</i></b><span style="font-weight: 400;">).&nbsp;</span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Access to the aforementioned Zweb Services will be differentiated according to the category in which the User falls. In particular:&nbsp;</span>
		</li>
		<ol style="list-style-type: lower-latin">
			<li style="font-weight: 400;"><b>Unauthenticated Users</b><span style="font-weight: 400;">: they will only have the opportunity to access the App and to search for content and view videos on the App.&nbsp;</span>
			</li>
			<li style="font-weight: 400;"><b>Authenticated Users</b><span style="font-weight: 400;">: in addition to what is provided for non-authenticated Users, they will have the opportunity to comment and "</span><i><span
						style="font-weight: 400;">like</span></i><span style="font-weight: 400;">" the videos on the App, actively participate in the so-called "</span><i><span
						style="font-weight: 400;">battle videos</span></i><span style="font-weight: 400;">" present on the App, follow the Users who fall into the category "</span><i><span
						style="font-weight: 400;">influencers</span></i><span style="font-weight: 400;">", save their videos and send them private messages.</span></li>
			<li style="font-weight: 400;"><b>Influencers</b><span style="font-weight: 400;">: in addition to what is provided for authenticated Users, they will have the possibility to upload videos, publish live streaming that can be seen by Users both authenticated and not, edit the profile page related to their biography and image.</span>
			</li>
		</ol>
	</ol>
	<li><b>Intellectual property rights</b>
		<ol>
			<li style="font-weight: 400;">The contents of the App (excluding the User Contents referred to in point 5 below), such as, by way of example, works, images, photographs, music, sounds and videos, slogans, documents, drawings, figures, texts, logos, trademarks and any other material in any format published on the App, including menus, web pages, the graphics, colors, patterns, tools, fonts and design of the App, the diagrams, layouts, and software that are part of the App, and any element protected by copyright and any other intellectual property right, are the exclusive property of the Company and/or its assignees (hereinafter <b>Zweb</b> Content). Reproduction, in whole or in part, in any form whatsoever, of the App and the Zweb Content is prohibited without the express written consent of the Company. The User is therefore only authorized to view the App and the Zweb Content and to perform all other acts (including the use of the Zweb Services) of temporary, personal and uneconomic use, which are an incidental, an integral and an essential part of the same viewing the App and the Zweb Content and all other navigation operations on the App that are performed only for a legitimate use of the App and the Zweb Content. </li>
		</ol>
	</li>
	<li><b>User Content</b>
		<ol>
			<li style="font-weight: 400;">By accepting the Conditions of Use, the User authorizes the Company to publish the User Content that will be provided by the User from time to time, by uploading them to the App.</li>
			<li style="font-weight: 400;">The User grants the Company a non-exclusive, unlimited, unconditional, sublicensable and transferable license, valid in all countries of the world to the maximum extent possible by law, to reproduce, publish, perform, transmit, transfer to other formats (format shifting), communicate to the public, make available to the public, process and/or edit, re-edit, translate, distribute, commercially exploit, and in any case use all or part of the User Content in any form and manner with reference, among other things - but not exclusively - to the relevant copyrights, intellectual property rights, personality (name, image and voice that the User transmits in relation to the User Content). The rights provided therein are independent of each other, and the exercise of one does not exclude the exercise of the other rights. It is understood that there is no obligation on the part of the Company to exercise all or part of the aforementioned rights. By accepting the Conditions of Use, the User acknowledges and in any case agrees that no compensation or remuneration other or additional to the rights to use the Zweb Services and Zweb Content provided under these Conditions of Use is provided for. </li>
		</ol>
	</li>
	<li><b>Guarantees</b>
		<ol>
			<li style="font-weight: 400;">The User represents, warrants and undertakes to the Company and its assignees (i) that the User Content entered on the App is original and that he/she is the owner of the relevant copyrights, personality rights, as well as any other rights relating thereto; (ii) that nothing entered is, or could be considered to be, obscene, violent, defamatory, blasphemous, gruesome or liable to upset minors or will, in any case, violate the rights of third parties (such as, by way of example and without limitation, moral rights, image rights, honor, decorum and/or reputation and/or confidentiality, industrial and intellectual property rights, personal data protection rights, publication rights or others) or legal provisions; (iii) to have in any case obtained all the necessary consents and releases from all parties having the right and/or responsibility to provide such permissions with regard to the User Contents and their use by the Company, and (iv) that neither the User Contents nor their use will result in the violation of the personal or property rights of third parties and/or of the law, also having regard to any authorizations or permissions by public authorities or self-regulatory or similar bodies. </li>
			<li style="font-weight: 400;">In the event that the User is not the sole owner of the rights to the User Content, the User is required to acquire all the documentation attesting to the granting to the User of the necessary authorizations by the entitled parties before entering the said User Content on the App (and to make available to the Company upon request by the latter). With particular regard to the publication of videos and images of minors, the User declares that he/she is the owner or in any case has already acquired the necessary written authorization (which the said User undertakes to make available to the Company upon Company’s request) from the parental authority. </li>
			<li style="font-weight: 400;">In the event that the Company or its assignees are subject to legal action, disturbance or other complaints or claims by third parties regarding the conduct or manner of use of the Zweb Services by a User and/or related User Content, the latter expressly agrees, to the extent permitted by law, to hold the Company and its assignees fully harmless and indemnified from and against any and all liability, loss, damage, cost, legal, professional and any other expenses of any kind incurred or suffered, without prejudice to any other rights or remedies the Company or its assignees may have. </li>
			<li style="font-weight: 400;">The Company does not provide any warranty in relation to or assume any liability against the risks of deterioration and/or piracy with regard to User Content save to the extent resulting from its own negligent or wilful misconduct. The backup or saving of User Content is in any case the responsibility of single Users.</li>
		</ol>
	</li>
	<li><b>Suspension and deletion of User Content and/or user generated content and/or User registration</b>
		<ol>
			<li style="font-weight: 400;">The Company does not carry out any control and/or monitoring, nor is it obliged to give prior approval to the User Content entered on the App, and cannot therefore be held liable in any way for the User Content, nor for any errors and/or omissions contained therein, as well as for any direct and/or indirect damages suffered or incurred by Users and/or third parties from their publication on the App. Should, however, the Company become aware of the presence of User Content in violation, even suspected, of these Conditions of Use and/or should the User Content entered on the App be deemed unlawful or inappropriate in the Company sole discretion, the Company may suspend and/or delete all or part of such User Content, the Zweb Services and/or the User registration, or permanently prevent the User from using the Zweb Services of the App.</li>
			<li style="font-weight: 400;">In the event that the Company receives a report from a third party that refers to an alleged violation of rights (such as, by way of example and without limitation, industrial and intellectual property rights and/or personality rights) by one or more User Content, the Company reserves the right to promptly suspend and/or delete such User Content and/or Zweb Services and/or the registration of the relevant Users. Alternatively, the Company may, at its sole discretion, send the User concerned an e-mail notifying him/her of the notification, requesting that within 3 (three) days from the sending of the same the User remove the User Content reported, or provide adequate proof of the lawfulness of his/her use of the User Content reported and in any case of his/her rights thereto. Once this period has elapsed, the Company shall be entitled to suspend and/or delete the User Content or the Zweb Service and/or the User registration without delay, and to permanently exclude the User from using the Zweb Services.</li>
		</ol>
	</li>
	<li><b>Principles of conduct for the use of Zweb Services</b>
		<ol>
			<li style="font-weight: 400;">In using the Zweb Services, the User undertakes to comply with the general principles of good behavior commonly known as "Netiquette Rules" and to respect the following general rules of conduct:
			<ol class="bullet-point">
				<li style="font-weight: 400;">avoid posting identical messages in multiple comments (cross posting); </li>
				<li style="font-weight: 400;">do not conduct "opinion wars" or quarrels: if there are personal diatribes, it is better to resolve them privately among those concerned;</li>
				<li style="font-weight: 400;">give comments titles that are always relevant to the topic; </li>
				<li style="font-weight: 400;">in case of reply to another User's message, quote only the part strictly necessary for the comprehensibility of the reply. </li>
			</ol>
			</li>
			<li style="font-weight: 400;">The User is also expressly forbidden from publishing content:
				<ol class="bullet-point">
					<li style="font-weight: 400;">of e-mail messages, without the express consent of the sender and the recipient;</li>
					<li style="font-weight: 400;">which are pornographic, obscene or otherwise sexually explicit;</li>
					<li style="font-weight: 400;">which contain scenes of violence (physical or even moral) evident and/or gratuitous;</li>
					<li style="font-weight: 400;">which contain scenes of another type, in any case evident and/or gratuitous, of a disgusting or reprehensible nature, or in any case likely to offend the common sensibility;</li>
					<li style="font-weight: 400;">which are not suitable for minors; </li>
					<li style="font-weight: 400;">which may constitute an infringement of industrial or intellectual property rights (for example, trademarks) or of personality (for example, images or photographs of third parties);</li>
					<li style="font-weight: 400;">which may be considered blasphemous, defamatory or otherwise offensive to anyone, including, inter alia, expressions of fanaticism, racism, hatred, irreverence or threats; </li>
					<li style="font-weight: 400;">including information that is known to be false or misleading, or otherwise likely to prejudice the conduct of other Users; </li>
					<li style="font-weight: 400;">involving gambling initiatives, contests, games that require participation for valuable consideration; </li>
					<li style="font-weight: 400;">which concern advertising or sponsorship of any kind, or refer to offers for sale of any product or service, or have to do with commercial initiatives;</li>
					<li style="font-weight: 400;">which violate or induce to violate any provision of law or regulation protecting even only private individuals or an order legitimately issued by a public authority.</li>
				</ol>
			</li>
			<li style="font-weight: 400;">The User is also expressly forbidden from carrying out the following activities:
				<ol class="bullet-point">
					<li style="font-weight: 400;">in any way make public personal information, mobile phone numbers, e-mail, home address, surname, etc.;</li>
					<li style="font-weight: 400;">use software tools or other activities that interfere, or are likely to interfere, with any of the App's operational functions, for example because they contain viruses, malware or other harmful components;</li>
					<li style="font-weight: 400;">send unsolicited e-mails or threats via e-mail or other means to other Users;</li>
					<li style="font-weight: 400;">use for commercial purposes, in any form or manner, the Zweb Content available on the App, even in part, including User Content submitted by other Users;</li>
					<li style="font-weight: 400;">in any way violate the personal data and/or personality rights of third parties;</li>
					<li style="font-weight: 400;">promote or provide information that may in any way facilitate or in any way make possible illegal activities, or that may induce them to be carried out or that may in any way cause harm to third parties.</li>
				</ol>
			</li>
			<li style="font-weight: 400;">If, in the Company’s opinion, the User breaches point 8.2 or point 8.3 above, the Company may, at its election, immediately terminate these Conditions of Use, suspend and/or delete any User Content, and/or delete the registration of the User.</li>
		</ol>
	</li>
	<li><b>Liability</b>
		<ol>
			<li style="font-weight: 400;">The Company has taken all precautions in order to assure its Users that the Zweb Contents are accurate and do not contain incorrect or outdated information, with respect to the date of their publication on the App and, as far as possible, afterwards. However, the Company shall not assume any liability towards Users for the accuracy and completeness of the contents published by the Company on the App, without prejudice to its liability for fraud and negligence and unless otherwise provided by law.
			</li>
			<li style="font-weight: 400;">The User acknowledges and agrees that his/her use of the Zweb Services is under his/her sole responsibility.  To the extent permitted by law, the Company will not be responsible for: (a) losses that were not caused by the Company’s breach of these Conditions of Use; (b) any loss or damage that was not, at the time that these Conditions of Use were formed between the User and the Company, a reasonably foreseeable consequence of the Company breaching these Conditions of Use; or (c) business losses.</li>
			<li style="font-weight: 400;">The User undertakes not to use the App, the Zweb Content and the Zweb Services in ways that are not expressly permitted by the Conditions of Use, contrary to law or that violate the rights of third parties. The User shall be solely and solely responsible for the use of the Zweb Content and Zweb Services on the App. The Company shall not be liable for any use of the App, Zweb Content and Zweb Services by any of its Users that is not in accordance with applicable law, except for liability for wilful misconduct and gross negligence.
			</li>
			<li style="font-weight: 400;">The Company cannot guarantee its Users that the App and the Zweb Services operate continuously, without interruption and in the absence of errors or malfunctions due to the connection to the Internet. The Company will make every effort to ensure continuous access to its App and Zweb Services even though the dynamic nature of the Internet and its content may not allow the App to operate without suspensions, interruptions or discontinuity due to the need to update the App.
			</li>
			<li style="font-weight: 400;">hose who access the App are responsible for equipping themselves with suitable antivirus and firewall protections and release the Company from any liability for any damage caused by viruses. They also release the Company from any liability deriving from damaged files, errors, omissions, service interruptions, deletion of contents, problems connected to the network, providers or telephone and/or telematic connections, unauthorized access, data alterations, failure and/or malfunction of the User's electronic equipment.
			</li>
			<li style="font-weight: 400;">Nothing in these Conditions of Use shall exclude or restrict the liability of either party to the other for death or personal injury resulting from negligence or for fraudulent misrepresentation or in any other circumstances where liability may not be so limited under any applicable law.
			</li>
		</ol>
	</li>
	<li><b>Links to other websites</b>
		<ol>
			<li style="font-weight: 400;">The App may contain hyperlinks (the "links") to other websites that have no connection with the App or the Company. The Company does not control or monitor such websites and their contents and cannot therefore be held responsible for the contents of such websites and the rules adopted by them also with regard to privacy and the processing of the User's personal data during the User's navigation and use of the Zweb Services.</li>
		</ol>
	</li>
	<li><b>Privacy</b>
		<ol>
			<li style="font-weight: 400;">
				For information on the methods and purposes of processing the User's personal data, please carefully read the privacy policy of the Company.
			</li>
		</ol>
	</li>
	<li><b>Reporting of infringements</b>
		<ol>
			<li style="font-weight: 400;">The use of the Zweb Services is based on freedom of expression, trust, mutual respect, compliance with the rules of law and the Conditions of Use. </li>
			<li style="font-weight: 400;">Users who have identified any User Content that they deem illegal, inappropriate, offensive or vulgar are invited to promptly report it to the Company, by filling out the abuse report form available on the App, at the bottom of the home page or in the manner indicated in point 15 below. </li>
			<li style="font-weight: 400;">Reports shall be based on verifiable and identifiable elements. In the absence of these requirements the Company will not be able to proceed with the possible removal of the reported content from the App. </li>
			<li style="font-weight: 400;">The User is invited to use his/her reports responsibly. Reports that prove to be unfounded and/or illegitimate will be a possible source of liability for the User.</li>
		</ol>
	</li>
	<li><b>Modifications and updates to the Conditions of Use and/or Zweb Services</b>
		<ol>
			<li style="font-weight: 400;">
				The Company may modify the terms and conditions of the Conditions of Use at any time for justified reasons, such as improving existing functions or features or adding new functions or features, implementing advances in science and technology, making reasonable technical adjustments to the Zweb Services and ensuring its operability or security, or for legal or regulatory reasons, communicating the changes to Users by means of a general notice posted on the App or through a link to such notice. The use of the Zweb Services after the entry into force of such changes implies full acceptance of the changes by the User. If the User does not agree with the new terms and conditions of the Conditions of Use, he/she is requested not to use the Zweb Services and to proceed with deactivating his/her User registration.
			</li>
			<li style="font-weight: 400;">
				The Company also reserves the right to communicate by e-mail to Users any changes of a technical nature or updates or news regarding the Zweb Services available on the App. 			</li>
		</ol>
	</li>
	<li><b>Applicabile Law and jurisdiction</b>
		<ol>
			<li style="font-weight: 400;">
				These Conditions of Use and any non-contractual obligations arising out of or in connection with these Conditions of Use are governed by and construed in accordance with the laws of England and Wale. However, in the case of Users who have their habitual residence in the European Union, nothing in this point 14.1 excludes the additional protection provided by the mandatory rules of their country of residence. The User accepts, and the Company accepts in turn, to submit to the non-exclusive jurisdiction of the Courts of England and Wales. However, the User, as a consumer, may also take legal action before the aforesaid court or the court of the Member State of the European Union in which the same is resident or domiciled in order to bring a claim under or in relation to these Conditions of Use. 			</li>
		</ol>
	</li>
	<li><b>Further information and communications</b>
		<ol>
			<li style="font-weight: 400;">
				For any further information and communication as well as to report abuses referred to in point 12 above, the User may contact L Financial Limited by e-mail at the address Blane.Queripel@PraxisIFM.com.</li>
		</ol>
	</li>
</ol>
