<?php
/**
 * Template Name: Influencer Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use function ZwebTheme\Utility\adjust_brightness;
use function ZwebTheme\Utility\get_colors;

get_header();
?>
<div class="site-main site-main--pull-up">
	<section class="influencer-page-header">
		<div class="influencer-page-header__bg">
			<!-- TODO ALT replace with dynamic name -->
			<picture>
				<source media="(min-width: 768px)" srcset="<?php echo esc_attr(get_stylesheet_directory_uri(). '/assets/images/featured-influencer.jpg');?>" />
				<source srcset="<?php echo esc_attr(get_stylesheet_directory_uri(). '/assets/images/featured-influencer.jpg');?>" />
				<img src="<?php echo esc_attr(get_stylesheet_directory_uri(). '/assets/images/featured-influencer.jpg');?>" class="influencer-page-header__bg-img" alt="Name" />
			</picture>
		</div>
		<div class="influencer-page-header__overlay">
			<div class="container">

				<div class="influencer-page-header__share">
					<a class="share-button" href="#social-sharing" aria-controls="social-sharing">
						<svg class="share-button__icon" xmlns="http://www.w3.org/2000/svg" width="19.337" height="19.26"><path d="M15.83 12.246a3.5 3.5 0 00-2.6 1.151l-6.275-3.1a3.384 3.384 0 00.054-.759L13.5 6.131a3.507 3.507 0 10-1.18-2.623 3.553 3.553 0 00.027.427L6.042 7.247a3.507 3.507 0 10-.372 5.182l6.655 3.286v.037a3.507 3.507 0 103.506-3.507z" /></svg>
					</a>
				</div>

				<div class="influencer-page-header__content">
					<a class="influencer-page-header__category">Lifestyle</a>
					<h1 class="influencer-page-header__title">Camihawke sdf</h1>

					<!--
					<div class="influencer-page-header__share influencer-page-header__share--mobile">
						<a class="share-button" href="#social-sharing" aria-controls="social-sharing">
							<svg class="share-button__icon" xmlns="http://www.w3.org/2000/svg" width="19.337" height="19.26"><path d="M15.83 12.246a3.5 3.5 0 00-2.6 1.151l-6.275-3.1a3.384 3.384 0 00.054-.759L13.5 6.131a3.507 3.507 0 10-1.18-2.623 3.553 3.553 0 00.027.427L6.042 7.247a3.507 3.507 0 10-.372 5.182l6.655 3.286v.037a3.507 3.507 0 103.506-3.507z" /></svg>
						</a>
					</div-->

					<p>Hi! My name is John, I’m a creative geek from San Francisco, CA.
						Contact me at john@mail.com</p>

					<div class="influencer-page-header__details">
						<div class="influencer-page-header__detail">
							152K
							<span class="influencer-page-header__detail-label">Visualizzazioni</span>
						</div>
						<div class="influencer-page-header__detail">
							13
							<span class="influencer-page-header__detail-label">Z Video</span>
						</div>
						<div class="influencer-page-header__detail">
							4 Aprile
							<span class="influencer-page-header__detail-label">Next Video</span>
						</div>
					</div>

				</div>

				<a class="influencer-page-header__down" href="#latest-video">
					<svg class="influencer-page-header__down-icon" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="22" height="11">
						<use xlink:href="#angle-down"></use>
					</svg>
				</a>
			</div>
		</div>
	</section>

	<section id="latest-video">
		<div class="container">
			<div class="influencer-updates">
				<div class="influencer-updates__latest">
					<h2><?php echo esc_html( 'Novità di Camihawke',  'zweb-theme' ); ?></h2>
					<article class="video-card">
						<a href="#video" class="video-card__link" title="<?php echo esc_attr( 'Watch Video',  'zweb-theme' ); ?>">
							<div class="video-card__cover">
								<img class="video-card__image" src="<?php echo esc_attr(get_stylesheet_directory_uri() .'/assets/images/video-image.jpg'); ?>" alt="<?php echo esc_attr( 'Watch Video',  'zweb-theme' ); ?>">
								<div class="video-card__duration">
									<!-- TODO replace with dynamic time -->
									26:55
								</div>
							</div>
						</a>
						<div class="video-card__details">
							<a class="video-card__details-link" href="#author" title="Mariano Di Vano Profile">
								<!-- TODO ALT replace with dynamic name -->
								<img class="video-card__author-image" src="<?php echo esc_attr(get_stylesheet_directory_uri() . '/assets/images/profile-image.jpg'); ?>" itemprop="image" alt="Mariano Di Vano">
							</a>
							<h2 class="video-card__title">
								<a class="video-card__title-link" href="#video" title="Watch Video">
									<!-- TODO replace with dynamic name -->
									Testo di ingombro su due righe per video tutorial
								</a>
							</h2>
						</div>
						<div class="video-card__meta">
							<p class="video-card__author-name">
								<!-- TODO TITLE replace with dynamic name -->
								<a class="video-card__author-link" href="#author" title="Mariano Di Vano Profile">
									<!-- TODO ALT replace with dynamic name -->
									Mariano Di Vano
								</a>
							</p>
							<div class="video-card__views">
								<!-- TODO ALT replace with dynamic views -->
								1.435
							</div>
							<div class="video-card__added">
								<!-- TODO ALT replace with dynamic days ago -->
								3 giorni fa
							</div>
						</div>
					</article>
				</div>

				<div class="influencer-updates__next">
					<h2><?php echo esc_html( 'Next Video',  'zweb-theme' ); ?></h2>
					<?php get_template_part( 'partials/influencer', 'next' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'partials/carousel-video', 'page' ); ?>

</div>

<?php get_footer(); ?>
