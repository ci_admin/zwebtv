<?php
/**
 * Template Name: Video Landing Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Admin\Settings;
use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;

get_header();

if ( 'video' === get_queried_object()->post_name ) {
	$query = Query::get_video_carousel_query( 24 );
} else if ( 'top-10' === get_queried_object()->post_name ) {
	$query = Query::get_query_for_curated_lists( Settings::get_curated_section( 'top_10' ) );
} else if ( 'da-non-perdere' === get_queried_object()->post_name ) {
	$query = Query::get_query_for_curated_lists( Settings::get_curated_section( 'da_non_perdere' ) );
}

?>
<div class="site-main site-main--landing">
	<section class="video-landing-page">
		<div class="container">

			<h1><?php the_title(); ?></h1>

			<ul class="video-grid">
				<?php if ( $query->have_posts() ) : ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php set_query_var( 'video_id', get_the_id() ); ?>
						<li class="video-grid__item">
							<?php get_template_part( 'partials/video-card' ); ?>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php endif ?>
			</ul>
		</div>
		<?php
		// On video page, show both
		if ( 'top-10' !== get_queried_object()->post_name ) {
			if ( Settings::get_curated_section( 'top_10' ) ) {
				set_query_var(
					'video_carousel_config',
					[
						'query' => Query::get_query_for_curated_lists( Settings::get_curated_section( 'top_10' ) ),
						'label' => __( 'Top 10', 'zweb' ),
						'link'  => '/top-10',
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
		}

		if ( 'da-non-perdere' !== get_queried_object()->post_name ) {
			if ( Settings::get_curated_section( 'da_non_perdere' ) ) {
				set_query_var(
					'video_carousel_config',
					[
						'query' => Query::get_query_for_curated_lists( Settings::get_curated_section( 'da_non_perdere' ) ),
						'label' => __( 'Da Non Perdere', 'zweb' ),
						'link'  => '/da-non-perdere',
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
		}

		get_template_part( 'partials/carousel-video-categories' );

		set_query_var( 'influencer_carousel_config', [ 'query' => Influencer::get_influencer_carousel_query() ] );
		get_template_part( 'partials/carousel-influencer' );
		?>
	</section>
</div>

<?php get_footer(); ?>
