<?php
/**
 * Template Name: Next Landing Page
 *
 * @package ZwebTheme
 */

use Zweb\Builder\Query;

$query = Query::get_upcoming_video_query( 8 );

get_header();
?>
<div class="site-main site-main--landing">
	<section class="influencer-next-page">
		<div class="container">
			<h1><?php esc_html_e( 'Prossime uscite', 'zweb-theme' ); ?></h1>
			<?php if ( $query->have_posts() ) : ?>
				<ul class="influencer-next-grid__items">
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php
						try {
							set_query_var( 'influencer', \Zweb\Taxonomy\Influencer::get_influencer_for_video( get_the_ID() ) );
							set_query_var( 'video', get_post() );
						} catch ( \Exception $exception ) {
							continue;
						}
						?>
						<li class="influencer-next-grid__item">
							<?php get_template_part( 'partials/influencer', 'next' ); ?>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				</ul>
			<?php else : ?>
				<div class="no-results"><?php echo esc_html_e( 'No next videos at the moment', 'zweb-theme' ); ?></div>
			<?php endif ?>
		</div>
	</section>
</div>

<?php get_footer(); ?>
