<?php
/**
 * The template for displaying the footer.
 *
 * @package ZwebTheme
 */

?>

<footer class="site-footer">
	<div class="site-footer__logo">
		<a href="<?php echo esc_url( get_home_url() ); ?>"
		   class="site-footer__logo-link">
			<img class="site-footer__logo-image"
				 src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/logo-2021.svg"
				 alt="<?php echo esc_html( bloginfo( 'name' ) ); ?>"/>
		</a>
	</div>

	<?php if ( get_page_template_slug() !== 'templates/page-launch.php' ) : ?>
		<nav class="site-footer__nav">
			<?php
			wp_nav_menu(
				[
					'menu'       => 'footer-nav',
					'menu_class' => 'footer-nav',
					'menu_id'    => 'footer-nav',
				]
			);
			?>
		</nav>
	<?php endif; ?>

	<?php
	wp_nav_menu(
		[
			'menu'       => 'social-media-icons',
			'menu_class' => 'site-footer__social social-menu social-menu--subtle',
			'menu_id'    => 'footer-social-media-icons',
		]
	);
	?>

</footer>

<?php get_template_part( 'partials/social-sharing' ); ?>

<?php wp_footer(); ?>
</body>
</html>
