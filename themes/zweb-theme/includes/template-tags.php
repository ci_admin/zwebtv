<?php
/**
 * Custom template tags for this theme.
 *
 * This file is for custom template tags only and it should not contain
 * functions that will be used for filtering or adding an action.
 *
 * All functions should be prefixed with ZwebTheme in order to prevent
 * pollution of the global namespace and potential conflicts with functions
 * from plugins.
 * Example: `zweb_theme_function()`
 *
 * @package ZwebTheme\Template_Tags
 *
 */

// phpcs:ignoreFile
use Carbon\Carbon;
use Zweb\Builder\Query;

/**
 * Echo the src of the profile image for the influencer term.
 *
 * @param $term_id
 */
function the_influencer_square_image_src( $term_id, $size = 'thumbnail' ) {
	try {
		$user               = \Zweb\Taxonomy\Influencer::get_user_from_term( $term_id );
		$profile_picture_id = get_user_meta( $user->ID, \Zweb\Role\Influencer::META_KEY . '_profile_image', true );
		if ( $profile_picture_id ) {
			$src = wp_get_attachment_image_src( $profile_picture_id, $size );
			if ( $src ) {
				echo esc_url( $src[0] );

				return;
			}
		}

		echo esc_url( get_avatar_url( $user->ID ) );

		return;
	} catch ( \Exception $exception ) {
		echo esc_url( get_stylesheet_directory_uri() . '/assets/images/profile-image.jpg' );
	}
}

/**
 * Echo the src of the card image for the influencer term.
 *
 * @param $term_id
 */
function the_influencer_card_image_src( $term_id, $size = 'upcoming_influencer' ) {
	try {
		$user               = \Zweb\Taxonomy\Influencer::get_user_from_term( $term_id );
		$profile_picture_id = get_user_meta( $user->ID, \Zweb\Role\Influencer::META_KEY . '_influencer_card_image', true );
		if ( $profile_picture_id ) {
			$src = wp_get_attachment_image_src( $profile_picture_id, $size );
			if ( $src ) {
				echo esc_url( $src[0] );

				return;
			}
		}

		echo esc_url( get_avatar_url( $user->ID ) );

		return;
	} catch ( \Exception $exception ) {
		echo esc_url( get_stylesheet_directory_uri() . '/assets/images/influencer-card-image.jpg' );
	}
}

/**
 * Echo the src of the upcoming image for the influencer term.
 *
 * @param $term_id
 */
function the_influencer_upcoming_image_src( $term_id, $size = 'upcoming_influencer' ) {
	try {
		$user                = \Zweb\Taxonomy\Influencer::get_user_from_term( $term_id );
		$upcoming_picture_id = get_user_meta( $user->ID, \Zweb\Role\Influencer::META_KEY . '_influencer_upcoming_image', true );
		if ( $upcoming_picture_id ) {
			$src = wp_get_attachment_image_src( $upcoming_picture_id, $size );
			if ( $src ) {
				echo esc_url( $src[0] );

				return;
			}
		}

	} catch ( \Exception $exception ) {

	}
	echo esc_url( get_stylesheet_directory_uri() . '/assets/images/upcoming-influencer.jpg' );
}

/**
 * Echo the src of the profile image for the influencer term.
 *
 * @param $term_id
 */
function the_influencer_landing_page_image_src( $term_id, $context = 'landing', $is_live = false ) {
	try {
		$image_name 		= $is_live ? '_landing_page_image_live' : '_landing_page_image';
		$user               = \Zweb\Taxonomy\Influencer::get_user_from_term( $term_id );
		$profile_picture_id = get_user_meta( $user->ID, \Zweb\Role\Influencer::META_KEY . $image_name, true );
		if ( $profile_picture_id ) {
			switch ( $context ) {
				case 'next-video':
					$size = 'influencer_next_video';
					break;
				case 'featured-mobile':
					$size = 'square';
					break;
				default:
					$size = 'full';
					break;
			}
			$src = wp_get_attachment_image_src( $profile_picture_id, $size );
			if ( $src ) {
				echo esc_url( $src[0] );

				return;
			}
		}

	} catch ( \Exception $exception ) {
	}
	if ( 'landing' === $context ) {
		echo esc_url( get_stylesheet_directory_uri() . '/assets/images/featured-influencer.jpg' );
	} elseif ( 'next-video' === $context ) {
		echo esc_url( get_stylesheet_directory_uri() . '/assets/images/next-video-image.jpg' );
	}
}

/**
 * Echo the src of the video category image
 *
 * @param $term_id
 * @param string $image_size
 */
function the_video_category_image_src( $term_id, $image_size = 'square' ) {
	$video_category_picture_id = get_term_meta( $term_id, \Zweb\Taxonomy\VideoCategory::META_KEY_IMAGE . '_id', true );
	if ( $video_category_picture_id ) {
		$src = wp_get_attachment_image_src( $video_category_picture_id, $image_size );
		if ( $src ) {
			echo esc_url( $src[0] );

			return;
		}
	}
	$default_image = get_stylesheet_directory_uri() . '/assets/images/video-category-square.jpg';
	if ( 'video_category_mobile' === $image_size ) {
		$default_image = get_stylesheet_directory_uri() . '/assets/images/video-category-mobile.jpg';
	}
	echo esc_url( $default_image );
}

/**
 * Get the landing page image for the Video Category, used on the taxonomy
 * archive page.
 *
 * @param $term_id
 */
function the_video_category_large_image_src( $term_id ) {
	$video_category_picture_id = get_term_meta( $term_id, 'landing_page_image_id', true );
	if ( $video_category_picture_id ) {
		$src = wp_get_attachment_image_src( $video_category_picture_id, 'full' );
		if ( $src ) {
			echo esc_url( $src[0] );

			return;
		}
	}

	echo esc_url( get_stylesheet_directory_uri() . '/assets/images/featured-influencer.jpg' );
}

/**
 * Get the date translated.
 *
 * @param $date
 * @param string $format
 * @param string $locale
 *
 * @throws \Exception
 */
function the_translated_date( $date, $format = '%e %B', $locale = 'it_IT' ) {
	$current_locale = get_locale();
	setlocale( LC_TIME, $locale );
	echo esc_html(
		Carbon::parse( $date )
			  ->locale( $locale )
			  ->formatLocalized( $format )
	);
	setlocale( LC_TIME, $current_locale );
}

/**
 * Generate a unique uuid used to avoid clashes if a video appears multiple times on a page.
 *
 * @link https://stackoverflow.com/a/15875555/397861
 * @return string
 * @throws \Exception
 */
function guidv4() {
	$data    = random_bytes( 16 );
	$data[6] = chr( ord( $data[6] ) & 0x0f | 0x40 ); // set version to 0100
	$data[8] = chr( ord( $data[8] ) & 0x3f | 0x80 ); // set bits 6-7 to 10

	return vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split( bin2hex( $data ), 4 ) );
}

/*
 * Echo the carousel with the latest videos from Studio Roma
 */
function the_studio_roma_carousel() {
	$studio_roma = get_page_by_title( 'Studio Roma', OBJECT, \Zweb\PostType\Studio::POST_TYPE_NAME );
	if ( $studio_roma instanceof WP_Post ) {
		set_query_var(
			'video_carousel_config',
			[
				'query'        => Query::get_studio_carousel_video_query( $studio_roma->ID, 8, 0 ),
				'label'        => __( 'Studio Roma', 'zweb' ),
				'link'         => get_the_permalink( $studio_roma ),
				'studio_id'    => $studio_roma->ID,
				'relationship' => 'studio',
			]
		);
		get_template_part( 'partials/carousel-video' );
	}
}

/**
 * Output the video duration
 *
 * @param $video_id
 */
function the_video_duration( $video_id ) {
	$duration = get_post_meta( $video_id, 'bc_duration', true ) / 1000;
	$format   = 'i:s';
	if ( $duration >= HOUR_IN_SECONDS ) {
		$format = 'G:i:s';
	}
	echo esc_html( gmdate( $format, $duration ) );
}
