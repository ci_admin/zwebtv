/**
 * Gutenberg block-specific JavaScript:
 * used in editor only
 */

import './post-status-info';
import './set-scheduled-time';
