const { registerPlugin } = wp.plugins;
const { PluginPostStatusInfo } = wp.editPost;

const PluginPostStatusInfoTest = () => {
	const meta = wp.data.select('core/editor').getEditedPostAttribute('meta');
	if (!meta.bc_video_is_processing) {
		return null;
	}
	return (
		<PluginPostStatusInfo>
			<p>
				<strong>
					The video is still being processed on Brightcove, and it will appear on the site
					as soon as Brightcove finishes processing
				</strong>
			</p>
		</PluginPostStatusInfo>
	);
};

registerPlugin('post-status-info-test', { render: PluginPostStatusInfoTest });
