const { registerPlugin } = wp.plugins;
const { PluginDocumentSettingPanel } = wp.editPost;
const {
	data: { useSelect, useDispatch },
	date: { dateI18n },
	element: { useState, useEffect },
	components: { DateTimePicker, Popover, Button, PanelRow },
} = wp;

const SetScheduledTime = () => {
	const { postType } = useSelect((select) => ({
		postType: select('core/editor').getCurrentPostType() || '',
	}));

	const [openDatePopup, setOpenDatePopup] = useState(false);
	const {
		meta,
		meta: { scheduledTime },
	} = useSelect((select) => ({
		meta: select('core/editor').getEditedPostAttribute('meta') || {},
	}));

	const { editPost } = useDispatch('core/editor');

	const [myData, setMyData] = useState(scheduledTime);

	useEffect(() => {
		editPost({
			meta: {
				...meta,
				scheduledTime: myData,
			},
		});
		// eslint-disable-next-line
	}, [myData]);

	if (postType !== 'zweb-live-battle') {
		return null;
	}
	return (
		<PluginDocumentSettingPanel
			className="my-document-setting-plugin"
			title="Live Battle Scheduled time"
		>
			<PanelRow>
				<span>Live battle start time</span>
				<div className="components-dropdown">
					<Button isLink onClick={() => setOpenDatePopup(!openDatePopup)}>
						{myData ? dateI18n('F j, Y g:i a', myData) : 'Pick Date & Time'}
					</Button>
					{openDatePopup && (
						<Popover
							position="bottom"
							onClose={() => {
								setOpenDatePopup(false);
							}}
						>
							<DateTimePicker
								label="My Date/Time Picker"
								currentDate={myData}
								onChange={(date) => {
									setMyData(Date.parse(date));
								}}
								is12Hour
							/>
						</Popover>
					)}
				</div>
			</PanelRow>
		</PluginDocumentSettingPanel>
	);
};

registerPlugin('set-scheduled-time', { render: SetScheduledTime, icon: null });
