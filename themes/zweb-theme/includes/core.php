<?php
/**
 * Core setup, site hooks and filters.
 *
 * @package ZwebTheme\Core
 */

namespace ZwebTheme\Core;

use Zweb\Admin\Settings;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @return void
 */
function setup() {
	$n = function ( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action( 'after_setup_theme', $n( 'i18n' ) );
	add_action( 'after_setup_theme', $n( 'theme_setup' ) );
	add_action( 'after_setup_theme', $n( 'setup_image_sizes' ) );
	add_action( 'wp_enqueue_scripts', $n( 'scripts' ) );
	add_action( 'wp_enqueue_scripts', $n( 'styles' ) );
	add_action( 'wp_head', $n( 'js_detection' ), 0 );
	add_action( 'wp_head', $n( 'add_manifest' ), 10 );
	add_action( 'wp_head', $n( 'add_gtm' ), 10 );
	add_action( 'wp_body_open', $n( 'add_gtm_no_script' ), 10 );

	add_filter( 'gdpr_privacy_bar_button_text', $n( 'privacy_bar_button_text' ) );
	add_filter( 'script_loader_tag', $n( 'script_loader_tag' ), 10, 2 );

	remove_filter( 'the_excerpt', 'wpautop' );
}

/**
 * Makes Theme available for translation.
 *
 * Translations can be added to the /languages directory.
 * If you're building a theme based on "zweb-theme", change the
 * filename of '/languages/ZwebTheme.pot' to the name of your project.
 *
 * @return void
 */
function i18n() {
	load_theme_textdomain( 'zweb-theme', ZWEB_THEME_PATH . '/languages' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function theme_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support(
		'html5',
		[
			'search-form',
			'gallery',
		]
	);

	// This theme uses wp_nav_menu() in three locations.
	register_nav_menus(
		[
			'primary' => esc_html__( 'Primary Menu', 'zweb-theme' ),
		]
	);
}

/**
 * Get script modified time
 *
 * @param string $script the script.
 *
 * @return false|int
 */
function script_file_modified_time( $script ) {
	return filemtime( ZWEB_THEME_PATH . "dist/js/${script}.js" );
}

/**
 * Get style modified time
 *
 * @param string $stylesheet The stylesheet
 *
 * @return false|int
 */
function style_file_modified_time( $stylesheet ) {
	return filemtime( ZWEB_THEME_PATH . "dist/css/${stylesheet}.css" );
}

/**
 * Enqueue scripts for front-end.
 *
 * @return void
 */
function scripts() {

	wp_enqueue_script(
		'addthis',
		'//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ec63cca3fbf561f',
		[],
		ZWEB_THEME_VERSION
	);

	wp_enqueue_script(
		'polyfill',
		'//cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js',
		[],
		ZWEB_THEME_VERSION,
		false
	);

	wp_enqueue_script(
		'frontend',
		ZWEB_THEME_TEMPLATE_URL . '/dist/js/frontend.js',
		[],
		script_file_modified_time( 'frontend' ),
		true
	);

	wp_localize_script(
		'frontend',
		'zweb',
		[
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'state' => Settings::get_current_application_state(),
		]
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_script(
			'styleguide',
			ZWEB_THEME_TEMPLATE_URL . '/dist/js/styleguide.js',
			[],
			script_file_modified_time( 'styleguide' ),
			true
		);
	}

}

/**
 * Enqueue styles for front-end.
 *
 * @return void
 */
function styles() {

	wp_enqueue_style(
		'styles',
		ZWEB_THEME_TEMPLATE_URL . '/dist/css/style.css',
		[],
		style_file_modified_time( 'style' )
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_style(
			'styleguide',
			ZWEB_THEME_TEMPLATE_URL . '/dist/css/styleguide-style.css',
			[],
			style_file_modified_time( 'styleguide-style' )
		);
	}
}

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @return void
 */
function js_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

/**
 * Add async/defer attributes to enqueued scripts that have the specified
 * script_execution flag.
 *
 * @link https://core.trac.wordpress.org/ticket/12009
 *
 * @param string $tag The script tag.
 * @param string $handle The script handle.
 *
 * @return string
 */
function script_loader_tag( $tag, $handle ) {
	$script_execution = wp_scripts()->get_data( $handle, 'script_execution' );

	if ( ! $script_execution ) {
		return $tag;
	}

	if ( 'async' !== $script_execution && 'defer' !== $script_execution ) {
		return $tag;
	}

	// Abort adding async/defer for scripts that have this script as a dependency. _doing_it_wrong()?
	foreach ( wp_scripts()->registered as $script ) {
		if ( in_array( $handle, $script->deps, true ) ) {
			return $tag;
		}
	}

	// Add the attribute if it hasn't already been added.
	if ( ! preg_match( ":\s$script_execution(=|>|\s):", $tag ) ) {
		$tag = preg_replace( ':(?=></script>):', " $script_execution", $tag, 1 );
	}

	return $tag;
}

/**
 * Appends a link tag used to add a manifest.json to the head
 *
 * @return void
 */
function add_manifest() {
	echo "<link rel='manifest' href='" . esc_url( ZWEB_THEME_TEMPLATE_URL . '/manifest.json' ) . "' />";
}

/**
 * Add Google tag manager script
 */
function add_gtm() {
	?>
	<script>
		window.getCookie = function(name) {
			var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
			if (match) return match[2];
		}
		const cookiePolicyAccepted = !!window.getCookie('gdpr%5Bprivacy_bar%5D');
		window.dataLayer = [{
			cookiePolicyAccepted:  cookiePolicyAccepted
		}];
	</script>
	<!-- Google Tag Manager -->
	<script>(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-T7ZM729');</script>
	<!-- End Google Tag Manager -->
	<?php
}

/**
 * Add Google tag manager ifram
 */
function add_gtm_no_script() {
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T7ZM729"
				height="0" width="0"
				style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
}

/**
 * Change cookie consent banner button text.
 *
 * @param string $button_text Default button text
 *
 * @return string $button_text
 */
function privacy_bar_button_text( $button_text ) {
	return __( 'Accetta', 'zweb-theme' );
}

/**
 * Adds image sizes
 *
 * @return void
 */
function setup_image_sizes() {
	add_image_size( 'large_banner', 1800, 872, [ 'center', 'center' ] );
	add_image_size( 'square', 400, 400, [ 'center', 'center' ] );
	add_image_size( 'video_thumb', 500, 281, [ 'center', 'center' ] );
	add_image_size( 'video_category_mobile', 300, 450, [ 'center', 'center' ] );
	add_image_size( 'upcoming_influencer', 300, 410, [ 'center', 'center' ] );
	add_image_size( 'influencer_next_video', 674, 386, [ 'center', 'center' ] );
}
