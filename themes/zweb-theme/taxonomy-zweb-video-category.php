<?php
/**
 * Template Name: Video Category Page
 *
 * @package ZwebTheme
 */

namespace ZwebTheme\Utility;

use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;

get_header();
?>
	<div class="site-main site-main--pull-up">
		<section class="video-category-page-header">
			<div class="video-category-page-header__bg">
				<picture>
					<source media="(min-width: 768px)"
							data-srcset="<?php the_video_category_large_image_src( get_queried_object()->term_id ); ?>"/>
					<source data-srcset="<?php the_video_category_large_image_src( get_queried_object()->term_id ); ?>"/>
					<img  src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/images/placeholder.gif"
						  data-src="<?php the_video_category_large_image_src( get_queried_object()->term_id ); ?>"
						 class="video-category-page-header__bg-img lazy"
						 alt="<?php echo esc_attr( get_queried_object()->name ); ?>"/>
				</picture>
			</div>
			<div class="video-category-page-header__overlay">
				<div class="container">
					<div class="video-category-page-header__content">
						<h1 class="video-category-page-header__title">
							<?php echo esc_html( get_queried_object()->name ); ?>
						</h1>

					</div>
					<a class="video-category-page-header__down"
					   href="#latest-videos">
						<svg class="video-category-page-header__down-icon"
							 data-name="Layer 1"
							 xmlns="http://www.w3.org/2000/svg" width="22"
							 height="11">
							<use xlink:href="#angle-down"></use>
						</svg>
					</a>
				</div>
			</div>
		</section>

		<div id="latest-videos">
			<?php
			set_query_var(
				'video_carousel_config',
				[
					'query'                  => Query::get_video_carousel_query(
						8,
						null,
						null,
						get_queried_object()
					),
					'video_category_term_id' => get_queried_object()->term_id,
					'label'                  => sprintf( __( 'Novità in "%s"', 'zweb-theme' ), get_queried_object()->name ),
				]
			);
			get_template_part( 'partials/carousel-video' );
			?>

			<?php
			$top_10_videos = get_term_meta( get_queried_object()->term_id, 'zweb-topten-videos_ids', true );
			if ( $top_10_videos ) {
				set_query_var(
					'video_carousel_config',
					[
						'query' => Query::get_query_for_curated_lists(
							$top_10_videos
						),
						'label' => sprintf( __( 'Top 10 in "%s"', 'zweb-theme' ), get_queried_object()->name ),
					]
				);
				get_template_part( 'partials/carousel-video' );
			}
			?>

		</div>

		<?php
		set_query_var( 'influencer_carousel_config', [ 'query' => Influencer::get_influencer_carousel_query() ] );
		get_template_part( 'partials/carousel-influencer' );
		?>

	</div>

	<?php
	get_footer();
