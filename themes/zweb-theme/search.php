<?php
/**
 * The template for displaying search results pages.
 *
 * @package ZwebTheme
 */

use Zweb\Builder\Query;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Influencer;

get_header(); ?>

	<div class="site-main site-main--landing">


			<div class="search-page-form">
				<div class="container">
					<?php echo get_search_form(); ?>
				</div>
			</div>

			<div class="search-results">
				<?php
				set_query_var(
					'video_carousel_config',
					[
						'query'        => Query::get_video_carousel_query( 8, null, null, null, 0, get_search_query() ),
						'label'        => __( 'Video', 'zweb' ),
						'search'       => get_search_query(),
						'relationship' => 'search',
					]
				);
				?>
				<?php get_template_part( 'partials/carousel-video', 'page' ); ?>
			</div>

			<div class="search-results">
				<?php get_template_part( 'partials/carousel-video-categories', 'page' ); ?>
			</div>

			<div class="search-results">
				<?php
				set_query_var( 'influencer_carousel_config', [ 'query' => Influencer::get_influencer_carousel_query( get_search_query() ) ] );
				get_template_part( 'partials/carousel-influencer' );
				?>
			</div>

			<div class="search-results">
				<?php get_template_part( 'partials/carousel', 'upcoming-videos' ); ?>
			</div>

	</div>

<?php
get_footer();
