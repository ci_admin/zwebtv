#!/bin/bash
composer install --no-dev -o

# Build the theme
pushd themes/zweb-theme || exit 1
# rm -rf node_modules/
# npm install
npm run build-release 
# composer install --no-dev -o
popd || exit 1

# Build the mu-plugin
pushd mu-plugins/zweb || exit 1
# composer install
# npm install
#grunt
npm run build-release
popd || exit 1

# Build the wp-grapql-cache
pushd mu-plugins/wp-graphql-cache || exit 1
npm install
composer install --no-dev -o
#npm run build-release
popd || exit 1

# Stop printing commands to screen
set +x