#!/bin/bash

# Print commands to the screen
set -x

# Catch Errors
set -euo pipefail
pwd

# Clone the WPE Git Repo to a temp folder
git clone git@git.wpengine.com:production/zwebtvdev.git /tmp/temprepo

# Ensure wp-content already exists in the repo, since WP Engine repos start at the siteroot
mkdir -p /tmp/temprepo/wp-content/

# Copy over the repo files to the git repo
pwd
ls -a
rsync -vrxc --delete ./ /tmp/temprepo/wp-content/ --exclude-from=$Repo_path/deploy-scripts/rsync-excludes.txt


pushd /tmp/temprepo || exit 1

# Add all files
git add .

# Output log of what changed, for debugging deploys
git status

# Commit Files
git commit -m "Deploying"

# Deploy
git push origin master

popd || exit 1

# Stop printing commands to the screen
set +x
