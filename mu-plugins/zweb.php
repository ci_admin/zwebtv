<?php
/**
 * Plugin Name: Zweb
 * Plugin URI:
 * Description: Custom functionality for the ZWEB plugin.
 * Version:     0.1.0
 * Author:      10up
 * Author URI:  https://10up.com
 * Text Domain: zweb
 * Domain Path: /languages
 *
 * @package Zweb
 */

$deps = array(
	__DIR__ . '/fieldmanager/fieldmanager.php',
	__DIR__ . '/content-types/10up-content-types.php',
	__DIR__ . '/zweb/plugin.php',
);

foreach ( $deps as $dep ) {
	if ( file_exists( $dep ) ) {
		// Ensure mu-plugins subdirectories can be symlinked.
		wp_register_plugin_realpath( $dep );
		require_once $dep;
	}
}
require_once __DIR__ . '/wp-graphql-cache/plugin.php';
