jQuery( function ( $ ) {

	var $custom_select2_template = {},
		custom_results = {},
		custom_list_ids = {},
		custom_rows = 0,
		custom_modal = null;

	/**
	 * Initialize and add event handlers to a select2 field.
	 */
	function init_select2( element ) {
		var $custom_select2 = $( element ),
			$el = $( 'input.fm-element', $custom_select2 ),
			item_data = $el.data( 'items' ),
			custom_list = parseInt( $el.data( 'custom-list' ) ),
			is_multiple = parseInt( $el.data( 'multiple' ) ),
			allow_clear = parseInt( $el.data( 'allow-clear' ) ),
			show_reorder = parseInt( $el.data( 'reorder' ) ),
			min_length = parseInt( $el.data( 'min-length' ) ),
			select_limit = parseInt( $el.data( 'select-limit' ) ),
			item_json = $el.data( 'item-json' ),
			field_class = $el.data( 'field-class' ),
			fm_id = $custom_select2.attr( 'id' ),
			$custom_list = $( '.tenup-content-types-select2-list', $el.parent() ),
			select2_type = $el.data( 'select2-type' );

		if ( 1 == custom_list && 1 == show_reorder ) {
			// Make list sortable
			$( '.tenup-content-types-select2-items', $custom_list ).sortable( {
				containment: 'parent',
				axis: 'y',
				scrollSensitivity: 40,
				tolerance: 'pointer',
                cursor: 'move',
				opacity: 0.6
			} );
		}

		// Set empty list
		custom_results[ fm_id ] = [];
		custom_list_ids[ fm_id ] = [];

		var el_select2_action = $el.data( 'action' ),
			el_select2_options = {
				width: '100%',
				multiple: false,
				placeholder: 'Search to add an item..',
				formatResult: function( object ) {
					custom_results[ fm_id ][ parseInt( object.id ) ] = object;

					return object.label;
				},
				formatSelection: function( object ) {
					return object.label;
				}
			};

		if ( 0 < min_length ) {
			el_select2_options.minimumInputLength = min_length;
		}

		if ( 'undefined' != typeof el_select2_action && el_select2_action ) {
			el_select2_options.ajax = {
				url: ajaxurl + '?action=' + $el.data( 'action' ),
				type: 'post',
				dataType: 'json',
				quietMillis: 250,
				data: function( term, page ) {
					return {
						fm_autocomplete_search: term,
						fm_search_nonce: $el.data( 'nonce' ),
						fm_context: $el.data( 'context' ),
						fm_subcontext: $el.data( 'subcontext' ),
						fm_name: $el.attr( 'name' )
					};
				},
				results: function( data, page ) {
					return { results: data };
				},
				cache: true
			};
		} else {
			el_select2_options.data = item_data;
		}

		if ( 1 != custom_list ) {
			el_select2_options.initSelection = function ( element, callback ) {
				var values = $( element ).val().split( ',' ),
					key = '',
					new_data = [];

				for ( key in values ) {
					if ( item_data.hasOwnProperty( key ) ) {
						new_data.push( item_data[key] );
					}
				}

				if ( 1 == is_multiple ) {
					callback( new_data );
				} else if ( 0 < new_data.length ) {
					callback( new_data.pop() );
				} else {
					callback();
				}
			};

			if ( 1 == allow_clear ) {
				el_select2_options.allowClear = true;
			}
		}

		if ( 1 == is_multiple ) {
			// Set max selection size
			if ( 1 != custom_list ) {
				el_select2_options.multiple = true;

				if ( 1 < select_limit ) {
					el_select2_options.maximumSelectionSize = select_limit;
				}
			}
		}

		function init_list() {

			var object_key = '',
				object = {},
				index = -1,
				list_ids = custom_list_ids[ fm_id ];

			for ( object_key in item_data ) {
				if ( ! item_data.hasOwnProperty( object_key ) ) {
					continue;
				}

				object = item_data[ object_key ];
				object_id = parseInt( object.id );

				if ( 1 == custom_list ) {
					index = list_ids.indexOf( object_id );

					if ( -1 === index || ( 0 == object_id && 'multi_post_link' == field_class ) ) {
						if ( 0 < select_limit ) {
							if ( select_limit == ( list_ids.length + 1 ) ) {
								$el.select2( 'disable' );
							} else if ( select_limit <= list_ids.length ) {
								$( '.tenup-content-types-select2-item-remove', $custom_list ).last().click();
							}
						}

						list_ids.push( object_id );
						custom_rows++;

						object.row = custom_rows;

						send_values_to_custom_list( object, $custom_list, select2_type );
					}
				}
			}

			if ( 1 == custom_list ) {
				// Send back nothing
				$el.select2( 'val', '' );
			}

		}

		if ( 1 == custom_list ) {
			init_list();
		}

		$el.select2( el_select2_options );

		if ( 1 == custom_list ) {
			$el.on( 'change', function( e ) {

				// Don't select anything
				e.preventDefault();

				var object = custom_results[ fm_id ][ parseInt( e.val ) ],
					object_id = parseInt( object.id ),
					list_ids = custom_list_ids[ fm_id ],
					index = list_ids.indexOf( object_id );

				if ( -1 === index ) {
					// Replace items
					if ( 0 < select_limit ) {
						if ( select_limit == ( list_ids.length + 1 ) ) {
							$el.select2( 'disable' );
						} else if ( select_limit <= list_ids.length ) {
							$( '.tenup-content-types-select2-item-remove', $custom_list ).last().click();
						}
					}

					list_ids.push( object_id );
					custom_rows++;

					object.row = custom_rows;

					send_values_to_custom_list( object, $custom_list, select2_type );
				}

				// Reset value
				$el.select2( 'data', null );

			} );

			$custom_select2.on( 'click', '.tenup-content-types-select2-item-remove', function( e ) {

				e.preventDefault();

				var $this = $( this ),
					value = $( 'input.tenup-content-types-select2-item-id', $this.closest( '.tenup-content-types-select2-item' ) ).val(),
					list_ids = custom_list_ids[ fm_id ],
					index = list_ids.indexOf( parseInt( value ) );

				if ( -1 < index ) {
					list_ids.splice( index, 1 );
				}

				$( this ).closest( '.tenup-content-types-select2-item' ).remove();

				if ( 0 == list_ids.length ) {
					$custom_list.addClass( 'hidden' );
				}

				$el.select2( 'enable' );

			} );

			$custom_select2.on( 'click', '.tenup-content-types-select2-item-add', function( e ) {

				e.preventDefault();

				var list_ids = custom_list_ids[ fm_id ],
					object = item_json;

				object.id = 0;

				// Replace items
				if ( 0 < select_limit ) {
					if ( select_limit == ( list_ids.length + 1 ) ) {
						$el.select2( 'disable' );
					} else if ( select_limit <= list_ids.length ) {
						$( '.tenup-content-types-select2-item-remove', $custom_list ).last().click();
					}
				}

				list_ids.push( object.id );
				custom_rows++;

				object.row = custom_rows;
				object.is_custom = true;

				send_values_to_custom_list( object, $custom_list, select2_type );

			} );

			$custom_select2.on( 'click', '.tenup-content-types-select2-item-add-thumbnail', function( e ) {

				e.preventDefault();

				var $this = $( this ),
					$thumbnail_wrapper = $this.parent();

				if ( null === custom_modal ) {
					custom_modal = wp.media( {
						title: 'Upload an image',
						library: { type: 'image' },
						multiple: false,
						button: { text: 'Set thumbnail' }
					} );

					custom_modal.on( 'select', function() {

						var image = custom_modal.state().get( 'selection' ).first(),
							$thumbnail_wrapper = custom_modal.custom_active_thumb,
							$alt_button = $( '.tenup-content-types-select2-item-remove-thumbnail', $thumbnail_wrapper ),
							$thumbnail = $( 'img', $thumbnail_wrapper ),
							$thumbnail_id = $( '.tenup-content-types-select2-item-thumbnail-id', $thumbnail_wrapper );

						$alt_button.removeClass( 'hidden' );
						$this.addClass( 'hidden' );

						$thumbnail_id.val( image.id );

						$thumbnail.attr( 'src', image.attributes.url );
						$thumbnail.removeClass( 'hidden' );

					} );
				}

				custom_modal.custom_active_thumb = $thumbnail_wrapper;

				custom_modal.open();
				custom_modal.content.mode( 'upload' );

			} );

			$custom_select2.on( 'click', '.tenup-content-types-select2-item-remove-thumbnail', function( e ) {

				e.preventDefault();

				var $this = $( this ),
					$thumbnail_wrapper = $this.parent(),
					$alt_button = $( '.tenup-content-types-select2-item-add-thumbnail', $thumbnail_wrapper ),
					$thumbnail_id = $( '.tenup-content-types-select2-item-thumbnail-id', $thumbnail_wrapper ),
					$thumbnail = $( 'img', $thumbnail_wrapper );

				$alt_button.removeClass( 'hidden' );
				$this.addClass( 'hidden' );

				$thumbnail_id.val( 0 );
				$thumbnail.attr( 'src', '' );
				$thumbnail.addClass( 'hidden' );

			} );
		}
	}

	// Only proceed to initialize if a select2 is found.
	if ( ! $( '.tenup-content-types-select2' ).length ) {
		return;
	}

	$( '.tenup-content-types-select2' ).each( function( index, element ) {
		// If this is a prototype field (used to clone) in repeatable groups, do not initialize.
		if ( null === this.closest('.fmjs-proto') ) {
			init_select2( this );
		}
	} );

	// Event triggered when the add new button is clicked on a repeatable group.
	$(document).on( 'fm_added_element', function( e, el ) {
		var newGroup = e.target,
			elements = newGroup.querySelectorAll( '.tenup-content-types-select2' ),
			elementsLength = elements.length;

		// Initialize the new select2 fields.
		for ( var i = 0; i < elementsLength; i++ ) {
			init_select2( elements[i] );
		}
	});

	function send_values_to_custom_list( object, $custom_list, select2_type ) {

		if ( ! $custom_select2_template.hasOwnProperty( select2_type ) ) {
			$custom_select2_template[ select2_type ] = wp.template( 'tenup-content-types-select2-item-' + select2_type );
		}

		// Send to the list
		$( '.tenup-content-types-select2-items', $custom_list ).append( $custom_select2_template[ select2_type ]( object ) );

		$custom_list.removeClass( 'hidden' );

	}

} );