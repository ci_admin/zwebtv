jQuery( function ( $ ) {

	$( document ).on( 'change', '.display-if-enhanced-trigger', function() {

		var $trigger = $( this ),
			val,
			name = $trigger.attr( 'name' );

		if ( $trigger.is( ':checkbox' ) ) {
			if ( $trigger.is( ':checked' ) ) {
				val = $trigger.val();
			}
			else {
				val = $trigger.siblings( 'input[type=hidden][name="' + name + '"]' ).val();
			}
		}
		else {
			if ( $trigger.is( ':radio' ) ) {
				val = $trigger.filter( ':checked' ).val();
			}
			else {
				val = $trigger.val().split( ',' );
			}
		}

		$( '.display-if-enhanced' ).each( function() {

			var condition_key = '',
				condition = {},
				$this = $( this ),
				$fm_wrapper = $this.closest( '.fm-wrapper' ),
				conditions = $this.data( 'display-if-enhanced' ),
				condition_values;

			if ( 'undefined' == typeof conditions ) {
				conditions = $( '.fm-element', $this ).data( 'display-if-enhanced' );
			}

			if ( 'undefined' == typeof conditions ) {
				return;
			}

			for ( condition_key in conditions ) {
				if ( ! conditions.hasOwnProperty( condition_key ) ) {
					continue;
				}

				condition = conditions[ condition_key ];

				if ( $trigger.is( 'form ' + condition.src ) ) {
					condition_values = condition.value;

					if ( 'object' != typeof condition_values ) {
						try {
							condition_values = condition_values.split( ',' );
						}
						catch ( e ) {
							// If jQuery already converted string to number.
							condition_values = [condition_values];
						}
					}

					if ( match_value( condition_values, val ) ) {
						$fm_wrapper.show();
					} else {
						$fm_wrapper.hide();
					}

					$this.trigger( 'fm_displayif_toggle' );
				}
			}

		} );

	} );

	// Initializes triggers to conditionally hide or show fields
	$( '.display-if-enhanced' ).each( function() {

		var condition_key = '',
			condition = {},
			$this = $( this ),
			conditions = $this.data( 'display-if-enhanced' ),
			$src;

		if ( 'undefined' == typeof conditions ) {
			conditions = $( '.fm-element', $this ).data( 'display-if-enhanced' );
		}

		if ( 'undefined' == typeof conditions ) {
			return;
		}

		for ( condition_key in conditions ) {
			if ( ! conditions.hasOwnProperty( condition_key ) ) {
				continue;
			}

			condition = conditions[ condition_key ];

			$src = $( 'form ' + condition.src );

			$src.addClass( 'display-if-enhanced-trigger' );
			$src.trigger( 'change' );
		}

	} );

	function match_value( values, match_value ) {

		var index = '',
			value = '';

		for ( index in values ) {
			if ( ! values.hasOwnProperty( index ) ) {
				continue;
			}

			value = values[ index ];

			if ( value == match_value ) {
				return true;
			}
		}

		return false;

	}

    // Trigger the checked radio button to show the enhanced fields on page load.
    var enhanced_trigger = $('.display-if-enhanced-trigger:checked');
    if ( enhanced_trigger ) {
        enhanced_trigger.trigger( 'change' );
    }

} );