<?php
/*
Plugin Name: 10up Content Types
Plugin URI:  http://10up.com
Description: Abstraction classes for Post Type and Taxonomy registration. Integrates with Fieldmanager for custom fields on Post Types, Taxonomies, and Users.
Version:     0.4.2
Author:      10up
Author URI:  http://10up.com
Text Domain: 10up-content-types
*/

define( 'TENUP_CONTENT_TYPES_VERSION', '0.4.2' );
define( 'TENUP_CONTENT_TYPES_DIR', plugin_dir_path( __FILE__ ) );
define( 'TENUP_CONTENT_TYPES_URL', plugin_dir_url( __FILE__ ) );

/**
 * Include files as necessary
 *
 * @since 0.1.0
 */
function tenup_content_types_init() {

	// Include classes
	require_once __DIR__ . '/includes/content-type.php';
	require_once __DIR__ . '/includes/post-type.php';
	require_once __DIR__ . '/includes/taxonomy.php';
	require_once __DIR__ . '/includes/options.php';
	require_once __DIR__ . '/includes/user.php';

}

add_action( 'plugins_loaded', 'tenup_content_types_init' );

/**
 * Register assets for use
 *
 * @since 0.1.0
 */
function tenup_content_types_register_assets() {

	$scripts = array(
		'tenup-content-types-display-if-enhanced' => TENUP_CONTENT_TYPES_URL . 'assets/js/fm-display-if-enhanced.js',
		'tenup-content-types-select2'             => TENUP_CONTENT_TYPES_URL . 'assets/select2/select2.min.js',
		'tenup-content-types-select2-init'        => TENUP_CONTENT_TYPES_URL . 'assets/js/fm-select2.js',
	);

	$styles = array(
		'tenup-content-types-admin'   => TENUP_CONTENT_TYPES_URL . 'assets/css/admin.css',
		'tenup-content-types-select2' => TENUP_CONTENT_TYPES_URL . 'assets/select2/select2.css',
	);

	foreach ( $scripts as $handle => $url ) {
		wp_register_script( $handle, $url, array(), TENUP_CONTENT_TYPES_VERSION, true );
	}

	foreach ( $styles as $handle => $url ) {
		wp_register_style( $handle, $url, array(), TENUP_CONTENT_TYPES_VERSION );
	}

}

add_action( 'wp_enqueue_scripts', 'tenup_content_types_register_assets', 9 );
add_action( 'admin_enqueue_scripts', 'tenup_content_types_register_assets', 9 );
