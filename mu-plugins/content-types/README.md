# 10up Content Types

This plugin was built to make it easier to create content types and add fields to them for WP VIP projects using the Fieldmanager plugin.

## Installation

1. Install Fieldmanager plugin
2. Install this plugin
3. Enjoy developing custom fields across all WP content types with ease

## Usage

### Defining content types

Add your content types into the `includes/content/` folder of your theme:

* Post types `includes/content/post-types/your-cpt.php`
* Taxonomies `includes/content/taxonomies/your-ct.php`
* Settings `includes/content/options.php`
* Users `includes/content/user.php`
* Custom field types **will not auto load** from this location, you need to include them manually

Add custom field types into the `includes/content/fields/` folder of your theme:

* My custom field type `includes/content/fields/my-custom-field-type.php`
* Custom field types **will auto load** from this location, you do not need to include them manually

### Including content types

Add this in your functions.php :

```php
// Include configuration classes
require_once 'includes/content/post-types/page.php';
require_once 'includes/content/post-types/post.php';
require_once 'includes/content/post-types/team-member.php';

// Initialize configurations and hooks
Page::get_instance();
Post::get_instance();
Team_Member::get_instance();
```

### Getting field values

You can use our helper function `get_content_fields()` off of your content type object to get values, or use the normal methods. Using the helper function helps to retrieve values from deeper in the Fieldmanager structure when using field groups inside of field groups.

```php
// Front page template

$page = Page::get_instance();

$content = $page->get_content_fields(); // Can also give a $object_id here too

$heading_text = $content['heading_text'];
?>

<?php if ( $heading_text ) : ?>
	<h2><?php echo esc_html( $heading_text ); ?></h2>
<?php endif; ?>
```

Normal WP methods still work, but don't provide insight into any other fields that might be on that page's Fieldmanager configuration:

```php
// Front page template

$heading_text = get_post_meta( get_the_ID(), 'heading_text', true );
?>

<?php if ( $heading_text ) : ?>
	<h2><?php echo esc_html( $heading_text ); ?></h2>
<?php endif; ?>
```

### Fields

Since we're extending the Fieldmanager plugin we have [all the fields available to us within the plugin](http://fieldmanager.org/docs/fields/). Registering a field occurs within any content type class in the same way.

Create a public function called `register_fields` and add a unique key to represent the meta box name. The example below will produce one meta box with two fields. Both fields in the example below will be stored under their own meta key 'last_name' and 'company_title'.

```
/**
 * {@inheritdoc}
 */
public function register_fields() {

    $fields = array();

    $fields['post-fields'] = array(
        'title'  => __( 'Post Fields', 'my-text-domain' ),
        'fields' => array(
            'last_name'       => array(
                'label' => __( 'Last Name', 'my-text-domain' ),
                'type'  => 'text',
            ),
            'company_title'   => array(
                'label' => __( 'Company Title', 'my-text-domain' ),
                'type'  => 'text',
            ),
        ),
    );

    return $fields;

}
```

All fields in a meta box can also be stored under one post meta key, the value corresponding to this single meta key will contain an array of each field. This can be done on any meta box by setting `'serialize_data' => true`. In the example below, both fields will be stored under the post meta key `post-fields` as `array( 'last_name' => 'value', 'company_title' => 'value' )`.

```
/**
 * {@inheritdoc}
 */
public function register_fields() {

    $fields = array();

    $fields['post-fields'] = array(
        'title'  => __( 'Post Fields', 'my-text-domain' ),
        'serialize_data' => true,
        'fields' => array(
            'last_name'       => array(
                'label' => __( 'Last Name', 'my-text-domain' ),
                'type'  => 'text',
            ),
            'company_title'   => array(
                'label' => __( 'Company Title', 'my-text-domain' ),
                'type'  => 'text',
            ),
        ),
    );

    return $fields;

}
```

### Custom Field Types

Fieldmanager allows us the ability to extend its existing field types and produce new custom field types. In addition to the list of fields available through Fieldmanager, 10up Content Types contains a `select2` and `multi_post_link` field. These fields allow any data source to be specified and will create a select2 lookup field. Here a list of 1 or more posts, terms or users can be selected, ordered and deleted in a list with the selected datasource values stored as an array.

This is a versatile field and can be used for several purposes such as curating posts, categories or even users onto a page. The below example creates a look up field for the 'post' post type and selects up to 7 posts.

```
/**
 * {@inheritdoc}
 */
public function register_fields() {

    $fields = array();

    $fields['post-fields'] = array(
        'title'  => __( 'Post Fields', 'my-text-domain' ),
        'serialize_data' => true,
        'fields' => array(
            'featured_posts'      => array(
                'label'           => __( 'Featured Posts', 'my-text-domain' ),
                'type'            => 'select2',
                'select_limit'    => 7,
                'use_custom_list' => true,
                'multiple'        => true,
                'datasource'      => new \Fieldmanager_Datasource_Post( array(
                    'query_args' => array(
                        'post_type' => 'post',
                    ),
                    'use_ajax'   => true,
                ) ),
                'description'     => __( 'Select 7 posts to be added to the category page featured area. Start by typing 3 letters in the title. You can drag and drop the posts into order.', 'my-text-domain' ),
            ),
        ),
    );

    return $fields;

}
```

## Examples

* [Post extending](https://github.com/10up/content-types/blob/master/examples/post-types/post.php)
* [Page extending](https://github.com/10up/content-types/blob/master/examples/post-types/page.php)
* [Custom Post Type](https://github.com/10up/content-types/blob/master/examples/post-type/team-member.php)
* [Category extending](https://github.com/10up/content-types/blob/master/examples/taxonomies/category.php)
* [Custom Taxonomy](https://github.com/10up/content-types/blob/master/examples/taxonomies/location.php)
* [User Fields](https://github.com/10up/content-types/blob/master/examples/user.php)
* [Options Page](https://github.com/10up/content-types/blob/master/examples/options/my-options-page.php)

## Contributors

* Scott Kingsley Clark (@sc0ttkclark)
* Brent van Rensburg (@brentvr)
* Mike Jordan
