<?php
/**
 * Category taxonomy configuration
 */
class Category extends \TenUp\ContentTypes\Taxonomy {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'category';

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields['category-fields'] = array(
			'title'  => __( 'Category Fields', 'my-text-domain' ),
			'fields' => array(
				'featured_image' => array(
					'label'              => __( 'Featured image', 'my-text-domain' ),
					'type'               => 'media',
					'button_label'       => __( 'Select an image', 'my-text-domain' ),
					'modal_button_label' => __( 'Select image', 'my-text-domain' ),
				),
			),
		);

		return $fields;

	}
}
