<?php
/**
 * Location taxonomy configuration
 */
class Location extends \TenUp\ContentTypes\Taxonomy {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'location';

	/**
	 * {@inheritdoc}
	 */
	public $labels = array(
		'singular_name' => 'Location',
		'name'          => 'Locations',
	);

	/**
	 * {@inheritdoc}
	 */
	public $post_types = array(
		'team_member',
	);

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields['location-fields'] = array(
			'title'  => __( 'Location Fields', 'my-text-domain' ),
			'fields' => array(
				'city'  => array(
					'label' => __( 'City', 'my-text-domain' ),
					'type'  => 'text',
				),
				'state' => array(
					'label' => __( 'State', 'my-text-domain' ),
					'type'  => 'text',
				),
			),
		);

		return $fields;

	}
}
