<?php
/**
 * My Options Page option configuration
 */
class My_Options_Page extends \TenUp\ContentTypes\Option {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'my-options-page';

	/**
	 * {@inheritdoc}
	 */
	public $page_title = 'My Options Page';

	/**
	 * {@inheritdoc}
	 */
	public $menu_title = 'My Options';

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields[ $this->name ] = array(
			'title'  => $this->page_title,
			'fields' => array(
				'api_url'   => array(
					'label' => __( 'API URL', 'my-text-domain' ),
					'type'  => 'text',
				),
				'api_token' => array(
					'label' => __( 'API Token', 'my-text-domain' ),
					'type'  => 'text',
				),
			),
		);

		return $fields;

	}

}
