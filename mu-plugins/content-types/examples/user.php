<?php
/**
 * User configuration
 */
class User extends \TenUp\ContentTypes\User {

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields['user-fields'] = array(
			'title'  => __( 'User fields', 'my-text-domain' ),
			'fields' => array(
				'avatar_image' => array(
					'label'              => __( 'Avatar image', 'my-text-domain' ),
					'type'               => 'media',
					'button_label'       => __( 'Select an image', 'my-text-domain' ),
					'modal_button_label' => __( 'Select image', 'my-text-domain' ),
				),
			),
		);

		return $fields;

	}

}
