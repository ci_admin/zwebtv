<?php
/**
 * Page post type configuration
 */
class Page extends \TenUp\ContentTypes\Post_Type {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'page';

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$post_id       = 0;
		$input_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );
		$input_get_id  = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );

		if ( ! is_admin() ) {
			$post_id = get_the_ID();
		} elseif ( ! empty( $input_post_id ) ) {
			$post_id = (int) $input_post_id;
		} elseif ( ! empty( $input_get_id ) ) {
			$post_id = (int) $input_get_id;
		}

		$post = get_post( $post_id );

		$fields = array();

		if ( empty( $post ) ) {
			return $fields;
		}

		// Get front page setting
		$show_on_front = get_option( 'show_on_front' );
		$page_on_front = (int) get_option( 'page_on_front' );

		$is_front_page    = false;
		$is_about_page    = false;
		$is_contact_page  = false;
		$is_our_work_page = false;

		if ( 'page' === $show_on_front && (int) $post_id === $page_on_front ) {
			$is_front_page = true;
		} elseif ( 'about' === $post->post_name ) {
			$is_about_page = true;
		} elseif ( 'contact' === $post->post_name ) {
			$is_contact_page = true;
		} elseif ( 'our-work' === $post->post_name ) {
			$is_our_work_page = true;
		}

		if ( $is_front_page ) {
			$fields['page-front'] = array(
				'title'  => __( 'Content Areas', 'my-text-domain' ),
				'fields' => array(
					'heading_text' => array(
						'label' => __( 'Heading Text', 'my-text-domain' ),
						'type'  => 'text',
					),
				),
			);
		} elseif ( $is_about_page ) {
			$fields['page-about'] = array(
				// Fields here
			);
		} elseif ( $is_contact_page ) {
			$fields['page-about'] = array(
				// Fields here
			);
		} elseif ( $is_our_work_page ) {
			$fields['page-our-work'] = array(
				// Fields here
			);
		}

		return $fields;

	}

}
