<?php
/**
 * Blog post type configuration
 */
class Post extends \TenUp\ContentTypes\Post_Type {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'post';

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields['post-fields'] = array(
			'title'  => __( 'Post Fields', 'my-text-domain' ),
			'fields' => array(
				'team_member' => array(
					'label'           => __( 'Team Member', 'my-text-domain' ),
					'type'            => 'select2',
					'multiple'        => false,
					'use_custom_list' => true,
					'show_view_link'  => false,
					'show_reorder'    => false,
					'datasource'      => new \Fieldmanager_Datasource_Post( array(
						'query_args' => array(
							'post_type' => 'team_member',
						),
						'use_ajax'   => true,
					) ),
				),
			),
		);

		return $fields;

	}

}
