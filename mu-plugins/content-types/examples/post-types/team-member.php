<?php
/**
 * Team post type configuration
 */
class Team_Member extends \TenUp\ContentTypes\Post_Type {

	/**
	 * {@inheritdoc}
	 */
	public $name = 'team_member';

	/**
	 * {@inheritdoc}
	 */
	public $labels = array(
		'singular_name' => 'Team Member',
		'name'          => 'Team Members',
	);

	/**
	 * {@inheritdoc}
	 */
	public $args = array(
		'supports'     => array(
			'title',
			'editor',
			'thumbnail',
		),
		'has_archive'  => true,
		'rewrite'      => array(
			'slug'       => 'team',
			'with_front' => true,
		),
		'hierarchical' => false,
	);

	/**
	 * {@inheritdoc}
	 */
	public function init() {

		// You can do different things here, for example we want to modify the $query in pre_get_posts
		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ) );

	}

	/**
	 * Hook into query for team members to display alphabetically by last name
	 *
	 * @param \WP_Query $query
	 */
	public function pre_get_posts( $query ) {

		if ( ! is_admin() && $query->is_main_query() && $query->is_post_type_archive( $this->name ) ) {
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'order', 'ASC' );
			$query->set( 'meta_key', 'last_name' );
		}

	}

	/**
	 * {@inheritdoc}
	 */
	public function register_fields() {

		$fields = array();

		$fields['team-fields'] = array(
			'title'  => __( 'Team Fields', 'my-text-domain' ),
			'fields' => array(
				'last_name'     => array(
					'label' => __( 'Last Name', 'my-text-domain' ),
					'type'  => 'text',
				),
				'company_title' => array(
					'label' => __( 'Company Title', 'my-text-domain' ),
					'type'  => 'text',
				),
				'supervisor'    => array(
					'label'           => __( 'Supervisor', 'my-text-domain' ),
					'type'            => 'select2',
					'multiple'        => true,
					'use_custom_list' => true,
					'show_view_link'  => false,
					'show_reorder'    => false,
					'datasource'      => new \Fieldmanager_Datasource_Post( array(
						'query_args' => array(
							'post_type' => 'team',
						),
						'use_ajax'   => true,
					) ),
				),
				'twitter_url'   => array(
					'label' => __( 'Twitter URL', 'my-text-domain' ),
					'type'  => 'text',
				),
				'author_color'  => array(
					'label'         => __( 'Color', 'my-text-domain' ),
					'type'          => 'colorpicker',
					'default_color' => '#b73030',
				),
			),
		);

		return $fields;

	}

}
