<?php
namespace TenUp\ContentTypes\Fields;

/**
 * Standard dropdown with using chosen for Fieldmanager
 */
class Select_No_Chosen extends \Fieldmanager_Select {

	/**
	 * {@inheritdoc}
	 */
	public function chosen_init() {

		return false;

	}
}
