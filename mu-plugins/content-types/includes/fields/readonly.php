<?php
namespace TenUp\ContentTypes\Fields;

/**
 * Read only custom field type for Fieldmanager
 */
class Read_Only extends \Fieldmanager_TextField {
	/**
	 * The capability a user must have to be able to edit normally
	 *
	 * @var string
	 */
	public $can_edit_capability;

	/**
	 * {@inheritdoc}
	 */
	public function form_element( $value = null ) {

		// Allow specific users to edit
		if ( $this->can_user_edit() ) {
			return parent::form_element( $value );
		}

		// Values aren't arrays for this field
		if ( is_array( $value ) ) {
			$value = implode( ' ', $value );
		}

		if ( '' === $value ) {
			return 'N/A';
		}

		return '<code>' . esc_html( $value ) . '</code>';

	}

	/**
	 * {@inheritdoc}
	 */
	public function presave( $value = null, $current_value = array() ) {

		// Allow specific users to edit
		if ( $this->can_user_edit() ) {
			return parent::presave( $value, $current_value );
		}

		// Values aren't arrays for this field
		if ( is_array( $current_value ) ) {
			$current_value = implode( ' ', $current_value );
		}

		// Don't allow changing value
		return $current_value;

	}

	/**
	 * Whether the current user can edit this field
	 *
	 * @return bool
	 */
	public function can_user_edit() {

		if ( $this->can_edit_capability && is_user_logged_in() && current_user_can( $this->can_edit_capability ) ) {
			return true;
		}

		return false;

	}
}
