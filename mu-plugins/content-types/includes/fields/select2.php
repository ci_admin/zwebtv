<?php
namespace TenUp\ContentTypes\Fields;

/**
 * Select2 custom field type for Fieldmanager
 */
class Select2 extends \Fieldmanager_TextField {

	/**
	 * Override $field_class
	 *
	 * @var string
	 */
	public $field_class = 'select2';

	/**
	 * Allow multiple selection
	 *
	 * @var boolean
	 */
	public $multiple = true;

	/**
	 * Set selection limit, default is no limit
	 *
	 * @var int
	 */
	public $select_limit = - 1;

	/**
	 * Set minimum text length, default is 3 characters must be typed to search
	 *
	 * @var int
	 */
	public $min_length = 3;

	/**
	 * Set ability to allow clearing selection
	 *
	 * @var boolean
	 */
	public $allow_clear = true;

	/**
	 * Use custom Select2 list
	 *
	 * @var boolean
	 */
	public $use_custom_list = false;

	/**
	 * Show thumbnails in custom Select2 list
	 *
	 * @var boolean
	 */
	public $show_thumbnail = false;

	/**
	 * Require an exact match; e.g. prevent the user from entering free text
	 *
	 * @var boolean
	 */
	public $exact_match = true;

	/**
	 * Show the edit link in the item list
	 *
	 * @var boolean
	 */
	public $show_edit_link = true;

	/**
	 * Show the view link in the item list
	 *
	 * @var boolean
	 */
	public $show_view_link = true;

	/**
	 * Show the reorder controls in the item list
	 *
	 * @var boolean
	 */
	public $show_reorder = true;

	/**
	 * The item JSON structure
	 *
	 * @var array
	 */
	public $item_json = array(
		'id'             => 0,
		'row'            => 0,
		'label'          => '',
		'show_thumbnail' => false,
		'show_reorder'   => false,
		'thumbnail_url'  => '',
		'thumbnail_id'   => 0,
		'edit_url'       => '',
		'view_url'       => '',
		'input_name'     => '',
	);

	/**
	 * Override constructor to add Chosen
	 *
	 * @param string $label
	 * @param array  $options
	 */
	public function __construct( $label = '', $options = array() ) {

		parent::__construct( $label, $options );

		if ( ! $this->multiple ) {
			$this->select_limit = 1;
		} elseif ( 1 === $this->select_limit ) {
			$this->multiple = false;
		}

		/**
		 * @var $datasource \Fieldmanager_Datasource
		 */
		$datasource = $this->datasource;

		if ( $datasource && $datasource->use_ajax ) {
			$action = $this->get_ajax_action();

			if ( $action ) {
				add_action( 'wp_ajax_' . $action, array( $this, 'autocomplete_search' ), 9 );
			}
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Thumbnails only for posts
		if ( ! is_a( $this->datasource, '\Fieldmanager_Datasource_Post' ) ) {
			$this->show_thumbnail = false;
		}

		$this->item_json['show_thumbnail'] = $this->show_thumbnail;
		$this->item_json['show_reorder']   = $this->show_reorder;

	}

	/**
	 * Get AJAX action for this field
	 *
	 * @return null|string
	 */
	public function get_ajax_action() {

		/**
		 * @var $datasource \Fieldmanager_Datasource
		 */
		$datasource = $this->datasource;

		$action = null;

		if ( $datasource ) {
			$action = 'tenup_content_types_' . $this->name . '_' . $datasource->get_ajax_action();
		}

		return $action;

	}

	/**
	 * Enqueue assets
	 */
	public function enqueue_scripts() {

		// Include jQuery Sortable
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-sortable' );

		// Include Underscore JS
		wp_enqueue_script( 'wp-util' );

		// Include Custom JS
		wp_enqueue_script( 'tenup-content-types-select2' );
		wp_enqueue_script( 'tenup-content-types-select2-init' );

		// Include Custom CSS
		wp_enqueue_style( 'tenup-content-types-select2' );
		wp_enqueue_style( 'tenup-content-types-admin' );

		// Add Underscore JS template
		add_action( 'admin_footer', array( $this, 'underscore_template' ), 11 );

	}

	/**
	 * Form element
	 *
	 * @param null|array $value
	 *
	 * @return string HTML
	 */
	public function form_element( $value = array() ) {

		$datasource = $this->datasource;

		$this->item_json['input_name'] = $this->get_form_name();

		$this->attributes['data-select2-type'] = 'select2';
		$this->attributes['data-custom-list']  = (int) $this->use_custom_list;
		$this->attributes['data-item-json']    = htmlspecialchars( wp_json_encode( $this->item_json ) );
		$this->attributes['data-field-class']  = $this->field_class;
		$this->attributes['data-multiple']     = (int) $this->multiple;
		$this->attributes['data-select-limit'] = (int) $this->select_limit;
		$this->attributes['data-min-length']   = (int) $this->min_length;
		$this->attributes['data-allow-clear']  = (int) $this->allow_clear;
		$this->attributes['data-reorder']      = (int) $this->show_reorder;

		if ( $datasource && $datasource->use_ajax ) {
			$this->attributes['data-action'] = $this->get_ajax_action();
			$this->attributes['data-nonce']  = wp_create_nonce( 'fm_search_nonce_' . $this->get_element_key() );

			list ( $context, $subcontext ) = fm_get_context();

			$this->attributes['data-context']    = $context;
			$this->attributes['data-subcontext'] = $subcontext;
		}

		// Add Item list
		$element = '';

		if ( $this->use_custom_list ) {
			$element = '<div class="tenup-content-types-select2-list hidden">'
						. '<h4 class="tenup-content-types-select2-heading">%s</h4>'
						. '<div class="tenup-content-types-select2-items"></div>'
						. '</div>';

			$element = sprintf(
				$element,
				esc_html__( 'Selected items', '10up-content-types' )
			);
		}

		// Handle values
		if ( ! is_array( $value ) ) {
			if ( $value ) {
				$value = array(
					$value,
				);
			} else {
				$value = array();
			}
		}

		$item_data = array();
		$count     = 0;

		foreach ( $value as $id ) {
			// If a taxonomy term has been deleted then remove it from the value.
			if ( is_a( $datasource, '\Fieldmanager_Datasource_Term' ) && isset( $datasource->taxonomy ) && ! term_exists( intval( $id ) ) ) {
				unset( $value[ $count ] );

				$count ++;

				continue;
				// If a post has been deleted then remove it from the value.
			} elseif ( is_a( $datasource, '\Fieldmanager_Datasource_Post' ) && isset( $datasource->query_args['post_type'] ) && ! $this->does_post_exists( $id ) ) {
				unset( $value[ $count ] );

				$count ++;

				continue;
			} elseif ( is_a( $datasource, '\Fieldmanager_Datasource_User' ) && ! get_userdata( $id ) ) {
				unset( $value[ $count ] );

				$count ++;

				continue;
			}

			if ( is_array( $id ) ) {
				$item = $id;
			} else {
				$item = $this->get_ajax_item( $id );
			}

			if ( $item ) {
				$item_data[] = $item;
			}

			$count ++;
		}

		$this->attributes['data-items'] = htmlspecialchars( wp_json_encode( $item_data ) );
		$this->attributes['data-value'] = htmlspecialchars( wp_json_encode( $value ) );

		if ( ! $this->use_custom_list ) {
			$value = implode( ',', $value );
		} else {
			$value = '';
		}

		$element .= sprintf(
			'<input type="%s" name="%s" id="%s" value="%s" %s class="fm-element" />',
			esc_attr( $this->input_type ),
			esc_attr( $this->get_form_name( '[]' ) ),
			esc_attr( $this->get_element_id() ),
			esc_attr( $value ),
			$this->get_element_attributes()
		);

		$wrapper_classes    = '';
		$wrapper_attributes = '';

		// Wrap whole element
		$element = sprintf(
			'<div class="tenup-content-types-select2%s" id="%s" %s>%s</div>',
			esc_attr( $wrapper_classes ),
			esc_attr( 'tenup-content-types-select2-' . $this->get_element_id() ),
			$wrapper_attributes,
			$element
		);

		return $element;

	}

	/**
	 * Add Underscore template for Backbone
	 */
	public function underscore_template() {

		static $printed;

		if ( empty( $printed ) ) {
			$printed = array();
		}

		if ( ! empty( $printed[ $this->field_class ] ) ) {
			return;
		}

		$printed[ $this->field_class ] = true;

		$template_file = __DIR__ . '/tmpl/select2.html';
		?>
		<script id="tmpl-tenup-content-types-select2-item-<?php echo esc_attr( $this->field_class ); ?>" type="text/html">
			<?php include $template_file; ?>
		</script>
		<?php

	}

	/**
	 * AJAX callback to find posts
	 */
	public function autocomplete_search() {

		// Check the nonce before we do anything
		$nonced = check_ajax_referer( 'fm_search_nonce_' . $this->get_element_key(), 'fm_search_nonce', false );

		if ( false !== $nonced ) {
			$input_name = filter_input( INPUT_POST, 'fm_name', FILTER_SANITIZE_STRING );
			$input_name = substr( $input_name, 0, - 2 );
			$items      = $this->get_items_for_ajax( filter_input( INPUT_POST, 'fm_autocomplete_search', FILTER_SANITIZE_STRING ), $input_name );

			// See if any results were returned and return them as an array
			if ( ! empty( $items ) ) {
				wp_send_json( $items );
			} else {
				wp_send_json( 0 );
			}
		}

	}

	/**
	 * Format items for use in AJAX.
	 *
	 * @param string|null $fragment to search
	 * @param string|null $input_name the input name
	 *
	 * @return array
	 */
	public function get_items_for_ajax( $fragment = null, $input_name = '' ) {
		$items  = $this->datasource->get_items( $fragment );
		$return = array();

		foreach ( $items as $id => $label ) {
			$return[] = $this->get_ajax_item( $id, $label, $input_name );
		}

		return $return;

	}

	/**
	 * Get item formatted for JS use
	 *
	 * @param int         $id    Item ID
	 * @param string|null $label Item label, if null then label will be pulled from item automatically
	 * @param string|null $input_name the input name, only used via ajax.
	 *
	 * @return array
	 */
	public function get_ajax_item( $id, $label = null, $input_name = '' ) {

		$datasource = $this->datasource;

		// Save post types and taxonomies for reference on other items
		static $post_types, $taxonomies;

		if ( ! isset( $post_types ) ) {
			$post_types = array();
		}

		if ( ! isset( $taxonomies ) ) {
			$taxonomies = array();
		}

		if ( empty( $input_name ) ) {
			$input_name = $this->get_form_name();
		}

		if ( ! $post_types && is_a( $this->datasource, '\Fieldmanager_Datasource_Post' ) ) {
			// Get post types list, only get singular labels
			$post_types = get_post_types( array(), 'objects' );
			$post_types = wp_list_pluck( $post_types, 'labels' );
			$post_types = wp_list_pluck( $post_types, 'singular_name' );
		} elseif ( ! $taxonomies && is_a( $this->datasource, '\Fieldmanager_Datasource_Term' ) ) {
			// Get taxonomies list, only get singular labels
			$taxonomies = get_taxonomies( array(), 'objects' );
			$taxonomies = wp_list_pluck( $taxonomies, 'labels' );
			$taxonomies = wp_list_pluck( $taxonomies, 'singular_name' );
		}

		// Get default JSON structure
		$item = $this->item_json;

		$item['id']         = (int) $id;
		$item['input_name'] = $input_name;

		if ( null !== $label ) {
			$item['label'] = $label;
		}

		if ( is_a( $this->datasource, '\Fieldmanager_Datasource_Post' ) ) {
			// Get label from ID automatically
			if ( null === $label ) {
				$item['label'] = get_the_title( $id );
			}

			$post_type = get_post_type( $id );

			// Check for a valid post type label to display
			if ( $post_type && ! empty( $post_types[ $post_type ] ) ) {
				$item['label'] .= ' [' . $post_types[ $post_type ] . ']';
			}

			// @todo Default thumbnail if none set on post?
			if ( has_post_thumbnail( $id ) ) {
				$item['thumbnail_id']  = get_post_thumbnail_id( $id );
				$item['thumbnail_url'] = get_the_post_thumbnail_url( $id, 'thumbnail' );
			}
		} elseif ( is_a( $this->datasource, '\Fieldmanager_Datasource_Term' ) ) {
			$term = get_term( $id );

			// Check for a valid term taxonomy label to display
			if ( $term && ! is_wp_error( $term ) ) {
				// Get label from ID automatically
				if ( null === $label ) {
					$item['label'] = $term->name;
				}

				if ( ! empty( $taxonomies[ $term->taxonomy ] ) ) {
					$item['label'] .= ' [' . $taxonomies[ $term->taxonomy ] . ']';
				}
			}
		} elseif ( is_a( $this->datasource, '\Fieldmanager_Datasource_User' ) ) {
			$user = get_userdata( $id );

			// Check for a valid term taxonomy label to display
			if ( $user && ! is_wp_error( $user ) ) {
				// Get label from ID automatically
				if ( null === $label ) {
					$item['label'] = $user->display_name;
				}
			}
		}

		if ( $this->show_edit_link ) {
			$item['edit_url'] = $datasource->get_edit_link( $id );

			// Pull the href out from Fieldmanager link
			if ( $item['edit_url'] ) {
				// Note: URL is already escaped
				$item['edit_url'] = explode( 'href="', $item['edit_url'] );
				$item['edit_url'] = explode( '"', $item['edit_url'][1] );
				$item['edit_url'] = $item['edit_url'][0];
			}
		}

		if ( $this->show_view_link ) {
			$item['view_url'] = $datasource->get_view_link( $id );

			// Pull the href out from Fieldmanager link
			if ( $item['view_url'] ) {
				// Note: URL is already escaped
				$item['view_url'] = explode( 'href="', $item['view_url'] );
				$item['view_url'] = explode( '"', $item['view_url'][1] );
				$item['view_url'] = $item['view_url'][0];
			}
		}

		if ( ! $item['edit_url'] ) {
			// Enforce empty string, not empty-ish
			$item['edit_url'] = '';
		}

		if ( ! $item['view_url'] ) {
			// Enforce empty string, not empty-ish
			$item['view_url'] = '';
		}

		return $item;

	}

	/**
	 * {@inheritdoc}
	 */
	public function presave_all( $values, $current_values ) {

		$limit = $this->select_limit;

		$values = array_filter( $values );

		if ( 0 < $limit ) {
			$value_count = count( $values );

			if ( $limit < $value_count ) {
				$values = array_slice( $values, 0, $limit );
			}
		}

		$values = array_values( $values );

		return $values;

	}

	/**
	 * Determines if a post, identified by the specified ID exist.
	 *
	 * @param    int $id The ID of the post to check
	 *
	 * @return   bool          True if the post exists; otherwise, false.
	 */
	function does_post_exists( $id ) {

		return is_string( get_post_status( $id ) );

	}

}

