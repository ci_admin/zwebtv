<?php
namespace TenUp\ContentTypes\Fields;

use TenUp\ContentTypes\Fields\Select2;

require_once __DIR__ . '/select2.php';

/**
 * Multi Post Link custom field type for Fieldmanager
 */
class Multi_Post_Link extends Select2 {

	/**
	 * {@inheritdoc}
	 */
	public $field_class = 'multi_post_link';

	/**
	 * {@inheritdoc}
	 */
	public function __construct( $label = '', $options = array() ) {

		parent::__construct( $label, $options );

		$this->item_json['is_custom']    = false;
		$this->item_json['custom_title'] = '';
		$this->item_json['custom_link']  = '';

		$args = array();

		if ( is_admin() && ! empty( $GLOBALS['post_ID'] ) ) {
			$args['post'] = (int) $GLOBALS['post_ID'];
		}

		wp_enqueue_media( $args );

	}

	/**
	 * {@inheritdoc}
	 */
	public function form_element( $value = array() ) {

		if ( is_array( $value ) ) {
			foreach ( $value as $k => $id ) {
				// Fill in the item arrays for custom items
				if ( is_array( $id ) ) {
					$item = $this->item_json;
					$item = array_merge( $item, $id );

					$item['input_name'] = $this->get_form_name();
					$item['is_custom']  = true;

					$item['thumbnail_id'] = (int) $item['thumbnail_id'];

					if ( 0 < $item['thumbnail_id'] ) {
						$thumbnail = wp_get_attachment_image_src( $item['thumbnail_id'] );

						if ( $thumbnail ) {
							$item['thumbnail_url'] = $thumbnail[0];
						}
					}

					$value[ $k ] = $item;
				}
			}
		}

		$element = parent::form_element( $value );

		$add_button = '<a href="#add" class="button tenup-content-types-select2-item-add">' . __( 'Add Custom Link', '10up-content-types' ) . '</a>';

		$element = str_replace( ' class="fm-element" />', ' class="fm-element" />' . $add_button, $element );

		return $element;

	}

	/**
	 * {@inheritdoc}
	 */
	public function presave_all( $values, $current_values ) {

		foreach ( $values as $k => $value ) {
			if ( is_array( $value ) ) {
				if ( isset( $value['id'] ) ) {
					$id = (int) $value['id'];

					if ( 0 < $id ) {
						// Force IDs as normal values that then interact with $this->get_item_ajax()
						$values[ $k ] = $id;
					} elseif ( '' === $value['custom_title'] && '' === $value['custom_link'] ) {
						// Remove empty custom link values
						unset( $values[ $k ] );
					}
				}
			}
		}

		$values = parent::presave_all( $values, $current_values );

		return $values;

	}
}
