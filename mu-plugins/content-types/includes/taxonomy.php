<?php
namespace TenUp\ContentTypes;

/**
 * Taxonomy helper class
 *
 * @package TenUp\ContentTypes
 *
 * @since 0.1.0
 */
class Taxonomy extends Content_Type {

	/**
	 * {@inheritdoc}
	 */
	public $type = 'term';

	/**
	 * Associated Post types
	 *
	 * @var array
	 *
	 * @since 0.1.0
	 */
	public $post_types = array();

	/**
	 * {@inheritdoc}
	 */
	public function register() {

		if ( ! taxonomy_exists( $this->name ) ) {
			// Build labels
			$labels = $this->labels;

			// Auto create label defaults from singular and plural labels
			if ( ! empty( $this->labels['name'] ) && ! empty( $this->labels['singular_name'] ) ) {
				$label_singular = $this->labels['singular_name'];
				$label_plural   = $this->labels['name'];

				$labels = array(
					'name'                  => $label_plural,
					'singular_name'         => $label_singular,
					'menu_name'             => $label_plural,
					'add_new_item'          => sprintf( _x( 'Add New %s', 'Taxonomy add_new_item', '10up-content-types' ), $label_singular ),
					'new_item_name'         => sprintf( _x( 'New %s', 'Taxonomy new_item_name', '10up-content-types' ), $label_singular ),
					'edit_item'             => sprintf( _x( 'Edit %s', 'Taxonomy edit_item', '10up-content-types' ), $label_singular ),
					'update_item'           => sprintf( _x( 'Edit %s', 'Taxonomy update_item', '10up-content-types' ), $label_singular ),
					'view_item'             => sprintf( _x( 'View %s', 'Taxonomy view_item', '10up-content-types' ), $label_singular ),
					'all_items'             => sprintf( _x( 'All %s', 'Taxonomy all_items', '10up-content-types' ), $label_plural ),
					'search_items'          => sprintf( _x( 'Search %s', 'Taxonomy search_items', '10up-content-types' ), $label_plural ),
					'parent_item_colon'     => sprintf( _x( 'Parent %s:', 'Taxonomy parent_item_colon', '10up-content-types' ), $label_singular ),
					'not_found'             => sprintf( _x( 'No %s found.', 'Taxonomy not_found', '10up-content-types' ), $label_plural ),
					'not_found_in_trash'    => sprintf( _x( 'No %s found in Trash.', 'Taxonomy not_found_in_trash', '10up-content-types' ), $label_plural ),
					'choose_from_most_used' => sprintf( _x( 'Choose from an existing %s', 'Taxonomy choose_from_most_used', '10up-content-types' ), $label_singular ),
					'popular_items'         => sprintf( _x( 'Popular %s', 'Taxonomy popular_items', '10up-content-types' ), $label_plural ),
				);

				$labels = array_merge( $labels, $this->labels );
			}

			// Setup default arguments
			$args = array(
				'hierarchical'       => true,
				'labels'             => $labels,
				'show_ui'            => true,
				'query_var'          => true,
				'show_tagcloud'      => false,
				'rewrite'            => array(
					'slug' => $this->name,
				),
				'public'             => true,
				'publicly_queryable' => true,
				'show_in_nav_menus'  => false,
				'show_in_quick_edit' => true,
				'show_admin_column'  => true,
			);

			if ( ! empty( $labels ) ) {
				$args['labels'] = $labels;
			}

			// Allow overrides
			if ( ! empty( $this->args ) ) {
				$args = array_merge( $args, $this->args );
			}

			// Register taxonomy
			register_taxonomy( $this->name, $this->post_types, $args );
		}

	}

	/**
	 * {@inheritdoc}
	 */
	public function get_field_value( $object_id, $field_name ) {

		$value = get_term_meta( $object_id, $field_name, true );

		return $value;

	}

}
