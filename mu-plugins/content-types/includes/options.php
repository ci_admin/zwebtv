<?php
namespace TenUp\ContentTypes;

/**
 * Option helper class
 *
 * @package TenUp\ContentTypes
 */
class Option extends Content_Type {

	/**
	 * {@inheritdoc}
	 */
	public $type = 'option';

	/**
	 * Page title
	 *
	 * @var string
	 */
	public $page_title = '';

	/**
	 * Menu title
	 *
	 * @var string
	 */
	public $menu_title = '';

	/**
	 * Capability needed to access settings page
	 *
	 * @var string
	 */
	public $capability = 'manage_options';

	/**
	 * {@inheritdoc}
	 */
	public function register() {

		if ( ! is_admin() ) {
			return;
		}

		/**
		 * @var $this Option
		 */

		// Register submenu page with Fieldmanager
		fm_register_submenu_page( $this->name, 'options-general.php', $this->page_title, $this->menu_title, $this->capability );

	}

	/**
	 * {@inheritdoc}
	 */
	public function get_field_value( $object_id, $field_name ) {

		$value = get_option( $field_name, '' );

		return $value;

	}

}
