<?php
namespace TenUp\ContentTypes;

/**
 * Post type helper class
 *
 * @package TenUp\ContentTypes
 *
 * @since 0.1.0
 */
class Post_Type extends Content_Type {

	/**
	 * {@inheritdoc}
	 */
	public $type = 'post';

	/**
	 * Menu icon
	 *
	 * @var string
	 *
	 * @since 0.1.0
	 */
	public $menu_icon = '';

	/**
	 * {@inheritdoc}
	 */
	public function register() {

		if ( ! post_type_exists( $this->name ) ) {
			// Build labels
			$labels = $this->labels;

			// Auto create label defaults from singular and plural labels
			if ( ! empty( $this->labels['name'] ) && ! empty( $this->labels['singular_name'] ) ) {
				$label_singular = $this->labels['singular_name'];
				$label_plural   = $this->labels['name'];

				$labels = array(
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'name_admin_bar'     => $label_singular,
					'add_new'            => _x( 'Add New', 'Post type add_new', '10up-content-types' ),
					'add_new_item'       => sprintf( _x( 'Add New %s', 'Post type add_new_item', '10up-content-types' ), $label_singular ),
					'new_item'           => sprintf( _x( 'New %s', 'Post type new_item', '10up-content-types' ), $label_singular ),
					'edit_item'          => sprintf( _x( 'Edit %s', 'Post type edit_item', '10up-content-types' ), $label_singular ),
					'view_item'          => sprintf( _x( 'View %s', 'Post type view_item', '10up-content-types' ), $label_singular ),
					'all_items'          => sprintf( _x( 'All %s', 'Post type all_items', '10up-content-types' ), $label_plural ),
					'search_items'       => sprintf( _x( 'Search %s', 'Post type search_items', '10up-content-types' ), $label_plural ),
					'parent_item_colon'  => sprintf( _x( 'Parent %s:', 'Post type parent_item_colon', '10up-content-types' ), $label_singular ),
					'not_found'          => sprintf( _x( 'No %s found.', 'Post type not_found', '10up-content-types' ), $label_plural ),
					'not_found_in_trash' => sprintf( _x( 'No %s found in Trash.', 'Post type not_found_in_trash', '10up-content-types' ), $label_plural ),
				);

				$labels = array_merge( $labels, $this->labels );
			}

			// Setup default arguments
			$args = array(
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => $this->name,
				'hierarchical'       => false,
				'menu_position'      => 5,
				'supports'           => array(
					'title',
					'editor',
					'excerpt',
					'author',
					'thumbnail',
				),
				'rewrite'            => array(
					'slug'       => $this->name,
					'with_front' => false,
				),
			);

			if ( ! empty( $labels ) ) {
				$args['labels'] = $labels;
			}

			if ( ! empty( $this->menu_icon ) ) {
				$args['menu_icon'] = $this->menu_icon;
			}

			// Allow overrides
			if ( ! empty( $this->args ) ) {
				$args = array_merge( $args, $this->args );
			}

			// Register post type
			register_post_type( $this->name, $args );
		}

	}

	/**
	 * {@inheritdoc}
	 */
	public function get_field_value( $object_id, $field_name ) {

		$value = get_post_meta( $object_id, $field_name, true );

		return $value;

	}

}
