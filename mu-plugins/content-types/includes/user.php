<?php
namespace TenUp\ContentTypes;

/**
 * User helper class
 *
 * @package TenUp\ContentTypes
 */
class User extends Content_Type {

	/**
	 * {@inheritdoc}
	 */
	public $type = 'user';

	/**
	 * {@inheritdoc}
	 */
	public function get_field_value( $object_id, $field_name ) {

		$value = get_metadata( 'user', $object_id, $field_name, true );

		return $value;

	}

}
