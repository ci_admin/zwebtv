<?php
namespace TenUp\ContentTypes;

/**
 * Content type helper class
 *
 * @package TenUp\ContentTypes
 *
 * @since   0.1.0
 */
class Content_Type {

	/**
	 * Content type
	 *
	 * @var string
	 *
	 * @since 0.1.0
	 */
	public $type = '';

	/**
	 * Content type name
	 *
	 * @var string
	 *
	 * @since 0.1.0
	 */
	public $name = '';

	/**
	 * Content type labels
	 *
	 * @var array
	 *
	 * @since 0.1.0
	 */
	public $labels = array();

	/**
	 * Content type args
	 *
	 * @var array
	 *
	 * @since 0.1.0
	 */
	public $args = array();

	/**
	 * Fields config
	 *
	 * @var array
	 *
	 * @since 0.1.0
	 */
	protected $config = array();

	/**
	 * Object instances
	 *
	 * @var Content_Type[]
	 *
	 * @since 0.1.0
	 */
	private static $instances;

	/**
	 * Content type constructor.
	 *
	 * @since 0.1.0
	 */
	protected function __construct() {

		$this->setup();

	}

	/**
	 * Get object instance, set one up if we don't have one
	 *
	 * @return Content_Type
	 *
	 * @since 0.1.0
	 */
	public static function get_instance() {

		$class = get_called_class();

		if ( empty( self::$instances[ $class ] ) ) {
			self::$instances[ $class ] = new static();
		}

		return self::$instances[ $class ];

	}

	/**
	 * Setup FM actions and filters
	 *
	 * @since 0.1.0
	 */
	public function setup() {

		add_action( 'init', array( $this, 'register' ) );
		add_action( 'init', array( $this, 'init' ) );

		// Add all of the FM hooks
		if ( class_exists( '\Fieldmanager_Group' ) ) {
			if ( in_array( $this->type, array( 'post', 'term', 'user' ), true ) ) {
				$fm_hook = false;

				if ( 'user' === $this->type ) {
					$fm_hook = sprintf( 'fm_%s', $this->type );
				} elseif ( ! empty( $this->name ) ) {
					$fm_hook = sprintf( 'fm_%s_%s', $this->type, $this->name );
				}

				if ( $fm_hook ) {
					add_action( $fm_hook, array( $this, 'fm_register' ) );
				}
			} elseif ( 'option' === $this->type ) {
				if ( ! empty( $this->name ) ) {
					$fm_hook = sprintf( 'fm_%s_%s', 'submenu', $this->name );

					add_action( $fm_hook, array( $this, 'fm_register' ) );
				}
			}

			add_filter( 'fm_element_classes', array( $this, 'fm_element_classes' ), 10, 3 );
		}

	}

	/**
	 * Add extra element classes depending on specific features
	 *
	 * @param array               $classes
	 * @param string              $field_name
	 * @param \Fieldmanager_Field $field
	 *
	 * @return array
	 *
	 * @since 0.1.0
	 */
	public function fm_element_classes( $classes, $field_name, $field ) {

		if ( ! empty( $field->attributes['data-display-if-enhanced'] ) ) {
			$classes[] = 'display-if-enhanced';
		}

		return $classes;

	}

	/**
	 * Prepare data-display-if-enhanced data
	 *
	 * @param array $conditions
	 *
	 * @return string
	 *
	 * @since 0.1.0
	 */
	public function prepare_display_if_enhanced( $conditions ) {

		if ( isset( $conditions['src'] ) ) {
			$conditions = array( $conditions );
		}

		$conditions = htmlspecialchars( wp_json_encode( $conditions ) );

		return $conditions;

	}

	/**
	 * Register content type
	 *
	 * @since 0.1.0
	 */
	public function register() {

		// Stub

	}

	/**
	 * Run specific actions on init
	 *
	 * @since 0.2.1
	 */
	public function init() {

		// Stub

	}

	/**
	 * Register meta for content type
	 *
	 * @return array
	 *
	 * @since 0.1.0
	 */
	public function register_fields() {

		return array();

	}

	/**
	 * Register meta with Fieldmanager
	 *
	 * @since 0.1.0
	 */
	public function fm_register() {

		if ( class_exists( '\Fieldmanager_Group' ) ) {
			// Get registered fields config
			$config = $this->register_fields();

			// Save config
			$this->config = $config;

			// Setup Fieldmanager_Group object(s)
			$this->setup_fm_groups( $this->config );

			/**
			 * Action that notes that fm_register() has completed
			 *
			 * @param array        $config Fields config that was registered
			 * @param Content_Type $this   Content Type object
			 *
			 * @since 0.2.0
			 */
			do_action( '10up_content_types_fm_registered', $config, $this );
		}

	}

	/**
	 * Setup Fieldmanager group objects
	 *
	 * @param array $groups Group configurations
	 *
	 * @return \Fieldmanager_Field|bool Fieldmanager field object or false if invalid
	 *
	 * @since 0.1.0
	 */
	public function setup_fm_groups( $groups ) {

		/**
		 * @var $first_group \Fieldmanager_Group
		 */
		$first_group = null;

		$sections = 0;

		foreach ( $groups as $section_id => $section ) {
			if ( empty( $section['title'] ) || empty( $section['fields'] ) ) {
				continue;
			}

			$sections ++;

			// Setup main group options
			$fm_group = array(
				'name'           => $section_id,
				'children'       => array(),
				'serialize_data' => false,
				'add_to_prefix'  => false,
			);

			if ( 'option' === $this->type && ! $first_group ) {
				$fm_group['name'] = $this->name;
			}

			// Get section context / priority
			$section_context  = 'normal';
			$section_priority = 'default';

			if ( ! empty( $section['context'] ) ) {
				$section_context = $section['context'];
			}

			if ( ! empty( $section['priority'] ) ) {
				$section_priority = $section['priority'];
			}

			if ( isset( $section['field_class'] ) && $section['field_class'] ) {
				$fm_group['field_class'] = 'group ' . $section['field_class'];
			}

			foreach ( $section['fields'] as $field_id => $field ) {
				$field = $this->setup_fm_field( $field_id, $field );

				if ( $field ) {
					// Register field and add it as child of group
					$fm_group['children'][ $field_id ] = $field;
				}
			}

			// Register group and add meta box
			$fm_group = new \Fieldmanager_Group( $fm_group );

			if ( 'term' === $this->type ) {
				if ( function_exists( 'get_term_meta' ) && apply_filters( '10up_content_types_term_meta', true ) ) {
					$fm_group->add_term_meta_box( $section['title'], $this->name );
				} else {
					$fm_group->add_term_form( $section['title'], $this->name );
				}
			} elseif ( 'user' === $this->type ) {
				$fm_group->add_user_form( $section['title'] );
			} elseif ( 'post' === $this->type ) {
				$fm_group->add_meta_box( $section['title'], $this->name, $section_context, $section_priority );
			} elseif ( 'option' === $this->type ) {
				if ( $first_group ) {
					$first_group->add_child( $fm_group );
				} else {
					$fm_group->activate_submenu_page();
				}
			}

			$first_group = $fm_group;
		}

	}

	/**
	 * Setup Fieldmanager field object
	 *
	 * @param string $field_id Field name
	 * @param array  $field    Field configuration
	 *
	 * @return \Fieldmanager_Field|bool Fieldmanager field object or false if invalid
	 *
	 * @since 0.1.0
	 */
	protected function setup_fm_field( $field_id, $field ) {

		// Map basic types to their corresponding class names
		$fm_types = array(
			'group'           => '\Fieldmanager_Group',
			'text'            => '\Fieldmanager_TextField',
			'textarea'        => '\Fieldmanager_TextArea',
			'editor'          => '\Fieldmanager_RichTextArea',
			'radio'           => '\Fieldmanager_Radios',
			'date'            => '\Fieldmanager_Datepicker',
			'checkbox'        => '\Fieldmanager_Checkbox',
			'checkbox_multi'  => '\Fieldmanager_Checkboxes',
			'link'            => '\Fieldmanager_Link',
			'media'           => '\Fieldmanager_Media',
			'select_post'     => '\Fieldmanager_DraggablePost',
			'select_chosen'   => '\Fieldmanager_Select',
			'hidden'          => '\Fieldmanager_Hidden',
			'colorpicker'     => '\Fieldmanager_Colorpicker',
			'readonly'        => '\TenUp\ContentTypes\Fields\Read_Only',
			'readonly_select' => '\TenUp\ContentTypes\Fields\Read_Only_Select',
			'select'          => '\TenUp\ContentTypes\Fields\Select_No_Chosen',
			'select2'         => '\TenUp\ContentTypes\Fields\Select2',
			'multi_post_link' => '\TenUp\ContentTypes\Fields\Multi_Post_Link',
		);

		// Default field type and FM field class
		$field_type     = 'text';
		$fm_field_class = $fm_types[ $field_type ];

		if ( isset( $field['type'] ) ) {
			$field_type = $field['type'];

			unset( $field['type'] );
		}

		/**
		 * Filter field types (and custom field types) supported for Fieldmanager integration
		 *
		 * @param array  $fm_types
		 * @param string $field_type
		 * @param array  $field
		 * @param string $field_id
		 *
		 * @return array
		 *
		 * @since 0.1.1
		 */
		$fm_types = apply_filters( '10up_content_types_fm_field_types', $fm_types, $field_type, $field, $field_id );

		if ( ! empty( $fm_types[ $field_type ] ) ) {
			$fm_field_class = $fm_types[ $field_type ];
		}

		// Setup main field options
		$field_options = array(
			'name' => $field_id,
		);

		$field_options = array_merge( $field_options, $field );

		if ( 'group' === $field_type && ! empty( $field_options['children'] ) ) {
			foreach ( $field_options['children'] as $child_field_id => $child_field ) {
				$child_field = $this->setup_fm_field( $child_field_id, $child_field );

				if ( $child_field ) {
					// Register field and add it as child of group
					$field_options['children'][ $child_field_id ] = $child_field;
				}
			}
		}

		// Load custom Fieldmanager field type
		if ( ! class_exists( $fm_field_class ) ) {
			if ( file_exists( __DIR__ . '/fields/' . $field_type . '.php' ) ) {
				require_once __DIR__ . '/fields/' . $field_type . '.php';
			} else {
				locate_template( 'includes/content/fields/' . $field_type . '.php', true );
			}
		}

		// Field type class does not exist, use default
		if ( ! class_exists( $fm_field_class ) ) {
			$field_type     = 'text';
			$fm_field_class = $fm_types[ $field_type ];
		}

		return new $fm_field_class( $field_options );

	}

	/**
	 * Get content fields from post
	 *
	 * @param null|int $object_id Object ID
	 *
	 * @return array
	 *
	 * @since 0.2.1
	 */
	public function get_content_fields( $object_id = null ) {

		if ( null === $object_id ) {
			if ( 'post' === $this->type ) {
				$object_id = get_the_ID();
			} elseif ( 'term' === $this->type ) {
				$queried_object = get_queried_object();

				if ( isset( $queried_object->taxonomy ) ) {
					$object_id = get_queried_object_id();
				}
			} elseif ( 'user' === $this->type ) {
				$queried_object = get_queried_object();

				if ( isset( $queried_object->user_login ) ) {
					$object_id = get_queried_object_id();
				}
			}
		}

		$groups = $this->register_fields();

		$content = $this->get_content_fields_from_group( $groups, $object_id );

		return $content;

	}

	/**
	 * Get content fields from group
	 *
	 * @param array    $fields    Content fields
	 * @param null|int $object_id Object ID
	 * @param array    $args      Additional options
	 *
	 * @return array
	 *
	 * @since 0.2.1
	 */
	public function get_content_fields_from_group( $fields, $object_id, $args = array() ) {

		$content = array();

		if ( empty( $args['depth'] ) ) {
			$args['depth'] = 0;
		}

		if ( empty( $args['content'] ) ) {
			$args['content'] = array();
		}

		if ( empty( $args['parent_name'] ) ) {
			$args['parent_name'] = '';
		}

		foreach ( $fields as $field_name => $field ) {
			if ( ( isset( $field['title'] ) && ! isset( $field['type'] ) ) || ( isset( $field['type'] ) && 'group' === $field['type'] ) ) {
				$children = array();

				if ( ! empty( $field['children'] ) ) {
					$children = $field['children'];
				} elseif ( ! empty( $field['fields'] ) ) {
					$children = $field['fields'];
				}

				if ( $children ) {
					$args['grouped'] = false;
					$args['prefix']  = false;

					if ( ! isset( $field['serialize_data'] ) || $field['serialize_data'] ) {
						$args['grouped'] = true;
					}

					if ( isset( $field['add_to_prefix'] ) && ! $field['add_to_prefix'] ) {
						$args['prefix'] = true;
					}

					$new_args = $args;

					$new_args['depth']++;

					$new_args['grandparent_name'] = $args['parent_name'];
					$new_args['parent_name']      = $field_name;
					$new_args['content']          = $content;

					$group_content = $this->get_content_fields_from_group( $children, $object_id, $new_args );

					$content = array_merge( $content, $group_content );
				}
			} else {
				$grandparent_field = $field_name;
				$parent_field      = null;

				$field_value = null;

				if ( ! empty( $args['grouped'] ) || 3 === $args['depth'] ) {
					$grandparent_field = $args['parent_name'];

					if ( 3 === $args['depth'] ) {
						$grandparent_field = $args['grandparent_name'];
						$parent_field      = $args['parent_name'];
					}

					if ( isset( $content[ $grandparent_field ] ) ) {
						$field_value = $content[ $grandparent_field ];
					} elseif ( isset( $args['content'][ $grandparent_field ] ) ) {
						$field_value = $args['content'][ $grandparent_field ];
					}
				} elseif ( ! empty( $args['prefix'] ) ) {
					$grandparent_field = $args['parent_name'] . '_' . $grandparent_field;
				}

				if ( null === $field_value ) {
					$field_value = $this->get_field_value( $object_id, $grandparent_field );
				}

				$content_value = '';

				if ( ! empty( $args['grouped'] ) || 3 === $args['depth'] ) {
					if ( $parent_field ) {
						if ( isset( $field_value[ $parent_field ][ $field_name ] ) ) {
							$content_value = $field_value[ $parent_field ][ $field_name ];
						}
					} elseif ( isset( $field_value[ $field_name ] ) ) {
						$content_value = $field_value[ $field_name ];
					}
				} else {
					$content_value = $field_value;
				}

				$content_value = $this->get_processed_value( $content_value, $field );

				if ( ! empty( $args['grouped'] ) || 3 === $args['depth'] ) {
					if ( ! $field_value || ! is_array( $field_value ) ) {
						$field_value = array();
					}

					if ( $parent_field ) {
						if ( ! is_array( $field_value[ $parent_field ] ) ) {
							$field_value[ $parent_field ] = array();
						}

						$field_value[ $parent_field ][ $field_name ] = $content_value;
					} else {
						$field_value[ $field_name ] = $content_value;
					}
				} else {
					$field_value = $content_value;
				}

				$content[ $grandparent_field ] = $field_value;
			}
		}

		return $content;

	}

	/**
	 * Process value based on field type
	 *
	 * @param string|mixed $value Value
	 * @param array        $field Content field
	 *
	 * @return string|array|bool
	 */
	public function get_processed_value( $value, $field ) {

		if ( 'media' === $field['type'] ) {
			if ( $value ) {
				$attachment_id = $value;

				$value = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );

				if ( $value ) {
					$value = array(
						'ID'       => $attachment_id,
						'src'      => $value[0],
						'width'    => $value[1],
						'height'   => $value[2],
						'alt_text' => get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ),
					);
				}
			}
		} elseif ( 'checkbox' === $field['type'] ) {
			$value = (boolean) $value;
		}

		return $value;

	}

	/**
	 * Get field value based on object
	 *
	 * @param int|null $object_id  Object ID
	 * @param string   $field_name Field name
	 *
	 * @return string|mixed Value of the field
	 */
	public function get_field_value( $object_id, $field_name ) {

		return '';

	}

}
