<?php

declare(strict_types=1);

namespace WPGraphQL\Extensions\Cache;

use GraphQL\Server\OperationParams;
use WPGraphQL\Extensions\Cache\Backend\AbstractBackend;

/**
 * Class that takes care of caching of full queries
 */
class QueryCache extends AbstractCache {

	/**
	 * GraphQL Query name this cache should match against
	 */
	protected $query_name = null;

	/**
	 * Whether the GraphQL Request is a Batch Request
	 *
	 * @var bool
	 */
	protected $batch_request = false;

	/**
	 * Stores the already cached operations on this request
	 *
	 * @var array
	 */
	protected $cached_operations = [];

	/**
	 * Stores the uncached operations on this request
	 *
	 * @var array
	 */
	protected $uncached_operations = [];

	/**
	 * Stores the operations cache_keys in the order they were received
	 *
	 * @var array
	 */
	protected $operations_keys = [];

	function __construct( $config ) {
		parent::__construct( $config );
		$this->query_name = $config['query_name'];
	}

	/**
	 * Activate the cache with the given backend if the cache did not have own
	 * custom backend.
	 */
	function activate( AbstractBackend $backend ) {
		if ( ! $this->backend ) {
			$this->backend = $backend;
		}

		add_action(
			'graphql_request_data',
			[ $this, '__action_graphql_request_data' ],
			10,
			2
		);

		add_action(
			'graphql_process_http_request_response',
			[ $this, '__action_graphql_process_http_request_response' ],
			// Use large value as this should be the last response filter
			// because we want to save the last version of the response to the
			// cache.
			1000,
			5
		);

		add_action(
			'graphql_response_set_headers',
			[
				$this,
				'__action_graphql_response_set_headers',
			]
		);
	}

	/**
	 * Build a ordered response from the now cached operations
	 *
	 * @return array
	 */
	protected function build_response() {
		$full_response = [];

		foreach ( $this->operations_keys as $operation_key ) {
			if ( isset( $this->cached_operations[ $operation_key ] ) ) {
				$result          = $this->cached_operations[ $operation_key ]['result'];
				$full_response[] = is_string( $result ) ? json_decode( $result ) : $result;
			}
		}

		return $this->batch_request ? $full_response : $full_response[0];
	}

	function __action_graphql_request_data(
		$parsed_body_params,
		$request_context
	) {
		$body = [];
		if ( isset( $parsed_body_params[0] ) ) {
			$this->batch_request = true;
			$body = $parsed_body_params;
		} else {
			$this->batch_request = false;
			$body = [ $parsed_body_params ];
		}

		$user_id = get_current_user_id();

		foreach ( $body as $operation ) {
			$args_hash  = empty( $operation['variables'] ) ? 'null' : Utils::hash( Utils::stable_string( $operation['variables'] ) );
			$query_hash = Utils::hash( $operation['query'] );
			$query_name = Utils::get_query_name( $operation['query'] );
			$this->key  = "query-{$query_name}-${user_id}-{$query_hash}-${args_hash}";

			$this->operations_keys[] = $this->key;
			$this->read_cache();

			if ( ! $this->has_hit() ) {
				$this->uncached_operations[ $this->key ] = [
					'operation' => $operation,
					'cache_key' => $this->key,
				];
			} else {
				$this->cached_operations[ $this->key ] = [
					'operation' => $operation,
					'result'    => $this->get_cached_data(),
					'cache_key' => $this->key,
				];
			}
		}

		if ( count( $this->uncached_operations ) === 0 ) {
			header(
				'Content-Type: application/json; charset=' .
					get_option( 'blog_charset' )
			);

			header( 'x-graphql-query-cache: HIT' );

			wp_send_json( $this->build_response() );
			die();
		}

		return array_map(
			function ( $operation ) {
				return $operation['operation'];
			},
			array_values( $this->uncached_operations )
		);
	}

	function __action_graphql_response_set_headers() {
		if ( count( $this->uncached_operations ) > 0 && count( $this->cached_operations ) === 0 ) {
			// Just add MISS header if we have match and have not already exited
			// with the cached response. respond_and_exit() handles the HIT header
			header( 'x-graphql-query-cache: MISS' );
		} else if ( count( $this->uncached_operations ) > 0 && count( $this->cached_operations ) > 0 ) {
			header( 'x-graphql-query-cache: PARTIAL HIT' );
		}
	}

	function __action_graphql_process_http_request_response(
		$response,
		$result,
		$operation_name,
		$query,
		$variables
	) {
		if ( ! empty( $response->errors ) ) {
			return;
		}
		$uncached_operations_values = array_values( $this->uncached_operations );
		foreach ( $uncached_operations_values as $index => $operation ) {
			$this->key = $operation['cache_key'];
			$data      = $result[ $index ];

			// Save results as pre encoded json
			$this->backend->set(
				$this->zone,
				$this->get_cache_key(),
				new CachedValue( wp_json_encode( $data ) ),
				$this->expire
			);

			$this->cached_operations[ $this->key ] = [
				'operation' => $operation['operation'],
				'result'    => $data,
				'cache_key' => $operation['cache_key'],
			];
		}

		$this->uncached_operations = [];

		header(
			'Content-Type: application/json; charset=' . get_option( 'blog_charset' )
		);

		wp_send_json( $this->build_response() );
		die();
	}
}
