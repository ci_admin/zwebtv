<?php

namespace Zweb\Role;

use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Studio;
use Zweb\PostType\Video;

/**
 * Class LiveBattleAdmin
 *
 * @package Zweb\Role
 */
class LiveBattleAdmin {

	const ROLE_NAME = 'live_battle_admin';

	/**
	 * Register.
	 */
	public static function register() {
		if ( get_option( 'live_battle_admin_version' ) !== ZWEB_VERSION ) {
			remove_role( self::ROLE_NAME );
			add_role(
				self::ROLE_NAME,
				__( 'Live Battle Admin', 'zweb' ),
				[
					'edit_live_battle'                       => true,
					'read_live_battle'                       => true,
					'delete_live_battle'                     => true,
					'publish_live_battles'                   => true,
					'edit_live_battles'                      => true,
					'read_live_battles'                      => true,
					'delete_live_battles'                    => true,
					'edit_others_live_battles'               => true,
					'delete_others_live_battles'             => true,
					LiveBattle::SET_LIVE_BATTLE_AS_READY_CAP => true,
					'read'                                   => true,
				]
			);

			add_option( 'live_battle_admin_version', ZWEB_VERSION );
		}

	}

	/**
	 * @param \WP_User $user
	 *
	 * @return bool
	 */
	public static function is_user_live_battle_admin( \WP_User $user ) {
		return in_array( self::ROLE_NAME, (array) $user->roles );
	}

}
