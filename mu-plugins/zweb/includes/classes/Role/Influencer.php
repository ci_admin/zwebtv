<?php

namespace Zweb\Role;

use Zweb\Firebase\Provider;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\VideoCategory;
use Zweb\PostType\Studio;
use TenUp\ContentTypes\Fields as TenupFields;


/**
 * Class Influencer
 *
 * @package Zweb\Role
 */
class Influencer {

	const ROLE_NAME = 'influencer';

	const META_KEY = 'zweb-influencer';

	/**
	 * Register the role when we change the version.
	 */
	public static function register() {
		add_filter( 'admin_body_class', [ __CLASS__, 'body_class' ] );
		add_action(
			'admin_init',
			[
				__CLASS__,
				'no_influencer_admin_access',
			],
			100
		);
		add_action( 'fm_user', [ __CLASS__, 'meta_fields' ] );

		if ( get_option( 'influencer_role_version' ) !== ZWEB_VERSION ) {
			add_role( self::ROLE_NAME, 'Influencer', [] );
			update_option( 'influencer_role_version', ZWEB_VERSION );
		}
	}

	/**
	 * Add meta fields.
	 *
	 * @throws \FM_Developer_Exception
	 */
	public static function meta_fields() {

		// Get user_id from the current request query string.
		$user_id = intval( $_REQUEST['user_id'] );

		// Check current user cap and if user is an influencer.
		$can_show_fields = current_user_can( 'edit_user', $user_id ) && self::is_user_influencer( new \WP_User( $user_id ) );

		if ( ! $can_show_fields && ! wp_doing_ajax() ) {
			return;
		}

		$influencer_user_fields['landing_page_image'] = new \Fieldmanager_Media(
			[
				'label' => 'Landing Page Image ( 1600 X 774 px ) ',
			]
		);

		$influencer_user_fields['landing_page_image_live'] = new \Fieldmanager_Media(
			[
				'label' => 'Landing Page Live Image ( 1600 X 774 px ) ',
			]
		);

		$influencer_user_fields['profile_image'] = new \Fieldmanager_Media(
			[
				'label' => 'Small Profile Image ( 400 X 400 px )',
			]
		);

		$influencer_user_fields['influencer_card_image'] = new \Fieldmanager_Media(
			[
				'label' => 'Influencer Card Image ( 300 X 410 px ) ',
			]
		);

		$influencer_user_fields['influencer_upcoming_image'] = new \Fieldmanager_Media(
			[
				'label' => 'Influencer Upcoming Image ( 300 X 410 px ) ',
			]
		);

		$influencer_user_fields['prevent_showing_in_influencer_page'] = new \Fieldmanager_Checkbox(
			[
				'label' => 'Prevent the influencer from appearing in the "All Influencer" page',
			]
		);

		$influencer_user_fields['show_as_upcoming'] = new \Fieldmanager_Checkbox(
			[
				'label' => 'Show the influencer as upcoming',
			]
		);

		$influencer_user_fields['upcoming_date'] = new \Fieldmanager_Datepicker(
			[
				'label'    => 'Date of reveal',
				'use_time' => true,
			]
		);

		$influencer_user_fields['video_category'] = new \Fieldmanager_Select(
			[
				'first_empty' => true,
				'label'       => 'Video Category',
				'datasource'  => new \Fieldmanager_Datasource_Term(
					[
						'taxonomy'      => VideoCategory::TAXONOMY_NAME,
						'taxonomy_args' => [
							'hide_empty' => false,
						],
					]
				),
			]
		);

		$influencer_user_fields['parent_studio'] = new \Fieldmanager_Select(
			[
				'first_empty' => true,
				'label'       => 'Parent Studio',
				'datasource'  => new \Fieldmanager_Datasource_Post(
					[
						'query_args' => [
							'post_type' => [ Studio::POST_TYPE_NAME ],
						],
					]
				),
			]
		);

		$influencer_user_field_group = new \Fieldmanager_Group(
			[
				'name'           => self::META_KEY,
				'serialize_data' => false,
				'children'       => $influencer_user_fields,
			]
		);
		$influencer_user_field_group->add_user_form( 'Influencers Info' );
	}

	/**
	 * Add class to the body when editing an influencer in the admin.
	 *
	 * @param array $classes
	 *
	 * @return string
	 */
	public static function body_class( $classes ) {
		$screen = get_current_screen();
		if ( 'user-edit' === $screen->id ) {
			if ( isset( $_GET['user_id'] ) ) {
				$user = new \WP_User( $_GET['user_id'] );
				if ( self::is_user_influencer( $user ) ) {
					$classes .= ' role-influencer';
				}
			}
		}

		return $classes;
	}

	/**
	 *  Prevent influencer from accessing the admin area.
	 */
	public static function no_influencer_admin_access() {
		$redirect = home_url( '/' );
		if ( self::is_user_influencer( wp_get_current_user() ) ) {
			wp_safe_redirect( $redirect );
			die();
		}

	}

	/**
	 * @param \WP_User $user
	 *
	 * @return bool
	 */
	public static function is_user_influencer( \WP_User $user ) {
		return in_array( self::ROLE_NAME, (array) $user->roles );
	}

	/**
	 * @param string $firebase_token
	 *
	 * @return \WP_User
	 * @throws \Exception
	 */
	public static function get_user_from_firebase_if_has_role( $firebase_token, $role = SELF::ROLE_NAME ) {
		$firebase      = Provider::register();
		$firebase_user = $firebase->is_valid_token( $firebase_token );
		if ( ! $firebase_user ) {
			throw new \Exception( __( 'Not a valid user token', 'zweb' ) );
		}
		$wp_user = get_user_by( 'email', $firebase_user->email );
		if ( ! $wp_user ) {
			throw new \Exception( __( 'Email is not valid in WordPress', 'zweb' ) );
		}
		if ( ! in_array( $role, (array) $wp_user->roles, true ) ) {
			throw new \Exception( __( 'The user is not allowed to upload videos', 'zweb' ) );
		}
		return $wp_user;
	}
}
