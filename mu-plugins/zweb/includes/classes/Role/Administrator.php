<?php

namespace Zweb\Role;

use Zweb\PostType\LiveBattle;

/**
 * Class Administrator
 *
 * @package Zweb\Role
 */
class Administrator {

	/**
	 * Register
	 */
	public static function modify() {
		add_action( 'admin_init', [ self::class, 'add_theme_caps' ], 11 );
	}

	/**
	 * Add capapbilities to admin.
	 */
	public static function add_theme_caps() {
		global $wp_roles;
		if ( isset( $wp_roles ) ) {
			$wp_roles->add_cap( 'administrator', 'edit_live_battle' );
			$wp_roles->add_cap( 'administrator', 'read_live_battle' );
			$wp_roles->add_cap( 'administrator', 'delete_live_battle' );
			$wp_roles->add_cap( 'administrator', 'publish_live_battles' );
			$wp_roles->add_cap( 'administrator', 'edit_live_battles' );
			$wp_roles->add_cap( 'administrator', 'edit_others_live_battles' );
			$wp_roles->add_cap( 'administrator', 'delete_live_battles' );
			$wp_roles->add_cap( 'administrator', 'delete_others_live_battles' );
			$wp_roles->add_cap( 'administrator', 'read_private_live_battles' );
			$wp_roles->add_cap( 'administrator', LiveBattle::SET_LIVE_BATTLE_AS_READY_CAP );
		}
	}
}
