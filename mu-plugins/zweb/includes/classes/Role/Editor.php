<?php

namespace Zweb\Role;

/**
 * Class Editor
 *
 * @package Zweb\Role
 */
class Editor {

	const ROLE_NAME = 'editor';

	/**
	 *  Register actions.
	 */
	public static function register() {
		add_filter( 'editable_roles', [ 'Zweb\Role\Editor', 'editable_roles' ] );
		add_filter( 'map_meta_cap', [ 'Zweb\Role\Editor', 'map_meta_cap' ], 10, 4 );

		$capabilities_to_add = [
			'delete_users',
			'create_users',
			'edit_users',
			'list_users',
			'add_users',
		];
		if ( get_option( 'editor_role_version' ) !== ZWEB_VERSION ) {
			$role = get_role( self::ROLE_NAME );
			foreach ( $capabilities_to_add as $cap ) {
				$role->add_cap( $cap );
			}
			update_option( 'editor_role_version', ZWEB_VERSION );
		}
	}

	/**
	 * @param \WP_User $user
	 *
	 * @return bool
	 */
	public static function is_user_editor( \WP_User $user ) {
		return in_array( self::ROLE_NAME, (array) $user->roles );
	}

	/**
	 * Get allowed role per user.
	 *
	 * @param $user
	 *
	 * @return array
	 */
	public static function get_allowed_roles( $user ) {
		if ( self::is_user_editor( $user ) ) {
			return [ Influencer::ROLE_NAME, self::ROLE_NAME ];
		}
		return array_keys( $GLOBALS['wp_roles']->roles );
	}

	/**
	 * Get roles which the current user can edit.
	 *
	 * @param $roles
	 *
	 * @return mixed
	 */
	public static function editable_roles( $roles ) {
		if ( $user = wp_get_current_user() ) {
			$allowed = self::get_allowed_roles( $user );
			foreach ( $roles as $role => $caps ) {
				if ( ! in_array( $role, $allowed ) ) {
					unset( $roles[ $role ] );
				}
			}
		}
		return $roles;
	}

	/**
	 * Prevent users deleting/editing users with a role outside their allowance.
	 *
	 * @param $caps
	 * @param $cap
	 * @param $user_ID
	 * @param $args
	 *
	 * @return mixed
	 */
	public static function map_meta_cap( $caps, $cap, $user_ID, $args ) {
		if ( ( $cap === 'edit_user' || $cap === 'delete_user' ) && $args ) {
			$the_user = get_userdata( $user_ID ); // The user performing the task
			$user     = get_userdata( $args[0] ); // The user being edited/deleted

			if ( $the_user && $user && $the_user->ID != $user->ID /* User can always edit self */ ) {
				$allowed = self::get_allowed_roles( $the_user );

				if ( array_diff( $user->roles, $allowed ) ) {
					// Target user has roles outside of our limits
					$caps[] = 'not_allowed';
				}
			}
		}

		return $caps;
	}
}
