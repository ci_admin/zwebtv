<?php

namespace Zweb\Admin;

/**
 * Class LiveStreaming
 *
 * @package Zweb\Admin
 */
class LiveStreaming {

	/**
	 * Register menu actions & hooks.
	 *
	 * @return void
	 */
	public static function register() {
		if ( function_exists( 'fm_register_submenu_page' ) ) {
			fm_register_submenu_page( 'zweb_options_live_streaming', 'options-general.php', 'ZWeb Live Streaming Settings', 'ZWeb Live Streaming' );
			add_action(
				'fm_submenu_zweb_options_live_streaming',
				[
					__CLASS__,
					'option_fields',
				]
			);
		}
	}

	/**
	 * Setup custom submenu lives treaming settings page.
	 *
	 * @return void
	 */
	public static function option_fields() {

		$fields['live_streaming'] = new \Fieldmanager_Group(
			[
				'label'          => 'Live Streaming',
				'name'           => 'live_streaming',
				'serialize_data' => true,
				'add_to_prefix'  => false,
				'children'       => [
					'vonage-api-key'          => new \Fieldmanager_TextField(
						[
							'name'  => 'vonage-api-key',
							'label' => __( 'Vonage API key', 'zweb' ),
						]
					),
					'vonage-secret'           => new \Fieldmanager_TextField(
						[
							'name'  => 'vonage-secret',
							'label' => __( 'Vonage API Secret', 'zweb' ),
						]
					),
					'brightcove-live-api-key' => new \Fieldmanager_TextField(
						[
							'name'  => 'brightcove-live-api-key',
							'label' => __( 'Brightcove Live API key', 'zweb' ),
						]
					),
				],
			]
		);

		$option_fields = new \Fieldmanager_Group(
			[
				'name'     => 'zweb_options_live_streaming',
				'children' => $fields,
			]
		);
		$option_fields->activate_submenu_page();
	}

	/**
	 * Get settings
	 *
	 * @return array
	 */
	public static function get_live_streaming_settings() {
		return get_option( 'zweb_options_live_streaming', [] );
	}

	/**
	 * Get key.
	 *
	 * @return mixed
	 *
	 * @throws \Exception
	 */
	public static function get_bc_live_api_key() {
		$options = self::get_live_streaming_settings();
		if ( isset( $options['live_streaming']['brightcove-live-api-key'] ) && $options['live_streaming']['brightcove-live-api-key'] ) {
			return $options['live_streaming']['brightcove-live-api-key'];
		}

		throw new \Exception( 'Brightcove live API key not set' );
	}

	/**
	 * Get vonage credentials
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */
	public static function get_vonage_key_secret() {
		$options = self::get_live_streaming_settings();
		if (
			isset( $options['live_streaming']['vonage-api-key'] ) && $options['live_streaming']['vonage-api-key'] &&
			isset( $options['live_streaming']['vonage-secret'] ) && $options['live_streaming']['vonage-secret']
		) {
			return [
				'api_key'    => $options['live_streaming']['vonage-api-key'],
				'api_secret' => $options['live_streaming']['vonage-secret'],
			];
		}

		throw new \Exception( 'Vonage Keys not set' );
	}
}
