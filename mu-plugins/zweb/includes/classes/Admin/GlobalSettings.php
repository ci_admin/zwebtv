<?php

namespace Zweb\Admin;

/**
 * Class GlobalSettings
 *
 * @package Zweb\Admin
 */
class GlobalSettings {

	/**
	 * Register menu actions & hooks.
	 *
	 * @return void
	 */
	public static function register() {
		if ( function_exists( 'fm_register_submenu_page' ) ) {
			fm_register_submenu_page( 'zweb_global_options', 'options-general.php', 'ZWeb Site Settings', 'ZWeb Site Settings' );
			add_action(
				'fm_submenu_zweb_global_options',
				[
					__CLASS__,
					'option_fields',
				]
			);
		}
	}

	/**
	 * Setup Global settings page.
	 *
	 * @return void
	 */
	public static function option_fields() {

		$fields['control_room_options'] = new \Fieldmanager_Group(
			[
				'label'          => 'Live Battle Control Room Settings',
				'name'           => 'control_room_options',
				'serialize_data' => true,
				'add_to_prefix'  => false,
				'children'       => [
					'control_room_video_id' => new \Fieldmanager_TextField(
						[
							'label' => esc_html__( 'Control Room Video ID', 'zweb' ),
						]
					),
				],
			]
		);

		$option_fields = new \Fieldmanager_Group(
			[
				'name'     => 'zweb_global_options',
				'children' => $fields,
			]
		);

		$option_fields->activate_submenu_page();
	}
}
