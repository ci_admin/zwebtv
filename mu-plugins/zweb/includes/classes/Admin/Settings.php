<?php

namespace Zweb\Admin;

use Zweb\PostType\Video;
use Zweb\Taxonomy\Influencer;
use Zweb\Taxonomy\VideoCategory;
use TenUp\ContentTypes\Fields as TenupFields;

/**
 * Class for the Settings page in the admin.
 *
 * @package Zweb\Admin
 */
class Settings {

	/**
	 * Register menu actions & hooks.
	 *
	 * @return void
	 */
	public static function register() {
		if ( function_exists( 'fm_register_submenu_page' ) ) {
			fm_register_submenu_page( 'zweb_options', 'options-general.php', 'ZWeb Settings', 'ZWeb' );
			add_action( 'fm_submenu_zweb_options', [
				__CLASS__,
				'option_fields',
			] );
		}
	}

	/**
	 * @return string
	 */
	public static function get_current_application_state() {
		return self::get_site_settings()['live']['state'];
	}

	/**
	 * @return string
	 */
	public static function get_annnouncement_text() {
		return self::get_site_settings()['live']['announcement-text'];
	}

	/**
	 * Setup custom submenu settings page.
	 *
	 * @return void
	 */
	public static function option_fields() {

		$fields['homepage'] = new \Fieldmanager_Group( [
			'label'          => 'Homepage',
			'name'           => 'homepage',
			'serialize_data' => true,
			'add_to_prefix'  => false,
			'children'       => self::get_homepage_fields(),
		] );

		$fields['sitewide'] = new \Fieldmanager_Group( [
			'label'          => 'General',
			'name'           => 'sitewide',
			'serialize_data' => true,
			'add_to_prefix'  => false,
			'children'       => self::get_sitewide_fields(),
		] );

		$fields['live'] = new \Fieldmanager_Group( [
			'label'          => 'Live settings',
			'name'           => 'live',
			'serialize_data' => true,
			'add_to_prefix'  => false,
			'children'       => self::get_live_fields(),
		] );

		$fields['apppromo'] = new \Fieldmanager_Group( [
			'label'          => 'App Promo',
			'name'           => 'apppromo',
			'serialize_data' => true,
			'add_to_prefix'  => false,
			'children'       => self::get_promo_fields(),
		] );

		$option_fields = new \Fieldmanager_Group( [
			'name'     => 'zweb_options',
			'children' => $fields,
		] );
		$option_fields->activate_submenu_page();
	}

	/**
	 * Get fields for the live section.
	 *
	 * @return array
	 */
	public static function get_live_fields() {
		return [
			'state'                 => new \Fieldmanager_Radios( [
				'name'          => 'state',
				'label'         => __( 'Current state of the website', 'zweb' ),
				'options'       => [
					'no-live'      => 'No Live',
					'announcement' => 'Announcement',
					'pre-live'     => 'Pre Live',
					'live'         => 'Live',
					'post-live'    => 'Post Live',
				],
				'default_value' => 'no-live',
			] ),
			'announcement-text'     => new \Fieldmanager_TextField( [
				'name'        => 'announcement-text',
				'label'       => __( 'Announcement Text', 'zweb' ),
				'description' => __( 'Enter the text that will appear on the homepage in the "Announcement" state', 'zweb' ),
			] ),
			'pre-live-text'         => new \Fieldmanager_TextField( [
				'name'  => 'pre-live-text',
				'label' => __( 'Pre Live Text', 'zweb' ),
			] ),
			'live-text'             => new \Fieldmanager_TextField( [
				'name'  => 'live-text',
				'label' => __( 'Live Text', 'zweb' ),
			] ),
			'post-live-text'        => new \Fieldmanager_TextField( [
				'name'  => 'post-live-text',
				'label' => __( 'Post Live Text', 'zweb' ),
			] ),
			'pre-live-image'        => new \Fieldmanager_Media( [
				'name'  => 'pre-live-image',
				'label' => __( 'Pre Live image', 'zweb' ),
			] ),
			'pre-live-image-title'  => new \Fieldmanager_TextArea( [
				'name'  => 'pre-live-image-title',
				'label' => __( 'Pre Live image title', 'zweb' ),
			] ),
			'pre-live-image-text'   => new \Fieldmanager_TextArea( [
				'name'  => 'pre-live-image-text',
				'label' => __( 'Pre Live image text', 'zweb' ),
			] ),
			'post-live-image'       => new \Fieldmanager_Media( [
				'name'  => 'post-live-image',
				'label' => __( 'Post Live image', 'zweb' ),
			] ),
			'post-live-image-title' => new \Fieldmanager_TextArea( [
				'name'  => 'post-live-image-title',
				'label' => __( 'Post Live image title', 'zweb' ),
			] ),
			'post-live-image-text'  => new \Fieldmanager_TextArea( [
				'name'  => 'post-live-image-text',
				'label' => __( 'Post Live image text', 'zweb' ),
			] ),
		];
	}

	/**
	 * Get sitewide option fields.
	 *
	 * @return array $fields  List of FM fields.
	 */
	public static function get_sitewide_fields() {
		$fields = [
			'top_10'                  => new TenupFields\Select2( [
				'label'           => 'Top 10',
				'name'            => 'top_10',
				'multiple'        => true,
				'use_custom_list' => true,
				'show_view_link'  => false,
				'select_limit'    => 10,
				'description'     => 'Start by typing at least three characters of the video title to search.',
				'datasource'      => new \Fieldmanager_Datasource_Post( [
					'query_args' => [
						'post_type' => [ Video::POST_TYPE_NAME ],
					],
				] ),
			] ),
			'da_non_perdere'          => new TenupFields\Select2( [
				'label'           => 'Da Non Perdere',
				'name'            => 'da_non_perdere',
				'multiple'        => true,
				'use_custom_list' => true,
				'show_view_link'  => false,
				'select_limit'    => 10,
				'description'     => 'Start by typing at least three characters of the video title to search.',
				'datasource'      => new \Fieldmanager_Datasource_Post( [
					'query_args' => [
						'post_type' => [ Video::POST_TYPE_NAME ],
					],
				] ),
			] ),
			'list_cats'               => new TenupFields\Select2( [
				'label'           => 'List of Categories',
				'name'            => 'list_cats',
				'multiple'        => true,
				'use_custom_list' => true,
				'show_view_link'  => false,
				'description'     => 'Start by typing at least three characters of the category to search.',
				'datasource'      => new \Fieldmanager_Datasource_Term( [
					'taxonomy'      => VideoCategory::TAXONOMY_NAME,
					'taxonomy_args' => [
						'hide_empty' => false,
					],
				] ),
			] ),
			'influencers'             => new TenupFields\Select2( [
				'label'           => 'Influencers',
				'name'            => 'influencers',
				'multiple'        => true,
				'use_custom_list' => true,
				'show_view_link'  => false,
				'description'     => 'Start by typing at least three characters of the influencer to search.',
				'datasource'      => new \Fieldmanager_Datasource_Term( [
					'taxonomy'      => Influencer::TAXONOMY_NAME,
					'taxonomy_args' => [
						'hide_empty' => false,
					],
				] ),
			] ),
			'email-for-notifications' => new \Fieldmanager_TextField( [
				'name'  => 'email-for-notifications',
				'label' => __( 'Email that receive notifications when video is uploaded', 'zweb' ),
			] ),
		];

		return $fields;
	}

	/**
	 * Get homepage option fields.
	 *
	 * @return array $fields  List of FM fields.
	 */
	public static function get_homepage_fields() {
		$fields = [
			'featured_influencer' => new TenupFields\Select2( [
				'label'       => 'Featured Influencer',
				'name'        => 'featured_influencer',
				'multiple'    => false,
				'description' => 'Start by typing at least three characters of the influencer to search.',
				'datasource'  => new \Fieldmanager_Datasource_Term( [
					'taxonomy'      => Influencer::TAXONOMY_NAME,
					'taxonomy_args' => [
						'hide_empty' => false,
					],
				] ),
			] ),
			'featured_video'      => new TenupFields\Select2( [
				'label'       => 'Featured Video',
				'name'        => 'featured_video',
				'multiple'    => false,
				'description' => 'Start by typing at least three characters of the video to search.',
				'datasource'  => new \Fieldmanager_Datasource_Post( [
					'query_args' => [
						'post_type' => [ Video::POST_TYPE_NAME ],
					],
				] ),
			] ),
		];

		return $fields;
	}

	/**
	 * Get fields for the app promo section.
	 *
	 * @return array
	 */
	public static function get_promo_fields() {
		return [
			'app-promo-image'       => new \Fieldmanager_Media( [
				'name'  => 'app-promo-image',
				'label' => __( 'App promo image', 'zweb' ),
			] ),
			'app-promo-image-title' => new \Fieldmanager_TextArea( [
				'name'  => 'app-promo-image-title',
				'label' => __( 'App promo image title', 'zweb' ),
			] ),
			'app-promo-image-text'  => new \Fieldmanager_TextArea( [
				'name'  => 'app-promo-image-text',
				'label' => __( 'App promo image text', 'zweb' ),
			] ),
			'app-promo-image-url'   => new \Fieldmanager_TextField( [
				'name'  => 'app-promo-image-url',
				'label' => __( 'App promo image link', 'zweb' ),
			] ),
		];
	}

	/**
	 * Get site settings
	 *
	 * @return array
	 */
	public static function get_site_settings() {
		return get_option( 'zweb_options', [] );
	}

	/**
	 * Get array of curated post ids
	 *
	 * @param $section
	 *
	 * @return array
	 */
	public static function get_curated_section( $section ) {
		$site_settings    = Settings::get_site_settings();
		$curated_post_ids = $site_settings['sitewide'][ $section ] ?? [];

		return $curated_post_ids;
	}
}
