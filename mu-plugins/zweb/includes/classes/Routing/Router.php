<?php

namespace Zweb\Routing;

use Zweb\Firebase\Firestore;
use Zweb\GraphQL\Mutations\StopBroadcast;
use Zweb\OpenTok\Wrapper;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\Routing\Route\BcNotifications;

/**
 * Class Router
 *
 * @package Zweb\Routing
 */
class Router {

	/**
	 * Register.
	 */
	public static function register() {
		add_filter( 'wp', [ static::class, 'perform_action' ] );
		add_filter( 'query_vars', [ static::class, 'query_vars' ] );
		self::add_rewrites();
	}

	/**
	 * Perform custom routing
	 *
	 * @param object $wp
	 */
	public static function perform_action( $wp ) {
		if ( get_query_var( 'live_battle_id' ) ) {
			$post_id = get_query_var( 'live_battle_id' );
			$action  = get_query_var( 'action' );
			switch ( $action ) {
				case 'set_ready':
					if ( current_user_can( LiveBattle::SET_LIVE_BATTLE_AS_READY_CAP ) && check_admin_referer( "set_ready_live_battle_$post_id" ) ) {
						update_post_meta( (int) $post_id, 'live_video_status', 'ready' );
						$firestore = Firestore::get_instance();
						$data      = [
							'status' => 'ready',
						];
						LiveBattle::update_firebase_document( $firestore, (int) $post_id, $data );
					}

					break;
				case 'notification':
					BcNotifications::dispatch();

					break;
				case 'set_finished':
					if ( current_user_can( LiveBattle::SET_LIVE_BATTLE_AS_READY_CAP ) && check_admin_referer( "set_finished_live_battle_$post_id" )
					) {
						$live_session_id = get_post_meta( $post_id, 'live_session_id', true );
						$broadcast_id    = get_post_meta( $post_id, 'broadcast_id', true );
						try {
							StopBroadcast::update_post_and_firebase( get_post( $post_id ), true, 'wpadmin' );
							StopBroadcast::stop_broadcasting_opentok( $broadcast_id );
							StopBroadcast::stop_live_streaming_on_brightcove( $live_session_id );
							self::kick_users_out_of_sessions( $post_id );
						} catch ( \Exception $exception ) {
							error_log( 'Error when stopping Broadcast. Message: ' . $exception->getMessage() );
						}
					}
					break;
			}
			wp_safe_redirect( admin_url( 'edit.php?post_type=' . LiveBattle::POST_TYPE_NAME ) );

			die();
		}
		if ( get_query_var( 'bc_video_id' ) ) {
			BcNotifications::dispatch( get_query_var( 'bc_video_id' ), get_query_var( 'post_type' ), get_query_var( 'token' ) );
			die();
		}
	}

	/**
	 * Add custom rewrite rules.
	 */
	public static function add_rewrites() {
		add_rewrite_rule( '^wp-admin/set-live-battle-as-ready/([0-9:]+)/?', 'index.php?live_battle_id=$matches[1]&action=set_ready', 'top' );
		add_rewrite_rule( '^wp-admin/set-live-battle-as-finished/([0-9:]+)/?', 'index.php?live_battle_id=$matches[1]&action=set_finished', 'top' );
		add_rewrite_rule(
			'^bc-live-video-notification/([0-9:]+)/([a-z0-9-]+)/?',
			'index.php?bc_video_id=$matches[1]&token=$matches[2]&action=notification&post_type=' . LiveVideo::POST_TYPE_NAME,
			'top'
		);
		add_rewrite_rule(
			'^bc-live-battle-notification/([0-9:]+)/([a-z0-9-]+)/?',
			'index.php?bc_video_id=$matches[1]&token=$matches[2]&action=notification&post_type=' . LiveBattle::POST_TYPE_NAME,
			'top'
		);
	}

	/**
	 * Add query vars.
	 *
	 * @param array $vars
	 *
	 * @return mixed
	 */
	public static function query_vars( $vars ) {
		$vars[] = 'live_battle_id';
		$vars[] = 'bc_video_id';
		$vars[] = 'post_type';
		$vars[] = 'action';
		$vars[] = 'token';

		return $vars;
	}

	/**
	 * @param int $post_id
	 */
	public static function kick_users_out_of_sessions( $post_id ) {
		$firestore     = Firestore::get_instance();
		$fs_client     = $firestore->get_firestore_client_instance();
		$opentok       = Wrapper::get_instance();
		$waiting_rooms = $fs_client->listDocuments(
			"liveBattles/$post_id/waitingRooms",
			[
				'pageSize' => 100,
			]
		);

		if ( $waiting_rooms['documents'] ) {
			foreach ( $waiting_rooms['documents'] as $document ) {
				try {
					$session_id = $document->get( 'sessionId' );
					$opentok->signal(
						$session_id,
						[
							'data' => '',
							'type' => 'disconnectFromWaitingRoom',
						]
					);
				} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
					// do nothing
				}
			}
		}
		$live_rooms = $fs_client->listDocuments(
			"liveBattles/$post_id/liveRooms",
			[
				'pageSize' => 100,
			]
		);

		if ( $live_rooms['documents'] ) {
			foreach ( $live_rooms['documents'] as $document ) {
				try {
					$session_id = $document->get( 'sessionId' );
					$opentok->signal(
						$session_id,
						[
							'data' => '',
							'type' => 'disconnectFromLive',
						]
					);
				} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
					// do nothing
				}
			}
		}
	}
}
