<?php

namespace Zweb\Routing\Route;

use Zweb\Firebase\Firestore;
use Zweb\GraphQL\Cache\GraphQLCache;
use Zweb\PostType\LiveVideo;
use Zweb\Traits\FirestoreVideo;

/**
 * Class BcNotifications
 *
 * @package Zweb\Routing\Route
 */
class BcNotifications {

	use FirestoreVideo;

	/**
	 * Get url for live job
	 *
	 * @param string $bc_video_id
	 *
	 * @return string
	 */
	public static function get_live_video_notifications_url( $bc_video_id ) {
		return get_site_url( null, "/bc-live-video-notification/$bc_video_id" );
	}

	/**
	 * Get url for live job
	 *
	 * @param string $bc_video_id
	 *
	 * @return string
	 */
	public static function get_live_battle_notifications_url( $bc_video_id ) {
		return get_site_url( null, "/bc-live-battle-notification/$bc_video_id" );
	}

	/**
	 * Get random token
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function generate_random_string() {
		$bytes = random_bytes( 16 );

		return bin2hex( $bytes );
	}

	/**
	 * Dispatch action
	 *
	 * @param string $bc_video_id
	 * @param string $post_type
	 * @param string $token
	 */
	public static function dispatch( $bc_video_id, $post_type, $token ) {
		try {
			$json    = file_get_contents( 'php://input' );
			$decoded = json_decode( $json, true );

			if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
				error_log( $bc_video_id . ' - ' . $decoded['event'] . ' - ' . $decoded['job']['state'] . ' - ' . $post_type . ' - ' . $token );
			}

			$args = [
				'posts_per_page' => 1,
				'post_type'      => $post_type,
				'meta_query'     => [
					[
						'key'     => 'bc_video_id',
						'value'   => (string) $bc_video_id,
						'compare' => '=',
					],
					[
						'key'     => 'token',
						'value'   => (string) $token,
						'compare' => '=',
					],
				],
			];

			$query = new \WP_Query( $args );
			$video = $query->get_posts();

			if ( $video ) {
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
					error_log( 'Video found: ' . $video->ID );
				}

				$video = $video[0];
				self::update_post_and_firebase( $video, $decoded['job']['state'] );
			}
		} catch ( \Exception $e ) {
			error_log( $e->getMessage() );
		}
	}


	/**
	 * Update status on WordPress and Firebase
	 *
	 * @param \WP_Post $post
	 * @param string $status
	 */
	public static function update_post_and_firebase( \WP_Post $post, string $status ) {
		update_post_meta( $post->ID, 'bc_video_status', $status );
		self::sync_to_firestore( $post );
		GraphQLCache::invalidate_cache();
	}
}
