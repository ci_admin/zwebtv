<?php

namespace Zweb\PostType;

use MrShan0\PHPFirestore\Fields\FirestoreReference;
use Zweb\Brightcove\BCIngestionApi;
use Zweb\Firebase\Firestore;
use Zweb\Multilingual\GoogleTranslate;
use Zweb\Notifications\VideoEvents;
use Zweb\Taxonomy\Influencer;
use Zweb\Traits\FirestoreVideo;

/**
 * Class LiveBattle
 *
 * @package Zweb\PostType
 */
class LiveBattle {
	use FirestoreVideo;

	const POST_TYPE_NAME = 'zweb-live-battle';

	const SET_LIVE_BATTLE_AS_READY_CAP = 'set_live_battle_as_ready';

	/**
	 * Register Battle CPT
	 */
	public static function register() {

		add_action(
			'save_post_' . self::POST_TYPE_NAME,
			[ static::class, 'update_translation_cache' ],
			10,
			3
		);

		add_action(
			'save_post_' . self::POST_TYPE_NAME,
			[ static::class, 'update_postmeta' ],
			10,
			3
		);

		add_filter(
			'post_row_actions',
			[ static::class, 'add_ready_link' ],
			10,
			2
		);

		add_filter(
			'display_post_states',
			[ static::class, 'display_post_states' ],
			10,
			2
		);

		add_action(
			'wp_trash_post',
			[ static::class, 'delete_from_firestore' ]
		);

		self::register_post_type();
	}

	/**
	 * Add link to mark battle as ready.
	 *
	 * @param array $actions
	 * @param \WP_Post $post
	 *
	 * @return array
	 */
	public static function add_ready_link( array $actions, \WP_Post $post ) {
		$post_id = $post->ID;
		if (
			self::POST_TYPE_NAME === $post->post_type &&
			current_user_can( self::SET_LIVE_BATTLE_AS_READY_CAP )
		) {
			$status = get_post_meta( $post_id, 'live_video_status', true );
			if ( 'scheduled' === $status ) {
				$href             = wp_nonce_url( "set-live-battle-as-ready/$post_id", "set_ready_live_battle_$post_id" );
				$actions['ready'] = "<a href='$href'>Ready</a>";
			}
			if ( 'streaming' === $status ) {
				$href             = wp_nonce_url( "set-live-battle-as-finished/$post_id", "set_finished_live_battle_$post_id" );
				$actions['streaming'] = "<a href='$href'>Stop Live Battle</a>";
			}
			if ( 'ready' === $status ) {
				$href                       = "https://zwebtv-f8ab1.web.app/b/$post_id";
				$actions['view_admin_link'] = "<a target='_blank' href='$href'>View Admin Link</a>";
			}
		}

		return $actions;
	}

	/**
	 * Show the status in the post admin list.
	 *
	 * @param array $post_states
	 * @param \WP_Post $post
	 *
	 * @return array
	 */
	public static function display_post_states( array $post_states, \WP_Post $post ) {
		if ( self::POST_TYPE_NAME === $post->post_type ) {
			unset( $post_states['scheduled'] );
			$status                           = get_post_meta( $post->ID, 'live_video_status', true );
			$post_states['live_battle_state'] = ucfirst( $status );
		}

		return $post_states;
	}

	/**
	 * Update the cache for the video title translation.
	 *
	 * @param int $post_id
	 *
	 * @return void
	 */
	public static function update_translation_cache( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$translation = GoogleTranslate::register();
		$field       = 'title_';
		$text        = get_the_title( $post_id );
		$translation->post_translation_update( $post_id, $field, $text );
	}


	/**
	 * @param int $post_id
	 * @param \WP_Post $post
	 * @param bool $update
	 *
	 * @throws \Exception
	 */
	public static function update_postmeta( int $post_id, \WP_Post $post, bool $update ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		if ( 'publish' !== $post->post_status ) {
			return;
		}
		// Set status as scheduled only on post creation.
		if ( ! $update || ! get_post_meta( $post_id, 'live_video_status', true ) ) {
			update_post_meta( $post_id, 'live_video_status', 'scheduled' );
		}
		// Set bc_video_status as scheduled only on post creation.
		if ( ! $update || ! get_post_meta( $post_id, 'bc_video_status', true ) ) {
			update_post_meta( $post_id, 'bc_video_status', 'scheduled' );
		}

		if ( ! get_post_meta( $post_id, 'bc_video_id', true ) ) {
			$cms_api = new BCIngestionApi();
			$video   = $cms_api->video_add( $post->post_title );
			if ( ! $video ) {
				throw new \Exception( 'Something went wrong when creating the video on Brightcove' );
			}
			update_post_meta( $post_id, 'bc_video_id', $video['id'] );
		}

		update_post_meta( $post_id, 'timezone', get_option( 'timezone_string' ) );

		$firestore   = Firestore::get_instance();
		$influencers = get_the_terms( $post, Influencer::TAXONOMY_NAME );
		if ( $influencers ) {
			$influencer = Influencer::get_user_from_term( $influencers[0]->term_id );
			$uid        = get_user_meta( $influencer->ID, 'zweb-influencer_firebase-uid', true );
		}

		$data = [
			'name'       => $post->post_title,
			'status'     => get_post_meta( $post_id, 'live_video_status', true ),
			'date'       => get_post_meta( $post_id, 'scheduledTime', true ) / 1000, // Javascript timestamp is * 1000
			'influencer' => isset( $uid ) ? new FirestoreReference( "/user/{$uid}" ) : '',
		];

		if ( $update ) {
			self::update_firebase_document( $firestore, $post_id, $data );
		} else {
			self::create_firebase_document( $firestore, $post_id, $data );
		}

		if ( $influencer && ! get_post_meta( $post_id, 'notification_sent', true ) ) {
			// Send the notification to firestore
			VideoEvents::live_video_notification(
				$post_id,
				get_post_meta( $post_id, 'scheduledTime', true ) / 1000,
				get_post_meta( $post_id, 'timezone', true ),
				VideoEvents::LIVE_BATTLE_SCHEDULED
			);
			update_post_meta( $post_id, 'notification_sent', true );
		}

		// sync to videos collection
		self::sync_to_firestore( $post );
	}

	/**
	 * Register the post type
	 */
	public static function register_post_type() {
		register_post_type(
			self::POST_TYPE_NAME,
			[
				'labels'              => [
					'name'               => __( 'Live Battles', 'zweb' ),
					'singular_name'      => __( 'Live Battle', 'zweb' ),
					'all_items'          => __( 'All Live Battles', 'zweb' ),
					'add_new_item'       => __( 'Add New Live Battle', 'zweb' ),
					'edit_item'          => __( 'Edit Live Battle', 'zweb' ),
					'new_item'           => __( 'New Live Battle', 'zweb' ),
					'view_item'          => __( 'View Live Battle', 'zweb' ),
					'search_items'       => __( 'Search Live Battles', 'zweb' ),
					'not_found'          => __( 'No Live Battles found.', 'zweb' ),
					'not_found_in_trash' => __( 'No Live Battles found in Trash.', 'zweb' ),
					'parent_item_colon'  => __( 'Parent Live Battle:', 'zweb' ),
				],
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_rest'        => true,
				'capability_type'     => [ 'live_battle', 'live_battles' ],
				'rewrite'             => [
					'slug' => 'live_battle',
				],
				'supports'            => [
					'title',
					'editor',
					'thumbnail',
					'excerpt',
					'custom-fields',
				],
				'menu_icon'           => 'dashicons-media-video',
				'template'            => [
					[
						'bc/brightcove',
					],
				],
				'template_lock'       => 'all',
				'show_in_graphql'     => true,
				'graphql_single_name' => 'liveBattle',
				'graphql_plural_name' => 'liveBattles',
			]
		);

		register_post_meta(
			self::POST_TYPE_NAME,
			'scheduledTime',
			[
				'show_in_rest'  => true,
				'single'        => true,
				'type'          => 'integer',
				'auth_callback' => function () {
					return current_user_can( 'edit_live_battle' );
				},
			]
		);
	}

	/**
	 * @param Firestore $firestore
	 * @param int $post_id
	 * @param array $data
	 */
	public static function update_firebase_document( Firestore $firestore, int $post_id, array $data ) {
		try {
			$firestore_client = $firestore->get_firestore_client_instance();
			$firestore_client->updateDocument(
				"liveBattles/$post_id",
				$data
			);

			return;
		} catch ( \Exception $exc ) {
			error_log( "Something went wrong when updating the document $post_id. message: " . $exc->getMessage() );
		}
		self::create_firebase_document( $firestore, $post_id, $data );
	}

	/**
	 * @param Firestore $firestore
	 * @param int $post_id
	 * @param array $data
	 */
	public static function create_firebase_document( Firestore $firestore, int $post_id, array $data ) {
		try {
			$firestore->create_document(
				'liveBattles',
				$post_id,
				$data
			);
		} catch ( \Exception $exc ) {
			error_log( $exc->getMessage() );
		}
	}
}
