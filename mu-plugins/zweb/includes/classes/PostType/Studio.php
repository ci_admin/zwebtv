<?php

namespace Zweb\PostType;

use Zweb\Role\Influencer;

/**
 * Class Studio
 *
 * @package Zweb\PostType
 */
class Studio {

	const ROLE_NAME = 'studio';

	const POST_TYPE_NAME = 'zweb-studio';

	/**
	 * Register Studio CPT
	 */
	public static function register() {
		self::register_post_type();

		add_filter( 'admin_body_class', [ __CLASS__, 'body_class' ] );

		if ( get_option( 'studio_role_version' ) !== ZWEB_VERSION ) {
			add_role( self::ROLE_NAME, 'Studio', [] );
			update_option( 'studio_role_version', ZWEB_VERSION );
		}
	}

	/**
	 * Register the post type
	 */
	public static function register_post_type() {
		register_post_type(
			self::POST_TYPE_NAME,
			[
				'labels'              => [
					'name'               => __( 'Studios', 'zweb' ),
					'singular_name'      => __( 'Studio', 'zweb' ),
					'all_items'          => __( 'All Studios', 'zweb' ),
					'add_new_item'       => __( 'Add New Studio', 'zweb' ),
					'edit_item'          => __( 'Edit Studio', 'zweb' ),
					'new_item'           => __( 'New Studio', 'zweb' ),
					'view_item'          => __( 'View Studio', 'zweb' ),
					'search_items'       => __( 'Search Studios', 'zweb' ),
					'not_found'          => __( 'No Studios found.', 'zweb' ),
					'not_found_in_trash' => __( 'No Studios found in Trash.', 'zweb' ),
					'parent_item_colon'  => __( 'Parent Studio:', 'zweb' ),
				],
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_rest'        => true,
				'rewrite'             => [
					'slug' => 'studio',
				],
				'supports'            => [
					'title',
					'editor',
					'thumbnail',
					'excerpt',
					'custom-fields',
				],
				'menu_icon'           => 'dashicons-media-video',
				'show_in_graphql'     => true,
				'graphql_single_name' => 'studio',
				'graphql_plural_name' => 'studios',
			]
		);
	}

	/**
	 * Add class to the body when editing an studio in the admin.
	 *
	 * @param string $classes
	 *
	 * @return string
	 */
	public static function body_class( $classes ) {
		$screen = get_current_screen();
		if ( 'user-edit' === $screen->id ) {
			if ( isset( $_GET['user_id'] ) ) {
				$user = new \WP_User( $_GET['user_id'] );
				if ( self::is_user_studio( $user ) ) {
					$classes .= ' role-studio';
				}
			}
		}

		return $classes;
	}

	/**
	 * @param \WP_User $user
	 *
	 * @return bool
	 */
	public static function is_user_studio( \WP_User $user ) {
		return in_array( self::ROLE_NAME, (array) $user->roles );
	}

	/**
	 * @param \WP_User $user
	 *
	 * @return bool|int
	 */
	public static function is_studio_influencer( \WP_User $user ) {
		return get_user_meta( $user->ID, 'zweb-influencer_parent_studio', true );
	}

	/**
	 * Get influencer terms for Studio.
	 *
	 * @param int $studio_id
	 *
	 * @return \WP_Term[]
	 */
	public static function get_influencers_for_studio( $studio_id ) {
		$user_query = new \WP_User_Query(
			[
				'role'       => Influencer::ROLE_NAME,
				'meta_key'   => 'zweb-influencer_parent_studio',
				'meta_value' => $studio_id,
				'number'     => 300,
				'orderby'    => 'name',
				'fields'     => 'ID',
			]
		);
		$term_ids   = [];

		foreach ( $user_query->get_results() as $user_id ) {
			$term_ids[] = get_user_meta( $user_id, \Zweb\Taxonomy\Influencer::META_KEY, true );
		}

		if ( ! $term_ids ) {
			return [];
		}

		$term_query = new \WP_Term_Query(
			[
				'include'    => $term_ids,
				'taxonomy'   => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
				'hide_empty' => false,
			]
		);

		return $term_query->get_terms();
	}
}
