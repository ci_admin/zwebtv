<?php

namespace Zweb\PostType;

use Zweb\Multilingual\GoogleTranslate;
use Zweb\Traits\FirestoreVideo;

/**
 * Class LiveVideo
 *
 * @package Zweb\PostType
 */
class LiveVideo {
	use FirestoreVideo;

	const POST_TYPE_NAME = 'zweb-live-video';

	/**
	 * Register Video CPT
	 */
	public static function register() {
		add_action(
			'rest_after_insert_' . self::POST_TYPE_NAME,
			[
				'Zweb\Brightcove\PostMeta',
				'save_brightcove_data',
			],
			10,
			2
		);

		add_action(
			'save_post_zweb-live-video',
			[ static::class, 'save_post' ],
			10,
			3
		);

		add_action(
			'wp_trash_post',
			[ static::class, 'delete_from_firestore' ]
		);

		self::register_post_type();
	}

	/**
	 * Update the cache for the video title translation.
	 *
	 * @param int $post_id
	 * @param \WP_Post $post
	 * @param bool $update
	 *
	 * @return void
	 */
	public static function save_post( $post_id, \WP_Post $post, bool $update ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		self::update_translation_cache( $post_id );
		self::sync_to_firestore( $post );
	}

	/**
	 * Update the cache for the video title translation.
	 *
	 * @param int $post_id The id of the post.
	 *
	 * @return void
	 */
	public static function update_translation_cache( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$translation = GoogleTranslate::register();
		$field       = 'title_';
		$text        = get_the_title( $post_id );
		$translation->post_translation_update( $post_id, $field, $text );
	}

	/**
	 * Register the post type
	 */
	public static function register_post_type() {
		register_post_type(
			self::POST_TYPE_NAME,
			[
				'labels'              => [
					'name'               => __( 'Live Videos', 'zweb' ),
					'singular_name'      => __( 'Live Video', 'zweb' ),
					'all_items'          => __( 'All Live Videos', 'zweb' ),
					'add_new_item'       => __( 'Add New Live Video', 'zweb' ),
					'edit_item'          => __( 'Edit Live Video', 'zweb' ),
					'new_item'           => __( 'New Live Video', 'zweb' ),
					'view_item'          => __( 'View Live Video', 'zweb' ),
					'search_items'       => __( 'Search Live Videos', 'zweb' ),
					'not_found'          => __( 'No Live Videos found.', 'zweb' ),
					'not_found_in_trash' => __( 'No Live Videos found in Trash.', 'zweb' ),
					'parent_item_colon'  => __( 'Parent Live Video:', 'zweb' ),
				],
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_rest'        => true,
				'rewrite'             => [
					'slug' => 'live',
				],
				'supports'            => [
					'title',
					'editor',
					'thumbnail',
					'excerpt',
					'custom-fields',
				],
				'menu_icon'           => 'dashicons-media-video',
				'template'            => [
					[
						'bc/brightcove',
					],
				],
				'template_lock'       => 'all',
				'show_in_graphql'     => true,
				'graphql_single_name' => 'liveVideo',
				'graphql_plural_name' => 'liveVideos',
			]
		);
	}


}
