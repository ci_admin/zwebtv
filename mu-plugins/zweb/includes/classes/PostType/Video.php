<?php

namespace Zweb\PostType;

use Zweb\PostStatus\ProcessingVideo;
use Zweb\Role\Influencer;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\VideoCategory;
use Zweb\Multilingual\GoogleTranslate;
use Zweb\Traits\FirestoreVideo;

/**
 * Class Video
 *
 * @package Zweb\PostType
 */
class Video {
	use FirestoreVideo;

	const POST_TYPE_NAME = 'zweb-video';

	/**
	 *  Register api hooks
	 */
	public static function register_api() {
		register_meta(
			'post',
			'bc_video_is_processing',
			[
				'object_subtype' => self::POST_TYPE_NAME,
				'show_in_rest'   => true,
				'single'         => true,
			]
		);
	}

	/**
	 * Register Video CPT
	 */
	public static function register() {
		add_action(
			'rest_after_insert_' . self::POST_TYPE_NAME,
			[
				'Zweb\Brightcove\PostMeta',
				'save_brightcove_data',
			],
			10,
			2
		);

		add_filter(
			'display_post_states',
			[ static::class, 'display_post_states' ],
			10,
			2
		);

		add_action(
			'save_post_zweb-video',
			[ static::class, 'save_post' ],
			10,
			3
		);

		self::register_post_type();
	}

	/**
	 * Show the status in the post admin list.
	 *
	 * @param Array $post_states
	 * @param \WP_Post $post
	 *
	 * @return mixed
	 */
	public static function display_post_states( $post_states, $post ) {
		if ( get_post_meta( $post->ID, 'bc_video_is_processing', true ) ) {
			$post_states['processing_video'] = _x( 'Processing Video', 'zweb' );
		}

		return $post_states;
	}

	/**
	 * Update the cache for the video title translation.
	 *
	 * @param int      $post_id
	 * @param \WP_Post $post
	 *
	 * @return void
	 */
	public static function save_post( $post_id, \WP_Post $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		self::update_translation_cache( $post_id );
		self::sync_to_firestore( $post );
	}

	/**
	 * Update the cache for the video title translation.
	 *
	 * @param int $post_id The id of the post.
	 *
	 * @return void
	 */
	public static function update_translation_cache( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$translation = GoogleTranslate::register();
		$field       = 'title_';
		$text        = get_the_title( $post_id );
		$translation->post_translation_update( $post_id, $field, $text );
	}

	/**
	 * Register the post type
	 */
	public static function register_post_type() {
		register_post_type(
			self::POST_TYPE_NAME,
			[
				'labels'              => [
					'name'               => __( 'Videos', 'zweb' ),
					'singular_name'      => __( 'Video', 'zweb' ),
					'all_items'          => __( 'All Videos', 'zweb' ),
					'add_new_item'       => __( 'Add New Video', 'zweb' ),
					'edit_item'          => __( 'Edit Video', 'zweb' ),
					'new_item'           => __( 'New Video', 'zweb' ),
					'view_item'          => __( 'View Video', 'zweb' ),
					'search_items'       => __( 'Search Videos', 'zweb' ),
					'not_found'          => __( 'No videos found.', 'zweb' ),
					'not_found_in_trash' => __( 'No videos found in Trash.', 'zweb' ),
					'parent_item_colon'  => __( 'Parent Video:', 'zweb' ),
				],
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_rest'        => true,
				'rewrite'             => [
					'slug' => 'video',
				],
				'supports'            => [
					'title',
					'editor',
					'thumbnail',
					'excerpt',
					'custom-fields',
				],
				'menu_icon'           => 'dashicons-media-video',
				'template'            => [
					[
						'bc/brightcove',
					],
				],
				'template_lock'       => 'all',
				'show_in_graphql'     => true,
				'graphql_single_name' => 'video',
				'graphql_plural_name' => 'videos',
			]
		);
	}

}
