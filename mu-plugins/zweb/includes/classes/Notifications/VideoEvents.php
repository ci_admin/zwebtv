<?php

namespace Zweb\Notifications;

use Zweb\Firebase\Provider;
use Zweb\Firebase\Firestore;
use MrShan0\PHPFirestore\Fields\FirestoreObject;

/**
 * Class VideoEvents
 *
 * @package Zweb\Notifications
 */
class VideoEvents {

	const LIVE_VIDEO_STARTED = 'LiveVideoStarted';

	const LIVE_VIDEO_SCHEDULED = 'LiveVideoScheduled';

	const LIVE_BATTLE_SCHEDULED = 'LiveBattleScheduled';

	const VIDEO_PUBLISHED = 'VideoPublished';

	const LIVE_BATTLE_STARTED = 'LiveBattleStarted';

	/**
	 * Register actions and filters
	 *
	 * @return void
	 */
	public static function register() {
		add_action( 'transition_post_status', [ static::class, 'video_published' ], 10, 3 );
	}

	/**
	 * Build notification fields for update
	 *
	 * @param string $new_status New post status.
	 * @param string $old_status Old post status.
	 * @param \WP_Post $post Post object.
	 *
	 * @return void
	 */
	public static function video_published( $new_status, $old_status, $post ) {
		if ( 'zweb-video' !== $post->post_type || $old_status === $new_status || 'publish' !== $new_status ) {
			return;
		}

		$video_title = get_the_title( $post->ID );
		try {
			$influencer      = \Zweb\Taxonomy\Influencer::get_influencer_for_video( $post->ID );
			$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
		} catch ( \Exception $e ) {
			// if no influencer is set, do not send notification.
			return;
		}

		$user_name     = $influencer_user->user_login;
		$influencer_id = $influencer->term_id;
		$bc_video_id   = get_post_meta( $post->ID, 'bc_video_id', true );
		$user_email    = $influencer_user->user_email;

		$firestore_fields = [
			'influencerRef' => '',
			'read'          => false,
			'type'          => 'event',
			'subType'       => self::VIDEO_PUBLISHED,
			'dateAdded'     => gmdate( 'Y-m-d H:i:s' ),
			'fromUser'      => '',
			'video'         => new FirestoreObject(
				[
					'bc_video_id' => $bc_video_id,
					'video_title' => $video_title,
					'video_id'    => "video-{$bc_video_id}",
				]
			),
		];

		$firestore_user_fields = [
			'avatar'        => '',
			'userName'      => $user_name,
			'influencer_id' => $influencer_id,
		];

		self::create_firestore_notification( $firestore_fields, $firestore_user_fields, $user_email );

	}

	/**
	 * Process live videos from the mutation
	 *
	 * @param int $post_id Post id.
	 * @param string $date_time Live video start date time.
	 * @param string $time_zone Live video timezone.
	 * @param string $notification_type Notification type name.
	 *
	 * @return void
	 */
	public static function live_video_notification( $post_id, $date_time, $time_zone, $notification_type ) {

		$video_title = get_the_title( $post_id );
		try {
			$influencer      = \Zweb\Taxonomy\Influencer::get_influencer_for_video( $post_id );
			$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
		} catch ( \Exception $e ) {
			// if no influencer is set, do not send notification.
			return;
		}

		$user_name     = $influencer_user->user_login;
		$influencer_id = $influencer->term_id;
		$bc_video_id   = get_post_meta( $post_id, 'bc_video_id', true );
		$user_email    = $influencer_user->user_email;
		$read          = $notification_type === self::LIVE_VIDEO_STARTED || $notification_type === self::LIVE_BATTLE_STARTED;

		if ( ! empty( $time_zone ) ) {
			$date_time_obj = new \DateTime( '@' . $date_time );
			$date_time_obj->setTimeZone( new \DateTimeZone( $time_zone ) );
			$start_date = $date_time_obj->format( 'Y-m-d' );
			$start_time = $date_time_obj->format( 'H:i:s' );
		} else {
			$start_date = gmdate( 'Y-m-d', $date_time );
			$start_time = gmdate( 'H:i:s', $date_time );
		}

		$firestore_fields = [
			'influencerRef' => '',
			'read'          => $read,
			'type'          => 'event',
			'subType'       => $notification_type,
			'dateAdded'     => gmdate( 'Y-m-d H:i:s' ),
			'fromUser'      => '',
			'video'         => new FirestoreObject(
				[
					'bc_video_id'    => $bc_video_id,
					'video_title'    => $video_title,
					'video_id'       => "video-{$bc_video_id}",
					'startDate'      => $start_date,
					'startTime'      => $start_time,
					'startTimeStamp' => $date_time,
				]
			),
		];

		$firestore_user_fields = [
			'avatar'        => '',
			'userName'      => $user_name,
			'influencer_id' => $influencer_id,
		];

		self::create_firestore_notification( $firestore_fields, $firestore_user_fields, $user_email );
	}

	/**
	 * Send notifications to firestore
	 *
	 * @param array $firestore_fields Firestore fields to update.
	 * @param array $firestore_user_fields Firestore user fields to update.
	 * @param string $user_email Influencer email on firestore.
	 *
	 * @return void
	 */
	public static function create_firestore_notification( $firestore_fields, $firestore_user_fields, $user_email ) {
		$instance = Provider::register();

		if ( $instance ) {
			$firebase = $instance->get_firebase_instance();
			$auth     = $firebase->getAuth();

			try {
				$firebase_user = $auth->getUserByEmail( $user_email );
			} catch ( \Exception $e ) {
				$firebase_user = null;
			}

			if ( $firebase_user && isset( $firebase_user->uid ) ) {
				$firestore = Firestore::get_instance();
				try {
					$firestore_user_fields['avatar'] = $firestore->get_document_field( 'users', $firebase_user->uid, 'avatar' );
				} catch ( \Exception $e ) {
					$firestore_user_fields['avatar'] = '';
				}

				$firestore_fields['influencerRef'] = "/users/{$firebase_user->uid}";
				$firestore_fields['fromUser']      = new FirestoreObject( $firestore_user_fields );

				$firestore->create_document( "notifications/{$firebase_user->uid}/messages", null, $firestore_fields );
			}
		}
	}
}
