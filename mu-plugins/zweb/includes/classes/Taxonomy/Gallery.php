<?php

namespace Zweb\Taxonomy;

use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;

/**
 * Class Gallery
 *
 * @package Zweb\Taxonomy
 */
class Gallery extends BaseTaxonomy {

	const TAXONOMY_NAME = 'zweb-gallery-category';

	public static function get_name() {
		return self::TAXONOMY_NAME;
	}

	/**
	 * Register the gallery taxonomy
	 */
	public function register() {
		parent::register();

		register_taxonomy(
			self::get_name(),
			[ Video::POST_TYPE_NAME, LiveVideo::POST_TYPE_NAME ],
			[
				'description'         => __( 'Used to tag videos with the relative gallery.', 'zweb' ),
				'public'              => true,
				'labels'              => self::get_labels( __( 'Gallery', 'zweb' ), __( 'Galleries', 'zweb' ) ),
				'show_in_rest'        => true,
				'rewrite'             => [ 'slug' => 'videos' ],
				'show_in_graphql'     => true,
				'graphql_single_name' => 'gallery',
				'graphql_plural_name' => 'galleries',
			]
		);
	}

	/**
	 * Get all the remaining term in the taxonomy, used for sliders.
	 *
	 * @param int $term_id_to_exclude
	 *
	 * @return array|int
	 */
	public static function get_remaining_terms( $term_id_to_exclude = 0 ) {
		$query = new \WP_Term_Query(
			[
				'taxonomy'       => self::TAXONOMY_NAME,
				'posts_per_page' => 10,
				'exclude'        => [ $term_id_to_exclude ],
			]
		);

		return $query->get_terms();
	}
}
