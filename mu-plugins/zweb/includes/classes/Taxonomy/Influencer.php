<?php

namespace Zweb\Taxonomy;

use Zweb\Admin\Settings;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;
use Zweb\PostType\LiveBattle;
use Zweb\Firebase\Provider;

/**
 * Class Influencer
 *
 * @package Zweb\Taxonomy
 */
class Influencer extends BaseTaxonomy {

	const TAXONOMY_NAME = 'zweb-influencer-category';

	const META_KEY = 'influencer_term';

	/**
	 * @param $term_id
	 * @param $post_type
	 *
	 * @return int|\WP_Post
	 * @throws \Exception
	 */
	public static function get_latest_video_by_influencer( $term_id, $post_type = Video::POST_TYPE_NAME ) {
		$query = new \WP_Query(
			[
				'post_type'      => $post_type,
				'posts_per_page' => 1,
				'post_status'    => 'publish',
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'compare' => 'NOT EXISTS',
					],
				],
				'tax_query'      => [
					[
						'taxonomy' => static::TAXONOMY_NAME,
						'field'    => 'term_id',
						'terms'    => $term_id,
					],
				],
			]
		);
		$posts = $query->get_posts();
		if ( $posts ) {
			return $posts[0];
		}
		throw new \Exception( 'No video from influencer' );
	}

	/**
	 * @param $term_id
	 *
	 * @return \WP_Post
	 *
	 */
	public static function get_next_video_by_influencer( $term_id ) {
		$query = new \WP_Query(
			[
				'post_type'      => Video::POST_TYPE_NAME,
				'posts_per_page' => 1,
				'post_status'    => 'future',
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'compare' => 'NOT EXISTS',
					],
				],
				'tax_query'      => [
					[
						'taxonomy' => static::TAXONOMY_NAME,
						'field'    => 'term_id',
						'terms'    => $term_id,
					],
				],
			]
		);
		$posts = $query->get_posts();
		if ( $posts ) {
			return $posts[0];
		}

		return null;
	}

	/**
	 * Get number of posts by influencer.
	 *
	 * @param $term_id
	 *
	 * @return int
	 */
	public static function get_post_count_by_influencer( $term_id ) {
		$query = new \WP_Query(
			[
				'post_type'      => Video::POST_TYPE_NAME,
				'posts_per_page' => 1,
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'compare' => 'NOT EXISTS',
					],
				],
				'tax_query'      => [
					[
						'taxonomy' => static::TAXONOMY_NAME,
						'field'    => 'term_id',
						'terms'    => $term_id,
					],
				],
			]
		);
		$query->get_posts();

		return $query->found_posts;
	}

	public static function get_name() {
		return self::TAXONOMY_NAME;
	}

	/**
	 * Register influencer taxonomy.
	 */
	public function register() {
		parent::register();

		add_action( 'user_register', [ $this, 'user_register' ] );
		add_action( 'delete_user', [ $this, 'delete_user' ] );
		add_action( 'profile_update', [ $this, 'profile_update' ] );
		add_action( 'user_profile_update_errors', [ $this, 'validate_influencer_registration' ], 10, 3 );

		self::register_taxonomy();
	}

	/**
	 * Update the term name when the user is updated, or create the term if
	 * it's missing.
	 *
	 * @param $user_id
	 */
	public function profile_update( $user_id ) {
		$user = get_user_by( 'id', $user_id );

		if ( \Zweb\Role\Influencer::is_user_influencer( $user ) ) {
			$term_id = get_user_meta( $user_id, self::META_KEY, true );

			if ( $term_id ) {
				wp_update_term( $term_id, self::TAXONOMY_NAME, [ 'name' => $user->data->display_name ] );
			} else {
				$term = wp_insert_term( $user->data->display_name, self::get_name() );

				if ( ! is_wp_error( $term ) ) {
					add_user_meta( $user_id, self::META_KEY, $term['term_id'] );
				}
			}
		}

	}

	/**
	 * When a user is deleted, delete the corresponding taxonomy term.
	 *
	 * @param $user_id
	 */
	public function delete_user( $user_id ) {
		$user = get_user_by( 'id', $user_id );

		if ( \Zweb\Role\Influencer::is_user_influencer( $user ) ) {
			wp_delete_term(
				get_user_meta( $user_id, self::META_KEY, true ),
				self::get_name()
			);
		}
	}

	/**
	 * When a user is created ensure the Influencer name is unique.
	 *
	 * @param $errors
	 * @param $update
	 * @param $user
	 */
	public function validate_influencer_registration( $errors, $update, $user ) {
		if ( ! $update ) {
			if ( in_array( \Zweb\Role\Influencer::ROLE_NAME, (array) $user->role ) ) {
				$display_name = "{$user->first_name}' '{$user->last_name}";
				if ( term_exists( $display_name, self::get_name() ) ) {
					$errors->add( 'user_error', esc_html__( 'Duplicate Influencer - choose a new first name and/ or last name ', 'zwebtv' ) );;
				}
			}
		}
	}

	/**
	 * When a user is created, create the corresponding taxonomy term.
	 *
	 * @param $user_id
	 */
	public function user_register( $user_id ) {
		$user = get_user_by( 'id', $user_id );
		if ( in_array( \Zweb\Role\Influencer::ROLE_NAME, (array) $user->roles ) ) {
			$term = wp_insert_term( $user->data->display_name, self::get_name() );

			if ( ! is_wp_error( $term ) ) {
				add_user_meta( $user_id, self::META_KEY, $term['term_id'] );
			}
		}

	}

	/**
	 * Register the taxonomy
	 */
	public static function register_taxonomy() {
		register_taxonomy(
			self::get_name(),
			[ Video::POST_TYPE_NAME, LiveVideo::POST_TYPE_NAME, LiveBattle::POST_TYPE_NAME ],
			[
				'description'         => __( 'Used to tag videos with the relative influencer.', 'zweb' ),
				'public'              => true,
				'labels'              => self::get_labels( __( 'Influencer', 'zweb' ), __( 'Influencers', 'zweb' ) ),
				'show_in_rest'        => true,
				'rewrite'             => [ 'slug' => 'autore' ],
				'show_in_graphql'     => true,
				'graphql_single_name' => 'influencer',
				'graphql_plural_name' => 'influencers',
				'capabilities'        => [
					'manage_terms' => 'manage_options',
					'edit_terms'   => 'manage_options',
					'delete_terms' => 'manage_options',
					'assign_terms' => 'edit_live_battle',
				],
			]
		);
	}

	/**
	 * Get the user for the term.
	 *
	 * @param $term_id
	 *
	 * @return \WP_User
	 */
	public static function get_user_from_term( $term_id ) {
		$user_query = new \WP_User_Query(
			[
				'meta_key'   => self::META_KEY,
				'meta_value' => $term_id,
			]
		);
		$result     = $user_query->get_results();
		if ( $result ) {
			return $result[0];
		}
		throw new \Exception( 'No user found' );
	}

	/**
	 * Get influencers related to the passed influencer.
	 * Related means that it has the same video category.
	 *
	 * @param \WP_Term $influencer
	 *
	 * @return \WP_Term_Query
	 */
	public static function get_related_influencers_carousel_query( \WP_Term $influencer ) {
		try {
			$influencer_user           = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
			$influencer_video_category = \Zweb\Taxonomy\Influencer::get_influencer_video_category( $influencer_user->ID );
			if ( ! $influencer_video_category ) {
				throw new \Exception( 'No category for influencer.' );
			}
		} catch ( \Exception $exception ) {
			throw new $exception;
		}

		// Get the users in the same category
		$query    = new \WP_User_Query(
			[
				'role'       => \Zweb\Role\Influencer::ROLE_NAME,
				'meta_key'   => 'zweb-influencer_video_category',
				'meta_value' => $influencer_video_category->term_id,
				'exclude'    => array_merge( [ $influencer_user->ID ], self::get_excluded_influencers() ),
				'number'     => 20,
				'fields'     => 'ID',
			]
		);
		$term_ids = [];

		foreach ( $query->get_results() as $user_id ) {
			$term_ids[] = get_user_meta( $user_id, self::META_KEY, true );
		}

		if ( ! $term_ids ) {
			throw new \Exception( 'No related influencer found' );
		}

		return new \WP_Term_Query(
			[
				'include'  => $term_ids,
				'taxonomy' => self::TAXONOMY_NAME,
			]
		);
	}

	/**
	 * Get the query for video carousels.
	 *
	 * @return \WP_Term_Query
	 */
	public static function get_influencer_carousel_query( $search = '' ) {
		if ( $search ) {
			return new \WP_Term_Query(
				[
					'taxonomy'       => self::TAXONOMY_NAME,
					'posts_per_page' => 20,
					'orderby'        => 'date',
					'order'          => 'DESC',
					'hide_empty'     => false,
					'search'         => $search,
					'exclude'        => self::get_excluded_influencers(),
				]
			);
		}
		$site_settings = Settings::get_site_settings();
		$influencers   = $site_settings['sitewide']['influencers'] ?? [];
		if ( $influencers ) {
			return new \WP_Term_Query(
				[
					'taxonomy'       => self::TAXONOMY_NAME,
					'posts_per_page' => 10,
					'orderby'        => 'include',
					'include'        => $influencers,
					'hide_empty'     => false,
				]
			);
		}

		return new \WP_Term_Query(
			[
				'taxonomy'       => self::TAXONOMY_NAME,
				'posts_per_page' => 20,
				'orderby'        => 'date',
				'order'          => 'DESC',
				'hide_empty'     => false,
				'exclude'        => self::get_excluded_influencers(),
			]
		);
	}

	/**
	 * Return the influencer featured on the home page.
	 *
	 * @return \WP_Term
	 * @throws \Exception
	 */
	public static function get_featured_influencer() {
		$options                = Settings::get_site_settings();
		$featured_influencer_id = $options['homepage']['featured_influencer'][0] ?? 0;
		if ( $featured_influencer_id ) {
			$featured_influencer = get_term( $featured_influencer_id, Influencer::TAXONOMY_NAME );
			if ( $featured_influencer instanceof \WP_Term ) {
				return $featured_influencer;
			}
		}
		// Fallback if not set.
		$query = new \WP_Term_Query(
			[
				'taxonomy'       => self::TAXONOMY_NAME,
				'posts_per_page' => 1,
				'orderby'        => 'date',
				'order'          => 'DESC',
				'hide_empty'     => true,
			]
		);
		$terms = $query->get_terms();
		if ( $terms ) {
			return $terms[0];
		}
		throw new \Exception( 'No featured influencer' );
	}

	/**
	 * Get the influencer for the video.
	 *
	 * @param $video_id
	 *
	 * @return \WP_Term
	 * @throws \Exception
	 */
	public static function get_influencer_for_video( $video_id ) {
		$influencers = wp_get_post_terms( $video_id, self::TAXONOMY_NAME );
		if ( $influencers ) {
			return $influencers[0];
		}
		throw new \Exception( 'No influencer for video' );
	}

	/**
	 * Get the video category for the influencer.
	 *
	 * @param $user_id
	 *
	 * @return \WP_Term
	 */
	public static function get_influencer_video_category( $user_id ) {
		$video_category = get_user_meta( $user_id, \Zweb\Role\Influencer::META_KEY . '_video_category', true );
		if ( $video_category ) {
			$term = get_term( $video_category, VideoCategory::TAXONOMY_NAME );
			if ( $term instanceof \WP_Term ) {
				return $term;
			}
		}

		return null;
	}

	/**
	 * Get the influencers to show on the landing page
	 *
	 * @return array[]
	 */
	public static function get_influencers_for_influencer_landing_page() {
		$query       = new \WP_Term_Query(
			[
				'taxonomy'   => self::TAXONOMY_NAME,
				'orderby'    => 'date',
				'order'      => 'DESC',
				'hide_empty' => false,
				'number'     => 200,
				'exclude'    => self::get_excluded_influencers( 'landing' ),
			]
		);
		$terms       = $query->get_terms();
		$influencers = [
			'upcoming' => [],
			'present'  => [],
		];
		foreach ( $terms as $term ) {
			try {
				$influencer_user = Influencer::get_user_from_term( $term->term_id );
				$is_upcoming     = get_user_meta( $influencer_user->ID, 'zweb-influencer_show_as_upcoming', true );
				$is_upcoming ? $influencers['upcoming'][] = $term : $influencers['present'][] = $term;
			} catch ( \Exception $exception ) {

			}
		}
		usort( $influencers['upcoming'],
			function ( $term_a, $term_b ) {
				try {
					$influencer_user = Influencer::get_user_from_term( $term_a->term_id );
					$upcoming_date_a = get_user_meta( $influencer_user->ID, 'zweb-influencer_upcoming_date', true );
					$influencer_user = Influencer::get_user_from_term( $term_b->term_id );
					$upcoming_date_b = get_user_meta( $influencer_user->ID, 'zweb-influencer_upcoming_date', true );
				} catch ( \Exception $exception ) {
					return 0;
				}

				return $upcoming_date_a - $upcoming_date_b;
			} );

		return $influencers;
	}

	/**
	 * Some influencers must not appear in lists, as they are not real
	 * influencer but entities like influencers.
	 *
	 * @return array array of influencer term ids
	 */
	public static function get_excluded_influencers( $page = '' ) {
		$users                 = new \WP_User_Query(
			[
				'role'       => \Zweb\Role\Influencer::ROLE_NAME,
				'meta_key'   => 'zweb-influencer_prevent_showing_in_influencer_page',
				'meta_value' => true,
				'number'     => 200,
				'fields'     => 'ID',
			]
		);
		$influencer_to_exclude = [];
		foreach ( $users->get_results() as $user_id ) {
			$influencer_term_id = get_user_meta( $user_id, 'influencer_term', true );
			if ( $influencer_term_id ) {
				$influencer_to_exclude[] = $influencer_term_id;
			}
		}
		if ( $page !== 'landing' ) {
			$users = new \WP_User_Query(
				[
					'role'       => \Zweb\Role\Influencer::ROLE_NAME,
					'meta_key'   => 'zweb-influencer_show_as_upcoming',
					'meta_value' => true,
					'number'     => 200,
					'fields'     => 'ID',
				]
			);
			foreach ( $users->get_results() as $user_id ) {
				$influencer_term_id = get_user_meta( $user_id, 'influencer_term', true );
				if ( $influencer_term_id ) {
					$influencer_to_exclude[] = $influencer_term_id;
				}
			}
		}

		return $influencer_to_exclude;
	}

	/**
	 * Get the influencer from the firebase uid.
	 *
	 * @param string $firebase_uid Firebase user id.
	 *
	 * @return string|null
	 */
	public static function get_influencer_from_uid( $firebase_uid ) {
		$influencer_user = get_users(
			[
				'role'       => 'influencer',
				'meta_query' => [
					[
						'key'     => 'zweb-influencer_firebase-uid',
						'compare' => 'EXISTS',
					],
					[
						'key'     => 'zweb-influencer_firebase-uid',
						'value'   => $firebase_uid,
						'compare' => '=',
					],
				],
			]
		);

		// If the user does not have the firebase uid stored in user meta then get it from firebase and update the user meta
		if ( empty( $influencer_user ) ) {
			try {
				$instance = Provider::register();

				if ( $instance ) {
					$firebase = $instance->get_firebase_instance();
					$auth     = $firebase->getAuth();

					if ( $firebase_uid && $auth ) {
						$firebase_user   = $auth->getUser( $firebase_uid );
						$influencer_user = get_user_by( 'email', $firebase_user->email );

						if ( ! empty( $influencer_user ) ) {
							update_user_meta( $influencer_user->ID, 'zweb-influencer_firebase-uid', $firebase_uid );
						}
					}
				}
			} catch ( \Exception $e ) {
				$influencer_id = null;
			}
		}

		if ( ! empty( $influencer_user ) ) {
			if ( is_array( $influencer_user ) ) {
				$influencer_id = get_user_meta( $influencer_user[0]->ID, 'influencer_term', true );
			} else {
				$influencer_id = get_user_meta( $influencer_user->ID, 'influencer_term', true );
			}
		} else {
			$influencer_id = null;
		}

		return $influencer_id;
	}
}
