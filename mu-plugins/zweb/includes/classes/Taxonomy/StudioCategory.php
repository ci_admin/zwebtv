<?php

namespace Zweb\Taxonomy;

use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;

/**
 * Class StudioCategory
 *
 * @package Zweb\Taxonomy
 */
class StudioCategory extends BaseTaxonomy {

	const TAXONOMY_NAME  = 'zweb-studio-category';
	const META_KEY_IMAGE = 'zweb-image';

	/**
	 * Get name method
	 *
	 * @return string
	 */
	public static function get_name() {
		return self::TAXONOMY_NAME;
	}

	/**
	 * Register Studio Category taxonomy
	 */
	public function register() {
		parent::register();

		register_taxonomy(
			self::get_name(),
			[ Video::POST_TYPE_NAME, LiveVideo::POST_TYPE_NAME ],
			[
				'description'         => __( 'Used for videos captured by the ZWeb studio team in Rome', 'zweb' ),
				'public'              => true,
				'labels'              => self::get_labels( __( 'Studio category', 'zweb' ), __( 'Studio categories', 'zweb' ) ),
				'show_in_rest'        => true,
				'show_in_graphql'     => true,
				'graphql_single_name' => 'studioCategory',
				'graphql_plural_name' => 'studioCategories',
			]
		);
	}

	/**
	 * Fields method
	 *
	 * @return array
	 */
	public function fields() {
		$fm_fields[ self::META_KEY_IMAGE ] = [
			'title'  => 'Image',
			'fields' => [
				'id' => new \Fieldmanager_Media( '' ),
			],
		];

		return $fm_fields;
	}
}
