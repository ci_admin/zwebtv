<?php

namespace Zweb\Taxonomy;

use Zweb\Admin\Settings;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;
use TenUp\ContentTypes\Fields as TenupFields;

/**
 * Class VideoCategory
 *
 * @package Zweb\Taxonomy
 */
class VideoCategory extends BaseTaxonomy {

	const TAXONOMY_NAME = 'zweb-video-category';
	const META_KEY_IMAGE = 'zweb-image';
	const META_KEY_TOPTEN_VIDEOS = 'zweb-topten-videos';

	public static function get_name() {
		return self::TAXONOMY_NAME;
	}

	/**
	 * Register Video Category taxonomy
	 */
	public function register() {
		parent::register();

		register_taxonomy(
			self::get_name(),
			[ Video::POST_TYPE_NAME, LiveVideo::POST_TYPE_NAME ],
			[
				'description'         => __( 'Used to categorize videos from Influencers.', 'zweb' ),
				'public'              => true,
				'labels'              => self::get_labels( __( 'Video category', 'zweb' ), __( 'Video categories', 'zweb' ) ),
				'show_in_rest'        => true,
				'rewrite'             => [ 'slug' => 'generi' ],
				'show_in_graphql'     => true,
				'graphql_single_name' => 'videoCategory',
				'graphql_plural_name' => 'videoCategories',
			]
		);
	}

	/**
	 * Get custom FM fields
	 *
	 * @return array $fm_fields
	 **/
	public function fields() {
		$fm_fields[ self::META_KEY_IMAGE ] = [
			'title'  => 'Image',
			'fields' => [
				'id' => new \Fieldmanager_Media( '' ),
			],
		];

		$fm_fields['landing_page_image'] = [
			'title'  => 'Image for Video Category page',
			'fields' => [
				'id' => new \Fieldmanager_Media( '' ),
			],
		];

		$fm_fields[ self:: META_KEY_TOPTEN_VIDEOS ] = [
			'title'  => 'Top 10',
			'fields' => [
				'ids' => new TenupFields\Select2( [
					'name'            => 'ids',
					'multiple'        => true,
					'use_custom_list' => true,
					'show_view_link'  => false,
					'select_limit'    => 10,
					'description'     => 'Start by typing at least three characters of the video title to search.',
					'datasource'      => new \Fieldmanager_Datasource_Post( [
						'query_args' => [
							'post_type' => [ Video::POST_TYPE_NAME ],
						],
					] ),
				] ),
			],
		];

		return $fm_fields;
	}

	/**
	 * Get the query for video carousels.
	 *
	 * @return \WP_Term_Query
	 */
	public static function get_video_category_carousel_query( $search = '' ) {
		if ( $search ) {
			return new \WP_Term_Query(
				[
					'taxonomy'       => self::TAXONOMY_NAME,
					'posts_per_page' => 10,
					'orderby'        => 'date',
					'order'          => 'DESC',
					'hide_empty'     => false,
					'search'         => $search,
				]
			);
		}

		$site_settings = Settings::get_site_settings();
		$video_cats    = $site_settings['sitewide']['list_cats'] ?? [];
		if ( $video_cats ) {
			return new \WP_Term_Query(
				[
					'taxonomy'       => self::TAXONOMY_NAME,
					'posts_per_page' => 10,
					'orderby'        => 'include',
					'include'        => $video_cats,
					'hide_empty'     => false,
				]
			);
		}

		// Fallback
		return new \WP_Term_Query(
			[
				'taxonomy'       => self::TAXONOMY_NAME,
				'posts_per_page' => 10,
				'orderby'        => 'date',
				'order'          => 'DESC',
				'hide_empty'     => false,
			]
		);
	}

	/**
	 * Get the video category for a video.
	 *
	 * @param $video_id
	 *
	 * @return \WP_Term
	 */
	public static function get_video_category_term_for_video( $video_id ) {
		$video_categories = wp_get_post_terms( $video_id, self::TAXONOMY_NAME );
		if ( $video_categories ) {
			return $video_categories[0];
		}

		return null;
	}
}
