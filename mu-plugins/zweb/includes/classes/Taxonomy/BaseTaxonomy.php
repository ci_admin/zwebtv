<?php

namespace Zweb\Taxonomy;

/**
 * Class BaseTaxonomy
 *
 * @package Zweb\Taxonomy
 */
abstract class BaseTaxonomy {

	/**
	 * Register method to trigger actions and filters.
	 *
	 * @return void
	 */
	public function register() {
		add_action( 'fm_term_' . $this->get_name(), [ $this, 'setup_fields' ] );
	}

	/**
	 * Abstract method for get name.
	 *
	 * @return void
	 */
	abstract public static function get_name();

	/**
	 * Get the labels for the taxonomy.
	 *
	 * @return array
	 */
	public static function get_labels( $singular_label, $plural_label ) {

		return [
			'name'                       => $plural_label,
			'singular_name'              => $singular_label,
			'search_items'               => sprintf( __( 'Search %s', 'zweb' ), $plural_label ),
			'popular_items'              => sprintf( __( 'Popular %s', 'zweb' ), $plural_label ),
			'all_items'                  => sprintf( __( 'All %s', 'zweb' ), $plural_label ),
			'edit_item'                  => sprintf( __( 'Edit %s', 'zweb' ), $singular_label ),
			'update_item'                => sprintf( __( 'Update %s', 'zweb' ), $singular_label ),
			'add_new_item'               => sprintf( __( 'Add New %s', 'zweb' ), $singular_label ),
			'new_item_name'              => sprintf( __( 'New %s Name', 'zweb' ), $singular_label ),
			'separate_items_with_commas' => sprintf( __( 'Separate %s with commas', 'zweb' ), strtolower( $plural_label ) ),
			'add_or_remove_items'        => sprintf( __( 'Add or remove %s', 'zweb' ), strtolower( $plural_label ) ),
			'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s', 'zweb' ), strtolower( $plural_label ) ),
			'not_found'                  => sprintf( __( 'No %s found.', 'zweb' ), strtolower( $plural_label ) ),
			'not_found_in_trash'         => sprintf( __( 'No %s found in Trash.', 'zweb' ), strtolower( $plural_label ) ),
			'back_to_items'              => sprintf( __( 'Back to %s.', 'zweb' ), strtolower( $plural_label ) ),
		];
	}

	/**
	 * Construct term fields.
	 *
	 * @return array $fields   List of FM fields.
	 */
	public function fields() {
		return [];
	}

	/**
	 * Setup fields for the taxonomy.
	 *
	 * @return void
	 */
	public function setup_fields() {
		foreach ( $this->fields() as $group_name => $group_fields ) {
			$fm_group_fields = new \Fieldmanager_Group( array(
				'name'           => $group_name,
				'serialize_data' => false,
        'children'       => $group_fields['fields'],
			) );

			$fm_group_fields->add_term_meta_box( $group_fields['title'], $this->get_name() );
		}
	}
}
