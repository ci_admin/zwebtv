<?php

namespace Zweb\Firebase;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Auth;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Exception\Auth\InvalidIdToken;
use Zweb\Firebase\Firestore;


/**
 * Class Provider
 *
 * @package Zweb\Firebase
 */
class Provider {
	/**
	 * The Firebase Provider instance.
	 *
	 * @var Provider
	 */
	public static $instance = null;

	/**
	 * The Firebase instance.
	 *
	 * @var Firebase
	 */
	private $firebase = null;

	/**
	 * Firebase constructor.
	 *
	 * @param array $args The array containing the credentials.
	 */
	public function __construct( array $args ) {
		$service_account = ServiceAccount::fromJson( json_encode( $args ) );
		$factory         = ( new Factory() )->withServiceAccount( $service_account )->withDisabledAutoDiscovery();
		$this->firebase  = $factory->create();
	}

	/**
	 * Init the firebase services.
	 *
	 * @return Firebase
	 *
	 * @throws \Exception When firebase credentials are incorrect.
	 */
	public static function register() {
		if ( ! class_exists( '\Kreait\Firebase\ServiceAccount' ) ) {
			return;
		}

		if ( is_null( self::$instance ) ) {
			$settings = get_option( 'zweb_firebase_options' );

			try {
				self::$instance = new Provider(
					[
						'type'                        => $settings['settings']['type'] ?? '',
						'project_id'                  => $settings['settings']['project_id'] ?? '',
						'private_key_id'              => $settings['settings']['private_key_id'] ?? '',
						'private_key'                 => $settings['settings']['private_key'] ?? '',
						'client_email'                => $settings['settings']['client_email'] ?? '',
						'client_id'                   => $settings['settings']['client_id'] ?? '',
						'auth_uri'                    => $settings['settings']['auth_uri'] ?? '',
						'token_uri'                   => $settings['settings']['token_uri'] ?? '',
						'auth_provider_x509_cert_url' => $settings['settings']['auth_provider_x509_cert_url'] ?? '',
						'client_x509_cert_url'        => $settings['settings']['client_x509_cert_url'] ?? '',
					]
				);
			} catch ( \Exception $e ) {
				self::$instance = null;
			}
		}

		return self::$instance;
	}

	/**
	 * Init the firebase services.
	 *
	 * @return Factory
	 */
	public function get_firebase_instance() {
		return $this->firebase;
	}

	/**
	 * Provision firestore services.
	 *
	 * @param string $email Forestore user email
	 * @param string $password Firestore user password
	 *
	 * @return Firestore
	 */
	public function provision_firestore( $email, $password ) {
		$instance = Firestore::get_instance();
		$instance->authorize_firestore_instance( $email, $password );

		return $instance;
	}

	/**
	 * Is valid token.
	 *
	 * @param string $auth_token Authentication token.
	 *
	 * @return \Kreait\Firebase\Auth\UserRecord firebase user.
	 * @throws \Exception On failed validation.
	 */
	public function is_valid_token( $auth_token ) {
		$auth = $this->firebase->getAuth();

		try {
			$verified_token = $auth->verifyIdToken( $auth_token );
		} catch ( \InvalidArgumentException $e ) {
			// throw new \Exception( 'The token could not be parsed: ' . $e->getMessage() );
			return false;
		} catch ( InvalidToken $e ) {
			// throw new \Exception( 'The token is invalid: ' . $e->getMessage() );
			return false;
		}

		$uid  = $verified_token->getClaim( 'sub' );
		$user = $auth->getUser( $uid );

		return $user;
	}
}
