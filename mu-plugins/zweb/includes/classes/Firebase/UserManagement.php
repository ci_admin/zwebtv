<?php

namespace Zweb\Firebase;

use Kreait\Firebase\Auth;
use Kreait\Firebase\Auth\UserRecord;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Zweb\Firebase\Provider;
use Zweb\Firebase\Firestore;
use Zweb\Multilingual\GoogleTranslate;
use Zweb\Role\Influencer;
use Zweb\Role\LiveBattleAdmin;

/**
 * Class UserManagement
 *
 * @package Zweb\Firebase
 */
class UserManagement {

	/**
	 * Register actions and filters
	 *
	 * @return void
	 */
	public function register() {
		add_filter( 'authenticate', [ $this, 'block_app_users' ] );
		add_action( 'lostpassword_post', [ $this, 'disable_lostpassword' ] );
		add_action( 'user_register', [
			$this,
			'provision_firebase_user',
		], 20, 1 );
		add_action( 'delete_user', [ $this, 'delete_firebase_user' ], 10, 1 );
		add_action( 'set_user_role', [
			$this,
			'update_firebase_user_role',
		], 10, 3 );
		add_action( 'profile_update', [
			$this,
			'update_firebase_user_data',
		], 10, 2 );
		add_action( 'init_graphql_request', [ $this, 'provision_wp_user' ] );
		add_filter( 'wp_new_user_notification_email', [ $this, 'disable_influencer_email' ], 10, 3 );
	}

	/**
	 * Blocks App Users from logging in diretly in WP.
	 *
	 * @param \WP_User|null $user The user object.
	 *
	 * @return \WP_User|\WP_Error
	 */
	public function block_app_users( $user ) {
		if ( ! is_null( $user ) ) {
			$is_app_user = get_user_meta( $user->ID, 'zweb_app_user', true );

			if ( $is_app_user ) {
				return new \WP_Error( 'Not allowed to login.' );
			}

			return $user;
		}
	}

	/**
	 * Prevents headeless users from recovering password from the wp admin
	 * interface.
	 *
	 * @param \WP_Error $errors Holds any errors.
	 *
	 * @return void
	 */
	public function disable_lost_password( \WP_Error $errors ) {
		// if we already have errors, nothing to do here.
		if ( $errors->get_error_codes() ) {
			return;
		}

		// If we get to this point we do have a user.
		$user_login = filter_input( INPUT_POST, 'user_login', FILTER_SANITIZE_STRING );

		if ( strpos( $user_login, '@' ) ) {
			$user = get_user_by( 'email', trim( wp_unslash( $user_login ) ) );
		} else {
			$user = get_user_by( 'login', $user_login );
		}

		$is_app_user = get_user_meta( $user->ID, 'zweb_app_user', true );

		if ( $is_app_user ) {
			wp_die( esc_html__( 'In order to reset your password go to your account settings', 'zwebtv' ) );
		}
	}

	/**
	 * Disable sending of an email notification for new influencers.
	 *
	 * @param array $wp_new_user_notification_email Mail array.
	 * @param \WP_User $user User object.
	 * @param string $blogname Blog name
	 *
	 * @return void|array
	 */
	public function disable_influencer_email( $wp_new_user_notification_email, $user, $blogname ) {
		if ( ! empty( $user ) ) {
			if ( in_array( \Zweb\Role\Influencer::ROLE_NAME, (array) $user->roles ) ) {
				return;
			}
		}

		return $wp_new_user_notification_email;
	}

	/**
	 * Deletes the associated firebase user account.
	 *
	 * @param int $user_id User id being deleted.
	 *
	 * @return void
	 */
	public function delete_firebase_user( $user_id ) {
		$user     = get_userdata( $user_id );
		$instance = Provider::register();

		if ( $instance ) {
			$firebase = $instance->get_firebase_instance();
			$auth     = $firebase->getAuth();

			if ( $user->user_email && $auth ) {
				try {
					$firebase_user = $auth->getUserByEmail( $user->user_email );

					if ( $firebase_user && isset( $firebase_user->uid ) ) {
						$auth->deleteUser( $firebase_user->uid );

						$firestore = Firestore::get_instance();
						$firestore->delete_document( 'users', $firebase_user->uid );
					}
				} catch ( \Exception $e ) {
					// nothing to do here
				}
			}
		}
	}

	/**
	 * Update Firebase when the user role changes.
	 *
	 * @param int $user_id User id.
	 * @param string $role New user role.
	 * @param string $old_roles Old user role.
	 *
	 * @return void
	 * @throws \Exception On failed validation.
	 */
	public function update_firebase_user_role( $user_id, $role, $old_roles ) {
		$user      = get_userdata( $user_id );
		$instance = Provider::register();

		if ( $instance ) {
			$firebase        = $instance->get_firebase_instance();
			$auth            = $firebase->getAuth();
			$roles_to_update = [
				Influencer::ROLE_NAME      => true,
				LiveBattleAdmin::ROLE_NAME => true,
			];

			if ( ! empty( $old_roles ) && isset( $roles_to_update[ $role ] ) ) {
				if ( $user->user_email && $auth ) {
					try {
						$firebase_user = $auth->getUserByEmail( $user->user_email );

						if ( $firebase_user && isset( $firebase_user->uid ) ) {
							$firestore = Firestore::get_instance();
							$firestore->set_document_field( 'users', $firebase_user->uid, 'userRole', $role );
						}
					} catch ( \Exception $e ) {
						$user_properties = [
							'email'    => $user->user_email,
							'password' => $user->user_pass,
						];

						$firebase_user = $auth->createUser( $user_properties );

						if ( $firebase_user && isset( $firebase_user->uid ) ) {
							$firestore = Firestore::get_instance();

							$firestore_fields = [
								'userName'        => $user->user_login,
								'userRole'        => $user->roles[0],
								'userDisplayName' => $user->display_name,
							];

							$firestore->create_document( 'users', $firebase_user->uid, $firestore_fields );
						}
					}
				}
			} elseif ( ! empty( $old_roles ) && isset( $roles_to_update[ $old_roles[0] ] ) ) {
				try {
					$firebase_user = $auth->getUserByEmail( $user->user_email );

					if ( $firebase_user && isset( $firebase_user->uid ) ) {
						$firestore = Firestore::get_instance();
						$firestore->set_document_field( 'users', $firebase_user->uid, 'userRole', $role );
					}
				} catch ( \Exception $e ) {
					// nothing to do here
				}
			}
		}
	}

	/**
	 * Update Firebase when the user email or bio changes.
	 *
	 * @param int $user_id User id.
	 * @param string $old_user_data Old user data.
	 *
	 * @return void
	 * @throws \Exception On failed validation.
	 */
	public function update_firebase_user_data( $user_id, $old_user_data ) {
		$user             = get_userdata( $user_id );
		$is_firebase_user = false;
		$instance         = Provider::register();

		if ( $instance ) {
			$firebase = $instance->get_firebase_instance();
			$auth     = $firebase->getAuth();

			// First test is this a firebase user, if not then provision one
			try {
				$firebase_user    = $auth->getUserByEmail( $old_user_data->user_email );
				$is_firebase_user = true;
			} catch ( \Exception $e ) {
				$this->provision_firebase_user( $user->ID );
			}

			if ( $user->user_email && $auth && $is_firebase_user ) {

				$this->maybe_update_password_on_firebase( $user, $auth, $firebase_user );

				if ( $user->user_email !== $old_user_data->user_email ) {
					try {
						if ( $firebase_user ) {
							$auth->changeUserEmail( $firebase_user->uid, $user->user_email );

							update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_user->uid );
						}
					} catch ( \Exception $e ) {
						// nothing to do here
					}
				}

				try {
					$user_meta     = get_user_meta( $user->ID, 'description', true );
					$influencer_id = get_user_meta( $user->ID, 'influencer_term', true );
					$profile_url   = get_user_meta( $user->ID, 'zweb-influencer_profile_image', true ) ? wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_profile_image', true ) ) : '';

					if ( $firebase_user ) {

						$firestore = Firestore::get_instance();
						$firestore->set_document_field( 'users', $firebase_user->uid, 'bio', $user_meta );
						$firestore->set_document_field( 'users', $firebase_user->uid, 'userDisplayName', $user->display_name );
						try {
							$avatar = $firestore->get_document_field( 'users', $firebase_user->uid, 'avatar' );

							if ( empty( $avatar ) ) {
								$firestore->set_document_field( 'users', $firebase_user->uid, 'avatar', $profile_url );
							} else {
								$profile_url = $avatar;
							}
						} catch ( \Exception $e ) {
							$firestore->set_document_field( 'users', $firebase_user->uid, 'avatar', $profile_url );
						}

						update_user_meta( $user->ID, 'zweb-influencer_avatar', $profile_url );
						update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_user->uid );
					}

					if ( ! empty( $influencer_id ) ) {
						$translation = GoogleTranslate::register();
						$field       = 'description_';
						$translation->user_translation_update( $user->ID, $field, $user_meta );
					}
				} catch ( \Exception $e ) {
					// nothing to do here
				}
			}
		}
	}

	/**
	 * Receive a bearer token from the http headers and validate it with firebase.
	 * If it is a valid auth token then see if the user exists in WP.
	 * If it does then update it.
	 *
	 * @return void
	 */
	public function provision_wp_user() {
		try {
			$token = self::get_http_header_token();

			if ( ! empty( $token ) ) {
				$instance = Provider::register();

				if ( $instance ) {
					$firebase_user = $instance->is_valid_token( $token );

					if ( ! empty( $firebase_user ) ) {
						$user = get_user_by( 'email', $firebase_user->email );

						if ( ! empty( $user ) ) {
							
								$firestore    = Firestore::get_instance();
								$user_name    = $firestore->get_document_field( 'users', $firebase_user->uid, 'userName' );
								$wp_user_name = ! empty( $user_name ) ? $user_name : $firebase_user->email;
								$bio          = $firestore->get_document_field( 'users', $firebase_user->uid, 'bio' );
								$user_role    = $firestore->get_document_field( 'users', $firebase_user->uid, 'UserRole' );
								$avatar       = $firestore->get_document_field( 'users', $firebase_user->uid, 'avatar' );

								wp_update_user(
									[
										'ID'          => $user->ID,
										'user_login'  => $wp_user_name,
										'user_email'  => $firebase_user->email,
										'description' => $bio,
										'role'        => $user_role,
									]
								);

								update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_user->uid );
								update_user_meta( $user->ID, 'zweb-influencer_avatar', $avatar );
							
						}
					}
				}
			}

		} catch ( \Exception $e ) {
			// nothing to do here
		}
	}

	/**
	 * Get the token from the http headers sent from the graphql query.
	 *
	 * @return null|string
	 */
	public static function get_http_header_token() {
		if ( \WPGraphQL\Router::is_graphql_request() ) {
			$headers = null;

			if ( isset( $_SERVER['Authorization'] ) ) {
				$headers = trim( $_SERVER['Authorization'] );
			} elseif ( isset( $_SERVER['HTTP_AUTHORIZATION'] ) ) {
				$headers = trim( $_SERVER['HTTP_AUTHORIZATION'] );
			}

			if ( ! empty( $headers ) ) {
				if ( preg_match( '/Bearer\s(\S+)/', $headers, $token ) ) {
					return $token[1];
				}
			}
		}

		return null;
	}

	/**
	 * Register a new user with firebase.
	 *
	 * @param int $user_id WordPress user id.
	 *
	 * @return void
	 * @throws \Exception On failed validation.
	 */
	public function provision_firebase_user( $user_id ) {
		$user = get_userdata( $user_id );

		if ( ! empty( $user ) ) {
			if ( Influencer::is_user_influencer( $user ) || LiveBattleAdmin::is_user_live_battle_admin( $user ) ) {
				$instance = Provider::register();

				if ( $instance ) {
					$firebase = $instance->get_firebase_instance();
					$auth     = $firebase->getAuth();

					if ( $user->user_email && $auth ) {
						$user_properties = [
							'email' => $user->user_email,
						];

						$user_pw = filter_input( INPUT_POST, 'pass1', FILTER_SANITIZE_STRING );

						if ( ! empty( $user_pw ) ) {
							$user_properties['password'] = $user_pw;
						}

						$firestore_fields = [
							'userName'        => $user->user_login,
							'userRole'        => $user->roles[0],
							'userDisplayName' => $user->display_name,
						];

						try {
							$firebase_user = $auth->createUser( $user_properties );

							if ( $firebase_user ) {
								$firestore = Firestore::get_instance();

								$firestore->create_document( 'users', $firebase_user->uid, $firestore_fields );

								update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_user->uid );
							}
						} catch ( \Exception $e ) {
							try {
								$firebase_user = $auth->getUserByEmail( $user->user_email );

								if ( $firebase_user ) {
									$user_meta = get_user_meta( $user->ID, 'description', true );
									$firestore = Firestore::get_instance();

									$firestore->set_document_field( 'users', $firebase_user->uid, 'bio', $user_meta );
									$firestore->set_document_field( 'users', $firebase_user->uid, 'userRole', $firestore_fields['userRole'] );
									$firestore->set_document_field( 'users', $firebase_user->uid, 'userName', $firestore_fields['userName'] );

									update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_user->uid );
								}
							} catch ( \Exception $e ) {
								throw new \Exception( 'Firebase user could not be updated: ' . $e->getMessage() );
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Update password on firebase for live battle admin.
	 *
	 * @param \WP_User $user
	 * @param Auth $auth
	 * @param UserRecord $firebase_user
	 *
	 */
	protected function maybe_update_password_on_firebase( \WP_User $user, Auth $auth, UserRecord $firebase_user ) {
		if ( isset( $_POST['pass1'] ) && '' !== $_POST['pass1'] &&
		     ( LiveBattleAdmin::is_user_live_battle_admin( $user ) || isset( $_POST['update-firebase']  ) )
		) {
			try {
				$auth->changeUserPassword( $firebase_user->uid, $_POST['pass1'] );
			} catch ( \Exception $e ) {
				error_log( 'Something went wrong when updating the password on firebase: ' . $e->getMessage() );
			}
		}
	}
}
