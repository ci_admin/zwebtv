<?php

namespace Zweb\Firebase;

/**
 * Class for the Settings page in the admin.
 *
 * @package Zweb\Firebase
 */
class Settings {

	/**
	 * Register menu actions & hooks.
	 *
	 * @return void
	 */
	public static function register() {
		if ( function_exists( 'fm_register_submenu_page' ) ) {
			fm_register_submenu_page( 'zweb_firebase_options', 'options-general.php', 'Firebase Settings', 'Firebase Settings' );
			add_action(
				'fm_submenu_zweb_firebase_options',
				[
					__CLASS__,
					'option_fields',
				]
			);
		}
	}

	/**
	 * Setup custom submenu settings page.
	 *
	 * @return void
	 */
	public static function option_fields() {

		$fields['settings'] = new \Fieldmanager_Group(
			[
				'name'           => 'settings',
				'serialize_data' => true,
				'add_to_prefix'  => false,
				'children'       => self::get_settings_fields(),
			]
		);

		$option_fields = new \Fieldmanager_Group(
			[
				'name'     => 'zweb_firebase_options',
				'children' => $fields,
			]
		);
		$option_fields->activate_submenu_page();
	}

	/**
	 * Get fields for the firebase credentials
	 *
	 * @return array
	 */
	public static function get_settings_fields() {
		return [
			'type'                        => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Type', 'zweb' ),
				]
			),
			'project_id'                  => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Project Id', 'zweb' ),
				]
			),
			'private_key_id'              => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Private Key Id', 'zweb' ),
				]
			),
			'private_key'                 => new \Fieldmanager_TextArea(
				[
					'label'    => esc_html__( 'Private Key', 'zweb' ),
					'sanitize' => function( $value ) {
						$new_value = str_replace( '\n', "\n", $value );
						return $new_value;
					},
				]
			),
			'client_email'                => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Client Email', 'zweb' ),
				]
			),
			'client_id'                   => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Client Id', 'zweb' ),
				]
			),
			'auth_uri'                    => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Auth Uri', 'zweb' ),
				]
			),
			'token_uri'                   => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Token Uri', 'zweb' ),
				]
			),
			'auth_provider_x509_cert_url' => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Auth Provider Url', 'zweb' ),
				]
			),
			'client_x509_cert_url'        => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Client Url', 'zweb' ),
				]
			),
			'api_key'                     => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Api Key', 'zweb' ),
				]
			),
			'translation_key'             => new \Fieldmanager_TextField(
				[
					'label' => esc_html__( 'Translation Key', 'zweb' ),
				]
			),
		];
	}
}
