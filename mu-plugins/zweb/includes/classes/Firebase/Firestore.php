<?php

namespace Zweb\Firebase;

use MrShan0\PHPFirestore\FirestoreClient;
use MrShan0\PHPFirestore\Authentication\FirestoreAuthentication;
use MrShan0\PHPFirestore\FirestoreDatabaseResource;
use Google\Auth\Credentials\ServiceAccountCredentials;
use MrShan0\PHPFirestore\FirestoreDocument;

/**
 * Class Firestore
 *
 * @package Zweb\Firebase
 */
class Firestore {
	/**
	 * The Firestore Provider instance.
	 *
	 * @var Firestore
	 */
	public static $instance = null;

	/**
	 * The Firestore instance.
	 *
	 * @var FirestoreClient
	 */
	private $firestore = null;

	/**
	 * The Firestore document.
	 *
	 * @var Document object
	 */
	private $document = null;

	/**
	 * Firestore constructor.
	 *
	 * @param array $args The array containing the credentials.
	 */
	private function __construct( array $args ) {
		$this->firestore = new FirestoreClient(
			$args['project_id'],
			$args['api_key'],
			[
				'database' => '(default)',
			]
		);

		// set an service account oauth 2 access token
		$this->firestore->authenticator()->setCustomToken( $args['oauth_token'] );
	}

	/**
	 * Init the Firestore services.
	 *
	 * @return Firestore
	 */
	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			$settings = get_option( 'zweb_firebase_options' );

			try {
				$scopes = [
					'https://www.googleapis.com/auth/userinfo.email',
					'https://www.googleapis.com/auth/firebase.database',
					'https://www.googleapis.com/auth/firebase',
					'https://www.googleapis.com/auth/cloud-platform',
				];

				if ( ! empty( $settings ) ) {
					$service_account = ServiceAccountCredentials::makeCredentials( $scopes, $settings['settings'] );

					self::$instance = new Firestore(
						[
							'project_id'  => $settings['settings']['project_id'] ?? '',
							'api_key'     => $settings['settings']['api_key'] ?? '',
							// TODO: Look into caching this? https://googleapis.github.io/google-auth-library-php/master/Google/Auth/Credentials/ServiceAccountCredentials.html
							'oauth_token' => $service_account->fetchAuthToken()['access_token'],
						]
					);
				} else {
					self::$instance = null;
				}
			} catch ( \Exception $e ) {
				self::$instance = null;
			}
		}

		return self::$instance;
	}

	/**
	 * Authorize the Firestore services.
	 *
	 * @param string $email Firestore user email
	 * @param string $password Firestore user password
	 *
	 * @return void
	 */
	public function authorize_firestore_instance( $email, $password ) {
		$this->firestore->authenticator()->signInEmailPassword( $email, $password );
	}

	/**
	 * Get firestore field.
	 *
	 * @param string $collection Firestore collection
	 * @param string $document_id Firestore document ID
	 * @param string $field Name of firestore field
	 *
	 * @return string $field
	 * @throws \Exception When firestore fails.
	 */
	public function get_document_field( $collection, $document_id, $field ) {
		try {
			$this->document = $this->firestore->getDocument( "{$collection}/{$document_id}" );
			$field          = $this->document->get( $field );
		} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
			throw new \Exception( 'Field was not found on firestore: ' . $e->getMessage() );
		}

		return $field;
	}

	/**
	 * Updated firestore field.
	 *
	 * @param string $collection Firestore collection
	 * @param string $document_id Firestore document ID
	 * @param string $field Field to update on firestore
	 * @param string $value Field value
	 *
	 * @return boolean
	 * @throws \Exception When firestore fails.
	 */
	public function set_document_field( $collection, $document_id, $field, $value ) {
		try {
			$this->firestore->updateDocument(
				"{$collection}/{$document_id}",
				[
					$field => $value,
				]
			);
		} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
			throw new \Exception( 'Field was not found on firestore: ' . $e->getMessage() );
		}

		return true;
	}

	/**
	 * Create firestore document.
	 *
	 * @param string $collection Firestore collection
	 * @param string $document_id Document ID to create
	 * @param array $fields Fields to update on firestore
	 *
	 * @return FirestoreDocument
	 * @throws \Exception When firestore fails.
	 */
	public function create_document( $collection, $document_id, $fields ) {
		try {
			if ( ! empty( $document_id ) ) {
				$response = $this->firestore->addDocument( $collection, $fields, $document_id );
			} else {
				$response = $this->firestore->addDocument( $collection, $fields );
			}
		} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
			throw new \Exception( 'Docmuent cannot be created on firestore: ' . $e->getMessage() );
		}

		return $response;
	}

	/**
	 * Delete firestore document.
	 *
	 * @param string $collection Firestore collection
	 * @param string $document_id Document ID to delete
	 *
	 * @return boolean
	 * @throws \Exception When firestore fails.
	 */
	public function delete_document( $collection, $document_id ) {
		$document = $this->firestore->getDocument( "{$collection}/{$document_id}" );

		if ( ! empty( $document ) ) {
			try {
				$this->firestore->deleteDocument( $document );
			} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
				throw new \Exception( 'Docmuent cannot be deleted on firestore: ' . $e->getMessage() );
			}
		}

		return true;
	}

	/**
	 * @return FirestoreClient
	 */
	public function get_firestore_client_instance() {
		return $this->firestore;
	}

	/**
	 * Add firestore notification document.
	 *
	 * @param string $collection   Firestore collection
	 * @param Array $fields Array of fields.
	 *
	 * @return boolean
	 * @throws \Exception When firestore fails.
	 */
	public function add_notification( $collection, $fields ) {
		try {
			$response = $this->firestore->addDocument( $collection, $fields );
		} catch ( \MrShan0\PHPFirestore\Exceptions\Client\FieldNotFound $e ) {
			throw new \Exception( 'Docmuent cannot be created on firestore: ' . $e->getMessage() );
		}
		return $response;
	}

}
