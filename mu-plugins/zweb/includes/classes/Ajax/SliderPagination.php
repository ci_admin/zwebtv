<?php

namespace Zweb\Ajax;

use Zweb\PostType\Video;
use Zweb\Builder\Query;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;
use Zweb\Taxonomy\VideoCategory;

/**
 * Class SliderPagination
 *
 * @package Zweb\Ajax
 */
class SliderPagination {

	/**
	 *  Register Ajax handlers
	 */
	public static function register() {
		add_action(
			'wp_ajax_load_more_slides',
			[
				'Zweb\Ajax\SliderPagination',
				'load_more_slides',
			]
		);
		add_action(
			'wp_ajax_nopriv_load_more_slides',
			[
				'Zweb\Ajax\SliderPagination',
				'load_more_slides',
			]
		);
	}

	/**
	 *  Sends html of new slides.
	 */
	public static function load_more_slides() {

		$to_return = [];
		$offset    = (int) $_POST['offset'];
		$offset    = 4 + $offset * 4;

		// Add logic for filtering.
		$gallery_term        = $_POST['galleryTermId'] ?? null;
		$influencer_term     = $_POST['influencerTermId'] ?? null;
		$video_category_term = $_POST['videoCategoryTermId'] ?? null;
		$gallery_term        = $gallery_term ? get_term( (int) $gallery_term, Gallery::TAXONOMY_NAME ) : null;
		$influencer_term     = $influencer_term ? get_term( (int) $influencer_term, Influencer::TAXONOMY_NAME ) : null;
		$video_category_term = $video_category_term ?
			get_term( (int) $video_category_term, VideoCategory::TAXONOMY_NAME ) :
			null;

		// add logic for related video:
		$relationship = $_POST['relationship'] ?? '';
		switch ( $relationship ) {
			case 'search':
				$search_term = $_POST['search'] ?? '';
				$query       = new \WP_Query(
					Query::get_video_carousel_query(
						4,
						$gallery_term,
						$influencer_term,
						$video_category_term,
						$offset,
						$search_term
					)
				);
				break;
			case 'influencer-related-video':
				$query = Query::get_related_video_for_influencer_query(
					$influencer_term,
					4,
					$offset
				);
				break;
			case 'video-related-video':
				$query = Query::get_related_video_for_video_query(
					isset( $_POST['videoId'] ) ? (int) $_POST['videoId'] : 0,
					4,
					$offset
				);
				break;
			case 'novita':
				$query = Query::build_novita_query(
					4,
					$gallery_term,
					$influencer_term,
					$video_category_term,
					$offset
				);
				break;
			case 'studio':
				$query = Query::get_studio_carousel_video_query(
					isset( $_POST['studioId'] ) ? (int) $_POST['studioId'] : 0,
					4,
					$offset
				);
				break;
			default:
				$query = Query::get_video_carousel_query(
					4,
					$gallery_term,
					$influencer_term,
					$video_category_term,
					$offset
				);
				break;
		}

		foreach ( $query->get_posts() as $post ) {
			$query->the_post();
			ob_start();
			?>
			<div class="video-carousel__item swiper-slide">
				<?php set_query_var( 'video_id', $post->ID ); ?>
				<?php get_template_part( 'partials/video-card', 'page' ); ?>
			</div>
			<?php
			$to_return[] = ob_get_clean();
		}

		wp_send_json_success( $to_return );
	}
}
