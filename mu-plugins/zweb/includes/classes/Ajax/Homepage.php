<?php

namespace Zweb\Ajax;

use Zweb\Admin\Settings;

class Homepage {

	/**
	 *  Register Ajax handlers
	 */
	public static function register() {
		add_action( 'wp_ajax_state_call', [
			static::class,
			'state_call',
		] );
		add_action( 'wp_ajax_nopriv_state_call', [
			static::class,
			'state_call',
		] );
	}

	public static function state_call() {
		$settings  = Settings::get_site_settings();
		$state     = Settings::get_current_application_state();
		$to_return = [
			'state'             => $state,
			'announcement_text' => Settings::get_annnouncement_text(),
		];

		if ( isset( $settings['live']["$state-text"] ) ) {
			$to_return['header_text'] = $settings['live']["$state-text"];
		}
		wp_send_json_success(
			$to_return
		);
	}
}