<?php

namespace Zweb\SEO;

use Zweb\PostType\Video;

/**
 * Class Override
 *
 * @package Zweb\Og
 */
class Og {

	/**
	 * Register
	 */
	public static function register() {
		add_filter( 'wpseo_opengraph_image', [ static::class, 'wpseo_opengraph_image' ] );
	}


	/**
	 * Get the correct og image.
	 *
	 * @param $image
	 *
	 * @return string
	 */
	public static function wpseo_opengraph_image( $image ) {
		if ( is_singular( Video::POST_TYPE_NAME ) ) {
			$yoast_facebook_image = get_post_meta(
				get_the_ID(),
				'_yoast_wpseo_opengraph-image',
				true
			);
			if ( $yoast_facebook_image ) {
				return $yoast_facebook_image;
			}
			$images = get_post_meta( get_the_ID(), 'bc_images', true );
			if ( isset( $images['poster']['src'] ) ) {
				return $images['poster']['src'];
			}
		}
		if ( is_tax( \Zweb\Taxonomy\Influencer::TAXONOMY_NAME ) ) {
			try {
				$user               = \Zweb\Taxonomy\Influencer::get_user_from_term( get_queried_object()->term_id );
				$profile_picture_id = get_user_meta(
					$user->ID,
					\Zweb\Role\Influencer::META_KEY . '_landing_page_image',
					true
				);
				$src = wp_get_attachment_image_src( $profile_picture_id, 'full' );
				if ( $src ) {
					return $src[0];
				}
			} catch ( \Exception $exception ) {
				// do nothing
			}
		}
		return $image;
	}
}