<?php
namespace Zweb\Multilingual;

use Google\Cloud\Translate\V2\TranslateClient;

/**
 * Class GoogleTranslate
 *
 * @package Zweb\Multilingual
 */
class GoogleTranslate {
	/**
	 * The GoogleTranslate instance.
	 *
	 * @var GoogleTranslate
	 */
	public static $instance = null;

	/**
	 * The GoogleTranslate client.
	 *
	 * @var Factory
	 */
	private $translate = null;

	/**
	 * GoogleTranslate constructor.
	 *
	 * @param array $args The array containing the credentials.
	 */
	public function __construct( array $args ) {
		$this->translate = new TranslateClient(
			[
				'key' => $args['api_key'],
			]
		);
	}

	/**
	 * Init the GoogleTranslate services.
	 *
	 * @return GoogleTranslate
	 */
	public static function register() {
		if ( is_null( self::$instance ) ) {
			$settings = get_option( 'zweb_firebase_options' );

			try {
				self::$instance = new GoogleTranslate(
					[
						'api_key' => $settings['settings']['translation_key'] ?? '',
					]
				);
			} catch ( \Exception $e ) {
				self::$instance = null;
			}
		}
		return self::$instance;
	}

	/**
	 * Translate and save the translation to post meta by field_language
	 *
	 * @param int    $post_id 	 WP post id.
	 * @param int    $field_name WP post meta field name.
	 * @param string $text       Text to be translated.
	 * @param string $target     Target language - 2 digit ISO code.
	 *
	 * @return string $translation
	 */
	public function get_post_translation( $post_id, $field_name, $text, $target ) {
		$data = get_post_meta( $post_id, $field_name, true );

		if ( empty( $data ) ) {
			$translation = $this->translate_text( $text, $target );

			update_post_meta( $post_id, $field_name, $translation );
		} else {
			return $data;
		}
		return $translation;
	}

	/**
	 * Translate and save the translation to user meta by field_language
	 *
	 * @param int    $user_id 	 WP user id.
	 * @param int    $field_name WP user meta field name.
	 * @param string $text       Text to be translated.
	 * @param string $target     Target language - 2 digit ISO code.
	 *
	 * @return string $translation
	 */
	public function get_user_translation( $user_id, $field_name, $text, $target ) {
		$data = get_user_meta( $user_id, $field_name, true );

		if ( empty( $data ) ) {
			$translation = $this->translate_text( $text, $target );

			update_user_meta( $user_id, $field_name, $translation );
		} else {
			return $data;
		}
		return $translation;
	}

	/**
	 * Update the translation to post meta by field_language on post update
	 *
	 * @param int    $post_id 	 WP post id.
	 * @param int    $field_name WP post meta field name.
	 * @param string $text       Text to be translated.
	 */
	public function post_translation_update( $post_id, $field_name, $text ) {
		$all_meta = get_post_meta( $post_id );
		$output   = [];

		foreach ( $all_meta as $meta_key => $meta_value ) {
			$matches = preg_match( "/{$field_name}/i", $meta_key );

			if ( $matches && '' !== $meta_value ) {
				$target_language = preg_replace( "/{$field_name}/i", '', $meta_key );
				$translation     = $this->translate_text( $text, $target_language );
				$result          = update_post_meta( $post_id, $meta_key, $translation );
			}
		}
	}

	/**
	 * Update the translation to user meta by field_language on user profile update
	 *
	 * @param int    $post_id 	 WP post id.
	 * @param int    $field_name WP post meta field name.
	 * @param string $text       Text to be translated.
	 */
	public function user_translation_update( $post_id, $field_name, $text ) {
		$all_meta = get_user_meta( $post_id );
		$output   = [];

		foreach ( $all_meta as $meta_key => $meta_value ) {
			$matches = preg_match( "/{$field_name}/i", $meta_key );

			if ( $matches && '' !== $meta_value ) {
				$target_language = preg_replace( "/{$field_name}/i", '', $meta_key );
				$translation     = $this->translate_text( $text, $target_language );
				$result          = update_user_meta( $post_id, $meta_key, $translation );
			}
		}
	}

	/**
	 * Use GoogleTranslate services to translate the text string
	 *
	 * @param string $text   Text to be translated.
	 * @param string $target Target language - 2 digit ISO code.
	 *
	 * @return string $translation
	 */
	public function translate_text( $text, $target ) {
		try {
			$source = $this->translate->detectLanguage( $text );
			$result = $this->translate->translate(
				$text,
				[
					'source' => $source['languageCode'],
					'target' => $target,
				]
			);

			$translation = $result['text'];
		} catch ( \Exception $e ) {
			return $text;
		}
		return $translation;
	}
}
