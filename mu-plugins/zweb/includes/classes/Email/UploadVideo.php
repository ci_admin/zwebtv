<?php

namespace Zweb\Email;

use Zweb\Admin\Settings;

/**
 * Class UploadVideo
 *
 * @package Zweb\Email
 */
class UploadVideo {

	/**
	 * Send email for video upload.
	 *
	 * @param $video_title
	 * @param $influencer_name
	 * @param $edit_link
	 */
	public static function send( $video_title, $influencer_name, $edit_link ) {
		$settings = Settings::get_site_settings();
		$email    = $settings['sitewide']['email-for-notifications'];
		if ( $email ) {
			$subject = sprintf( __( 'A new video has been uploaded by %s', 'zweb' ), $influencer_name );
			$body    = sprintf(
				__( 'The video %s has been uploaded by %s and it\'s awaiting moderation. Here is the link %s', 'zweb' ),
				$video_title,
				$influencer_name,
				$edit_link
			);
			$mail_sent = wp_mail( $email, $subject, $body );
			if ( ! $mail_sent ) {
				error_log( 'There was an error sending the mail' );
			}
		}
	}
}