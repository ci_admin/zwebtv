<?php

namespace Zweb\Brightcove;

use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;

/**
 * Class Player
 *
 * @package Zweb\Brightcove
 */
class Player {

	/**
	 * Register hooks
	 */
	public static function register() {
		add_filter(
			'brightcove_get_all_player',
			[
				static::class,
				'brightcove_get_all_player',
			]
		);
	}

	/**
	 * Return only default player if it's set.
	 *
	 * @param $players
	 *
	 * @return array
	 */
	public static function brightcove_get_all_player( $players ) {
		global $bc_accounts;
		$account_id = $bc_accounts->get_account_id();
		$screen = get_current_screen();
		$restricted_types = [
			Video::POST_TYPE_NAME => true,
		];
		if (
			'post' === $screen->base && isset( $restricted_types[$screen->id] ) &&
			$account_id &&
			isset( $players[ $account_id ] ) &&
			defined( 'BC_DEFAULT_PLAYER' ) && BC_DEFAULT_PLAYER
		) {
			$player_to_return = array_filter( $players[ $account_id ], function ( $player ) {
				return $player['id'] === BC_DEFAULT_PLAYER;
			} );

			if ( $player_to_return ) {
				return [ $account_id => $player_to_return ];
			}
		}

		return $players;
	}
}