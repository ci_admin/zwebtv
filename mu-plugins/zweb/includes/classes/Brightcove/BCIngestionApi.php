<?php

namespace Zweb\Brightcove;

require_once( BRIGHTCOVE_PATH . 'includes/class-bc-utility.php' );
require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-api.php' );
require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-oauth.php' );
require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-cms-api.php' );
require_once( BRIGHTCOVE_PATH . 'includes/class-bc-logging.php' );

/**
 * Class BCIngestionApi
 *
 * @package Zweb\Brightcove
 */
class BCIngestionApi extends \BC_CMS_API {

	/**
	 * Get s3
	 *
	 * @param string $video_id
	 * @param string $video_file_name
	 *
	 * @return bool|mixed|string
	 */
	public function upload_urls( $video_id, $video_file_name ) {

		return $this->send_request(
			esc_url_raw(
				self::DI_BASE_URL . $this->get_account_id() . '/videos/' . $video_id .
				'/upload-urls/' . sanitize_file_name( $video_file_name )
			)
		);
	}

	/**
	 * Get s3 urls
	 *
	 * @param string $video_id
	 * @param string $signed_api_url
	 * @param string $profile
	 *
	 * @return bool|mixed|string
	 */
	public function ingest_s3( $video_id, $signed_api_url, $profile = '' ) {
		$profile = apply_filters( 'brightcove_ingest_profile', $profile );

		$data                   = ( ! empty( $profile ) ) ? [ 'profile' => sanitize_text_field( $profile ) ] : [];
		$data['master']         = [ 'url' => esc_url_raw( $signed_api_url ) ];
		$data['capture-images'] = true;

		return $this->send_request( esc_url_raw( self::DI_BASE_URL . $this->get_account_id() . '/videos/' . $video_id . '/ingest-requests' ), 'POST', $data );
	}

	/**
	 * Add manifest
	 *
	 * @param string $video_id
	 * @param string $remote_url
	 *
	 * @return bool|mixed|string
	 */
	public function hls_manifest( $video_id, $remote_url ) {
		$data['remote_url'] = esc_url_raw( $remote_url );
		return $this->send_request( esc_url_raw( self::CMS_BASE_URL . $this->get_account_id() . '/videos/' . $video_id . '/assets/hls_manifest' ), 'POST', $data );
	}
}
