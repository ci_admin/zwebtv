<?php

namespace Zweb\Brightcove;

use Zweb\Admin\Settings;
use Zweb\Builder\Query;
use Zweb\Taxonomy\Influencer;

/**
 * Class Live
 *
 * @package Zweb\Brightcove
 */
class Live {

	/**
	 * Check if the Roma Studio is live.
	 *
	 * @return bool
	 */
	public static function is_studio_roma_live() {
		$settings = Settings::get_site_settings();

		return $settings['live']['state'] === 'live';
	}
}