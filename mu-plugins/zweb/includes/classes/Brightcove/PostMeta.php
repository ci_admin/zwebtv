<?php

namespace Zweb\Brightcove;

class PostMeta {

	/**
	 * This hooks runs on save and puts some metadata of the video in post_meta.
	 *
	 * @param $post_id
	 * @param $post
	 */
	public static function save_brightcove_data( $post ) {
		global $bc_accounts;
		$post_id = $post->ID;

		$blocks = parse_blocks( $post->post_content );
		if ( $blocks && isset( $blocks[0]['attrs'], $blocks[0]['attrs']['video_id'] ) ) {
			require_once( BRIGHTCOVE_PATH . 'includes/class-bc-utility.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-api.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-oauth.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-cms-api.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/class-bc-logging.php' );

			$bc_cms      = new \BC_CMS_API();
			$video_id    = $blocks[0]['attrs']['video_id'];
			$account_id  = $bc_accounts->get_account_id();
			$player_id   = $blocks[0]['attrs']['player_id'];
			$bc_video    = $bc_cms->video_get( $video_id );
			$update_data = [
				'tags' => [],
			];

			// Set the square and 16:9 video ID's and player details as post meta.
			update_post_meta( $post_id, 'bc_player_id', $player_id );
			update_post_meta( $post_id, 'bc_video_id', $video_id );
			update_post_meta( $post_id, 'bc_account_id', $account_id );

			// Get the associated Influencer.
			try {
				$influencer          = \Zweb\Taxonomy\Influencer::get_influencer_for_video( $post_id );
				$influencer_user     = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
				$update_data['tags'] = [ $influencer_user->user_login ];

				// If this is a studio influencer's video then attach the studio id to the post meta
				$studio_influencer = \Zweb\PostType\Studio::is_studio_influencer( $influencer_user );

				if ( $studio_influencer ) {
					update_post_meta( $post_id, 'influencer_studio_id', $studio_influencer );
				}
			} catch ( \Exception $exception ) {
			}

			// Set the influencer username as a tag on both videos, only do this when the value changes.
			if ( isset( $update_data['tags'][0] ) && $update_data['tags'][0] !== $bc_video['tags'][0] ) {
				$bc_cms->video_update( $video_id, $update_data );
			}

			if ( ! $bc_video || ! $bc_video['complete'] ) {
				update_post_meta( $post_id, 'bc_video_is_processing', true );
			} else {
				// Set the video duration and images as post meta.
				update_post_meta( $post_id, 'bc_duration', $bc_video['duration'] );
				update_post_meta( $post_id, 'bc_images', $bc_video['images'] );
				delete_post_meta( $post_id, 'bc_video_is_processing' );
			}
		}
	}
}
