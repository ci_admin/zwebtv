<?php

namespace Zweb\Brightcove;

/**
 * Class Ingestion
 *
 * @package Zweb\Brightcove
 */
class Ingestion {

	/**
	 * Register filter
	 */
	public static function register() {
		add_filter( 'brightcove_ingest_profile', [ static::class, 'brightcove_ingest_profile'] );
	}

	/**
	 *
	 * Select the ingestion profile
	 * @param $profile
	 *
	 * @return string
	 */
	public static function brightcove_ingest_profile( $profile ) {
		return 'multi-platform-standard-static-with-mp4';
	}
}