<?php

namespace Zweb\Brightcove;

use Zweb\PostType\Video;

/**
 * Class BrightcoveCron
 *
 * @package Zweb\Brightcove
 */
class BrightcoveCron {

	public function __construct() {
		add_action( 'wp_loaded', [ $this, 'set_cron_event' ] );
		add_filter( 'cron_schedules', [
			$this,
			'brightcove_ingestion_cron_schedule',
		] );
	}

	/**
	 * Add a new cron schedule of every 2 minutes.
	 *
	 * @param array $schedules
	 *
	 * @return array
	 */
	public function brightcove_ingestion_cron_schedule( $schedules ) {
		$schedules['every-2-minutes'] = [
			'interval' => 2 * MINUTE_IN_SECONDS,
			'display'  => __( 'Every 2 minutes', 'zweb' ),
		];

		return $schedules;
	}

	/**
	 * Schedules brightcove ingestion cron event for every 5 minutes.
	 *
	 * @return void
	 */
	public function set_cron_event() {
		add_action( 'brightcove_finished_processing_videos', [
			$this,
			'brightcove_finished_processing_videos',
		] );

		if ( ! wp_next_scheduled( 'brightcove_finished_processing_videos' ) ) {
			wp_schedule_event( time(), 'every-2-minutes', 'brightcove_finished_processing_videos' );
		}
	}

	/**
	 * Check if posts which have videos which are being processed can be
	 * published
	 */
	function brightcove_finished_processing_videos() {

		$processing_video_posts = new \WP_Query(
			[
				'posts_per_page' => 20,
				'fields'         => 'ids',
				'post_type'      => Video::POST_TYPE_NAME,
				'post_status'    => [ 'pending', 'publish' ],
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'value'   => true,
						'compare' => '=',
					],
				],
			]
		);

		if ( $processing_video_posts->have_posts() ) {
			require_once( BRIGHTCOVE_PATH . 'includes/class-bc-utility.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-api.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-oauth.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/api/class-bc-cms-api.php' );
			require_once( BRIGHTCOVE_PATH . 'includes/class-bc-logging.php' );

			$bc_cms = new \BC_CMS_API();
			foreach ( $processing_video_posts->get_posts() as $post_id ) {
				$video_id = get_post_meta( $post_id, 'bc_video_id', true );
				$video    = $bc_cms->video_get( $video_id );

				if ( ! is_wp_error( $video ) && $this->is_video_complete( $video ) ) {
					update_post_meta( $post_id, 'bc_duration', $video['duration'] );
					update_post_meta( $post_id, 'bc_images', $video['images'] );
					delete_post_meta( $post_id, 'bc_video_is_processing' );
				}
			}
		}
	}

	/**
	 * Has the video finished processing?
	 *
	 * @param $video
	 *
	 * @return bool
	 */
	public function is_video_complete( $video ) {
		return $video && $video['complete'] && isset( $video['duration'], $video['images'] );
	}

}
