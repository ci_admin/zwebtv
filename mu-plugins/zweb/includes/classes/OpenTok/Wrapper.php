<?php

namespace Zweb\OpenTok;

use OpenTok\OpenTok;
use Zweb\Admin\LiveStreaming;

/**
 * Class Wrapper
 *
 * @package Zweb\OpenTok
 */
class Wrapper {

	/**
	 * @var OpenTok
	 */
	protected static $instance;

	/**
	 * Get instance
	 *
	 * @return OpenTok
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			$settings       = LiveStreaming::get_vonage_key_secret();
			self::$instance = new OpenTok( $settings['api_key'], $settings['api_secret'] );
		}

		return self::$instance;
	}
}
