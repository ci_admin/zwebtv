<?php

namespace Zweb\GraphQL\Resolvers;

use Zweb\Admin\Settings;

/**
 * Class PromoBoxResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class PromoBoxResolver {
	/**
	 *  Register new types for Videos custom post type.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_promobox_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_promobox_field',
			]
		);
	}

	/**
	 * Build new Promo box fields.
	 */
	public static function register_promobox_type() {
		register_graphql_object_type(
			'PromoBox',
			[
				'description' => __( 'Promo box fields', 'zweb' ),
				'fields'      => [
					'app_promo_image'       => [
						'type'        => 'String',
						'name'        => 'app_promo_image',
						'description' => __( 'App promo box image', 'zweb' ),
					],
					'app_promo_image_title' => [
						'type'        => 'String',
						'name'        => 'app_promo_image_title',
						'description' => __( 'App promo box title', 'zweb' ),
					],
					'app_promo_image_text'  => [
						'type'        => 'String',
						'name'        => 'app_promo_image_text',
						'description' => __( 'App promo box text', 'zweb' ),
					],
					'app_promo_image_url'   => [
						'type'        => 'String',
						'name'        => 'app_promo_image_url',
						'description' => __( 'App promo box url', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Build new Promo box query type.
	 */
	public static function register_promobox_field() {
		register_graphql_field(
			'RootQuery',
			'getPromoBox',
			[
				'name'        => 'getPromoBox',
				'description' => __( 'Get promo box settings', 'zweb' ),
				'type'        => 'PromoBox',
				'resolve'     => function() {
					$settings = Settings::get_site_settings();

					return [
						'app_promo_image'       => wp_get_attachment_url( $settings['apppromo']['app-promo-image'] ),
						'app_promo_image_title' => $settings['apppromo']['app-promo-image-title'],
						'app_promo_image_text'  => $settings['apppromo']['app-promo-image-text'],
						'app_promo_image_url'   => $settings['apppromo']['app-promo-image-url'],
					];
				},
			]
		);
	}
}
