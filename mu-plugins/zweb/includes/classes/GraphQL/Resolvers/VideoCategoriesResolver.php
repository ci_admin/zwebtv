<?php

namespace Zweb\GraphQL\Resolvers;

/**
 * Class VideoCategoriesResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class VideoCategoriesResolver {
	/**
	 *  Register new types for VideoCategories.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_category_meta',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_field',
			]
		);
	}

	/**
	 * Register video meta on the video category query.
	 */
	public static function register_video_category_meta() {
		register_graphql_field(
			'VideoCategory',
			'categoryImage',
			[
				'type'        => 'String',
				'name'        => 'categoryImage',
				'description' => __( 'Category Image', 'zweb' ),
				'resolve'     => function( \WPGraphQL\Model\Term $term ) {
					$category_img     = get_term_meta( $term->term_id, 'zweb-image_id', true );
					$category_img_url = wp_get_attachment_image_url( $category_img, 'full' );
					return $category_img_url;
				}
			]
		);

		register_graphql_field(
			'VideoCategory',
			'categoryLandingPageImage',
			[
				'type'        => 'String',
				'name'        => 'categoryLandingPageImage',
				'description' => __( 'Category Landing Page Image', 'zweb' ),
				'resolve'     => function( \WPGraphQL\Model\Term $term ) {
					$landing_img     = get_term_meta( $term->term_id, 'landing_page_image_id', true );
					$landing_img_url = wp_get_attachment_image_url( $landing_img, 'full' );
					return $landing_img_url;
				}
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_video_type() {
		register_graphql_object_type(
			'categoryVideos',
			[
				'description' => __( 'Related videos', 'zweb' ),
				'fields'      => [
					'id'          => [
						'type'        => 'Int',
						'description' => __( 'Post ID', 'zweb' ),
					],
					'title'       => [
						'type'        => 'String',
						'description' => __( 'Post/ Video title', 'zweb' ),
					],
					'uri'         => [
						'type'        => 'String',
						'description' => __( 'Post/ Video URI', 'zweb' ),
					],
					'bcAccountId' => [
						'type'        => 'String',
						'description' => __( 'Brightcove Account ID', 'zweb' ),
					],
					'bcVideoID'   => [
						'type'        => 'String',
						'description' => __( 'Brightcove Video ID', 'zweb' ),
					],
					'bcDuration'  => [
						'type'        => 'String',
						'description' => __( 'Brightcove viedo duration', 'zweb' ),
					],
					'bcImage'     => [
						'type'        => 'String',
						'description' => __( 'Brightcove video cover images', 'zweb' ),
					],
					'bcPlayerID'  => [
						'type'        => 'String',
						'description' => __( 'Brightcove player ID', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_video_field() {
		register_graphql_field(
			'VideoCategory',
			'getVideos',
			[
				'name'        => 'getVideos',
				'description' => __( 'Get videos', 'zweb' ),
				'type'        => [ 'list_of' => 'categoryVideos' ],
				'resolve'     => function( \WPGraphQL\Model\Term $term ) {

					$related = new \WP_Query(
						[
							'post_type'      => 'zweb-video',
							'post_status'    => 'publish',
							'posts_per_page' => 100,
							'tax_query'      => [
								[
									'taxonomy' => 'zweb-video-category',
									'field'    => 'term_id',
									'terms'    => $term->term_id,
								],
							],
						]
					);

					$related_posts = $related->get_posts();
					$related_array = array();

					foreach ( $related_posts as $index => $related_post ) {
						$related_array[ $index ]['id']          = $related_post->ID;
						$related_array[ $index ]['title']       = $related_post->post_title;
						$related_array[ $index ]['uri']         = wp_make_link_relative( get_permalink( $related_post->ID ) );
						$related_array[ $index ]['bcAccountId'] = get_post_meta( $related_post->ID, 'bc_account_id', true );
						$related_array[ $index ]['bcVideoID']   = get_post_meta( $related_post->ID, 'bc_video_id', true );
						$related_array[ $index ]['bcDuration']  = get_post_meta( $related_post->ID, 'bc_duration', true );
						$related_array[ $index ]['bcImage']     = wp_json_encode( get_post_meta( $related_post->ID, 'bc_images', true ) );
						$related_array[ $index ]['bcPlayerID']  = get_post_meta( $related_post->ID, 'bc_player_id', true );
					}

					return $related_array;
				}
			]
		);
	}
}
