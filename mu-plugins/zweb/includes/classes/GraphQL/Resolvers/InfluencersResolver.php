<?php

namespace Zweb\GraphQL\Resolvers;

use Zweb\Multilingual\GoogleTranslate;
use Zweb\Firebase\Provider;
use Zweb\Firebase\Firestore;

/**
 * Class InfluencersResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class InfluencersResolver {
	/**
	 * The Target laguage for translation.
	 *
	 * @var String
	 */
	private static $target_language = null;

	/**
	 *  Register new types for Influencer custom taxonomy.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_user_meta',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_influencers_query_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_field',
			]
		);
	}

	/**
	 * Register user meta on the influencer query.
	 */
	public static function register_user_meta() {
		register_graphql_field(
			'Influencer',
			'user_login',
			[
				'type'        => 'String',
				'name'        => 'user_login',
				'description' => __( 'Influencer user login', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return $influencer_user->user_login;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_nicename',
			[
				'type'        => 'String',
				'name'        => 'user_nicename',
				'description' => __( 'Influencer user nicename', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return $influencer_user->user_nicename;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'display_name',
			[
				'type'        => 'String',
				'name'        => 'display_name',
				'description' => __( 'Influencer user display name', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return $influencer_user->display_name;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'first_name',
			[
				'type'        => 'String',
				'name'        => 'first_name',
				'description' => __( 'Influencer user first name', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$first_name      = get_user_meta( $influencer_user->ID, 'first_name', true );

						return $first_name;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'last_name',
			[
				'type'        => 'String',
				'name'        => 'last_name',
				'description' => __( 'Influencer user last name', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$last_name       = get_user_meta( $influencer_user->ID, 'last_name', true );

						return $last_name;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_email',
			[
				'type'        => 'String',
				'name'        => 'user_email',
				'description' => __( 'Influencer user email', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return sanitize_email( $influencer_user->user_email );
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_image_url',
			[
				'type'        => 'String',
				'name'        => 'user_image_url',
				'description' => __( 'Influencer user image url', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return get_avatar_url( $influencer_user->ID );
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_bio',
			[
				'type'        => 'String',
				'name'        => 'user_bio',
				'description' => __( 'Influencer user bio', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term, $args ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$bio             = $influencer_user->description;

						if ( ! empty( self::$target_language ) ) {
							if ( ! empty( $bio ) ) {
								$translation = GoogleTranslate::register();
								$target_lang = self::$target_language;
								$field_name  = "description_{$target_lang}";
								$bio         = $translation->get_user_translation( $influencer_user->ID, $field_name, $bio, $target_lang );
							}
						}

						return html_entity_decode( $bio, ENT_QUOTES );
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_category_id',
			[
				'type'        => 'String',
				'name'        => 'user_category_id',
				'description' => __( 'Influencer user category id', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );

						return $influencer_user->{'zweb-influencer_video_category'};
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_category_name',
			[
				'type'        => 'String',
				'name'        => 'user_category_name',
				'description' => __( 'Influencer user category name', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user    = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_category_name = $influencer_user->{'zweb-influencer_video_category'} ? get_term( $influencer_user->{'zweb-influencer_video_category'} )->name : '';

						return $user_category_name;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_landing_image',
			[
				'type'        => 'String',
				'name'        => 'user_landing_image',
				'description' => __( 'Influencer user landing page image', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user    = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_landing_image = $influencer_user->{'zweb-influencer_landing_page_image'} ? wp_get_attachment_url( $influencer_user->{'zweb-influencer_landing_page_image'} ) : '';

						return $user_landing_image;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_landing_live_image',
			[
				'type'        => 'String',
				'name'        => 'user_landing_live_image',
				'description' => __( 'Influencer user landing page live image', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user         = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_landing_live_image = $influencer_user->{'zweb-influencer_landing_page_image_live'} ? wp_get_attachment_url(
							$influencer_user->{'zweb-influencer_landing_page_image_live'}
						) : '';

						return $user_landing_live_image;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_profile_image',
			[
				'type'        => 'String',
				'name'        => 'user_profile_image',
				'description' => __( 'Influencer user profile image', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user    = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$instance           = Provider::register();
						$firebase           = $instance->get_firebase_instance();
						$auth               = $firebase->getAuth();
						$firebase_user      = $auth->getUserByEmail( $influencer_user->user_email );
						$firestore          = Firestore::get_instance();
						$user_profile_image = $firestore->get_document_field( 'users', $firebase_user->uid, 'avatar' );

						update_user_meta( $influencer_user->ID, 'zweb-influencer_avatar', $user_profile_image );

						return $user_profile_image;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_card_image',
			[
				'type'        => 'String',
				'name'        => 'user_card_image',
				'description' => __( 'Influencer card image', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_card_image = $influencer_user->{'zweb-influencer_influencer_card_image'} ? wp_get_attachment_url( $influencer_user->{'zweb-influencer_influencer_card_image'} ) : '';

						return $user_card_image;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_upcoming_image',
			[
				'type'        => 'String',
				'name'        => 'user_upcoming_image',
				'description' => __( 'Influencer upcoming image', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user     = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_upcoming_image = $influencer_user->{'zweb-influencer_influencer_upcoming_image'} ? wp_get_attachment_url(
							$influencer_user->{'zweb-influencer_influencer_upcoming_image'}
						) : '';

						return $user_upcoming_image;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_show_upcoming',
			[
				'type'        => 'String',
				'name'        => 'user_show_upcoming',
				'description' => __( 'Influencer show upcoming', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user    = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_show_upcoming = $influencer_user->{'zweb-influencer_show_as_upcoming'};

						return $user_show_upcoming;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_upcoming_date',
			[
				'type'        => 'String',
				'name'        => 'user_upcoming_date',
				'description' => __( 'Influencer upcoming date', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user    = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_upcoming_date = $influencer_user->{'zweb-influencer_upcoming_date'};

						return $user_upcoming_date;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'user_show_influencer',
			[
				'type'        => 'String',
				'name'        => 'user_show_influencer',
				'description' => __( 'Influencer prevent showing on influencer page', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user      = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$user_show_influencer = $influencer_user->{'zweb-influencer_prevent_showing_in_influencer_page'};

						return $user_show_influencer;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Influencer',
			'Firebase Influencer Id',
			[
				'type'        => 'String',
				'name'        => 'influencer_id',
				'description' => __( 'Firebase uid', 'zweb' ),
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {
					try {
						$influencer_user = \Zweb\Taxonomy\Influencer::get_user_from_term( $term->term_id );
						$firebase_uid    = get_user_meta( $influencer_user->ID, 'zweb-influencer_firebase-uid', true );

						if ( empty( $firebase_uid ) ) {
							$instance      = Provider::register();
							$firebase      = $instance->get_firebase_instance();
							$auth          = $firebase->getAuth();
							$firebase_user = $auth->getUserByEmail( $influencer_user->user_email );
							$firebase_uid  = $firebase_user->uid;

							update_user_meta( $influencer_user->ID, 'zweb-influencer_firebase-uid', $firebase_uid );
						}

						return $firebase_uid;
					} catch ( \Exception $exception ) {
						return null;
					}
				},
			]
		);
	}

	/**
	 * Build new InfluencersQuery type.
	 */
	public static function register_influencers_query_type() {
		$config = [
			'fromType'       => 'RootQuery',
			'toType'         => 'Influencer',
			'fromFieldName'  => 'InfluencersQuery',
			'connectionArgs' => self::register_connection_args(),
			'resolve'        => function ( $id, $args, $context, $info ) {
				$resolver = new \WPGraphQL\Data\Connection\TermObjectConnectionResolver( $id, $args, $context, $info, 'zweb-influencer-category' );

				// Get connectionArgs applied.
				$connection_args = $args && isset( $args['where'] ) ? $args['where'] : [];

				if ( ! empty( $connection_args['language'] ) ) {
					self::$target_language = $connection_args['language'];
				}

				$term_array = [];

				// Query by influencer_id (influencer_id).
				if ( ! empty( $connection_args['influencer_id'] ) ) {
					$term_array[] = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $connection_args['influencer_id'] );
				}

				// Query by list of influencer_ids being followed.
				if ( isset( $connection_args['influencer_ids'] ) && is_array( $connection_args['influencer_ids'] ) ) {
					if ( ! empty( $connection_args['influencer_ids'] ) ) {
						foreach ( $connection_args['influencer_ids'] as $influencer ) {
							$term_array[] = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $influencer );
						}
					} elseif ( [] === $connection_args['influencer_ids'] ) {
						$term_array = [ 0 ];
					}
				}

				// Query by category id (category_id).
				if ( ! empty( $connection_args['category_id'] ) ) {
					$category_id = $connection_args['category_id'];
					$users       = get_users(
						[
							'meta_query' => [
								'relation' => 'AND',
								[
									'key'     => 'zweb-influencer_video_category',
									'compare' => 'EXISTS',
								],
								[
									'key'     => 'zweb-influencer_video_category',
									'value'   => $category_id,
									'compare' => '=',
								],
							],
						]
					);

					$cat_array = [];
					foreach ( $users as $user ) {
						$category_influencer = get_user_meta( $user->ID, 'influencer_term', true );
						$cat_array[]         = $category_influencer;
					}

					if ( empty( $cat_array ) ) {
						$term_array = [ 0 ];
					} else {
						if ( ! empty( $term_array ) ) {
							$term_array = array_intersect( $cat_array, $term_array );
							if ( empty( $term_array ) ) {
								$term_array = [ 0 ];
							}
						} else {
							$term_array = $cat_array;
						}
					}
				}

				// Query by the influencer being active (influencer_active).
				if ( isset( $connection_args['influencer_active'] ) ) {
					if ( true === $connection_args['influencer_active'] ) {
						$users = get_users(
							[
								'meta_query' => [
									'relation' => 'OR',
									[
										'key'     => 'zweb-influencer_show_as_upcoming',
										'compare' => 'NOT EXISTS',
									],
									[
										'key'     => 'zweb-influencer_show_as_upcoming',
										'value'   => true,
										'compare' => '!=',
									],
								],
							]
						);
					} else {
						$users = get_users(
							[
								'meta_query' => [
									'relation' => 'AND',
									[
										'key'     => 'zweb-influencer_show_as_upcoming',
										'compare' => 'EXISTS',
									],
									[
										'key'     => 'zweb-influencer_show_as_upcoming',
										'value'   => true,
										'compare' => '=',
									],
								],
							]
						);
					}

					$active_array = [];
					foreach ( $users as $user ) {
						$influencer     = get_user_meta( $user->ID, 'influencer_term', true );
						$active_array[] = $influencer;
					}

					if ( empty( $active_array ) ) {
						$term_array = [ 0 ];
					} else {
						if ( ! empty( $term_array ) ) {
							$term_array = array_intersect( $term_array, $active_array );
							if ( empty( $term_array ) ) {
								$term_array = [ 0 ];
							}
						} else {
							$term_array = $active_array;
						}
					}
				}

				if ( ! empty( $term_array ) ) {
					$resolver->setQueryArg( 'term_taxonomy_id', $term_array );
				}

				$exclude_array = [];

				// Exclude all studio influencers
				$studio_influencers = get_users(
					[
						'meta_query' => [
							'relation' => 'AND',
							[
								'key'     => 'zweb-influencer_parent_studio',
								'compare' => 'EXISTS',
							],
							[
								'key'     => 'zweb-influencer_parent_studio',
								'value'   => '',
								'compare' => '!=',
							],
						],
					]
				);

				foreach ( $studio_influencers as $studio_influencer ) {
					$exclude_array[] = get_user_meta( $studio_influencer->ID, 'influencer_term', true );
				}

				// Exclude Highlights
				// This has currently been hardcoded but can be removed once data is set up correctly
				$highlights = get_user_by( 'login', 'Highlights' );

				if ( ! empty( $highlights ) ) {
					$exclude_array[] = get_user_meta( $highlights->ID, 'influencer_term', true );
				}

				// Exclude Studio Roma
				// This has currently been hardcoded but can be removed once data is set up correctly
				$roma = get_user_by( 'login', 'StudioRoma' );

				if ( ! empty( $roma ) ) {
					$exclude_array[] = get_user_meta( $roma->ID, 'influencer_term', true );
				}

				if ( ! empty( $exclude_array ) ) {
					$resolver->setQueryArg( 'exclude', $exclude_array );
				}

				// Query by page_size (number) and page (page).
				if ( ! empty( $connection_args['page_size'] ) ) {
					$resolver->setQueryArg( 'number', $connection_args['page_size'] );
					if ( ! empty( $connection_args['page'] ) ) {
						$page_offset = ( $connection_args['page'] - 1 ) * $connection_args['page_size'];
						$resolver->setQueryArg( 'offset', $page_offset );
					}
				}

				// Query by field (orderby).
				if ( ! empty( $connection_args['field'] ) ) {
					$resolver->setQueryArg( 'orderby', $connection_args['field'] );
				}

				// Query by field (direction).
				if ( ! empty( $connection_args['direction'] ) ) {
					$resolver->setQueryArg( 'order', $connection_args['direction'] );
				}

				// Query by search term.
				if ( ! empty( $connection_args['search_term'] ) ) {
					$resolver->setQueryArg( 'orderby', 'slug' );
					$resolver->setQueryArg( 'search', $connection_args['search_term'] );
				}

				$connection = $resolver->get_connection();

				return $connection;
			},
		];
		register_graphql_connection( $config );
	}


	/**
	 * Register connection arguments on the influencer query.
	 */
	public static function register_connection_args() {
		return [
			'influencer_id'     => [
				'type'        => 'String',
				'name'        => 'influencer_id',
				'description' => __( 'Influencer ID for video.', 'zweb' ),
			],
			'influencer_active' => [
				'type'        => 'Boolean',
				'name'        => 'influencer_active',
				'description' => __( 'Is influencer active.', 'zweb' ),
			],
			'category_id'       => [
				'type'        => 'Int',
				'name'        => 'category_id',
				'description' => __( 'Category ID for influencer.', 'zweb' ),
			],
			'page_size'         => [
				'type'        => 'Int',
				'name'        => 'page_size',
				'description' => __( 'Page size for influencer.', 'zweb' ),
			],
			'page'              => [
				'type'        => 'Int',
				'name'        => 'page',
				'description' => __( 'Page for influencer.', 'zweb' ),
			],
			'field'             => [
				'type'        => 'String',
				'name'        => 'field',
				'description' => __( 'Field for influencer.', 'zweb' ),
			],
			'direction'         => [
				'type'        => 'String',
				'name'        => 'direction',
				'description' => __( 'Direction for influencer.', 'zweb' ),
			],
			'search_term'       => [
				'type'        => 'String',
				'name'        => 'search_term',
				'description' => __( 'Search term for influencer.', 'zweb' ),
			],
			'influencer_ids'    => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'influencer_ids',
				'description' => __( 'List of influencers being followed.', 'zweb' ),
			],
			'language'          => [
				'type'        => 'String',
				'name'        => 'language',
				'description' => __( 'Translation language.', 'zweb' ),
			],
		];
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_video_type() {
		register_graphql_object_type(
			'influencerVideos',
			[
				'description' => __( 'Related videos', 'zweb' ),
				'fields'      => [
					'id'          => [
						'type'        => 'Int',
						'name'        => 'id',
						'description' => __( 'Post ID', 'zweb' ),
					],
					'title'       => [
						'type'        => 'String',
						'name'        => 'title',
						'description' => __( 'Post/ Video title', 'zweb' ),
					],
					'uri'         => [
						'type'        => 'String',
						'name'        => 'uri',
						'description' => __( 'Post/ Video URI', 'zweb' ),
					],
					'bcAccountId' => [
						'type'        => 'String',
						'name'        => 'bcAccountId',
						'description' => __( 'Brightcove Account ID', 'zweb' ),
					],
					'bcVideoID'   => [
						'type'        => 'String',
						'name'        => 'bcVideoID',
						'description' => __( 'Brightcove Video ID', 'zweb' ),
					],
					'bcDuration'  => [
						'type'        => 'String',
						'name'        => 'bcDuration',
						'description' => __( 'Brightcove viedo duration', 'zweb' ),
					],
					'bcImage'     => [
						'type'        => 'String',
						'name'        => 'bcImage',
						'description' => __( 'Brightcove video cover images', 'zweb' ),
					],
					'bcPlayerID'  => [
						'type'        => 'String',
						'name'        => 'bcPlayerID',
						'description' => __( 'Brightcove player ID', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_video_field() {
		register_graphql_field(
			'Influencer',
			'getVideos',
			[
				'description' => __( 'Get videos', 'zweb' ),
				'name'        => 'getVideos',
				'type'        => [ 'list_of' => 'influencerVideos' ],
				'resolve'     => function ( \WPGraphQL\Model\Term $term ) {

					$related = new \WP_Query(
						[
							'post_type'      => 'zweb-video',
							'post_status'    => 'publish',
							'posts_per_page' => 100,
							'tax_query'      => [
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => $term->term_id,
								],
							],
						]
					);

					$related_posts = $related->get_posts();
					$related_array = [];

					if ( ! empty( self::$target_language ) ) {
						$translation = GoogleTranslate::register();
					}

					foreach ( $related_posts as $index => $related_post ) {
						$related_array[ $index ]['id']          = $related_post->ID;
						$related_array[ $index ]['uri']         = wp_make_link_relative( get_permalink( $related_post->ID ) );
						$related_array[ $index ]['bcAccountId'] = get_post_meta( $related_post->ID, 'bc_account_id', true );
						$related_array[ $index ]['bcVideoID']   = get_post_meta( $related_post->ID, 'bc_video_id', true );
						$related_array[ $index ]['bcDuration']  = get_post_meta( $related_post->ID, 'bc_duration', true );
						$related_array[ $index ]['bcImage']     = wp_json_encode( get_post_meta( $related_post->ID, 'bc_images', true ) );
						$related_array[ $index ]['bcPlayerID']  = get_post_meta( $related_post->ID, 'bc_player_id', true );

						$video_title = html_entity_decode( $related_post->post_title, ENT_QUOTES );
						if ( ! empty( self::$target_language ) ) {
							if ( ! empty( $related_post->post_title ) ) {
								$target_lang                      = self::$target_language;
								$field_name                       = "title_{$target_lang}";
								$related_array[ $index ]['title'] = $translation->get_post_translation( $related_post->ID, $field_name, $video_title, $target_lang );
							}
						} else {
							$related_array[ $index ]['title'] = $video_title;
						}
					}

					return $related_array;
				},
			]
		);
	}
}
