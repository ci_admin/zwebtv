<?php

namespace Zweb\GraphQL\Resolvers;

use Zweb\Admin\Settings;
use Zweb\Multilingual\GoogleTranslate;
use Zweb\Firebase\UserManagement;
use Zweb\Firebase\Provider;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;

/**
 * Class VideosResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class VideosResolver {
	/**
	 * The Target laguage for translation.
	 *
	 * @var String
	 */
	private static $target_language = null;

	/**
	 * Hardcoded list of posts that needs not to be returned.
	 *
	 * @var int[]
	 */
	private static $excluded_posts = [ 331 => true ];

	/**
	 *  Register new types for Videos custom post type.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_meta',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_videos_query_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_related_video_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_related_video_field',
			]
		);

		add_filter(
			'graphql_resolve_field',
			function ( $result, $source, $args, $context, $info, $type_name, $field_key, $field, $field_resolver ) {
				if (
					'Video' === $type_name && 'status' === $field_key
					&& in_array( $source->post_type, [ LiveVideo::POST_TYPE_NAME, LiveBattle::POST_TYPE_NAME ], true )
				) {
					$result = get_post_meta( $source->ID, 'live_video_status', true );
					$result = 'streaming' === $result ? 'publish' : $result;
				}

				return $result;
			},
			10,
			9
		);
	}


	/**
	 * Register video meta on the videos query.
	 */
	public static function register_video_meta() {
		register_graphql_field(
			'Video',
			'bc_player_id',
			[
				'type'        => 'String',
				'name'        => 'bc_player_id',
				'description' => __( 'Brightcove player ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_player_id = get_post_meta( $post->ID, 'bc_player_id', true );

					return $bc_player_id;
				},
			]
		);

		register_graphql_field(
			'Video',
			'bcStatus',
			[
				'type'        => 'String',
				'name'        => 'bcStatus',
				'description' => __( 'Brightcove Video status ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'bc_video_status', true );
				},
			]
		);

		register_graphql_field(
			'Video',
			'bc_video_id',
			[
				'type'        => 'String',
				'name'        => 'bc_video_id',
				'description' => __( 'Brightcove video ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_video_id = get_post_meta( $post->ID, 'bc_video_id', true );

					return $bc_video_id;
				},
			]
		);

		register_graphql_field(
			'Video',
			'bc_account_id',
			[
				'type'        => 'String',
				'name'        => 'bc_account_id',
				'description' => __( 'Brightcove account ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_account_id = get_post_meta( $post->ID, 'bc_account_id', true );

					return $bc_account_id;
				},
			]
		);

		register_graphql_field(
			'Video',
			'bc_duration',
			[
				'type'        => 'String',
				'name'        => 'bc_duration',
				'description' => __( 'Brightcove video duration', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_duration = get_post_meta( $post->ID, 'bc_duration', true );

					return $bc_duration;
				},
			]
		);

		register_graphql_field(
			'Video',
			'bc_images',
			[
				'type'        => 'String',
				'name'        => 'bc_images',
				'description' => __( 'Brightcove image placeholder', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_images = wp_json_encode( get_post_meta( $post->ID, 'bc_images', true ) );

					return $bc_images;
				},
			]
		);

		register_graphql_field(
			'Video',
			'studio_stream_video_id',
			[
				'type'        => 'String',
				'name'        => 'studio_stream_video_id',
				'description' => __( 'Brightcove control room Video ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					if ( LiveBattle::POST_TYPE_NAME === get_post_type( $post->ID ) ) {
						$bc_control_room    = get_option( 'zweb_global_options' );
						$bc_stream_video_id = $bc_control_room['control_room_options']['control_room_video_id'] ? $bc_control_room['control_room_options']['control_room_video_id'] : '';

						return $bc_stream_video_id;
					} else {
						return null;
					}
				},
			]
		);

		register_graphql_field(
			'Video',
			'video_title',
			[
				'type'        => 'String',
				'name'        => 'video_title',
				'description' => __( 'Video title', 'zweb' ),
				'resolve'     => function ( $post ) {
					$video_title = get_the_title( $post->ID );

					if ( ! empty( self::$target_language ) ) {
						if ( ! empty( $video_title ) ) {
							$translation = GoogleTranslate::register();
							$target_lang = self::$target_language;
							$field_name  = "title_{$target_lang}";
							$video_title = $translation->get_post_translation( $post->ID, $field_name, $video_title, $target_lang );
						}
					}

					return html_entity_decode( $video_title, ENT_QUOTES );
				},
			]
		);

		register_graphql_field(
			'Video',
			'type',
			[
				'type'        => 'String',
				'name'        => 'type',
				'description' => __( 'Video type', 'zweb' ),
				'resolve'     => function ( $post ) {
					$video_type = '';
					switch ( get_post_type( $post->ID ) ) {
						case Video::POST_TYPE_NAME:
							$video_type = 'vod';
							break;
						case LiveVideo::POST_TYPE_NAME:
							$video_type = 'live';
							break;
						case LiveBattle::POST_TYPE_NAME:
							$video_type = 'battle';
							break;
					}

					return $video_type;
				},
			]
		);

		register_graphql_field(
			'Video',
			'timezone',
			[
				'type'        => 'String',
				'name'        => 'timezone',
				'description' => __( 'Timezone of the Scheduled video', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'timezone', true );
				},
			]
		);

		register_graphql_field(
			'Video',
			'broadcastId',
			[
				'type'        => 'String',
				'name'        => 'broadcastId',
				'description' => __( 'Vonage Broadcast ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'broadcast_id', true );
				},
			]
		);

		register_graphql_field(
			'Video',
			'liveSessionId',
			[
				'type'        => 'String',
				'name'        => 'liveSessionId',
				'description' => __( 'Brightcove live session id', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'live_session_id', true );
				},
			]
		);

		register_graphql_field(
			'Video',
			'liveStart',
			[
				'type'        => 'String',
				'name'        => 'liveStart',
				'description' => __( 'Brightcove live video start time', 'zweb' ),
				'resolve'     => function ( $post ) {
					$start_time = (int) get_post_meta( $post->ID, 'scheduledTime', true );
					// Live battle time is set with a custom block and uses Javascript timestamp which has more resolution.
					if ( LiveBattle::POST_TYPE_NAME === $post->post_type ) {
						$start_time = $start_time / 1000;
					}

					return (string) $start_time;
				},
			]
		);

		register_graphql_field(
			'Video',
			'studioId',
			[
				'type'        => 'String',
				'name'        => 'studioId',
				'description' => __( 'Studio ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'influencer_studio_id', true );
				},
			]
		);
	}

	/**
	 * Build new VideosQuery type.
	 */
	public static function register_videos_query_type() {
		// Configure new query
		$config = [
			'fromType'       => 'RootQuery',
			'toType'         => 'Video',
			'fromFieldName'  => 'VideosQuery',
			'connectionArgs' => self::register_connection_args(),
			'resolve'        => function ( $id, $args, $context, $info ) {
				// Get connectionArgs applied.
				$connection_args = $args && isset( $args['where'] ) ? $args['where'] : [];

				$need_to_exclude_hardcoded_posts = true;

				if ( ! empty( $connection_args['language'] ) ) {
					self::$target_language = $connection_args['language'];
				}

				if ( isset( $connection_args['type'] ) && $connection_args['type'] ) {
					$post_type = [];
					// TODO: split this into type and status.
					foreach ( $connection_args['type'] as $video_type ) {
						switch ( $video_type ) {
							case 'normal':
								$post_type[] = Video::POST_TYPE_NAME;
								break;
							case 'live':
								$post_type[] = LiveVideo::POST_TYPE_NAME;
								break;
							case 'scheduled':
								$post_type[] = LiveVideo::POST_TYPE_NAME;
								break;
							case 'streaming':
								$post_type[] = LiveVideo::POST_TYPE_NAME;
								break;
							case 'battle':
								$post_type[] = LiveBattle::POST_TYPE_NAME;
								break;
						}
					}
				} else {
					$post_type = [
						Video::POST_TYPE_NAME,
						LiveVideo::POST_TYPE_NAME,
						LiveBattle::POST_TYPE_NAME,
					];
				}

				$resolver = new \WPGraphQL\Data\Connection\PostObjectConnectionResolver( $id, $args, $context, $info, $post_type );

				// Exclude all videos which do not have an influencer associated
				// This gets overridden if an influencer id is passed as an argument
				$resolver->set_query_arg(
					'tax_query',
					[
						[
							'taxonomy' => 'zweb-influencer-category',
							'field'    => 'term_id',
							'operator' => 'EXISTS',
						],
					]
				);

				if ( ! empty( $connection_args['type'] ) ) {

					if ( in_array( 'scheduled', $connection_args['type'], true ) || in_array( 'streaming', $connection_args['type'], true ) ) {
						$resolver->set_query_arg(
							'meta_query',
							[
								'relation' => 'OR',
								[
									'key'     => 'bc_video_status',
									'compare' => 'NOT EXISTS',
								],
								[
									'key'     => 'bc_video_status',
									'value'   => [ 'scheduled', 'waiting', 'processing' ],
									'compare' => 'IN',
								],
							]
						);
					} elseif ( in_array( 'live', $connection_args['type'], true ) ) {
						$resolver->set_query_arg(
							'meta_query',
							[
								'relation' => 'AND',
								[
									'key'     => 'bc_video_status',
									'value'   => [ 'processing' ],
									'compare' => 'IN',
								],
							]
						);
					} elseif ( in_array( 'normal', $connection_args['type'], true ) ) {
						$resolver->set_query_arg(
							'meta_query',
							[
								[
									'key'     => 'bc_video_status',
									'compare' => 'NOT EXISTS',
								],
							]
						);
					}
				} else {
					$resolver->set_query_arg(
						'meta_query',
						[
							'relation' => 'OR',
							[
								'key'     => 'bc_video_status',
								'compare' => 'NOT EXISTS',
							],
							[
								'key'     => 'bc_video_status',
								'value'   => [ 'finishing', 'finished', 'disconnected' ],
								'compare' => 'NOT IN',
							],
						]
					);
				}

				// Deal with video by bc video id
				if ( ! empty( $connection_args['video_id'] ) ) {
					$resolver->set_query_arg(
						'meta_query',
						[
							[
								'key'     => 'bc_video_id',
								'value'   => $connection_args['video_id'],
								'compare' => '=',
							],
						]
					);
				}

				// Deal with videos by bc video id
				if ( isset( $connection_args['video_ids'] ) && is_array( $connection_args['video_ids'] ) ) {
					if ( ! empty( $connection_args['video_ids'] ) ) {
						$resolver->set_query_arg(
							'meta_query',
							[
								[
									'key'     => 'bc_video_id',
									'value'   => $connection_args['video_ids'],
									'compare' => 'IN',
								],
							]
						);
					} elseif ( [] === $connection_args['video_ids'] ) {
						$resolver->set_query_arg(
							'meta_query',
							[
								[
									'key'     => 'bc_video_id',
									'value'   => 0,
									'compare' => 'IN',
								],
							]
						);
					}
				}

				// If 'only_my_videos' filter set then retrieve the influencer id from the bearer token sent
				if ( ! empty( $connection_args['only_my_videos'] ) ) {
					$token = UserManagement::get_http_header_token();

					if ( ! empty( $token ) ) {
						$firebase = Provider::register();

						if ( $firebase ) {
							$user = $firebase->is_valid_token( $token );

							if ( $user ) {
								$connection_args['influencer_id'] = $user->uid;
							}
						}
					}
				}

				// Filter by category_id and influencer_id.
				if ( ! empty( $connection_args['category_id'] ) && ! empty( $connection_args['influencer_id'] ) ) {
					$influencer_id = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $connection_args['influencer_id'] );
					$resolver->set_query_arg(
						'tax_query',
						[
							'relation' => 'AND',
							[
								'taxonomy' => 'zweb-video-category',
								'field'    => 'term_id',
								'terms'    => $connection_args['category_id'],
							],
							[
								'taxonomy' => 'zweb-influencer-category',
								'field'    => 'term_id',
								'terms'    => $influencer_id,
							],
						]
					);
				} else {
					if ( ! empty( $connection_args['category_id'] ) ) {
						$resolver->set_query_arg(
							'tax_query',
							[
								'relation' => 'AND',
								[
									'taxonomy' => 'zweb-video-category',
									'field'    => 'term_id',
									'terms'    => $connection_args['category_id'],
								],
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'operator' => 'EXISTS',
								],
							]
						);
					}

					if ( ! empty( $connection_args['influencer_id'] ) ) {
						$influencer_id = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $connection_args['influencer_id'] );
						$resolver->set_query_arg(
							'tax_query',
							[
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => $influencer_id,
								],
							]
						);
					}
				}

				// Filter by list of influencer_ids being followed.
				if ( isset( $connection_args['influencer_ids'] ) && is_array( $connection_args['influencer_ids'] ) ) {
					if ( ! empty( $connection_args['influencer_ids'] ) ) {
						$influencer_ids = [];

						foreach ( $connection_args['influencer_ids'] as $firebase_id ) {
							$influencer_ids[] = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $firebase_id );
						}

						$resolver->set_query_arg(
							'tax_query',
							[
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => $influencer_ids,
								],
							]
						);
					} elseif ( [] === $connection_args['influencer_ids'] ) {
						$resolver->set_query_arg(
							'tax_query',
							[
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => 0,
								],
							]
						);
					}
				}

				// Query for studio videos
				if ( ! empty( $connection_args['studio_id'] ) ) {
					$studio_videos_args = [
						'post_type'      => $post_type,
						'post_status'    => [ 'publish', 'future' ],
						'fields'         => 'ids',
						'posts_per_page' => 500,
						'meta_query'     => [
							[
								'key'     => 'influencer_studio_id',
								'value'   => $connection_args['studio_id'],
								'compare' => '=',
							],
						],
					];

					$studio_videos_query = new \WP_Query( $studio_videos_args );
					$studio_videos       = $studio_videos_query->get_posts();
					$studio_videos_array = [];

					foreach ( $studio_videos as $studio_video ) {
						if ( 'finished' !== get_post_meta( $studio_video, 'live_video_status', true ) ) {
							if ( ! isset( self::$excluded_posts[ (int) $studio_video ] ) ) {
								$studio_videos_array[] = $studio_video;
							}
						}
					}

					if ( ! $studio_videos_array ) {
						$studio_videos_array = [ 0 ];
					}

					$resolver->set_query_arg( 'post__in', $studio_videos_array );
					$need_to_exclude_hardcoded_posts = false;
				}

				// Query for top 10 videos
				if ( ! empty( $connection_args['video_top10'] ) ) {
					$top10_list = Settings::get_curated_section( 'top_10' );
					if ( ! $top10_list ) {
						$top10_list = [ 0 ];
					}
					$resolver->set_query_arg( 'post__in', $top10_list );
					$need_to_exclude_hardcoded_posts = false;
				}

				// Query for Da non perdere
				if ( ! empty( $connection_args['video_da_non_perdere'] ) ) {
					$da_non_perdere_list = Settings::get_curated_section( 'da_non_perdere' );
					if ( ! $da_non_perdere_list ) {
						$da_non_perdere_list = [ 0 ];
					}
					$resolver->set_query_arg( 'post__in', $da_non_perdere_list );
					$need_to_exclude_hardcoded_posts = false;
				}

				// Query by page_size (posts_per_page) and page (page).
				if ( ! empty( $connection_args['page_size'] ) ) {
					$resolver->set_query_arg( 'posts_per_page', $connection_args['page_size'] );
					if ( ! empty( $connection_args['page'] ) ) {
						$resolver->set_query_arg( 'offset', $connection_args['page'] );
					}
				}

				if ( empty( $connection_args['search_term'] ) ) {
					$order_args = [
						'post_type' => 'ASC',
					];

					// Query by field (orderby).
					if ( ! empty( $connection_args['field'] ) && ! empty( $connection_args['direction'] ) ) {
						$order_args[ $connection_args['field'] ] = $connection_args['direction'];
					}

					$resolver->set_query_arg( 'orderby', $order_args );
				} else {
					$resolver->set_query_arg( 's', $connection_args['search_term'] );
				}

				if ( true === $need_to_exclude_hardcoded_posts ) {
					$resolver->set_query_arg( 'post__not_in', array_keys( self::$excluded_posts ) );
				}

				$connection = $resolver->get_connection();

				return $connection;
			},
		];
		register_graphql_connection( $config );
	}

	/**
	 * Register connection arguments on the videos query.
	 */
	public static function register_connection_args() {
		return [
			'category_id'          => [
				'type'        => 'Int',
				'name'        => 'category_id',
				'description' => __( 'Category ID for video.', 'zweb' ),
			],
			'influencer_id'        => [
				'type'        => 'String',
				'name'        => 'influencer_id',
				'description' => __( 'Influencer ID for video.', 'zweb' ),
			],
			'page_size'            => [
				'type'        => 'Int',
				'name'        => 'page_size',
				'description' => __( 'Page size for video.', 'zweb' ),
			],
			'page'                 => [
				'type'        => 'Int',
				'name'        => 'page',
				'description' => __( 'Page for video.', 'zweb' ),
			],
			'field'                => [
				'type'        => 'String',
				'name'        => 'field',
				'description' => __( 'Field for video.', 'zweb' ),
			],
			'direction'            => [
				'type'        => 'String',
				'name'        => 'direction',
				'description' => __( 'Direction for video.', 'zweb' ),
			],
			'search_term'          => [
				'type'        => 'String',
				'name'        => 'search_term',
				'description' => __( 'Search term for video.', 'zweb' ),
			],
			'video_id'             => [
				'type'        => 'String',
				'name'        => 'video_id',
				'description' => __( 'BC Video ID for video.', 'zweb' ),
			],
			'video_top10'          => [
				'type'        => 'Boolean',
				'name'        => 'video_top10',
				'description' => __( 'Top 10 curated videos.', 'zweb' ),
			],
			'video_da_non_perdere' => [
				'type'        => 'Boolean',
				'name'        => 'video_da_non_perdere',
				'description' => __( 'Da Non Perdere curated videos.', 'zweb' ),
			],
			'influencer_ids'       => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'influencer_ids',
				'description' => __( 'List of influencers being followed.', 'zweb' ),
			],
			'video_ids'            => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'video_ids',
				'description' => __( 'List of favourite videos.', 'zweb' ),
			],
			'language'             => [
				'type'        => 'String',
				'name'        => 'language',
				'description' => __( 'Translation language.', 'zweb' ),
			],
			'type'                 => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'type',
				'description' => __( 'Type of video.', 'zweb' ),
			],
			'post_status'          => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'post_status',
				'description' => __( 'Post status.', 'zweb' ),
			],
			'studio_id'            => [
				'type'        => 'Int',
				'name'        => 'studio_id',
				'description' => __( 'Studio ID.', 'zweb' ),
			],
			'only_my_videos'       => [
				'type'        => 'Boolean',
				'name'        => 'only_my_videos',
				'description' => __( 'Return only videos based on the id token from the app.', 'zweb' ),
			],
		];
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_related_video_type() {
		register_graphql_object_type(
			'relatedVideos',
			[
				'description' => __( 'Related videos', 'zweb' ),
				'fields'      => [
					'id'          => [
						'type'        => 'Int',
						'name'        => 'id',
						'description' => __( 'Post ID', 'zweb' ),
					],
					'title'       => [
						'type'        => 'String',
						'name'        => 'title',
						'description' => __( 'Post/ Video title', 'zweb' ),
					],
					'uri'         => [
						'type'        => 'String',
						'name'        => 'uri',
						'description' => __( 'Post/ Video URI', 'zweb' ),
					],
					'bcAccountId' => [
						'type'        => 'String',
						'name'        => 'bcAccountId',
						'description' => __( 'Brightcove Account ID', 'zweb' ),
					],
					'bcVideoID'   => [
						'type'        => 'String',
						'name'        => 'bcVideoID',
						'description' => __( 'Brightcove Video ID', 'zweb' ),
					],
					'bcDuration'  => [
						'type'        => 'String',
						'name'        => 'bcDuration',
						'description' => __( 'Brightcove viedo duration', 'zweb' ),
					],
					'bcImage'     => [
						'type'        => 'String',
						'name'        => 'bcImage',
						'description' => __( 'Brightcove video cover images', 'zweb' ),
					],
					'bcPlayerID'  => [
						'type'        => 'String',
						'name'        => 'bcPlayerID',
						'description' => __( 'Brightcove player ID', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_related_video_field() {
		register_graphql_field(
			'Video',
			'getRelatedVideos',
			[
				'name'        => 'getRelatedVideos',
				'description' => __( 'Get related videos', 'zweb' ),
				'type'        => [ 'list_of' => 'relatedVideos' ],
				'resolve'     => function ( $post ) {
					$term = wp_get_object_terms( $post->ID, 'zweb-video-category' );

					if ( isset( $term[0] ) ) {
						$term_id = $term[0]->term_id;
					} else {
						$term_id = 0;
					}

					$related = new \WP_Query(
						[
							'post_type'      => 'zweb-video',
							'post_status'    => 'publish',
							'posts_per_page' => 100,
							'tax_query'      => [
								[
									'taxonomy' => 'zweb-video-category',
									'field'    => 'term_id',
									'terms'    => $term_id,
								],
							],
						]
					);

					$related_posts = $related->get_posts();
					$related_array = [];

					if ( ! empty( self::$target_language ) ) {
						$translation = GoogleTranslate::register();
					}

					foreach ( $related_posts as $index => $related_post ) {
						if ( $post->ID !== $related_post->ID ) {
							$related_array[ $index ]['id']          = $related_post->ID;
							$related_array[ $index ]['uri']         = wp_make_link_relative( get_permalink( $related_post->ID ) );
							$related_array[ $index ]['bcAccountId'] = get_post_meta( $related_post->ID, 'bc_account_id', true );
							$related_array[ $index ]['bcVideoID']   = get_post_meta( $related_post->ID, 'bc_video_id', true );
							$related_array[ $index ]['bcDuration']  = get_post_meta( $related_post->ID, 'bc_duration', true );
							$related_array[ $index ]['bcImage']     = wp_json_encode( get_post_meta( $related_post->ID, 'bc_images', true ) );
							$related_array[ $index ]['bcPlayerID']  = get_post_meta( $related_post->ID, 'bc_player_id', true );

							$video_title = html_entity_decode( $related_post->post_title, ENT_QUOTES );
							if ( ! empty( self::$target_language ) ) {
								if ( ! empty( $related_post->post_title ) ) {
									$target_lang                      = self::$target_language;
									$field_name                       = "title_{$target_lang}";
									$related_array[ $index ]['title'] = $translation->get_post_translation( $related_post->ID, $field_name, $video_title, $target_lang );
								}
							} else {
								$related_array[ $index ]['title'] = $video_title;
							}
						}
					}

					return $related_array;
				},
			]
		);
	}
}
