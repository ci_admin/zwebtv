<?php

namespace Zweb\GraphQL\Resolvers;

use Zweb\Admin\Settings;
use Zweb\Multilingual\GoogleTranslate;
use Zweb\Firebase\Provider;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;

/**
 * Class StudiosResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class StudiosResolver {
	/**
	 * The Studio query args.
	 *
	 * @var array
	 */
	public static $args = null;

	/**
	 * The Target laguage for translation.
	 *
	 * @var String
	 */
	private static $target_language = null;

	/**
	 *  Register new types for Videos custom post type.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_studio_meta',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_studios_query_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_studio_influencer',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_studio_video_type',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_studio_influencer_field',
			]
		);
	}

	/**
	 * Register studio meta on the videos query.
	 */
	public static function register_studio_meta() {
		register_graphql_field(
			'Studio',
			'studio_img',
			[
				'type'        => 'String',
				'name'        => 'studio_img',
				'description' => __( 'Studio Image', 'zweb' ),
				'resolve'     => function( $post ) {
					$studio_img = get_the_post_thumbnail_url( $post->ID, 'full' ) ? get_the_post_thumbnail_url( $post->ID, 'full' ) : '';
					return $studio_img;
				}
			]
		);
	}

	/**
	 * Build new VideosQuery type.
	 */
	public static function register_studios_query_type() {
		// Configure new query
		$config = [
			'fromType'       => 'RootQuery',
			'toType'         => 'Studio',
			'fromFieldName'  => 'StudiosQuery',
			'connectionArgs' => self::register_connection_args(),
			'resolve'        => function( $id, $args, $context, $info ) {
				// Get connectionArgs applied.
				$connection_args = $args && isset( $args['where'] ) ? $args['where'] : [];
				self::$args      = $connection_args;

				if ( ! empty( $connection_args['language'] ) ) {
					self::$target_language = $connection_args['language'];
				}

				$resolver = new \WPGraphQL\Data\Connection\PostObjectConnectionResolver( $id, $args, $context, $info, 'zweb-studio' );

				// QFilter by studio id
				if ( ! empty( $connection_args['studio_id'] ) ) {
					$resolver->setQueryArg( 'post__in', [ $connection_args['studio_id'] ] );
				}

				// Query by page_size (posts_per_page) and page (page).
				if ( ! empty( $connection_args['page_size'] ) ) {
					$resolver->setQueryArg( 'posts_per_page', $connection_args['page_size'] );
					if ( ! empty( $connection_args['page'] ) ) {
						$resolver->setQueryArg( 'offset', $connection_args['page'] );
					}
				}

				// Query by field (orderby).
				if ( ! empty( $connection_args['field'] ) ) {
					$resolver->setQueryArg( 'orderby', $connection_args['field'] );
				}

				// Query by field (direction).
				if ( ! empty( $connection_args['direction'] ) ) {
					$resolver->setQueryArg( 'order', $connection_args['direction'] );
				}

				$connection = $resolver->get_connection();

				return $connection;
			},
		];
		register_graphql_connection( $config );
	}

	/**
	 * Register connection arguments on the videos query.
	 */
	public static function register_connection_args() {
		return [
			'influencer_id'  => [
				'type'        => 'String',
				'name'        => 'influencer_id',
				'description' => __( 'Influencer ID for the studio.', 'zweb' ),
			],
			'studio_id'      => [
				'type'        => 'Int',
				'name'        => 'studio_id',
				'description' => __( 'Studio ID.', 'zweb' ),
			],
			'category_id'    => [
				'type'        => 'Int',
				'name'        => 'category_id',
				'description' => __( 'Category ID associated with video.', 'zweb' ),
			],
			'page_size'      => [
				'type'        => 'Int',
				'name'        => 'page_size',
				'description' => __( 'Page size for the studio.', 'zweb' ),
			],
			'page'           => [
				'type'        => 'Int',
				'name'        => 'page',
				'description' => __( 'Page for the studio.', 'zweb' ),
			],
			'field'          => [
				'type'        => 'String',
				'name'        => 'field',
				'description' => __( 'Field for the studio.', 'zweb' ),
			],
			'direction'      => [
				'type'        => 'String',
				'name'        => 'direction',
				'description' => __( 'Direction for the studio.', 'zweb' ),
			],
			'search_term'    => [
				'type'        => 'String',
				'name'        => 'search_term',
				'description' => __( 'Search term for the studio.', 'zweb' ),
			],
			'type'           => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'type',
				'description' => __( 'Type of video.', 'zweb' ),
			],
			'post_status'    => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'post_status',
				'description' => __( 'Post status.', 'zweb' ),
			],
			'language'       => [
				'type'        => 'String',
				'name'        => 'language',
				'description' => __( 'Translation language.', 'zweb' ),
			],
			'influencer_ids' => [
				'type'        => [
					'list_of' => 'String',
				],
				'name'        => 'influencer_ids',
				'description' => __( 'List of influencers.', 'zweb' ),
			],
		];
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_studio_influencer() {
		register_graphql_object_type(
			'studioInfluencer',
			[
				'description' => __( 'Related videos', 'zweb' ),
				'fields'      => [
					'id'                      => [
						'type'        => 'String',
						'name'        => 'id',
						'description' => __( 'Firebase UID', 'zweb' ),
					],
					'influencer_id'           => [
						'type'        => 'String',
						'name'        => 'influencer_id',
						'description' => __( 'Firebase UID', 'zweb' ),
					],
					'display_name'            => [
						'type'        => 'String',
						'name'        => 'display_name',
						'description' => __( 'Influencer display name', 'zweb' ),
					],
					'user_nicename'           => [
						'type'        => 'String',
						'name'        => 'user_nicename',
						'description' => __( 'Influencer nice name', 'zweb' ),
					],
					'user_email'              => [
						'type'        => 'String',
						'name'        => 'user_email',
						'description' => __( 'Influencer email', 'zweb' ),
					],
					'user_image_url'          => [
						'type'        => 'String',
						'name'        => 'user_image_url',
						'description' => __( 'Influencer Image URL', 'zweb' ),
					],
					'user_bio'                => [
						'type'        => 'String',
						'name'        => 'user_bio',
						'description' => __( 'Influencer bio', 'zweb' ),
					],
					'user_category_id'        => [
						'type'        => 'String',
						'name'        => 'user_category_id',
						'description' => __( 'Influencer category id', 'zweb' ),
					],
					'user_category_name'      => [
						'type'        => 'String',
						'name'        => 'user_category_name',
						'description' => __( 'Influencer user category name', 'zweb' ),
					],
					'user_landing_image'      => [
						'type'        => 'String',
						'name'        => 'user_landing_image',
						'description' => __( 'Influencer user landing page image', 'zweb' ),
					],
					'user_landing_live_image' => [
						'type'        => 'String',
						'name'        => 'user_landing_live_image',
						'description' => __( 'Influencer user landing page live image', 'zweb' ),
					],
					'user_profile_image'      => [
						'type'        => 'String',
						'name'        => 'user_profile_image',
						'description' => __( 'Influencer user profile image', 'zweb' ),
					],
					'user_card_image'         => [
						'type'        => 'String',
						'name'        => 'user_card_image',
						'description' => __( 'Influencer card image', 'zweb' ),
					],
					'user_upcoming_image'     => [
						'type'        => 'String',
						'name'        => 'user_upcoming_image',
						'description' => __( 'Influencer upcoming image', 'zweb' ),
					],
					'user_show_upcoming'      => [
						'type'        => 'String',
						'name'        => 'user_show_upcoming',
						'description' => __( 'Influencer show upcoming', 'zweb' ),
					],
					'user_upcoming_date'      => [
						'type'        => 'String',
						'name'        => 'user_upcoming_date',
						'description' => __( 'Influencer upcoming date', 'zweb' ),
					],
					'user_show_influencer'    => [
						'type'        => 'String',
						'name'        => 'user_show_influencer',
						'description' => __( 'Influencer prevent showing on influencer page', 'zweb' ),
					],
					'influencer_videos'       => [
						'type'        => [
							'list_of' => 'studioVideoInfluencer',
						],
						'name'        => 'influencer_videos',
						'description' => __( 'List of influencers being followed.', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_studio_video_type() {
		register_graphql_object_type(
			'studioVideoInfluencer',
			[
				'description' => __( 'Influencer videos', 'zweb' ),
				'fields'      => [
					'video_id'        => [
						'type'        => 'String',
						'name'        => 'video_id',
						'description' => __( 'Studio video id', 'zweb' ),
					],
					'video_title'     => [
						'type'        => 'String',
						'name'        => 'video_title',
						'description' => __( 'Studio video title', 'zweb' ),
					],
					'bc_player_id'    => [
						'type'        => 'String',
						'name'        => 'bc_player_id',
						'description' => __( 'Brightcove player ID', 'zweb' ),
					],
					'bc_video_id'     => [
						'type'        => 'String',
						'name'        => 'bc_video_id',
						'description' => __( 'Brightcove video ID', 'zweb' ),
					],
					'bc_account_id'   => [
						'type'        => 'String',
						'name'        => 'bc_account_id',
						'description' => __( 'Brightcove account ID', 'zweb' ),
					],
					'bc_duration'     => [
						'type'        => 'String',
						'name'        => 'bc_duration',
						'description' => __( 'Brightcove video duration', 'zweb' ),
					],
					'bc_images'       => [
						'type'        => 'String',
						'name'        => 'bc_images',
						'description' => __( 'Brightcove image placeholder', 'zweb' ),
					],
					'type'            => [
						'type'        => 'String',
						'name'        => 'type',
						'description' => __( 'Video type', 'zweb' ),
					],
					'timezone'        => [
						'type'        => 'String',
						'name'        => 'timezone',
						'description' => __( 'Timezone of the Scheduled video', 'zweb' ),
					],
					'liveVideoStatus' => [
						'type'        => 'String',
						'name'        => 'liveVideoStatus',
						'description' => __( 'Status of the live video', 'zweb' ),
					],
					'broadcastId'     => [
						'type'        => 'String',
						'name'        => 'broadcastId',
						'description' => __( 'Vonage Broadcast ID', 'zweb' ),
					],
					'liveSessionId'   => [
						'type'        => 'String',
						'name'        => 'liveSessionId',
						'description' => __( 'Brightcove live session id', 'zweb' ),
					],
					'liveStart'       => [
						'type'        => 'String',
						'name'        => 'liveStart',
						'description' => __( 'Brightcove live session id', 'zweb' ),
					],
				],
			]
		);
	}

	/**
	 * Register a new graphql type.
	 */
	public static function register_studio_influencer_field() {
		register_graphql_field(
			'Studio',
			'getStudioInfluencer',
			[
				'name'        => 'getStudioInfluencer',
				'description' => __( 'Get studio influencers', 'zweb' ),
				'type'        => [ 'list_of' => 'studioInfluencer' ],
				'resolve'     => function( $post ) {

					if ( ! empty( self::$args['influencer_id'] ) ) {
						$influencer_id = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( self::$args['influencer_id'] );
						$user_args     = [
							'role'       => 'influencer',
							'meta_query' =>
							[
								'relation' => 'AND',
								[
									'key'     => 'influencer_term',
									'value'   => $influencer_id,
									'compare' => '=',
								],
								[
									'key'     => 'zweb-influencer_parent_studio',
									'value'   => $post->ID,
									'compare' => '=',
								]
							]
						];
					} elseif ( ! empty( self::$args['category_id'] ) ) {
						$user_args = [
							'role'       => 'influencer',
							'meta_query' =>
							[
								'relation' => 'AND',
								[
									'key'     => 'zweb-influencer_video_category',
									'value'   => self::$args['category_id'],
									'compare' => '=',
								],
								[
									'key'     => 'zweb-influencer_parent_studio',
									'value'   => $post->ID,
									'compare' => '=',
								]
							]
						];
					} else {
						$user_args = [
							'role'       => 'influencer',
							'meta_query' => [
								[
									'key'     => 'zweb-influencer_parent_studio',
									'value'   => $post->ID,
									'compare' => '=',
								]
							]
						];
					}

					// Filter by list of influencer_ids being followed.
					if ( isset( self::$args['influencer_ids'] ) && is_array( self::$args['influencer_ids'] ) ) {
						if ( ! empty( self::$args['influencer_ids'] ) ) {
							$influencer_ids = array();

							foreach ( self::$args['influencer_ids'] as $firebase_id ) {
								$influencer_ids[] = \Zweb\Taxonomy\Influencer::get_influencer_from_uid( $firebase_id );
							}

							$user_args = [
								'role'       => 'influencer',
								'meta_query' =>
								[
									'relation' => 'AND',
									[
										'key'     => 'influencer_term',
										'value'   => $influencer_ids,
										'compare' => 'IN',
									],
									[
										'key'     => 'zweb-influencer_parent_studio',
										'value'   => $post->ID,
										'compare' => '=',
									]
								]
							];
						} elseif ( [] === self::$args['influencer_ids'] ) {
							$user_args = [
								'role'       => 'influencer',
								'meta_query' =>
								[
									'relation' => 'AND',
									[
										'key'     => 'influencer_term',
										'value'   => 0,
										'compare' => '=',
									],
									[
										'key'     => 'zweb-influencer_parent_studio',
										'value'   => $post->ID,
										'compare' => '=',
									]
								]
							];
						}
					}

					// Search using influencer name
					if ( ! empty( self::$args['search_term'] ) ) {
						$search    = self::$args['search_term'];
						$user_args = [
							'role'           => 'influencer',
							'search'         => "*{$search}*",
							'search_columns' => [
								'display_name',
							],
							'meta_query'     => [
								[
									'key'     => 'zweb-influencer_parent_studio',
									'value'   => $post->ID,
									'compare' => '=',
								]
							]
						];
					}

					$users      = new \WP_User_Query( $user_args );
					$user_meta  = $users->get_results();
					$user_array = array();

					if ( ! empty( self::$target_language ) ) {
						$translation = GoogleTranslate::register();
					}

					foreach ( $user_meta as $user_index => $user ) {
						$user_array[ $user_index ]['display_name']            = $user->display_name;
						$user_array[ $user_index ]['user_nicename']           = $user->user_nicename;
						$user_array[ $user_index ]['user_email']              = $user->user_email;
						$user_array[ $user_index ]['user_image_url']          = get_avatar_url( $user->ID );
						$user_array[ $user_index ]['user_landing_image']      = wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_landing_page_image', true ) );
						$user_array[ $user_index ]['user_landing_live_image'] = wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_landing_page_image_live', true ) );
						$user_array[ $user_index ]['user_profile_image']      = get_user_meta( $user->ID, 'zweb-influencer_avatar', true );
						$user_array[ $user_index ]['user_card_image']         = wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_influencer_card_image', true ) );
						$user_array[ $user_index ]['user_upcoming_image']     = wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_influencer_upcoming_image', true ) );
						$user_array[ $user_index ]['user_show_upcoming']      = get_user_meta( $user->ID, 'zweb-influencer_show_as_upcoming', true );
						$user_array[ $user_index ]['user_upcoming_date']      = get_user_meta( $user->ID, 'zweb-influencer_upcoming_date', true );
						$user_array[ $user_index ]['user_show_influencer']    = get_user_meta( $user->ID, 'zweb-influencer_prevent_showing_in_influencer_page', true );

						$category_id = get_user_meta( $user->ID, 'zweb-influencer_video_category', true );
						$category    = get_term( get_user_meta( $user->ID, 'zweb-influencer_video_category', true ) );
						$category_name = $category && isset( $category->name ) ? $category->name : '';

						$user_array[ $user_index ]['user_category_id']   = $category_id;
						$user_array[ $user_index ]['user_category_name'] = $category_name;

						if ( ! empty( self::$target_language ) ) {
							if ( ! empty( get_user_meta( $user->ID, 'description', true ) ) ) {
								$influencer_bio                        = html_entity_decode( get_user_meta( $user->ID, 'description', true ) );
								$target_lang                           = self::$target_language;
								$field_name                            = "description_{$target_lang}";
								$user_array[ $user_index ]['user_bio'] = $translation->get_user_translation( $user->ID, $field_name, $influencer_bio, $target_lang );
							}
						} else {
							$user_array[ $user_index ]['user_bio'] = html_entity_decode( get_user_meta( $user->ID, 'description', true ) );
						}

						// Get the firebase user
						try {
							$firebase_uid = get_user_meta( $user->ID, 'zweb-influencer_firebase-uid', true );

							if ( empty( $firebase_uid ) ) {
								$instance      = Provider::register();
								$firebase      = $instance->get_firebase_instance();
								$auth          = $firebase->getAuth();
								$firebase_user = $auth->getUserByEmail( $user->user_email );
								$firebase_uid  = $firebase_user->uid;

								update_user_meta( $user->ID, 'zweb-influencer_firebase-uid', $firebase_uid );
							}
						} catch ( \Exception $exception ) {
							$firebase_uid = null;
						}

						// Both fields to store firebase UID for backwards compatability
						$user_array[ $user_index ]['id']            = $firebase_uid;
						$user_array[ $user_index ]['influencer_id'] = $firebase_uid;

						// Post type filter
						if ( isset( self::$args['type'] ) ) {
							$post_type = array();
							foreach ( self::$args['type'] as $video_type ) {
								switch ( $video_type ) {
									case 'normal':
										$post_type[] = Video::POST_TYPE_NAME;
										break;
									case 'live':
										$post_type[] = LiveVideo::POST_TYPE_NAME;
										break;
									case 'scheduled':
										$post_type[] = LiveVideo::POST_TYPE_NAME;
										break;
									case 'streaming':
										$post_type[] = LiveVideo::POST_TYPE_NAME;
										break;
								}
							}
						} else {
							$post_type = [
								Video::POST_TYPE_NAME,
								LiveVideo::POST_TYPE_NAME,
								LiveBattle::POST_TYPE_NAME,
							];
						}

						// Post status filter
						if ( empty( self::$args['post_status'] ) ) {
							$post_status = [
								'publish',
								'future',
							];
						} else {
							$post_status = self::$args['post_status'];
						}

						// Get videos for the influencer
						$video_array = new \WP_Query(
							[
								'post_type'      => $post_type,
								'post_status'    => $post_status,
								'posts_per_page' => 100,
								'tax_query'      => [
									[
										'taxonomy' => 'zweb-influencer-category',
										'field'    => 'term_id',
										'terms'    => get_user_meta( $user->ID, 'influencer_term', true ),
									],
								],
								'orderby'        => 'type',
								'order'          => 'ASC',
							]
						);

						$studio_videos = $video_array->get_posts();

						foreach ( $studio_videos as $studio_index => $studio_video ) {
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['video_id']        = $studio_video->ID;
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['bc_player_id']    = get_post_meta( $studio_video->ID, 'bc_player_id', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['bc_video_id']     = get_post_meta( $studio_video->ID, 'bc_video_id', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['bc_account_id']   = get_post_meta( $studio_video->ID, 'bc_account_id', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['bc_duration']     = get_post_meta( $studio_video->ID, 'bc_duration', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['bc_images']       = wp_json_encode( get_post_meta( $studio_video->ID, 'bc_images', true ) );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['type']            = get_post_type( $studio_video->ID );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['timezone']        = get_post_meta( $studio_video->ID, 'timezone', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['liveVideoStatus'] = get_post_meta( $studio_video->ID, 'live_video_status', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['broadcastId']     = get_post_meta( $studio_video->ID, 'broadcast_id', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['liveSessionId']   = get_post_meta( $studio_video->ID, 'live_session_id', true );
							$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['liveStart']       = get_post_meta( $studio_video->ID, 'scheduledTime', true );

							$video_title = html_entity_decode( get_the_title( $studio_video->ID ) );
							if ( ! empty( self::$target_language ) ) {
								if ( ! empty( get_the_title( $studio_video->ID ) ) ) {
									$target_lang = self::$target_language;
									$field_name  = "title_{$target_lang}";

									$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['video_title'] = $translation->get_post_translation( $studio_video->ID, $field_name, $video_title, $target_lang );
								}
							} else {
								$user_array[ $user_index ]['influencer_videos'][ $studio_index ]['video_title'] = $video_title;
							}
						}
					}

					return $user_array;
				}
			]
		);
	}
}
