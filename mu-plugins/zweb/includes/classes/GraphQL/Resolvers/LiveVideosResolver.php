<?php

namespace Zweb\GraphQL\Resolvers;

use Zweb\PostType\LiveVideo;
use Zweb\Taxonomy\Influencer;

/**
 * Class VideosResolver
 *
 * @package Zweb\GraphQL\Resolvers
 */
class LiveVideosResolver {

	/**
	 *  Register new types for Live Videos custom post type.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_video_meta',
			]
		);

		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_videos_query_type',
			]
		);
	}

	/**
	 * Register video meta on the videos query.
	 */
	public static function register_video_meta() {
		register_graphql_field(
			'LiveVideo',
			'bcPlayerId',
			[
				'type'        => 'String',
				'name'        => 'bcPlayerId',
				'description' => __( 'Brightcove player ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_player_id = get_post_meta( $post->ID, 'bc_player_id', true );

					return $bc_player_id;
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'bcVideoId',
			[
				'type'        => 'String',
				'name'        => 'bcVideoId',
				'description' => __( 'Brightcove video ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_video_id = get_post_meta( $post->ID, 'bc_video_id', true );

					return $bc_video_id;
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'bcAccountId',
			[
				'type'        => 'String',
				'name'        => 'bcAccountId',
				'description' => __( 'Brightcove account ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					$bc_account_id = get_post_meta( $post->ID, 'bc_account_id', true );

					return $bc_account_id;
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'timezone',
			[
				'type'        => 'String',
				'name'        => 'timezone',
				'description' => __( 'Timezone of the Scheduled video', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'timezone', true );
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'liveVideoStatus',
			[
				'type'        => 'String',
				'name'        => 'liveVideoStatus',
				'description' => __( 'Status of the live video', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'live_video_status', true );
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'broadcastId',
			[
				'type'        => 'String',
				'name'        => 'broadcastId',
				'description' => __( 'Vonage Broadcast ID', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'broadcast_id', true );
				},
			]
		);

		register_graphql_field(
			'LiveVideo',
			'liveSessionId',
			[
				'type'        => 'String',
				'name'        => 'liveSessionId',
				'description' => __( 'Brightcove live session id', 'zweb' ),
				'resolve'     => function ( $post ) {
					return get_post_meta( $post->ID, 'live_session_id', true );
				},
			]
		);
	}

	/**
	 * Build new VideosQuery type.
	 */
	public static function register_videos_query_type() {
		// Configure new query
		$config = [
			'fromType'       => 'RootQuery',
			'toType'         => 'LiveVideo',
			'fromFieldName'  => 'LiveVideosQuery',
			'connectionArgs' => self::register_connection_args(),
			'resolve'        => function ( $id, $args, $context, $info ) {
				$resolver = new \WPGraphQL\Data\Connection\PostObjectConnectionResolver( $id, $args, $context, $info, LiveVideo::POST_TYPE_NAME );

				// Get connectionArgs applied.
				$connection_args = $args['where'] ? $args['where'] : [];

				if ( ! empty( $connection_args['liveVideoStatus'] ) ) {
					$resolver->set_query_arg(
						'meta_query',
						[
							[
								'key'     => 'live_video_status',
								'value'   => $connection_args['liveVideoStatus'],
								'compare' => '=',
							],
						]
					);
				}

				if ( ! empty( $connection_args['influencer_id'] ) ) {
					$resolver->set_query_arg(
						'tax_query',
						[
							[
								'taxonomy' => Influencer::TAXONOMY_NAME,
								'field'    => 'term_id',
								'terms'    => $connection_args['influencer_id'],
							],
						]
					);
				}

				// Filter by list of influencer_ids being followed.
				if ( isset( $connection_args['influencer_ids'] ) && is_array( $connection_args['influencer_ids'] ) ) {
					if ( ! empty( $connection_args['influencer_ids'] ) ) {
						$resolver->set_query_arg(
							'tax_query',
							[
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => $connection_args['influencer_ids'],
								],
							]
						);
					} elseif ( [] === $connection_args['influencer_ids'] ) {
						$resolver->set_query_arg(
							'tax_query',
							[
								[
									'taxonomy' => 'zweb-influencer-category',
									'field'    => 'term_id',
									'terms'    => 0,
								],
							]
						);
					}
				}

				// Query by page_size (posts_per_page) and page (page).
				if ( ! empty( $connection_args['page_size'] ) ) {
					$resolver->set_query_arg( 'posts_per_page', $connection_args['page_size'] );
					if ( ! empty( $connection_args['page'] ) ) {
						$resolver->set_query_arg( 'offset', $connection_args['page'] );
					}
				}

				// Query by field (orderby).
				if ( ! empty( $connection_args['field'] ) ) {
					$resolver->set_query_arg( 'orderby', $connection_args['field'] );
				}

				// Query by field (direction).
				if ( ! empty( $connection_args['direction'] ) ) {
					$resolver->set_query_arg( 'order', $connection_args['direction'] );
				}

				// Query by search term.
				if ( ! empty( $connection_args['search_term'] ) ) {
					$resolver->set_query_arg( 's', $connection_args['search_term'] );
				}

				return $resolver->get_connection();
			},
		];

		register_graphql_connection( $config );
	}

	/**
	 * Register connection arguments on the videos query.
	 */
	public static function register_connection_args() {
		return [
			'influencer_id'   => [
				'type'        => 'Int',
				'name'        => 'influencer_id',
				'description' => __( 'Influencer ID for live video.', 'zweb' ),
			],
			'page_size'       => [
				'type'        => 'Int',
				'name'        => 'page_size',
				'description' => __( 'Page size for live video.', 'zweb' ),
			],
			'page'            => [
				'type'        => 'Int',
				'name'        => 'page',
				'description' => __( 'Page for live video.', 'zweb' ),
			],
			'field'           => [
				'type'        => 'String',
				'name'        => 'field',
				'description' => __( 'Field for live video.', 'zweb' ),
			],
			'direction'       => [
				'type'        => 'String',
				'name'        => 'direction',
				'description' => __( 'Direction for live video.', 'zweb' ),
			],
			'search_term'     => [
				'type'        => 'String',
				'name'        => 'search_term',
				'description' => __( 'Search term for live video.', 'zweb' ),
			],
			'liveVideoStatus' => [
				'type'        => 'String',
				'name'        => 'liveVideoStatus',
				'description' => __( 'Live video status, either "streaming" or "scheduled".', 'zweb' ),
			],
			'influencer_ids'  => [
				'name'        => 'influencer_ids',
				'type'        => [
					'list_of' => 'Int',
				],
				'description' => __( 'List of influencers being followed.', 'zweb' ),
			],

		];
	}

}
