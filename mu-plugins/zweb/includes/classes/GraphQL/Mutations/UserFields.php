<?php

namespace Zweb\GraphQL\Mutations;

/**
 * Class UserFields
 *
 * @package Zweb\GraphQL\Mutations
 */
class UserFields {
	/**
	 * Register input fields on the user mutation.
	 *
	 * @return array All input fields on the mutation.
	 */
	public static function register_input_fields() {
		return [
			'user_bio' => [
				'type'        => 'String',
				'description' => __( 'User bio', 'zweb' ),
			],
		];
	}

	/**
	 * Register output fields on the user mutation.
	 *
	 * @return array All output fields on the mutation.
	 */
	public static function register_output_fields() {
		return [
			'user_message' => [
				'type'        => 'String',
				'description' => __( 'User message', 'zweb' ),
			],
		];
	}

	/**
	 * Mutate fields for output.
	 *
	 * @param array $message Message for mutation
	 *
	 * @return array Output array for mutation.
	 */
	public static function mutate_user_output( $message ) {
		$user_message = null;
		if ( ! empty( $message ) ) {
			$user_message = $message;
		}

		return [
			'user_message' => $user_message,
		];
	}
}
