<?php

namespace Zweb\GraphQL\Mutations;

use OpenTok\MediaMode;
use Zweb\Admin\LiveStreaming;
use Zweb\OpenTok\Wrapper;
use Zweb\Role\Influencer;

/**
 * Class GetLiveStreamSessionAndToken
 *
 * @package Zweb\GraphQL\Mutations
 */
class GetLiveStreamSessionAndToken extends AbstractMutation {

	/**
	 * Register mutation
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'getLiveStreamSessionAndToken',
			[
				'inputFields'         => [
					'video_name'    => [
						'type'        => 'String',
						'description' => __( 'Video Name', 'zweb' ),
					],
					'firebaseToken' => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
				],
				'outputFields'        => [
					'apiKey'    => [
						'type'        => 'String',
						'description' => __( 'Vonage Api Key', 'zweb' ),
					],
					'token'     => [
						'type'        => 'String',
						'description' => __( 'Vonage Token', 'zweb' ),
					],
					'sessionId' => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
					'status'    => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message'   => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						Influencer::get_user_from_firebase_if_has_role(
							$input['firebaseToken']
						);
						$opentok    = Wrapper::get_instance();
						$session    = $opentok->createSession( [ 'mediaMode' => MediaMode::ROUTED ] );
						$session_id = $session->getSessionId();
						$token      = $session->generateToken();
						$settings   = LiveStreaming::get_vonage_key_secret();

						return [
							'apiKey'    => $settings['api_key'],
							'token'     => $token,
							'sessionId' => $session_id,
							'status'    => 200,
						];
					} catch ( \Exception $exception ) {
						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}
				},
			]
		);
	}
}
