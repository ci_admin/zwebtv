<?php

namespace Zweb\GraphQL\Mutations;

use Mockery\Exception;
use OpenTok\Broadcast;
use OpenTok\Layout;
use Zweb\Admin\LiveStreaming;
use Zweb\Brightcove\BCIngestionApi;
use Zweb\Brightcove\PostMeta;
use Zweb\OpenTok\Wrapper;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;
use Zweb\Role\Influencer;
use Zweb\Notifications\VideoEvents;
use Zweb\Routing\Route\BcNotifications;
use Zweb\Traits\FirestoreVideo;

/**
 * Class StartBroadcastNoOpentok
 *
 * @package Zweb\GraphQL\Mutations
 */
class StartBroadcastNoOpentok extends AbstractMutation {

	use FirestoreVideo;

	/**
	 * Register mutation
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'startBroadcastNoOpentok',
			[
				'inputFields'         => [
					'videoName'     => [
						'type'        => 'String',
						'description' => __( 'Video Name', 'zweb' ),
					],
					'sessionId'     => [
						'type'        => 'String',
						'description' => __( 'Vonage Session Id', 'zweb' ),
					],
					'wordpressId'   => [
						'type'        => 'Int',
						'description' => __( 'Wordpress Id', 'zweb' ),
					],
					'firebaseToken' => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
				],
				'outputFields'        => [
					'rtmpUrl'        => [
						'type'        => 'String',
						'description' => __( 'Brightcove Live ID', 'zweb' ),
					],
					'liveSessionId'  => [
						'type'        => 'String',
						'description' => __( 'Brightcove Live ID', 'zweb' ),
					],
					'liveVideoId'    => [
						'type'        => 'String',
						'description' => __( 'Brightcove Video ID', 'zweb' ),
					],
					'liveVideoStart' => [
						'type'        => 'String',
						'description' => __( 'Brightcove Video Start', 'zweb' ),
					],
					'status'         => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message'        => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {

						$wp_user = Influencer::get_user_from_firebase_if_has_role(
							$input['firebaseToken']
						);
						$wp_id   = false;
						if ( isset( $input['wordpressId'] ) && $input['wordpressId'] ) {
							$post = get_post( $input['wordpressId'] );
							if ( $post ) {
								$wp_id = $input['wordpressId'];
								if ( absint( $post->post_author ) !== absint( $wp_user->ID ) ) {
									throw new \Exception( __( 'You can only start a live video you have scheduled', 'zweb' ) );
								}
							} else {
								throw new \Exception( __( 'The scheduled live video was not found in the system', 'zweb' ) );
							}
						}
						$title               = sprintf( __( 'Live Video by %1$s on %2$s', 'zweb' ), $wp_user->display_name, gmdate( 'd-m-Y h:i:s' ) );
						$post_title          = $input['videoName'] ? $input['videoName'] : $title;
						$cms_api             = new BCIngestionApi();

						if ( $wp_id ) {
							$bc_video_id = get_post_meta( $wp_id, 'bc_video_id', true );
						} else {
							$video = $cms_api->video_add( $title );
							if ( ! $video ) {
								throw new \Exception( 'Something went wrong when creating the video on Brightcove' );
							}
							$bc_video_id = $video['id'];
						}

						if ( ! $bc_video_id ) {
							throw new \Exception( 'Missing Brightcove video ID' );
						}
						$token = BcNotifications::generate_random_string();
						$notifications_url = BcNotifications::get_live_video_notifications_url( "$bc_video_id/$token" );
						$live_video_response = self::get_live_video( $post_title, $notifications_url );

						$bc_video_start = isset( $video['schedule']['starts_at'] ) && $video['schedule']['starts_at'] ? $video['schedule']['starts_at'] : get_post_meta(
							$wp_id,
							'scheduledTime',
							true
						);
						$hls            = $cms_api->hls_manifest( $bc_video_id, $live_video_response['playback_url'] );
						if ( ! $hls ) {
							throw new \Exception( 'Something went wrong when associating the video to the live stream on Brightcove' );
						}

						// If there is no Wordpress ID the video is not scheduled, so we set the bc_video_status to waitingNotScheduled
						if ( ! $wp_id ) {
							$wp_id = self::create_video_cpt_with_block(
								$wp_user,
								$post_title,
								LiveVideo::POST_TYPE_NAME,
								$bc_video_id
							);
							update_post_meta( $wp_id, 'bc_video_status', 'waitingNotScheduled' );
						} else {
							update_post_meta( $wp_id, 'bc_video_status', 'waiting' );
						}

						if ( ! $wp_id ) {
							throw new \Exception( 'Something went wrong when creating the video on WordPress' );
						}

						update_post_meta( $wp_id, 'server_url', $live_video_response['stream_url'] );
						update_post_meta( $wp_id, 'stream_name', $live_video_response['stream_name'] );
						update_post_meta( $wp_id, 'live_session_id', $live_video_response['id'] );
						update_post_meta( $wp_id, 'live_video_status', 'streaming' );

						update_post_meta( $wp_id, 'bc_video_id', $bc_video_id );
						update_post_meta( $wp_id, 'token', $token );
						update_post_meta( $wp_id, 'bc_video_start', $bc_video_start );
						delete_post_meta( $wp_id, 'bc_video_is_processing' );

						self::sync_to_firestore( get_post( $wp_id ) );
						$timezone = get_post_meta( $wp_id, 'timezone', true );

						// Send the notification to firestore
						VideoEvents::live_video_notification(
							$wp_id,
							$bc_video_start,
							$timezone,
							VideoEvents::LIVE_VIDEO_STARTED
						);

						return [
							'rtmpUrl'        => $live_video_response['stream_url'] . '/' . $live_video_response['stream_name'],
							'liveSessionId'  => $live_video_response['id'],
							'liveVideoId'    => $bc_video_id,
							'liveVideoStart' => $bc_video_start,
							'status'         => 200,
						];
					} catch ( \Exception $exception ) {
						if ( isset( $live_video_response['id'] ) ) {
							StopBroadcast::stop_live_streaming_on_brightcove( $live_video_response['id'] );
						}

						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}

				},
			]
		);
	}


	/**
	 * Get video
	 *
	 * @param string $video_name
	 * @param string $notifications_url
	 *
	 * @return mixed
	 *
	 * @throws \Exception
	 */
	public static function get_live_video( $video_name, $notifications_url ) {

		$url  = 'https://api.bcovlive.io/v1/jobs';
		$body = <<<JSON
{
   "live_stream":true,
   "region":"eu-central-1",
   "reconnect_time":1800,
   "notifications": [
       "$notifications_url"
   ],
   "outputs":[
      {
         "label":"hls1080p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":2400,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "label":"hls720p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":1843,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "label":"hls480p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":819,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "videocloud":{
            "video":{
               "name":"$video_name"
            }
         }
      }
   ]
}
JSON;

		$response = wp_safe_remote_post(
			$url,
			[
				'body'    => $body,
				'headers' => [
					'X-API-KEY'    => LiveStreaming::get_bc_live_api_key(),
					'Content-Type' => 'application/json',
				],
				'timeout' => 20,
			]
		);
		$code     = wp_remote_retrieve_response_code( $response );
		if ( 200 !== $code ) {
			throw new \Exception( __( 'Live video on Brightcove can\'t be created', 'zweb' ) );
		}

		$body = wp_remote_retrieve_body( $response );

		return json_decode( $body, true );
	}
}
