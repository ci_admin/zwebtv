<?php
/**
 * AbstractMutation File
 *
 * @package Zweb
 */

namespace Zweb\GraphQL\Mutations;

use Zweb\Brightcove\PostMeta;
use Zweb\Firebase\Provider;
use Zweb\Firebase\UserManagement;
use Zweb\PostType\Video;
use Zweb\Role\Influencer;

/**
 * Class AbstractMutation
 *
 * @package Zweb\GraphQL\Mutations
 */
abstract class AbstractMutation {

	/**
	 * Register
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_mutation',
			]
		);
	}

	/**
	 * Get influencer form header
	 *
	 * @return \WP_User
	 *
	 * @throws \Exception
	 */
	public static function get_inluencer_user_from_header() {
		$token = UserManagement::get_http_header_token();
		if ( ! $token ) {
			throw new \Exception( __( 'No Token found', 'zweb' ) );
		}
		$firebase = Provider::register();
		$user     = $firebase->is_valid_token( $token );
		if ( ! $user ) {
			throw new \Exception( __( 'No Firebase User found', 'zweb' ) );
		}
		$wp_user = get_user_by( 'email', $user->email );
		if ( ! $wp_user ) {
			throw new \Exception( __( 'No WordPress User found', 'zweb' ) );
		}
		if ( ! Influencer::is_user_influencer( $wp_user ) ) {
			throw new \Exception( __( 'User is not an influencer', 'zweb' ) );
		}

		return $wp_user;
	}

	/**
	 * Register mutation
	 *
	 * @throws \Exception
	 */
	public static function register_mutation() {
		throw new \Exception( 'Method must be implemented' );
	}

	/**
	 * Create a post
	 *
	 * @param \WP_User $influencer
	 * @param string   $post_title
	 * @param string   $post_type
	 * @param string   $bc_video_id
	 * @param string   $post_status
	 * @param int|boolean $wp_id
	 *
	 * @return int|\WP_Error
	 */
	public static function create_video_cpt_with_block(
		\WP_User $influencer,
		$post_title, $post_type,
		$bc_video_id,
		$post_status = 'publish',
		$wp_id = false
	) {
		$player = 'default';
		if ( defined( 'BC_DEFAULT_PLAYER' ) && BC_DEFAULT_PLAYER ) {
			$player = BC_DEFAULT_PLAYER;
		}
		$post_content = <<<GUT
<!-- wp:bc/brightcove {"account_id":"6156229735001","player_id":"$player","video_id":"$bc_video_id","playlist_id":"","experience_id":"","video_ids":"","embed":"in-page","autoplay":"","playsinline":"","picture_in_picture":"","height":"100%","width":"100%","min_width":"0px","max_width":"640px","padding_top":"56%"} /-->
GUT;

		$args = [
			'post_author'  => $influencer->ID,
			'post_status'  => $post_status,
			'post_type'    => $post_type,
			'post_title'   => $post_title,
			'post_content' => $post_content,
		];

		if ( $wp_id ) {
			$args = [
				'ID'           => $wp_id,
				'post_status'  => $post_status,
				'post_content' => $post_content,
			];
		}

		$id = $wp_id ? wp_update_post( $args ) : wp_insert_post( $args );

		if ( ! $wp_id ) {
			wp_set_object_terms(
				$id,
				(int) get_user_meta(
					$influencer->ID,
					\Zweb\Taxonomy\Influencer::META_KEY,
					true
				),
				\Zweb\Taxonomy\Influencer::TAXONOMY_NAME
			);
		}

		PostMeta::save_brightcove_data( get_post( $id ) );

		return $id;
	}
}
