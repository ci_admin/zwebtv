<?php

namespace Zweb\GraphQL\Mutations;

use Mockery\Exception;
use OpenTok\Broadcast;
use OpenTok\Layout;
use Zweb\Admin\LiveStreaming;
use Zweb\Brightcove\BCIngestionApi;
use Zweb\Brightcove\PostMeta;
use Zweb\Firebase\Firestore;
use Zweb\OpenTok\Wrapper;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;
use Zweb\Role\Influencer;
use Zweb\Notifications\VideoEvents;
use Zweb\Role\LiveBattleAdmin;
use Zweb\Routing\Route\BcNotifications;
use Zweb\Traits\FirestoreVideo;

/**
 * Class StartBroadcastLiveBattle
 *
 * @package Zweb\GraphQL\Mutations
 */
class StartBroadcastLiveBattle extends AbstractMutation {

	use FirestoreVideo;

	/**
	 * Register mutation
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'startBroadcastLiveBattle',
			[
				'inputFields'         => [
					'videoName'     => [
						'type'        => 'String',
						'description' => __( 'Video Name', 'zweb' ),
					],
					'sessionId'     => [
						'type'        => 'String',
						'description' => __( 'Vonage Session Id', 'zweb' ),
					],
					'wordpressId'   => [
						'type'        => 'Int',
						'description' => __( 'Wordpress Id', 'zweb' ),
					],
					'firebaseToken' => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
				],
				'outputFields'        => [
					'broadcastId'    => [
						'type'        => 'String',
						'description' => __( 'Vonage Broadcast ID', 'zweb' ),
					],
					'liveSessionId'  => [
						'type'        => 'String',
						'description' => __( 'Brightcove Live ID', 'zweb' ),
					],
					'liveVideoId'    => [
						'type'        => 'String',
						'description' => __( 'Brightcove Video ID', 'zweb' ),
					],
					'liveVideoStart' => [
						'type'        => 'String',
						'description' => __( 'Brightcove Video Start', 'zweb' ),
					],
					'status'         => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message'        => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						$wp_user = Influencer::get_user_from_firebase_if_has_role(
							$input['firebaseToken'],
							LiveBattleAdmin::ROLE_NAME
						);
						$wp_id   = false;
						if ( isset( $input['wordpressId'] ) && $input['wordpressId'] ) {
							$live_battle = get_post( $input['wordpressId'] );
							if ( $live_battle && LiveBattle::POST_TYPE_NAME === $live_battle->post_type ) {
								$wp_id = $input['wordpressId'];
							} else {
								throw new \Exception( __( 'The Live battle was not found in the system', 'zweb' ) );
							}
						} else {
							throw new \Exception( __( 'The id of the Live Battle must be specified', 'zweb' ) );
						}
						$title               = sprintf( __( 'Live Battle started by %1$s on %2$s', 'zweb' ), $wp_user->display_name, gmdate( 'd-m-Y h:i:s' ) );
						$post_title          = $input['videoName'] ?? $title;
						$cms_api             = new BCIngestionApi();

						if ( $wp_id && get_post_meta( $wp_id, 'bc_video_id', true ) ) {
							$bc_video_id = get_post_meta( $wp_id, 'bc_video_id', true );
						} else {
							$video = $cms_api->video_add( $title );
							if ( ! $video ) {
								throw new \Exception( 'Something went wrong when creating the video on Brightcove' );
							}
							$bc_video_id = $video['id'];
						}

						if ( ! $bc_video_id ) {
							throw new \Exception( 'Missing Brightcove video ID' );
						}

						$token = BcNotifications::generate_random_string();
						$notifications_url = BcNotifications::get_live_battle_notifications_url( "$bc_video_id/$token" );
						$live_video_response = self::get_live_video( $post_title, $notifications_url );

						$bc_video_start = $video['schedule']['starts_at'] ?? time();
						$hls            = $cms_api->hls_manifest( $bc_video_id, $live_video_response['playback_url'] );

						if ( ! $hls ) {
							throw new \Exception( 'Something went wrong when associating the video to the live stream on Brightcove' );
						}

						$opentok = Wrapper::get_instance();
						$rmtp    = [
							'id'         => $live_video_response['id'],
							'serverUrl'  => $live_video_response['stream_url'],
							'streamName' => $live_video_response['stream_name'],
						];

						$options = [
							'layout'      => Layout::createCustom(
								[
									'stylesheet' => "
										.container {
										  height: 100%;
										  margin: 0;
										  width: 100%;
										  position: relative;
										}
										stream {
										  height: 50%;
										  object-fit: cover;
										  width: 50%;
										  position: absolute;
										}
										.main-influencer {
										  top: 0;
										  right: 0;
										}
										.participants-1 {
										  width: 100%;
										  height: 100%;
										}
										.participants-2 {
										  width: 50%;
										  height: 100%;
										}
										.participants-3,
										.participants-4 {
										  width: 50%;
										}
										.participants-1.no-influencer {
										  top: 0;
										}
										.participants-2.no-influencer {
										  top: 0;
										  right: 0;
										}
										.participants-2.no-influencer:nth-child(2) {
										  top: 0;
										  right: 50%;
										}
										.participants-3.no-influencer:first-child {
										  top: 0;
										  right: 0;
										  height: 100%;
										}
										.participants-3.no-influencer:nth-child(2) {
										  top: 0;
										  right: 50%;
										}
										.participants-3.no-influencer:nth-child(3) {
										  top: 50%;
										  right: 50%;
										}
										.participants-4.no-influencer:first-child {
										  top: 0;
										  right: 0;
										}
										.participants-4.no-influencer:nth-child(2) {
										  top: 50%;
										  right: 0;
										}
										.participants-4.no-influencer:nth-child(4) {
										  top: 50%;
										  right: 50%;
										}
										.participants-2.participant.has-influencer {
										  top: 0;
										  right: 50%;
										}
										.participants-2.main-influencer {
										  top: 0;
										}
										.participants-3.main-influencer {
										  height: 100%;
										}
										.participants-3.has-influencer.participant:first-child,
										.participants-3.main-influencer
										  + .participants-3.has-influencer.participant:nth-child(2) {
										  top: 0;
										  right: 50%;
										}
										.participants-3.has-influencer.participant:nth-child(3),
										.participants-3.has-influencer.participant:nth-child(2) {
										  top: 50%;
										  right: 50%;
										}
										.participants-4.main-influencer
										  + .participants-4.has-influencer.participant:nth-child(2),
										.participants-4.has-influencer.participant:first-child {
										  top: 50%;
										  right: 0;
										}
										.participants-4.has-influencer.participant:first-child
										  ~ .participants-4.has-influencer.participant:last-child {
										  top: 50%;
										  right: 50%;
										}
										.participants-4.has-influencer.participant:nth-child(1)
										  + .participants-4.has-influencer.participant:nth-child(2)
										  + .participants-4.has-influencer.participant:nth-child(3) {
										  top: 50%;
										  right: 50%;
										}
										.participants-4.has-influencer.main-influencer
										  + .participants-4.has-influencer.participant:nth-child(2)
										  + .participants-4.has-influencer.participant:nth-child(3)
										  + .participants-4.has-influencer.participant:nth-child(4) {
										  top: 50%;
										  right: 50%;
										}
									",
								]
							),
							'maxDuration' => 36000,
							'resolution'  => '1280x720',
							'outputs'     => [ 'rtmp' => $rmtp ],
						];

						$broadcast = $opentok->startBroadcast( $input['sessionId'], $options );

						$wp_id = self::create_video_cpt_with_block(
							$wp_user,
							$live_battle->post_title,
							LiveBattle::POST_TYPE_NAME,
							$bc_video_id,
							'publish',
							$wp_id
						);

						if ( ! $wp_id ) {
							throw new \Exception( 'Something went wrong when creating the video on WordPress' );
						}

						update_post_meta( $wp_id, 'server_url', $live_video_response['stream_url'] );
						update_post_meta( $wp_id, 'stream_name', $live_video_response['stream_name'] );
						update_post_meta( $wp_id, 'live_session_id', $live_video_response['id'] );
						update_post_meta( $wp_id, 'broadcast_id', $broadcast->id );
						update_post_meta( $wp_id, 'live_video_status', 'streaming' );
						update_post_meta( $wp_id, 'bc_video_id', $bc_video_id );
						update_post_meta( $wp_id, 'bc_video_start', $bc_video_start );
						update_post_meta( $wp_id, 'token', $token );
						delete_post_meta( $wp_id, 'bc_video_is_processing' );
						update_post_meta( $wp_id, 'bc_video_status', 'waiting' );

						self::sync_to_firestore( get_post( $wp_id ) );

						$firestore = Firestore::get_instance();
						$data      = [
							'status'        => 'streaming',
							'broadcastId'   => $broadcast->id,
							'liveSessionId' => $live_video_response['id'],
						];
						LiveBattle::update_firebase_document( $firestore, $wp_id, $data );

						// Send the notification to firestore
						VideoEvents::live_video_notification(
							$wp_id,
							get_post_meta( $wp_id, 'scheduledTime', true ) / 1000,
							get_post_meta( $wp_id, 'timezone', true ),
							VideoEvents::LIVE_BATTLE_STARTED
						);

						return [
							'broadcastId'    => $broadcast->id,
							'liveSessionId'  => $live_video_response['id'],
							'liveVideoId'    => $bc_video_id,
							'liveVideoStart' => $bc_video_start,
							'status'         => 200,
						];
					} catch ( \Exception $exception ) {
						if ( isset( $live_video_response['id'] ) ) {
							StopBroadcast::stop_live_streaming_on_brightcove( $live_video_response['id'] );
						}

						if ( ! empty( $broadcast ) && $broadcast instanceof Broadcast ) {
							$broadcast->stop();
						}

						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}

				},
			]
		);
	}

	/**
	 * Get video
	 *
	 * @param string $video_name
	 * @param string $notifications_url
	 *
	 * @return mixed
	 *
	 * @throws \Exception
	 */
	public static function get_live_video( $video_name, $notifications_url ) {
		$url  = 'https://api.bcovlive.io/v1/jobs';

		$body = <<<JSON
{
   "live_stream":true,
   "region":"eu-central-1",
   "reconnect_time":1800,
   "notifications": [
       "$notifications_url"
   ],
   "outputs":[
      {
         "label":"hls1080p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":2400,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "label":"hls720p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":1843,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "label":"hls480p",
         "live_stream":true,
         "video_codec":"h264",
         "h264_profile":"main",
         "video_bitrate":819,
         "segment_seconds":6,
         "keyframe_interval":60
      },
      {
         "videocloud":{
            "video":{
               "name":"$video_name"
            }
         }
      }
   ]
}
JSON;

		$response = wp_safe_remote_post(
			$url,
			[
				'body'    => $body,
				'headers' => [
					'X-API-KEY'    => LiveStreaming::get_bc_live_api_key(),
					'Content-Type' => 'application/json',
				],
				'timeout' => 20,
			]
		);
		$code     = wp_remote_retrieve_response_code( $response );
		if ( 200 !== $code ) {
			throw new \Exception( __( 'Live video on Brightcove can\'t be created', 'zweb' ) );
		}

		$body = wp_remote_retrieve_body( $response );

		return json_decode( $body, true );
	}
}
