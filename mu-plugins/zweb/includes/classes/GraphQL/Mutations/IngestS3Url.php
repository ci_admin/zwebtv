<?php

namespace Zweb\GraphQL\Mutations;

use Zweb\Brightcove\BCIngestionApi;
use Zweb\Brightcove\PostMeta;
use Zweb\Email\UploadVideo;
use Zweb\PostType\Video;
use Zweb\Role\Influencer;

/**
 * Class IngestS3Url
 *
 * @package Zweb\GraphQL\Mutations
 */
class IngestS3Url extends AbstractMutation {

	/**
	 * Register user mutation to create user data.
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'IngestS3Url',
			[
				'inputFields'         => [
					'firebase_token'  => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
					'video_id'        => [
						'type'        => 'String',
						'description' => __( 'Video Id', 'zweb' ),
					],
					'api_request_url' => [
						'type'        => 'String',
						'description' => __( 'name of the video to upload', 'zweb' ),
					],
					'video_name'      => [
						'type'        => 'String',
						'description' => __( 'Video Name', 'zweb' ),
					],
				],
				'outputFields'        => [
					'job_id'  => [
						'type'        => 'String',
						'description' => __( 'Request url for ingestion', 'zweb' ),
					],
					'status'  => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message' => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						$wp_user = Influencer::get_user_from_firebase_if_has_role(
							$input['firebase_token']
						);
					} catch ( \Exception $exception ) {
						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}
					$cms_api = new BCIngestionApi();
					$s3_urls = $cms_api->ingest_s3( $input['video_id'], $input['api_request_url'] );

					$id = self::create_video_cpt_with_block(
						$wp_user,
						$input['video_name'],
						Video::POST_TYPE_NAME,
						$input['video_id'],
						'pending'
					);

					UploadVideo::send( $input['video_name'], $wp_user->display_name, get_edit_post_link( $id ) );

					return [
						'job_id'  => $s3_urls['id'],
						'status'  => 200,
						'message' => "Post $id created in WordPress.",
					];
				},
			]
		);
	}
}
