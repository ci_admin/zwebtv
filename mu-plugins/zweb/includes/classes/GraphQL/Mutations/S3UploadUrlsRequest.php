<?php

namespace Zweb\GraphQL\Mutations;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Zweb\Brightcove\BCIngestionApi;
use Zweb\Role\Influencer;

/**
 * Class S3UploadUrlsRequest
 *
 * @package Zweb\GraphQL\Mutations
 */
class S3UploadUrlsRequest extends AbstractMutation {

	/**
	 * Register user mutation to create user data.
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'S3UploadUrlsMutation',
			[
				'inputFields'         => [
					'firebase_token' => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
					'filename'       => [
						'type'        => 'String',
						'description' => __( 'Filename to upload', 'zweb' ),
					],
					'video_name'     => [
						'type'        => 'String',
						'description' => __( 'name of the video to upload', 'zweb' ),
					],
				],
				'outputFields'        => [
					'status'            => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message'           => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
					'api_request_url'   => [
						'type'        => 'String',
						'description' => __( 'Request url for ingestion', 'zweb' ),
					],
					'video_id'          => [
						'type'        => 'String',
						'description' => __( 'The ID of the Video on Brightcove', 'zweb' ),
					],
					'bucket'            => [
						'type'        => 'String',
						'description' => __( 'S3 bucket for PUT', 'zweb' ),
					],
					'object_key'        => [
						'type'        => 'String',
						'description' => __( 'S3 object key for PUT', 'zweb' ),
					],
					'access_key_id'     => [
						'type'        => 'String',
						'description' => __( 'S3 access key id for PUT', 'zweb' ),
					],
					'secret_access_key' => [
						'type'        => 'String',
						'description' => __( 'S3 secret access key for PUT', 'zweb' ),
					],
					'session_token'     => [
						'type'        => 'String',
						'description' => __( 'S3 session token for PUT', 'zweb' ),
					],
					'pre_signed_url'    => [
						'type'        => 'String',
						'description' => __( 'S3 pre_signed_url for PUT', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						Influencer::get_user_from_firebase_if_has_role(
							$input['firebase_token']
						);
					} catch ( \Exception $exception ) {
						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}
					$cms_api     = new BCIngestionApi();
					$video       = $cms_api->video_add( $input['video_name'] );
					$s3_urls     = $cms_api->upload_urls( $video['id'], $input['filename'] );
					$credentials = new Credentials(
						$s3_urls['access_key_id'],
						$s3_urls['secret_access_key'],
						$s3_urls['session_token']
					);

					$s3_client = new S3Client(
						[
							'version'     => 'latest',
							'region'      => 'us-east-1',
							'credentials' => $credentials,
						]
					);

					$cmd = $s3_client->getCommand(
						'PutObject',
						[
							'Bucket'      => $s3_urls['bucket'],
							'Key'         => $s3_urls['object_key'],
							'ContentType' => 'video/mp4',
						]
					);

					$request = $s3_client->createPresignedRequest( $cmd, '+20 minutes' );

					return [
						'api_request_url'   => $s3_urls['api_request_url'],
						'video_id'          => $video['id'],
						'bucket'            => $s3_urls['bucket'],
						'object_key'        => $s3_urls['object_key'],
						'access_key_id'     => $s3_urls['access_key_id'],
						'secret_access_key' => $s3_urls['secret_access_key'],
						'session_token'     => $s3_urls['session_token'],
						'pre_signed_url'    => $request->getUri(),
						'status'            => 200,

					];
				},
			]
		);
	}
}
