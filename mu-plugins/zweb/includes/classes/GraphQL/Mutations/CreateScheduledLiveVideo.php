<?php

namespace Zweb\GraphQL\Mutations;

use Zweb\Brightcove\BCIngestionApi;
use Zweb\PostType\LiveVideo;
use Zweb\Role\Influencer;
use Zweb\Notifications\VideoEvents;
use Zweb\Traits\FirestoreVideo;

/**
 * Class CreateScheduledLiveVideo
 *
 * @package Zweb\GraphQL\Mutations
 */
class CreateScheduledLiveVideo extends AbstractMutation {

	use FirestoreVideo;

	/**
	 * Check if valid timestamp
	 *
	 * @param string $timestamp
	 *
	 * @return bool
	 */
	public static function is_timestamp( $timestamp ) {
		try {
			new \DateTime( '@' . $timestamp );
		} catch ( \Exception $e ) {
			return false;
		}

		return true;
	}

	/**
	 * Register mutation
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'createScheduledLiveVideo',
			[
				'inputFields'         => [
					'videoName'      => [
						'type'        => 'String',
						'description' => __( 'Live Video Name', 'zweb' ),
					],
					'scheduledTime'  => [
						'type'        => 'String',
						'description' => __( 'Live Video Scheduled Time', 'zweb' ),
					],
					'timezoneString' => [
						'type'        => 'String',
						'description' => __( 'Live Video Scheduled Timezone', 'zweb' ),
					],
					'firebaseToken'  => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
				],
				'outputFields'        => [
					'wordPressId' => [
						'type'        => 'Int',
						'description' => __( 'WordPress ID of the created Live Video', 'zweb' ),
					],
					'status'      => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message'     => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						if ( ! self::is_timestamp( $input['scheduledTime'] ) ) {
							throw new \Exception( $input['scheduledTime'] . ' is not a valid timestamp' );
						}

						if ( time() > $input['scheduledTime'] ) {
							throw new \Exception( 'Scheduled time must be in the future' );
						}

						$wp_user = Influencer::get_user_from_firebase_if_has_role(
							$input['firebaseToken']
						);

						$wp_id = wp_insert_post(
							[
								'post_author' => $wp_user->ID,
								'post_status' => 'publish',
								'post_type'   => LiveVideo::POST_TYPE_NAME,
								'post_title'  => $input['videoName'],
								'post_date'   => gmdate( 'Y-m-d H:i:s', $input['scheduledTime'] ),
							],
							true
						);

						if ( is_wp_error( $wp_id ) ) {
							throw new \Exception( $wp_id->get_error_message() );
						}

						wp_set_object_terms(
							$wp_id,
							(int) get_user_meta(
								$wp_user->ID,
								\Zweb\Taxonomy\Influencer::META_KEY,
								true
							),
							\Zweb\Taxonomy\Influencer::TAXONOMY_NAME
						);
						update_post_meta( $wp_id, 'timezone', $input['timezoneString'] );
						update_post_meta( $wp_id, 'live_video_status', 'scheduled' );
						update_post_meta( $wp_id, 'scheduledTime', $input['scheduledTime'] );
						update_post_meta( $wp_id, 'bc_video_status', 'scheduled' );

						$cms_api = new BCIngestionApi();
						$video   = $cms_api->video_add( $input['videoName'] );
						if ( ! $video ) {
							throw new \Exception( 'Something went wrong when creating the video on Brightcove' );
						}
						$bc_video_id = $video['id'];

						update_post_meta( $wp_id, 'bc_video_id', $bc_video_id );

						self::sync_to_firestore( get_post( $wp_id ) );
						// Send the notification to firestore
						VideoEvents::live_video_notification(
							$wp_id,
							$input['scheduledTime'],
							$input['timezoneString'],
							VideoEvents::LIVE_VIDEO_SCHEDULED
						);
					} catch ( \Exception $exception ) {
						if ( isset( $wp_id ) && $wp_id ) {
							wp_delete_post( $wp_id, true );
						}
						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}

					return [
						'wordPressId' => $wp_id,
						'status'      => 200,
					];
				},
			]
		);
	}
}
