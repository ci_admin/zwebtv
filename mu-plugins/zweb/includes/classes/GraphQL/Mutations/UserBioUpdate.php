<?php

namespace Zweb\GraphQL\Mutations;

use Zweb\GraphQL\Mutations\UserFields;
use Zweb\Firebase\Provider;
use Zweb\Firebase\UserManagement;
use Zweb\Firebase\Firestore;

/**
 * Class UserBioUpdate
 *
 * @package Zweb\GraphQL\Mutations
 */
class UserBioUpdate {
	/**
	 *  Register new mutation types for User bio update.
	 */
	public static function register() {
		add_action(
			'graphql_register_types',
			[
				static::class,
				'register_user_bio_mutation_update',
			]
		);
	}

	/**
	 * Register user mutation to update bio email.
	 */
	public static function register_user_bio_mutation_update() {
		register_graphql_mutation(
			'userBioUpdateMutation',
			[
				'inputFields'         => UserFields::register_input_fields(),
				'outputFields'        => UserFields::register_output_fields(),
				'mutateAndGetPayload' => function( $input, $context, $info ) {
					$message = __( 'Bio update failed', 'zweb' );
					$token   = UserManagement::get_http_header_token();

					if ( ! empty( $token ) && ! empty( $input ) ) {
						$firebase = Provider::register();

						if ( $firebase ) {
							$user = $firebase->is_valid_token( $token );

							if ( $user ) {
								$wp_user = get_user_by( 'email', $user->email );

								$user_data = wp_update_user(
									[
										'ID'          => $wp_user->ID,
										'description' => $input['user_bio'],
									]
								);

								if ( ! is_wp_error( $user_data ) ) {
									$message = __( 'Bio update successful', 'zweb' );
								}
							}
						}
					}
					return UserFields::mutate_user_output( $message );
				}
			]
		);
	}
}
