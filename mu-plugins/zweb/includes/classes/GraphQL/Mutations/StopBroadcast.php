<?php

namespace Zweb\GraphQL\Mutations;

use Zweb\Admin\LiveStreaming;
use Zweb\Firebase\Firestore;
use Zweb\GraphQL\Cache\GraphQLCache;
use Zweb\OpenTok\Wrapper;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\Role\Influencer;
use Zweb\Role\LiveBattleAdmin;
use Zweb\Traits\FirestoreVideo;

/**
 * Class StopBroadcast
 *
 * @package Zweb\GraphQL\Mutations
 */
class StopBroadcast extends AbstractMutation {
	use FirestoreVideo;

	/**
	 * Register mutation
	 */
	public static function register_mutation() {
		register_graphql_mutation(
			'stopBroadcast',
			[
				'inputFields'         => [
					'broadcastId'   => [
						'type'        => 'String',
						'description' => __( 'Vonage Broadcast ID', 'zweb' ),
					],
					'liveSessionId' => [
						'type'        => 'String',
						'description' => __( 'Brightcove Live ID', 'zweb' ),
					],
					'firebaseToken' => [
						'type'        => 'String',
						'description' => __( 'Firebase token', 'zweb' ),
					],
					'isLiveBattle'  => [
						'type'        => 'Boolean',
						'description' => __( 'Are we stopping a live battle', 'zweb' ),
					],
				],
				'outputFields'        => [
					'status'  => [
						'type'        => 'Int',
						'description' => __( 'Status code for the call', 'zweb' ),
					],
					'message' => [
						'type'        => 'String',
						'description' => __( 'Error message', 'zweb' ),
					],
				],
				'mutateAndGetPayload' => function ( $input, $context, $info ) {
					try {
						$role           = Influencer::ROLE_NAME;
						$post_type      = LiveVideo::POST_TYPE_NAME;
						$is_live_battle = false;
						if ( isset( $input['isLiveBattle'] ) && $input['isLiveBattle'] ) {
							$role           = LiveBattleAdmin::ROLE_NAME;
							$post_type      = LiveBattle::POST_TYPE_NAME;
							$is_live_battle = true;
						}
						Influencer::get_user_from_firebase_if_has_role(
							$input['firebaseToken'],
							$role
						);

						self::stop_live_streaming_on_brightcove( $input['liveSessionId'] );

						self::stop_broadcasting_opentok( $input['broadcastId'] );


						$args = [
							'posts_per_page' => 1,
							'post_type'      => $post_type,
							'meta_query'     => [
								[
									'key'     => 'live_session_id',
									'value'   => $input['liveSessionId'],
									'compare' => '=',
								],
							],
						];

						$query = new \WP_Query( $args );
						$video = $query->get_posts();

						if ( $video ) {
							$video = $video[0];
							self::update_post_and_firebase( $video, $is_live_battle );
						}

						return [
							'status' => 200,
						];
					} catch ( \Exception $exception ) {
						error_log( $exception->getMessage() );

						return [
							'message' => $exception->getMessage(),
							'status'  => 500,
						];
					}

				},
			]
		);
	}

	/**
	 * Cancel live streaming
	 *
	 * @param string $live_video_id
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static function stop_live_streaming_on_brightcove( $live_video_id ) {
		$url = "https://api.bcovlive.io/v1/jobs/$live_video_id/stop";

		$response = wp_remote_request(
			$url,
			[
				'headers' => [
					'X-API-KEY'    => LiveStreaming::get_bc_live_api_key(),
					'Content-Type' => 'application/json',
				],
				'method'  => 'PUT',
			]
		);
		$code     = wp_remote_retrieve_response_code( $response );
		if ( 200 !== $code ) {
			throw new \Exception( __( 'Live video can\'t be stopped', 'zweb' ) );
		}

		$body = wp_remote_retrieve_body( $response );

		return json_decode( $body, true );

	}

	/**
	 * Stop OpenTok Broadcast
	 *
	 * @param $broadcast_id
	 *
	 */
	public static function stop_broadcasting_opentok( $broadcast_id ) {
		$opentok = Wrapper::get_instance();
		try {
			$opentok->stopBroadcast( $broadcast_id );
		} catch ( \Exception $exception ) {
			error_log( 'Error when stopping OT. Message: ' . $exception->getMessage() );
		}
	}

	/**
	 * Update status on WordPress and Firebase
	 *
	 * @param \WP_Post $video
	 * @param bool $is_live_battle
	 */
	public static function update_post_and_firebase( \WP_Post $video, bool $is_live_battle, $stopped_from = 'mutation' ) {
		update_post_meta( $video->ID, 'live_video_status', 'finished' );
		self::sync_to_firestore( $video );
		if ( $is_live_battle ) {
			$firestore = Firestore::get_instance();
			$data      = [
				'status'      => 'finished',
				'stoppedFrom' => $stopped_from,
			];
			LiveBattle::update_firebase_document( $firestore, $video->ID, $data );
		}
		GraphQLCache::invalidate_cache();
	}
}
