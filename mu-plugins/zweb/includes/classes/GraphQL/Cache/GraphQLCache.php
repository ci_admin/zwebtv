<?php

namespace Zweb\GraphQL\Cache;

use WPGraphQL\Extensions\Cache\Backend\AbstractBackend;
use WPGraphQL\Extensions\Cache\CacheManager;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Studio;
use Zweb\PostType\Video;

/**
 * Class GraphQLCache
 *
 * @package Zweb\GraphQL\Cache
 */
class GraphQLCache {

	/**
	 * Register hooks.
	 */
	public static function register() {
		if ( ! class_exists( 'WPGraphQL\Extensions\Cache\CacheManager' ) ) {
			error_log( 'Graph QL cache plugin not activated' );

			return;
		}

		CacheManager::register_graphql_query_cache( [
			'query_name' => '*',
			'expire'     => 120,
		] );

		add_filter( 'graphql_cache_backend', [
			static::class,
			'graphql_cache_backend',
		] );

		add_action( 'save_post', [
			static::class,
			'maybe_invalidate_cache',
		], 10, 2 );

		add_action( 'user_register', [ static::class, 'invalidate_cache' ] );
		add_action( 'delete_user', [ static::class, 'invalidate_cache' ] );
		add_action( 'profile_update', [ static::class, 'invalidate_cache' ] );
	}

	/**
	 * Try to set Memcache as the backend.
	 *
	 * @param AbstractBackend $backend
	 *
	 * @return AbstractBackend
	 */
	public static function graphql_cache_backend( AbstractBackend $backend ) {
		try {
			return new WPCacheBackend();
		} catch ( \Exception $exc ) {
			return $backend;
		}
	}

	/**
	 * Maybe invalidate cache when saving post.
	 *
	 * @param int $post_id
	 * @param \WP_Post $post
	 */
	public static function maybe_invalidate_cache( $post_id, \WP_Post $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$post_statuses_to_check = [
			'publish' => true,
			'trash'   => true,
		];
		$post_types_to_check    = [
			LiveVideo::POST_TYPE_NAME  => true,
			LiveBattle::POST_TYPE_NAME => true,
			Studio::POST_TYPE_NAME     => true,
			Video::POST_TYPE_NAME      => true,
		];

		if ( isset( $post_statuses_to_check[ $post->post_status ], $post_types_to_check[ $post->post_type ] ) ) {
			self::invalidate_cache();
		}
	}

	/**
	 * Invalidate cache.
	 */
	public static function invalidate_cache() {
		CacheManager::clear();
	}
}