<?php

declare( strict_types=1 );

namespace Zweb\GraphQL\Cache;

use WPGraphQL\Extensions\Cache\Backend\AbstractBackend;
use WPGraphQL\Extensions\Cache\CachedValue;

/**
 * Class WPCacheBackend
 *
 * @package Zweb\GraphQL\Cache
 */
class WPCacheBackend extends AbstractBackend {

	/**
	 * @inheritDoc
	 */
	public function set( string $zone, string $key, CachedValue $data, $expire = null ): void {
		wp_cache_set( $key, $data, $zone, (int) $expire );
	}

	/**
	 * @inheritDoc
	 */
	public function get( string $zone, string $key ): ?CachedValue {
		$cached_value = wp_cache_get( $key, $zone, false, $found );
		if ( false === $found ) {
			return null;
		}

		if ( $cached_value instanceof CachedValue ) {
			return $cached_value;
		}

		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function delete( string $zone, string $key ): bool {
		return wp_cache_delete( $key, $zone );
	}

	/**
	 * @inheritDoc
	 */
	public function clear_zone( string $zone ): bool {
		return $this->clear();
	}

	/**
	 * @inheritDoc
	 */
	public function clear(): bool {
		return wp_cache_flush();
	}
}