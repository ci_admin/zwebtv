<?php

namespace Zweb\Traits;

use MrShan0\PHPFirestore\Fields\FirestoreReference;
use Zweb\Firebase\Firestore;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Influencer;

trait FirestoreVideo {

	/**
	 * Checks if the current post should be synced to firestore or not.
	 *
	 * @param \WP_Post $post The post object.
	 *
	 * @return bool
	 */
	protected static function should_sync( \WP_Post $post ) {
		$post_type   = get_post_type( $post );
		$status      = get_post_status( $post );
		$bc_video_id = get_post_meta( $post->ID, 'bc_video_id', true );

		if ( ! $bc_video_id ) {
			return false;
		}

		if ( 'publish' !== $status ) {
			return false;
		}

		if ( ! in_array( $post_type, [ LiveVideo::POST_TYPE_NAME, Video::POST_TYPE_NAME, LiveBattle::POST_TYPE_NAME ], true ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Retrieves the type of the video.
	 *
	 * @param \WP_Post $post The post object.
	 *
	 * @return string
	 */
	protected static function get_type( \WP_Post $post ) {
		$post_type = get_post_type( $post );

		if ( Video::POST_TYPE_NAME === $post_type ) {
			return 'vod';
		}

		if ( LiveVideo::POST_TYPE_NAME === $post_type ) {
			return 'live';
		}

		if ( LiveBattle::POST_TYPE_NAME === $post_type ) {
			return 'battle';
		}

		return 'vod';
	}

	/**
	 * Retrieves the status of the video.
	 *
	 * @param \WP_Post $post The Post object.
	 *
	 * @return string
	 */
	protected static function get_status( \WP_Post $post ) {
		$post_type = get_post_type( $post );

		if ( Video::POST_TYPE_NAME === $post_type ) {
			return 'publish';
		}

		if ( LiveVideo::POST_TYPE_NAME === $post_type || LiveBattle::POST_TYPE_NAME === $post_type ) {
			$live_video_status = get_post_meta( $post->ID, 'live_video_status', true );

			return 'streaming' === $live_video_status ? 'publish' : $live_video_status;
		}

		return 'publish';
	}

	/**
	 * Syncs a Video/Live Video to Firestore
	 *
	 * @param \WP_Post $post The post object.
	 *
	 * @return void
	 */
	public static function sync_to_firestore( \WP_Post $post ) {
		$firestore = Firestore::get_instance();

		if ( $firestore ) {
			$firestore_client = $firestore->get_firestore_client_instance();
			$influencers      = get_the_terms( $post, Influencer::TAXONOMY_NAME );

			if ( ! self::should_sync( $post ) || ! $influencers ) {
				return;
			}

			$bc_video_id   = get_post_meta( $post->ID, 'bc_video_id', true );
			$document_path = "videos/{$bc_video_id}";
			$influencer    = Influencer::get_user_from_term( $influencers[0]->term_id );
			$uid           = get_user_meta( $influencer->ID, 'zweb-influencer_firebase-uid', true );

			if ( ! $uid ) {
				return;
			}

			try {
				$firestore_client->updateDocument(
					$document_path,
					[
						'title'    => get_the_title( $post ),
						'type'     => self::get_type( $post ),
						'status'   => self::get_status( $post ),
						'bcStatus' => get_post_meta( $post->ID, 'bc_video_status', true ),
						'user'     => new FirestoreReference( "/user/{$uid}" ),
					]
				);
			} catch ( \Exception $e ) {
				error_log( "There was an problem updating the video document {$post->ID}" );
			}
		}
	}


	/**
	 * Removes the video document in firestore
	 *
	 * @param int $post_id The post id
	 *
	 * @return void
	 */
	public static function delete_from_firestore( $post_id ) {
		$firestore = Firestore::get_instance();

		if ( $firestore ) {
			$firestore_client = $firestore->get_firestore_client_instance();
			$bc_video_id      = get_post_meta( $post_id, 'bc_video_id', true );

			if (
				! $bc_video_id ||
				! in_array( get_post_type( $post_id ), [ LiveVideo::POST_TYPE_NAME, Video::POST_TYPE_NAME, LiveBattle::POST_TYPE_NAME ], true ) ) {
				return;
			}

			$document_path = "videos/{$bc_video_id}";

			try {
				$firestore_client->deleteDocument( $document_path );
			} catch ( \Exception $e ) {
				error_log( "There was an problem removing the video document {$post_id}" );
			}
		}
	}
}

