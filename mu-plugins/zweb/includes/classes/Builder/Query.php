<?php

namespace Zweb\Builder;

use Zweb\PostType\LiveVideo;
use Zweb\PostType\Studio;
use Zweb\PostType\Video;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;
use Zweb\Taxonomy\VideoCategory;

/**
 * This class handles building WP_Query objects.
 *
 * @package Zweb\Builder
 */
class Query {

	/**
	 * Create the object for the Novità carousel.
	 *
	 * @param int           $how_many
	 * @param \WP_Term|null $gallery_term
	 * @param \WP_Term|null $influencer_term
	 * @param \WP_Term|null $video_category_term
	 * @param int $offset
	 *
	 * @return \WP_Query
	 */
	public static function build_novita_query(
		$how_many = 8,
		\WP_Term $gallery_term = null,
		\WP_Term $influencer_term = null,
		\WP_Term $video_category_term = null,
		$offset = 0
	) {
		$query       = self::get_video_carousel_query( $how_many, $gallery_term, $influencer_term, $video_category_term, $offset );
		$tax_query   = $query->get( 'tax_query' ) || [];
		$tax_query[] = [
			'taxonomy' => Influencer::TAXONOMY_NAME,
			'terms'    => Influencer::get_excluded_influencers(),
			'field'    => 'term_id',
			'operator' => 'NOT IN',
		];
		$query->set( 'tax_query', $tax_query );

		return $query;
	}

	/**
	 * Get videos in the same category as the provided video, excluding it.
	 *
	 * @param string $video_id
	 * @param int $how_many
	 * @param int $offset
	 *
	 * @return \WP_Query
	 */
	public static function get_related_video_for_video_query( $video_id, $how_many = 8, $offset = 0 ) {
		$query = self::get_video_carousel_query(
			$how_many,
			null,
			null,
			VideoCategory::get_video_category_term_for_video( $video_id ),
			$offset
		);
		$query->set( 'post__not_in', [ $video_id ] );

		return $query;
	}

	/**
	 * Get videos in the same video category of the influencer, excluding the
	 * influencer videos.
	 *
	 * @param \WP_Term $influencer
	 * @param int $how_many
	 * @param int $offset
	 *
	 * @return \WP_Query
	 */
	public static function get_related_video_for_influencer_query( \WP_Term $influencer, $how_many = 8, $offset = 0 ) {
		try {
			// get the influencer category
			$influencer_user           = \Zweb\Taxonomy\Influencer::get_user_from_term( $influencer->term_id );
			$influencer_video_category = \Zweb\Taxonomy\Influencer::get_influencer_video_category( $influencer_user->ID );
		} catch ( \Exception $exception ) {
			$influencer_video_category = null;
		}
		$query     = self::get_video_carousel_query( $how_many, null, null, $influencer_video_category, $offset );
		$tax_query = $query->get( 'tax_query' );
		if ( ! $tax_query ) {
			$tax_query = [];
		}
		$tax_query[] = [
			'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
			'field'    => 'term_id',
			'terms'    => $influencer->term_id,
			'operator' => 'NOT IN',
		];
		$query->set( 'tax_query', $tax_query );

		return $query;
	}

	/**
	 * Add tax query parameter if needed.
	 *
	 * @param array $args
	 * @param \WP_Term|null $gallery_term
	 * @param \WP_Term|null $influencer_term
	 * @param \WP_Term|null $video_category_term
	 *
	 * @return array
	 */
	protected static function maybe_add_tax_query_to_args(
		array $args,
		\WP_Term $gallery_term = null,
		\WP_Term $influencer_term = null,
		\WP_Term $video_category_term = null
	): array {
		if ( $gallery_term || $influencer_term || $video_category_term ) {
			$args['tax_query'] = [];
			if ( $gallery_term ) {
				$args['tax_query'][] = [
					'taxonomy' => Gallery::TAXONOMY_NAME,
					'field'    => 'term_id',
					'terms'    => $gallery_term->term_id,
				];
			}
			if ( $influencer_term ) {
				$args['tax_query'][] = [
					'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
					'field'    => 'term_id',
					'terms'    => $influencer_term->term_id,
				];
			}
			if ( $video_category_term ) {
				$args['tax_query'][] = [
					'taxonomy' => VideoCategory::TAXONOMY_NAME,
					'field'    => 'term_id',
					'terms'    => $video_category_term->term_id,
				];
			}
		}

		return $args;
	}

	/**
	 * Get query for upcoming videos.
	 *
	 * @param int $how_many
	 * @param string $search
	 *
	 * @return \WP_Query
	 */
	public static function get_upcoming_video_query( $how_many = 4, $search = '' ) {
		return new \WP_Query(
			[
				'post_type'      => Video::POST_TYPE_NAME,
				'posts_per_page' => $how_many,
				'post_status'    => 'future',
				'no_found_rows'  => true,
				's'              => $search,
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'compare' => 'NOT EXISTS',
					],
				],
			]
		);
	}

	/**
	 * Get query for carousels in curated lists
	 *
	 * @param array $curated_video_ids
	 *
	 * @return \WP_Query
	 */
	public static function get_query_for_curated_lists( array $curated_video_ids ) {
		$query = new \WP_Query(
			[
				'post_type'      => Video::POST_TYPE_NAME,
				'posts_per_page' => 10,
				'post_status'    => 'publish',
				'post__in'       => array_map( 'absint', $curated_video_ids ),
				'orderby'        => 'post__in',
				'meta_query'     => [
					[
						'key'     => 'bc_video_is_processing',
						'compare' => 'NOT EXISTS',
					],
				],
			]
		);

		return $query;
	}

	/**
	 * Get the query for the video carousel.
	 *
	 * @param int $how_many
	 * @param \WP_Term|null $gallery_term
	 * @param \WP_Term|null $influencer_term
	 * @param \WP_Term|null $video_category_term
	 * @param int $offset
	 * @param string $search
	 * @param string $post_status
	 *
	 * @return \WP_Query
	 */
	public static function get_video_carousel_query(
		$how_many = 8,
		\WP_Term $gallery_term = null,
		\WP_Term $influencer_term = null,
		\WP_Term $video_category_term = null,
		$offset = 0,
		$search = '',
		$post_status = 'publish'
	) {
		$args = [
			'post_type'      => Video::POST_TYPE_NAME,
			'posts_per_page' => $how_many,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'offset'         => $offset,
			's'              => $search,
			'post_status'    => $post_status,
			'meta_query'     => [
				[
					'key'     => 'bc_video_is_processing',
					'compare' => 'NOT EXISTS',
				],
			],
		];
		$args = self::maybe_add_tax_query_to_args(
			$args,
			$gallery_term,
			$influencer_term,
			$video_category_term
		);

		return new \WP_Query(
			$args
		);
	}

	/**
	 * Get query for Studio
	 *
	 * @param int $studio_id
	 * @param int $how_many
	 * @param int $offset
	 *
	 * @return \WP_Query
	 */
	public static function get_studio_carousel_video_query( int $studio_id, $how_many = 8, $offset = 0 ) {
		$influencers = Studio::get_influencers_for_studio( $studio_id );
		$query = self::get_video_carousel_query( $how_many, null, null, null, $offset );
		$tax_query = $query->get( 'tax_query' );
		if ( ! $tax_query ) {
			$tax_query = [];
		}
		$tax_query[] = [
			'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
			'field'    => 'term_id',
			'terms'    => wp_list_pluck( $influencers, 'term_id' ),
			'operator' => 'IN',
		];
		$query->set( 'tax_query', $tax_query );

		return $query;
	}

	/**
	 * @param \WP_Term $influencer
	 *
	 * @return \WP_Query
	 * @throws \Exception
	 */
	public static function get_live_video_by_influencer_query( \WP_Term $influencer ) {
		$query = new \WP_Query(
			[
				'post_type'      => LiveVideo::POST_TYPE_NAME,
				'posts_per_page' => 1,
				'tax_query'      => [
					[
						'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
						'field'    => 'term_id',
						'terms'    => $influencer->term_id,
					],
				],
			]
		);
		if ( ! $query->have_posts() ) {
			throw new \Exception( 'no live videos' );
		}

		return $query;
	}
}
