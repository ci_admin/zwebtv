<?php

namespace Zweb\Migration;

use Zweb\Firebase\Firestore;
use Zweb\Migration\Utils\ProgressBar;
use Zweb\PostType\Studio;
use Zweb\Role\Influencer;
use Zweb\Role\LiveBattleAdmin;

/**
 * WP-CLI command for ZWebTV Migration.
 *
 * @package Zweb\Migration
 */
class ZwebCommands extends \WP_CLI_Command {

	/**
	 * Custom WP-CLI command to associate new Studio influencers with videos.
	 * Delete old association with `Studio Roma` and `Gallery` categories.
	 *
	 * @param array $args - Array of command arguments.
	 * @param array $opts - Array of command options.
	 */
	public function migration_studio_roma_videos( $args = [], $opts = [] ) {
		if ( ! empty( $opts['from_gallery'] ) ) {
			$gallery_term = $opts['from_gallery'];
		} else {
			\WP_CLI::error( 'Please provide a from gallery term' );
		}

		if ( ! empty( $opts['to_influencer'] ) ) {
			$influencer_term = $opts['to_influencer'];
		} else {
			\WP_CLI::error( 'Please provide a to influencer term' );
		}

		$video_query = new \WP_Query(
			[
				'post_type'      => [
					\Zweb\PostType\Video::POST_TYPE_NAME,
					\Zweb\PostType\LiveVideo::POST_TYPE_NAME,
				],
				'post_status'    => 'publish',
				'posts_per_page' => 500,
				'tax_query'      => [
					'relation' => 'AND',
					[
						'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
						'field'    => 'slug',
						'terms'    => 'studio-roma',
					],
					[
						'taxonomy' => \Zweb\Taxonomy\Gallery::TAXONOMY_NAME,
						'field'    => 'term_id',
						'terms'    => $gallery_term,
					],
				],
			]
		);

		$total        = $video_query->found_posts;
		$videos       = $video_query->get_posts();
		$msg          = "Assigning new Studio terms for {$total} videos...";
		$progress_bar = new ProgressBar( $msg, $total );

		foreach ( $videos as $index => $video ) {
			\WP_CLI::log( "\nAssigning category term {$influencer_term} to post ids in {$video->ID} ..." );
			// Add new taxonomy to video
			$term_id = wp_set_object_terms(
				$video->ID,
				(int) $influencer_term,
				\Zweb\Taxonomy\Influencer::TAXONOMY_NAME
			);

			if ( ! is_wp_error( $term_id ) ) {
				// Remove Studio Roma
				wp_remove_object_terms(
					$video->ID,
					'studio-roma',
					\Zweb\Taxonomy\Influencer::TAXONOMY_NAME
				);

				// Remove gallery term
				wp_remove_object_terms(
					$video->ID,
					(int) $gallery_term,
					\Zweb\Taxonomy\Gallery::TAXONOMY_NAME
				);
			}

			$progress_bar->tick();
		}

		$progress_bar->finish();
		\WP_CLI::success( "Assigned new Studio terms for {$total} videos..." );
	}

	/**
	 * Set the postmeta value for studio videos.
	 */
	public function update_studio_video() {
		$studio_query = new \WP_Query(
			[
				'post_type'      => [
					\Zweb\PostType\Studio::POST_TYPE_NAME,
				],
				'post_status'    => 'publish',
				'posts_per_page' => 500,
			]
		);
		foreach ( $studio_query->get_posts() as $studio ) {
			\WP_CLI::log( "Found studio {$studio->post_name} \n" );
			$influencers = Studio::get_influencers_for_studio( $studio->ID );
			$term_ids    = [];
			foreach ( $influencers as $influencer ) {
				\WP_CLI::log( "Found influencer {$influencer->name} \n" );
				$term_ids[] = $influencer->term_id;
			}
			$video_query = new \WP_Query(
				[
					'post_type'      => [
						\Zweb\PostType\Video::POST_TYPE_NAME,
						\Zweb\PostType\LiveVideo::POST_TYPE_NAME,
					],
					'post_status'    => 'publish',
					'posts_per_page' => 1000,
					'fields'         => 'ids',
					'tax_query'      => [
						[
							'taxonomy' => \Zweb\Taxonomy\Influencer::TAXONOMY_NAME,
							'field'    => 'term_id',
							'terms'    => $term_ids,
						],
					],
				]
			);

			$total        = $video_query->found_posts;
			$videos       = $video_query->get_posts();
			$msg          = "Assigning new Studio postmeta for {$total} videos...";
			$progress_bar = new ProgressBar( $msg, $total );

			foreach ( $videos as $post_id ) {
				update_post_meta( $post_id, 'zweb-influencer_parent_studio', $studio->ID );

				$progress_bar->tick();
			}

			$progress_bar->finish();
			\WP_CLI::log( "Assigned new Studio meta for Studio with ID $studio->ID for {$total} videos..." );
		}

		\WP_CLI::success( 'finished' );
	}

	/**
	 * WP-CLI command used to sync the influencers and live battle admin users to
	 * the Firebase emulator.
	 */
	public function sync_users_fb_emulator( $args = [], $opts = [] ) {
		$firestore_emulator = getenv( 'FIRESTORE_EMULATOR_HOST' );

		if ( ! $firestore_emulator ) {
			\WP_CLI::error( 'This command requires WP to be set up to use the firestore emulator.' );
		}

		$users = get_users(
			[
				'role__in' => [ Influencer::ROLE_NAME, LiveBattleAdmin::ROLE_NAME ],
				'number'  => -1,
			]
		);

		$firestore = Firestore::get_instance()->get_firestore_client_instance();

		foreach ( $users as $user ) {
			$uid              = get_user_meta( $user->ID, 'zweb-influencer_firebase-uid', true );
			$user_description = get_user_meta( $user->ID, 'description', true );
			$profile_url      = get_user_meta( $user->ID, 'zweb-influencer_profile_image', true ) ? wp_get_attachment_url( get_user_meta( $user->ID, 'zweb-influencer_profile_image', true ) ) : '';
			$role             = $user->roles[0];

			if ( ! $uid ) {
				\WP_CLI::log( 'Skipping user as it does not have an uid' );
				continue;
			}

			try {
				\WP_CLI::log( sprintf( 'Syncing user #%d (%s) - %s', $user->ID, $uid, $user->user_email ) );
				$firestore->updateDocument(
					"users/{$uid}",
					[
						'avatar'              => $profile_url,
						'dateAdded'           => gmdate( 'Y-m-d H:i:s', strtotime( $user->user_registered ) ),
						'userRole'            => $role,
						'bio'                 => $user_description,
						'userDisplayName'     => $user->display_name,
						'userName'            => $user->display_name,
						'emulator_user_email' => $user->user_email,
					]
				);
			} catch ( \Exception $e ) {
				\WP_CLI::log( 'There was a problem syncing ' . $user->ID . ' ' . $user->user_email );
			}
		}
	}
}
