<?php

namespace Zweb\Migration\Utils;

class ProgressBar {

	public $bar;

	public function __construct( $msg, $total ) {
		$this->bar = $this->build( $msg, $total );
	}

	public function tick() {
		if ( $this->is_wp_cli() ) {
			$this->bar->tick();
		}
	}

	public function finish() {
		if ( $this->is_wp_cli() ) {
			$this->bar->finish();
		}
	}

	/* helpers */

	function is_wp_cli() {
		return defined( 'WP_CLI' ) && WP_CLI;
	}

	function build( $msg, $total ) {
		if ( is_null( $this->bar ) ) {
			if ( $this->is_wp_cli() ) {
				$this->bar = new \cli\progress\Bar( $msg, $total );
			} else {
				$this->bar = false;
			}
		}

		return $this->bar;
	}

}
