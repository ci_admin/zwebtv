<?php
/**
 * Core plugin functionality.
 *
 * @package Zweb
 */

namespace Zweb\Core;

use \WP_Error as WP_Error;
use Zweb\Ajax\Homepage;
use Zweb\Ajax\SliderPagination;
use Zweb\Brightcove\Ingestion;
use Zweb\Brightcove\Player;
use Zweb\GraphQL\Cache\GraphQLCache;
use Zweb\GraphQL\Mutations\CreateScheduledLiveVideo;
use Zweb\GraphQL\Mutations\GetLiveStreamSessionAndToken;
use Zweb\GraphQL\Mutations\IngestS3Url;
use Zweb\GraphQL\Mutations\S3UploadUrlsRequest;
use Zweb\GraphQL\Mutations\StartBroadcastNoOpentok;
use Zweb\GraphQL\Mutations\StartBroadcastLiveBattle;
use Zweb\GraphQL\Mutations\StopBroadcast;
use Zweb\GraphQL\Resolvers\LiveVideosResolver;
use Zweb\PostType\LiveBattle;
use Zweb\PostType\LiveVideo;
use Zweb\Routing\Router;
use Zweb\SEO\Og;
use Zweb\PostType\Video;
use Zweb\GraphQL\Resolvers\VideosResolver;
use Zweb\GraphQL\Resolvers\InfluencersResolver;
use Zweb\GraphQL\Resolvers\StudiosResolver;
use Zweb\GraphQL\Resolvers\VideoCategoriesResolver;
use Zweb\GraphQL\Resolvers\PromoBoxResolver;
use Zweb\GraphQL\Mutations\UserBioUpdate;
use Zweb\Firebase\UserManagement;
use Zweb\Role;
use Zweb\Taxonomy\Gallery;
use Zweb\Taxonomy\Influencer;
use Zweb\Taxonomy\StudioCategory;
use Zweb\Taxonomy\VideoCategory;
use Zweb\Brightcove\BrightcoveCron;
use Zweb\Admin;
use Zweb\Firebase;
use Zweb\PostType\Studio;
use Zweb\Notifications\VideoEvents;
use Zweb\Migration\ZwebCommands;

/**
 * Default setup routine
 *
 * @return void
 */
function setup() {
	$n = function ( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action( 'init', $n( 'i18n' ) );
	// we need to init after brightcove.
	add_action( 'init', $n( 'init' ), 29 );
	add_action( 'init', $n( 'load_custom_fields' ) );
	add_action( 'wp_enqueue_scripts', $n( 'scripts' ) );
	add_action( 'wp_enqueue_scripts', $n( 'styles' ) );
	add_action( 'admin_enqueue_scripts', $n( 'admin_scripts' ) );
	add_action( 'admin_enqueue_scripts', $n( 'admin_styles' ) );

	// perform some clean up of things we don't need.
	add_action( 'wp_dashboard_setup', $n( 'remove_draft_widget' ), 999 );
	add_action( 'admin_bar_menu', $n( 'remove_default_post_type_menu_bar' ), 999 );
	add_action( 'admin_menu', $n( 'remove_unused_menus' ) );
	add_action( 'init', $n( 'remove_comment_support' ), 100 );
	add_action( 'rest_api_init', $n( 'rest_api_init' ) );

	// Editor styles. add_editor_style() doesn't work outside of a theme.
	add_filter( 'mce_css', $n( 'mce_css' ) );
	// Hook to allow async or defer on asset loading.
	add_filter( 'script_loader_tag', $n( 'script_loader_tag' ), 10, 2 );

	// Filter locale.
	add_filter( 'locale', $n( 'locale' ) );

	do_action( 'zweb_loaded' );
}

/**
 * Registers the default textdomain.
 *
 * @return void
 */
function i18n() {
	$locale = apply_filters( 'plugin_locale', get_locale(), 'zweb' );
	load_textdomain( 'zweb', WP_LANG_DIR . '/zweb/zweb-' . $locale . '.mo' );
	load_plugin_textdomain( 'zweb', false, plugin_basename( ZWEB_PATH ) . '/languages/' );
}

/**
 * Initialize rest api
 */
function rest_api_init() {
	Video::register_api();
}

/**
 * Set locale to Italian on front-end
 *
 * @param string $locale
 *
 * @return string $locale
 */
function locale( $locale ) {
	if ( is_admin() ) {
		return $locale;
	}

	return 'it_IT';
}

/**
 * Initializes the plugin and fires an action other plugins can hook into.
 *
 * @return void
 */
function init() {
	do_action( 'zweb_init' );

	// Register video.
	Video::register();
	LiveVideo::register();
	LiveBattle::register();
	Studio::register();

	// Register taxonomies.
	$tax_gallery    = new Gallery();
	$tax_influencer = new Influencer();
	$tax_studio     = new StudioCategory();
	$tax_video      = new VideoCategory();
	new BrightcoveCron();
	Ingestion::register();
	Player::register();
	Og::register();
	Router::register();

	// Register.
	$tax_gallery->register();
	$tax_influencer->register();
	$tax_studio->register();
	$tax_video->register();

	// Register graphql resolvers
	VideosResolver::register();
	LiveVideosResolver::register();
	InfluencersResolver::register();
	StudiosResolver::register();
	VideoCategoriesResolver::register();
	PromoBoxResolver::register();

	// Initialize Firebase user management
	$firebase_users = new UserManagement();
	$firebase_users->register();

	// Register graphql mutation
	S3UploadUrlsRequest::register();
	IngestS3Url::register();
	UserBioUpdate::register();
	GetLiveStreamSessionAndToken::register();
	StartBroadcastNoOpentok::register();
	StopBroadcast::register();
	CreateScheduledLiveVideo::register();
	StartBroadcastLiveBattle::register();

	// Notifications
	VideoEvents::register();

	// Register and remove roles.
	Role\Influencer::register();
	Role\Editor::register();
	Role\Administrator::modify();
	Role\LiveBattleAdmin::register();
	remove_role( 'subscriber' );
	remove_role( 'contributor' );
	remove_role( 'author' );
	SliderPagination::register();
	Homepage::register();

	GraphQLCache::register();

	// Migration CLI
	if ( defined( 'WP_CLI' ) && WP_CLI ) {
		\WP_CLI::add_command(
			'zweb',
			'Zweb\Migration\ZwebCommands'
		);
	}

	if ( is_admin() ) {
		Admin\Settings::register();
		Firebase\Settings::register();
		Admin\LiveStreaming::register();
		Admin\GlobalSettings::register();
	}
}

/**
 * Load custom fields.
 */
function load_custom_fields() {

	// Require Select2 class file.
	if ( defined( 'TENUP_CONTENT_TYPES_DIR' ) ) {
		require_once TENUP_CONTENT_TYPES_DIR . '/includes/fields/select2.php';
	}
}

/**
 * Activate the plugin
 *
 * @return void
 */
function activate() {
	// First load the init scripts in case any rewrite functionality is being loaded
	init();
	flush_rewrite_rules();
}

/**
 * Deactivate the plugin
 *
 * Uninstall routines should be in uninstall.php
 *
 * @return void
 */
function deactivate() {

}


/**
 * The list of knows contexts for enqueuing scripts/styles.
 *
 * @return array
 */
function get_enqueue_contexts() {
	return [ 'admin', 'frontend', 'shared' ];
}

/**
 * Generate an URL to a script, taking into account whether SCRIPT_DEBUG is enabled.
 *
 * @param string $script Script file name (no .js extension)
 * @param string $context Context for the script ('admin', 'frontend', or 'shared')
 *
 * @return string|WP_Error URL
 */
function script_url( $script, $context ) {

	if ( ! in_array( $context, get_enqueue_contexts(), true ) ) {
		return new WP_Error( 'invalid_enqueue_context', 'Invalid $context specified in Zweb script loader.' );
	}

	return ZWEB_URL . "dist/js/${script}.js";

}

/**
 * Remove comment support.
 */
function remove_comment_support() {
	remove_post_type_support( 'page', 'comments' );
	remove_post_type_support( 'post', 'comments' );
}


/**
 * Remove unused menus.
 */
function remove_unused_menus() {
	remove_menu_page( 'edit.php' );
	remove_menu_page( 'edit-comments.php' );
}

/**
 * Remove adding new post from the admin bar.
 *
 * @param \WP_Admin_Bar $wp_admin_bar
 */
function remove_default_post_type_menu_bar( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'new-post' );
}

/**
 * Remove the draft widget from the dasboard.
 */
function remove_draft_widget() {
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

/**
 * Generate an URL to a stylesheet, taking into account whether SCRIPT_DEBUG is enabled.
 *
 * @param string $stylesheet Stylesheet file name (no .css extension)
 * @param string $context Context for the script ('admin', 'frontend', or 'shared')
 *
 * @return string URL
 */
function style_url( $stylesheet, $context ) {

	if ( ! in_array( $context, get_enqueue_contexts(), true ) ) {
		return new WP_Error( 'invalid_enqueue_context', 'Invalid $context specified in Zweb stylesheet loader.' );
	}

	return ZWEB_URL . "dist/css/${stylesheet}.css";

}

/**
 * Get modified time
 *
 * @param string $script
 *
 * @return false|int
 */
function script_file_modified_time( $script ) {
	return filemtime( ZWEB_PATH . "dist/js/${script}.js" );
}

/**
 * @param string $stylesheet
 *
 * @return false|int
 */
function style_file_modified_time( $stylesheet ) {
	return filemtime( ZWEB_PATH . "dist/css/${stylesheet}.css" );
}

/**
 * Enqueue scripts for front-end.
 *
 * @return void
 */
function scripts() {

	wp_enqueue_script(
		'zweb_shared',
		script_url( 'shared', 'shared' ),
		[],
		script_file_modified_time( 'shared' ),
		true
	);

	wp_enqueue_script(
		'zweb_frontend',
		script_url( 'frontend', 'frontend' ),
		[],
		script_file_modified_time( 'frontend' ),
		true
	);

}

/**
 * Enqueue scripts for admin.
 *
 * @return void
 */
function admin_scripts() {

	wp_enqueue_script(
		'zweb_shared',
		script_url( 'shared', 'shared' ),
		[],
		script_file_modified_time( 'shared' ),
		true
	);

	wp_enqueue_script(
		'zweb_admin',
		script_url( 'admin', 'admin' ),
		[],
		script_file_modified_time( 'admin' ),
		true
	);

}

/**
 * Enqueue styles for front-end.
 *
 * @return void
 */
function styles() {

	wp_enqueue_style(
		'zweb_shared',
		style_url( 'shared-style', 'shared' ),
		[],
		style_file_modified_time( 'shared-style' )
	);

	if ( is_admin() ) {
		wp_enqueue_style(
			'zweb_admin',
			style_url( 'admin-style', 'admin' ),
			[],
			style_file_modified_time( 'admin-style' )
		);
	} else {
		wp_enqueue_style(
			'zweb_frontend',
			style_url( 'style', 'frontend' ),
			[],
			style_file_modified_time( 'style' )
		);
	}

}

/**
 * Enqueue styles for admin.
 *
 * @return void
 */
function admin_styles() {

	wp_enqueue_style(
		'zweb_shared',
		style_url( 'shared-style', 'shared' ),
		[],
		style_file_modified_time( 'shared-style' )
	);

	wp_enqueue_style(
		'zweb_admin',
		style_url( 'admin-style', 'admin' ),
		[],
		style_file_modified_time( 'admin-style' )
	);

}

/**
 * Enqueue editor styles. Filters the comma-delimited list of stylesheets to load in TinyMCE.
 *
 * @param string $stylesheets Comma-delimited list of stylesheets.
 *
 * @return string
 */
function mce_css( $stylesheets ) {
	if ( ! empty( $stylesheets ) ) {
		$stylesheets .= ',';
	}

	return $stylesheets . ZWEB_URL . ( ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ?
			'assets/css/frontend/editor-style.css' :
			'dist/css/editor-style.min.css' );
}

/**
 * Add async/defer attributes to enqueued scripts that have the specified script_execution flag.
 *
 * @link https://core.trac.wordpress.org/ticket/12009
 *
 * @param string $tag The script tag.
 * @param string $handle The script handle.
 *
 * @return string
 */
function script_loader_tag( $tag, $handle ) {
	$script_execution = wp_scripts()->get_data( $handle, 'script_execution' );

	if ( ! $script_execution ) {
		return $tag;
	}

	if ( 'async' !== $script_execution && 'defer' !== $script_execution ) {
		return $tag; // _doing_it_wrong()?
	}

	// Abort adding async/defer for scripts that have this script as a dependency. _doing_it_wrong()?
	foreach ( wp_scripts()->registered as $script ) {
		if ( in_array( $handle, $script->deps, true ) ) {
			return $tag;
		}
	}

	// Add the attribute if it hasn't already been added.
	if ( ! preg_match( ":\s$script_execution(=|>|\s):", $tag ) ) {
		$tag = preg_replace( ':(?=></script>):', " $script_execution", $tag, 1 );
	}

	return $tag;
}
