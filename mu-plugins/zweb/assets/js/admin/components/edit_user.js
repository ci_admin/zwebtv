jQuery(function ($) {
	$(document).on('click', '.wp-generate-pw', function () {
		$(this)
			.closest('tr')
			.after(
				'<tr class="user-pass1-wrap">\n' +
					'\t<th><label for="pass1">Update password in Firebase</label></th>\n' +
					'\t<td>\n' +
					'\t\t<input type="checkbox" name="update-firebase" value="1" />\n' +
					'\t</td>\n' +
					'</tr>',
			);
	});
});
