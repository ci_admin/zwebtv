jQuery(function ($) {
	$(document).on('click', '.upload_image_button', function (e) {
		e.preventDefault();
		const $button = $(this);

		const mediaFrame = wp.media({
			title: 'Select or upload image',
			library: {
				type: 'image',
			},
			button: {
				text: 'Select',
			},
			multiple: false,
		});

		mediaFrame.on('select', function () {
			const attachment = mediaFrame.state().get('selection').first().toJSON();

			const $wrapper = $button.closest('.taxonomy-img-field');

			$wrapper.find('.taxonomy-img-wrap').attr('src', attachment.url).css('display', 'block');
			$wrapper.find('#taxonomy_image').val(attachment.id);
		});

		// Finally, open the modal
		mediaFrame.open();
	});

	// Clear image field when adding a new taxonomy term with ajax.
	$(document).ajaxSuccess(function (evt, xhr, opts) {
		if (
			xhr.status >= 200 &&
			xhr.status < 300 &&
			opts.data &&
			/(^|&)action=add-tag($|&)/.test(opts.data) &&
			xhr.responseXML &&
			$('wp_error', xhr.responseXML).length === 0
		) {
			$('.taxonomy-img-wrap').removeAttr('src').css('display', 'none');
			$('#taxonomy_image').val('');
		}
	});
});
