<?php
/**
 * Plugin Name: Zweb
 * Plugin URI:
 * Description: Custom functionality for the ZWEB plugin.
 * Version:     0.1.0
 * Author:      10up
 * Author URI:  https://10up.com
 * Text Domain: zweb
 * Domain Path: /languages
 *
 * @package Zweb
 */

// Useful global constants.
define( 'ZWEB_VERSION', '1.7.5' );
define( 'ZWEB_URL', plugin_dir_url( __FILE__ ) );
define( 'ZWEB_PATH', plugin_dir_path( __FILE__ ) );
define( 'ZWEB_INC', ZWEB_PATH . 'includes/' );

// Include files.
require_once ZWEB_INC . 'functions/core.php';

// Activation/Deactivation.
register_activation_hook( __FILE__, '\Zweb\Core\activate' );
register_deactivation_hook( __FILE__, '\Zweb\Core\deactivate' );

// Bootstrap.
Zweb\Core\setup();

// Require Composer autoloader if it exists.
if ( file_exists( ZWEB_PATH . '/vendor/autoload.php' ) ) {
	require_once ZWEB_PATH . 'vendor/autoload.php';
}
